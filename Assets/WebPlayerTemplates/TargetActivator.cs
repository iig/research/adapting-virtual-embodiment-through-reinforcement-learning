﻿using UnityEngine;
using  IIG.Experiment_Generic;

public class TargetActivator : MonoBehaviour {

    public TargetController targetController;

    [Range(0f,0.5f)]
    public float timeInsideMin;
    [Range(0f, 0.5f)]
    public float timeInsideMax;
    public bool ignoreTrackedBody = false;

    private float mTimer = 0f;
    private float mDelay = 0f;
    private bool isHandInside = false;

    private readonly string rightColliderName = "tennis_ball_right";
    private readonly string leftColliderName = "tennis_ball_left";

    void OnEnable()
    {
        mDelay = Random.Range(timeInsideMin, timeInsideMax);
    }

    void Update()
    {
        if (isHandInside)
            mTimer += Time.deltaTime;

        if (mTimer > mDelay || ExperimentEvent_Generic.GetEventDown(ExperimentEvent_Generic.ExperimentEvent.ActivateTarget))
        {
            targetController.TargetHit(gameObject);
            ResetTarget();
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (!enabled) // disabled behavors are still triggered.
            return;

        if (ignoreTrackedBody)
        {
            var parents = other.GetComponentsInParent<Transform>();
            foreach (Transform p in parents)
                if (p.name.Equals("TrackedBody"))
                    return;
        }

        if ((targetController.isRightHanded && other.name.Equals(rightColliderName))
            || (!targetController.isRightHanded && other.name.Equals(leftColliderName)))
            isHandInside = true;
    }

    void OnTriggerExit(Collider other)
    {
        if ((targetController.isRightHanded && other.name.Equals(rightColliderName))
            || (!targetController.isRightHanded && other.name.Equals(leftColliderName)))
        {
            ResetTarget();
        }
    }

    void ResetTarget()
    {
        isHandInside = false;
        mTimer = 0f;
    }
}
