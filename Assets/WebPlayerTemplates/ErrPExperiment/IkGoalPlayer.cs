﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace ErrPExperiment
{
    public class IkGoalPlayer : MonoBehaviour
    {

        [Header("Body-related")]
        public Character_Generator_Settings AvatarDistorted;
        public Character_Generator_Settings AvatarOriginal;
        public Transform initialLocation;

        [Header("Distortion")]
        Transform originalIkGoal;
        public GameObject[] reachingTarget;
        public GameObject handBallOriginal;
        public GameObject handBallDistorted;
        public enum BoneType { LeftHand, RightHand, LeftTennisBall, RightTennisBall };
        public BoneType boneType;

        public bool applyDistortion = true;

        public bool isRadiusDependent = true;
        [Range(0, 1)]
        public float ratioOfTheTarget = 1f;

        public bool isRadiusTargetDependent = true;

        [Range(0, 100)]
        public float drange = 0f;
        public float radius = 0.125f;



        private Vector3 myRestPosition;
        private Quaternion myRestRotation;
        private float distance;
        float ratioTargetBall = 0.15f;//0.15f



        void Awake()
        {
            if (initialLocation != null)
            {
                transform.position = initialLocation.position;
                transform.rotation = initialLocation.rotation;

            }

            myRestPosition = transform.position;
            myRestRotation = transform.rotation;


        }




        void Update()
        {


            if (isRadiusTargetDependent)
            {

                for(int i=0; i<reachingTarget.Length;i++)
                {
                    float ratio = reachingTarget[i].transform.lossyScale.x / reachingTarget[i].transform.localScale.x;


                    reachingTarget[i].transform.localScale = ((1 + ratioTargetBall) * handBallDistorted.transform.lossyScale) * (1 / ratio);


                 

                }

                isRadiusTargetDependent = !isRadiusTargetDependent;
            }




            //Target
            if (originalIkGoal == null)
            {
                if (boneType == BoneType.LeftHand && GameObject.Find(AvatarOriginal.settingsReferences.leftHand.name + "Target") != null)
                    originalIkGoal = GameObject.Find(AvatarOriginal.settingsReferences.leftHand.name + "Target").transform;



                if (boneType == BoneType.RightHand && GameObject.Find(AvatarOriginal.settingsReferences.rightHand.name + "Target") != null)
                    originalIkGoal = GameObject.Find(AvatarOriginal.settingsReferences.rightHand.name + "Target").transform;


            }

        }



        void LateUpdate()
        {
            if (originalIkGoal != null)
            {
                if (applyDistortion)
                {
                    RangeDistortion();

                }
                else
                {
                    transform.position = originalIkGoal.position;
                    transform.rotation = originalIkGoal.rotation;

                     //handBallDistorted.transform.position = handBallOriginal.transform.position;
                    // handBallDistorted.transform.rotation = handBallOriginal.transform.rotation;
                }
            }

        }

        void RangeDistortion()
        {
           // distance = Vector3.Distance(originalIkGoal.transform.position, reachingTarget.transform.position);
            // distance = Vector3.Distance(handBallOriginal.transform.position, reachingTarget.transform.position);


            if (distance >= drange)
            {

                //handBallDistorted.transform.position = GetDistortedPosition();
                transform.position = GetDistortedPosition();

            }
            else
            {

                transform.position = originalIkGoal.transform.position;
                //handBallDistorted.transform.position = handBallOriginal.transform.position;



            }

            transform.rotation = originalIkGoal.rotation;
            // handBallDistorted.transform.rotation = handBallOriginal.transform.rotation;


        }

        Vector3 GetDistortedPosition()
        {
            if (isRadiusDependent)
            {
               // radius = reachingTarget.transform.lossyScale.x * ratioOfTheTarget;

                drange = radius / 0.25f;



                //isRadiusDependent = !isRadiusDependent;
            }

            // normalisation du radius pour comparer a X
            //radius = (reachingTarget.transform.lossyScale.x * ratioOfTheTarget) / drange;

            //REPLAY THE MOVEMENT ( READ THE LIST OF POSITION PREVIOUSLY  LOADED)
            return originalIkGoal.transform.position;
        }


    }
}


