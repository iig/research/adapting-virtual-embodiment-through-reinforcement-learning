﻿using System.Collections;
using System.IO;
using System;
using System.Collections.Generic;
using UnityEngine;
using ReadWriteCsv;
using IIG;



public class ExperimentController_Extension_Old : MonoBehaviour {

	[Header( "Experiment Components" )]

	public ExperimentSetting ExperimentSetting;
	public Calibration_Manager calibrationManager;
    public IIG.MocapInputController mocapInputController;
	public GameObject ScriptManager;
	public GameObject[] trackingManagers;

	public GameObject redCrossHair;
	public GameObject arrowDirection;

    public GameObject rightDistortedTarget;
    public GameObject leftDistortedTarget;

    public GameObject cylinderFakeHand;
    public GameObject cylinderRealHand;

    public GameObject questionsManagerSeveral;
	public GameObject questionsManagerOne;
	public GameObject questionStart;
    public GameObject messageBreak;

    public GameObject preTaskInstruction;
    public GameObject taskInstruction;
    public GameObject taskInstruction1;
	public GameObject taskInstruction2;
   

    public GameObject leftObjectHeld;
    public GameObject rightObjectHeld;

    public GameObject carpet;
    public GameObject chair;
    public GameObject table;

    public GameObject targets;


  

	

    int f = 0;
    int t = 0;
	int r = 0;

	bool flag7;
	bool flag8;
    bool isBreak2;
    bool experimentStart;
    bool trainingStart = true;
    bool isFinished;
    bool isRecordingPos;
    bool isStartedQuestion;
    bool playMovement;
    bool recordMovement;
    bool block1;
    bool block2;
    bool isSet;
    bool isAnsweredBreak;
    bool isQuestion;
    bool isPreTaskDone = false;
    bool isTaskDone;
    bool isStopRecorded;
    bool isQuestionAnswered;
    bool isPreTaskTrigger = false;
    bool isAnimationStart;
    bool isR;
	bool isTimeDone;
	bool isPreTaskDoneReturn;
	bool isInit;
	bool isSetParameter;
	bool isOnlineInProgress;

    float timeStarted = 0;
    float time = 0;
	float timeAfterEvent = 1.0f;//2.5f;
	float timeCrossValidated = 1.5f;
	float timeArrow = 2.0f;
	float[] timeBasket = new float[] { 0f, 1f, 2f, 3f};//{ 0.5f, 0.8f, 1f, 1.3f, 1.5f };

	Transform formerParentDistortedTarget;
    Vector3 initialFakeCylinderPosition;

	float randomClassifierScoreValue = 0;

	//Parameter for loading
	public List<List<float>> conditionsList = new List<List<float>>();
	public List<List<float>> questionsList = new List<List<float>>();
	public List<List<float>> timeList = new List<List<float>>();
	List<List<float>> conditionsList1 = new List<List<float>>();
	public List<List<float>> questionsList1 = new List<List<float>>();
	public List<List<float>> timeList1 = new List<List<float>>();
	public List<List<float>> randomClassifier = new List<List<float>>();
	int numberOfTrialsPerCondition =0;
    int numberOfConditionBlock1 = 0;
	int numberOfConditionBlock2 = 0;
	int nberBlock1 = 0;
	int nberBlock2 = 0;
	int randomClassifierLength = 0;
	int h = 0;
    int nbOfTrainingLoop;
    float[] colorCylinder = new float[] { 1f, 1f, 1f, 1f, 1f,0f, 0f, 0f, 0f, 0f};
    float[] numberCylinder = new float[] { 1, 2, 3 };

    //State
    int trigger1=0;


    Vector3 initialPositionVirtualHand;
    Vector3 initialPositionOriginalHand;
    Vector3 initialCylindrePosition;
    Vector3 initialCylindreRotation;
	Vector3 initialHandRotation;



    Target_Calibration targetCalibration;
    RHIExperiment.IkGoalStaticDistorter distortedTarget;
    ErrPExperiment.SliderSingleQuestionManager questionManagerComponentSeveral;
	ErrPExperiment.SliderSingleQuestionManager questionManagerComponentOne;
	Modularity.PlayerLog_Tracker playerLogTracker;//= new Modularity.PlayerLog_Tracker();
    List<Vector3> worldPositions = new List<Vector3>();
    GameObject trackerOriginalHand;
    GameObject trackerReplayedHand;
    List<GameObject> cuboidAvatar= new List<GameObject>();
	SeatCalibration seatControl;




    // Use this for initialization
    void Awake() {

		playerLogTracker = ScriptManager.GetComponent<Modularity.PlayerLog_Tracker>();

		if ( ExperimentInterface.Instance.isRightHanded )
        {
            trackerOriginalHand = ExperimentSetting.avatarOriginalComponent.settingsReferences.rightHand.gameObject;
            trackerReplayedHand = ExperimentSetting.avatarDistortedComponent.settingsReferences.rightHand.gameObject;
            distortedTarget = rightDistortedTarget.GetComponent<RHIExperiment.IkGoalStaticDistorter>();

        }
        else
        {
            trackerOriginalHand = ExperimentSetting.avatarOriginalComponent.settingsReferences.leftHand.gameObject;
            trackerReplayedHand = ExperimentSetting.avatarDistortedComponent.settingsReferences.leftHand.gameObject;
            distortedTarget = leftDistortedTarget.GetComponent<RHIExperiment.IkGoalStaticDistorter>();

        }


		questionManagerComponentSeveral = questionsManagerSeveral.GetComponent<ErrPExperiment.SliderSingleQuestionManager>();
		questionManagerComponentOne = questionsManagerOne.GetComponent<ErrPExperiment.SliderSingleQuestionManager>();



		//load file config
		if ( ExperimentInterface.Instance.protocole == ExperimentInterface.Protocoles.Offline) 
		{
			 numberOfTrialsPerCondition = 1;//300;
			 numberOfConditionBlock1 = 8;//9;//3;//9
			 numberOfConditionBlock2 = 72;//100;//3;//2;//9
			 nberBlock1 = 1;
			 nberBlock2 = 6;
			LoadConfig( ExperimentInterface.Instance.configPath + ExperimentInterface.Instance.configConditionFileName, ref conditionsList, ref conditionsList1,
				numberOfTrialsPerCondition, numberOfConditionBlock1, nberBlock1, numberOfConditionBlock2, nberBlock2);
			
			//numberOfConditionBlock1 = 8;
			LoadConfig( ExperimentInterface.Instance.configPath + ExperimentInterface.Instance.configTimeFileName, ref timeList, ref timeList1,
			numberOfTrialsPerCondition, numberOfConditionBlock1, nberBlock1, numberOfConditionBlock2, nberBlock2);
		}
		else 
		{
			numberOfTrialsPerCondition = 1;
			numberOfConditionBlock1 = 3;
			numberOfConditionBlock2 = 100;
			nberBlock1 = 1;
			nberBlock2 = 4;
			LoadConfig( ExperimentInterface.Instance.configPath + ExperimentInterface.Instance.configConditionOnlineFileName, ref conditionsList, ref conditionsList1,
				numberOfTrialsPerCondition, numberOfConditionBlock1, nberBlock1, numberOfConditionBlock2, nberBlock2);
			
			
			numberOfConditionBlock1 = 6;
			LoadConfig( ExperimentInterface.Instance.configPath + ExperimentInterface.Instance.configTimeOnlineFileName, ref timeList, ref timeList1,
			numberOfTrialsPerCondition, numberOfConditionBlock1, nberBlock1, numberOfConditionBlock2, nberBlock2);

			if ( ExperimentInterface.Instance.protocole == ExperimentInterface.Protocoles.RandomClassifier)
			{
				randomClassifierLength = 30;
				LoadListFromCSV( ExperimentInterface.Instance.configPath + ExperimentInterface.Instance.randomClassifierFileName, ref randomClassifier, randomClassifierLength);
			}



		}
	


        Time.timeScale = 1;


        nbOfTrainingLoop = ExperimentInterface.Instance.nbOfFourTrainingLoops * 4;
      


    }


    // Update is called once per frame
    void Update()
    {
		////////////////////////////////////////////////////////////////////////
		//////////////////Interface Set//////////////////////////////////////////
		/////////////////////////////////////////////////////////////////////////
		distortedTarget.isAnimation = ExperimentInterface.Instance.isAnimation;
		
		///////////////////////////////////////////////////////////////////////////////////////////
		/////////////////////////////////////////////////////////////////////////////////////////////



		if ( ExperimentInterface.Instance.noTraining )
        {
            redCrossHair.SetActive(false);
            table.SetActive(false);
            nbOfTrainingLoop = 1;
 
        }


		if (!ExperimentSetting.experimentStop && ExperimentSetting.start)
        {

            if (trainingStart)
            {

				if (Input.GetKeyDown( ExperimentInterface.Instance.validateBlock )) {
					t = nbOfTrainingLoop;
					isFinished = true;
				//	isPreTaskDone = true;
					//targets.transform.GetChild(0).GetComponent<TargetReaching>().flagEnter = true;
				//	targets.transform.GetChild(1).GetComponent<TargetReaching>().flagEnter = true;
				//	targets.transform.GetChild(2).GetComponent<TargetReaching>().flagEnter = true;
				//	targets.transform.GetChild(3).GetComponent<TargetReaching>().flagEnter = true;
					//Debug.Log("training");
				}


					
				if(CalibrationTable.isLoad && ExperimentInterface.Instance.isPretask && !flag8)
				{
					TaskEmbodiment(1, true);

					if (isPreTaskDone)
					{
				

						isPreTaskDoneReturn = true;
						isPreTaskTrigger = false;
						isPreTaskDone = false;
						flag8 = true;

					}


				}
				else if(CalibrationTable.isLoad && !ExperimentInterface.Instance.isPretask )
				{
					isPreTaskDoneReturn = true;

					flag8 = true;
			
				}

				//Explanation block 
				if (t < nbOfTrainingLoop && isPreTaskDoneReturn) //&& isPreTaskDone)
                {
					// Different Conditions ( Experiment 1=Embodied, 2=Non Embodied 3=Cubuoid, 4= Control , 5=Control non embody,6=Embodied + Correction) 

					if (!isFinished)
                    {

				

						if ( ExperimentInterface.Instance.protocole != ExperimentInterface.Protocoles.Offline) {
							if (t < (nbOfTrainingLoop / 4))
								Loop(ref isFinished, 4, 1, 1, 1000 + t, 0, timeBasket[2], true);
							else if (((nbOfTrainingLoop / 4) <= t) && (t < (nbOfTrainingLoop / 2)))
								Loop(ref isFinished, 1, 1, 1, 1000 + t, 0, timeBasket[0], true);
							else if (((nbOfTrainingLoop / 2) <= t) && (t < (3 * nbOfTrainingLoop / 4)))
								Loop(ref isFinished, 6, 1, 1, 1000 + t, 0, timeBasket[3], true);
							else
							{
								isFinished = true;
							}
								
						} else {
							if (t < (nbOfTrainingLoop / 4))
								Loop(ref isFinished, 4, 1, 1, 1000 + t, 0, timeBasket[0], true);
							else if (((nbOfTrainingLoop / 4) <= t) && (t < (nbOfTrainingLoop / 2)))
								Loop(ref isFinished, 5, 1, 1, 1000 + t, 0, timeBasket[2], true);
							else if (((nbOfTrainingLoop / 2) <= t) && (t < (3 * nbOfTrainingLoop / 4)))
								Loop(ref isFinished, 1, 1, 1, 1000 + t, 0, timeBasket[1], true);
							else
								Loop(ref isFinished, 2, 1, 1, 1000 + t, 0, timeBasket[3], true);
						}
			
                    }


                    if (isFinished)
                    {
                       // Debug.Log("3");
                        t++;

                        if (t < nbOfTrainingLoop)
                            isFinished = false;


                    }


                }
                


            }


            if (isFinished && !isStartedQuestion && !ExperimentInterface.Instance.infinitTraining && t == nbOfTrainingLoop)
            {
               // Debug.Log("4");

                if (trainingStart)
                {

					calibrationManager.showAvatar = false;
                    trainingStart = false;
                   // cursor.SetActive(true);

                    rightObjectHeld.SetActive(false);
                    leftObjectHeld.SetActive(false);
					isPreTaskDoneReturn = false;
					trigger1 = 9;
					playerLogTracker.SetIsSent(true);
					table.SetActive(false);
					carpet.GetComponent<Renderer>().enabled = false;
					foreach (Renderer renderer in chair.GetComponentsInChildren<Renderer>())
						renderer.enabled = false;
					//chair.GetComponent<Renderer>().enabled = false;
					//question "are you ready" -> experimentStar=true 
					questionStart.SetActive(true);
                }


                if (questionStart.GetComponent<SingleQuestionManagerButton>().isAnswered != true)
                {

                    return;
                }
                else
                {

					

					// cursor.SetActive(false);
					isFinished = false;
                    isStartedQuestion = true;
                    experimentStart = true;
					calibrationManager.showAvatar = true;

                    rightObjectHeld.SetActive(true);
                    leftObjectHeld.SetActive(true);



                    carpet.GetComponent<Renderer>().enabled = true;
					foreach (Renderer renderer in chair.GetComponentsInChildren<Renderer>())
						renderer.enabled = true;
					//chair.GetComponent<Renderer>().enabled = true;

					// block2 = true;
					block1 =true;
                   // s = 0;
                    //isPreTaskDone = false;
                    
                }

            }

            if (experimentStart && isStartedQuestion)
            {

                if ( ExperimentInterface.Instance.experimentDesign == ExperimentInterface.ExperimentDesign.Factoriel)
                {

                    if (block1 || block2)
                    {


                        // if ((j==1 && isBreak) || (conditionsList3[subjectID][j] == 1 && ((i== conditionsList[subjectID].Count/ 3 && isBreak2) 
                        //  || (i == 2*(conditionsList[subjectID].Count / 3) && isBreak3))))
                        if (isBreak2)
                        {
                           

                            // if (!flag5)
                            //     {

                          //  cursor.SetActive(true);

                            //question "are you ready" -> experimentStar=true 
                            messageBreak.SetActive(true);

                            ActivateObjects(false, 0);

							/*  showAvatar = false;

                              rightObjectHeld.SetActive(false);
                              leftObjectHeld.SetActive(false);


                              carpet.GetComponent<Renderer>().enabled = false;
                              chair.GetComponent<Renderer>().enabled = false;*/

							//block1 = true;
							//block2 = false;

							// flag5 = true;
							isBreak2 = false;
                            isAnsweredBreak = true;

                            //    }


                            trigger1 = 8;
                            playerLogTracker.SetIsSent(true);



                        }
                        else if (messageBreak.GetComponent<SingleQuestionManagerButton>().isAnswered && isAnsweredBreak)
                        {




                          //  cursor.SetActive(false);

                            //Alaways begin a new block with embodiment
                            ActivateObjects(true, 1);

                            /* rightObjectHeld.SetActive(true);
                             leftObjectHeld.SetActive(true);


                             showAvatar = true;

                             carpet.GetComponent<Renderer>().enabled = true;
                             chair.GetComponent<Renderer>().enabled = true;*/

                            isAnsweredBreak = false;



                        }
                        else if (block1 && !isAnsweredBreak) //Training Block
                        {



							if (Input.GetKeyDown( ExperimentInterface.Instance.validateBlock )) {
								f = conditionsList[ExperimentInterface.Instance.subjectID].Count / nberBlock1;
								//isPreTaskDone = true;
							//	targets.transform.GetChild(0).GetComponent<TargetReaching>().flagEnter = true;
								//targets.transform.GetChild(1).GetComponent<TargetReaching>().flagEnter = true;
								//targets.transform.GetChild(2).GetComponent<TargetReaching>().flagEnter = true;
							//	targets.transform.GetChild(3).GetComponent<TargetReaching>().flagEnter = true;
							//	Debug.Log("block1");
							}


							if ( ExperimentInterface.Instance.isPretask ) {

								TaskEmbodiment(1, true);

								if (isPreTaskDone) {
									isPreTaskTrigger = false;
									isPreTaskDone = false;
									ExperimentInterface.Instance.isPretask = false;
								}
								

							}
							else {



								if (h < nberBlock1) {
									if (f < conditionsList[ExperimentInterface.Instance.subjectID].Count / nberBlock1) {


										if (!isFinished) {

											if (f == 0) {
												//REMPLACER 3  PAR LE TABLEAU
												Loop(ref isFinished, conditionsList[ExperimentInterface.Instance.subjectID][f + h * (conditionsList[ExperimentInterface.Instance.subjectID].Count / nberBlock1)], /*questionsList1[subjectID][f + h * (questionsList1[subjectID].Count / nberBlock1)]*/1,
											1, f + h * (conditionsList[ExperimentInterface.Instance.subjectID].Count / nberBlock1), 1, timeList[ExperimentInterface.Instance.subjectID][f + h * (conditionsList[ExperimentInterface.Instance.subjectID].Count / nberBlock1)]/*timeBasket[UnityEngine.Random.Range(0, timeBasket.Length)]*/, true);
											}
											else {

												//REMPLACER 3 dans speedbasket PAR LE TABLEAU
												Loop(ref isFinished, conditionsList[ExperimentInterface.Instance.subjectID][f + h * (conditionsList[ExperimentInterface.Instance.subjectID].Count / nberBlock1)],/* questionsList1[subjectID][f + h * (questionsList1[subjectID].Count / nberBlock1)]*/1,
																						/*questionsList1[subjectID][(f - 1) + h * (questionsList1[subjectID].Count / nberBlock1)]*/1, f + h * (conditionsList[ExperimentInterface.Instance.subjectID].Count / nberBlock1), 1
																						, timeList[ExperimentInterface.Instance.subjectID][f + h * (conditionsList[ExperimentInterface.Instance.subjectID].Count / nberBlock1)] /*timeBasket[UnityEngine.Random.Range(0, timeBasket.Length)]*/, true);
											}


										}


										if (isFinished) {


											f++;
											isFinished = false;

											// if (f == (conditionsList[subjectID].Count / 2))
											//   isBreak2 = true;

										}

									}
									else {
										f = 0;
										h++;

										if (h < nberBlock1) {
											isBreak2 = true;
											ExperimentInterface.Instance.isPretask = true;
											//isPreTaskDone = false;
											// block1 = true;
											// block2 = false;
										}

										// j++;

									}
								}
								else {
									block1 = false;
									block2 = true;
									ExperimentInterface.Instance.isPretask = true;
									isBreak2 = true;
									h = 0;
									f = 0;
								}

							}



						}
                        else if (block2 && !isAnsweredBreak) //&& !isAnsweredBreak) // Experience Block
                        {
                            // Load the file for the movement
                            // if(f==0)
                            // {
                            // movementLogTracker.fileOptions.fileToLoad = "subjectID_" + subjectID + "_" + "Movement0" + ".tkr.csv";
                            //movementLogTracker.m_mocapSource = Modularity.PlayerLog_Tracker.MocapSource.FILE;
                            // }

                            if (Input.GetKeyDown( ExperimentInterface.Instance.validateBlock ))
                            {
                                f = conditionsList1[ExperimentInterface.Instance.subjectID].Count / nberBlock2;
								//isPretask = true;
								//isPreTaskDone = true;
								//targets.transform.GetChild(0).GetComponent<TargetReaching>().flagEnter = true;
								//targets.transform.GetChild(1).GetComponent<TargetReaching>().flagEnter = true;
								//targets.transform.GetChild(2).GetComponent<TargetReaching>().flagEnter = true;
								//targets.transform.GetChild(3).GetComponent<TargetReaching>().flagEnter = true;
							//	Debug.Log("block2");
                            }


							if ( ExperimentInterface.Instance.isPretask ) {
								TaskEmbodiment(1, true);

								if (isPreTaskDone) 
								{
									isPreTaskTrigger = false;
									isPreTaskDone = false;
									ExperimentInterface.Instance.isPretask = false;
								}

							}
							else {



								if (h < nberBlock2) {
									if (f < conditionsList1[ExperimentInterface.Instance.subjectID].Count / nberBlock2) {
										

										if (!isFinished) {

											if (f == 0) {
												
												//REMPLACER 3  PAR LE TABLEAU
												Loop(ref isFinished, conditionsList1[ExperimentInterface.Instance.subjectID][f + h * (conditionsList1[ExperimentInterface.Instance.subjectID].Count / nberBlock2)], 1/*questionsList1[subjectID][f + h * (questionsList1[subjectID].Count / nberBlock2)]*/,
											1, f + h * (conditionsList1[ExperimentInterface.Instance.subjectID].Count / nberBlock2), 2, timeList1[0][f + h * (conditionsList1[ExperimentInterface.Instance.subjectID].Count / nberBlock2)]/*timeBasket[UnityEngine.Random.Range(0, timeBasket.Length) ]*/, false);
											}
											else {

												//REMPLACER 3 dans speedbasket PAR LE TABLEAU
												Loop(ref isFinished, conditionsList1[ExperimentInterface.Instance.subjectID][f + h * (conditionsList1[ExperimentInterface.Instance.subjectID].Count / nberBlock2)], 1/*questionsList1[subjectID][f + h * (questionsList1[subjectID].Count / nberBlock2)]*/,
																						/*questionsList1[subjectID][(f - 1) + h * (questionsList1[subjectID].Count / nberBlock2)]*/1, f + h * (conditionsList1[ExperimentInterface.Instance.subjectID].Count / nberBlock2), 2
																						, timeList1[0][f + h * (conditionsList1[ExperimentInterface.Instance.subjectID].Count / nberBlock2)]/*timeBasket[UnityEngine.Random.Range(0,timeBasket.Length)]*/, false);
											}


										}


										if (isFinished) {


											f++;
											isFinished = false;

											// if (f == (conditionsList[subjectID].Count / 2))
											//   isBreak2 = true;

										}

									}
									else {
										f = 0;
										h++;

										if (h < nberBlock2) {
											isBreak2 = true;
											ExperimentInterface.Instance.isPretask = true;
										//	isPreTaskDone = false;
											// block1 = true;
											// block2 = false;
										}

										// j++;

									}
								}
								else {
									block2 = false;

								}
							}
                        }



                    }
                    else
                    {
                        experimentStart = false;
                        timeStarted = 0;
                    }





                }
            }

            if (!trainingStart && isStartedQuestion && !experimentStart)
            {

				//activate the Waiting Message
				calibrationManager.waitingMessage.SetActive(true);

                //disable
                 rightObjectHeld.SetActive(false);
                 leftObjectHeld.SetActive(false);
				//cylinderFakeHand.GetComponent<MeshRenderer>().enabled = false;


				calibrationManager.showAvatar = false;

                carpet.GetComponent<Renderer>().enabled = false;
				foreach (Renderer renderer in chair.GetComponentsInChildren<Renderer>())
					renderer.enabled = false;
				//chair.GetComponent<Renderer>().enabled = false;


				if ( timeStarted > 0.5f )
					ExperimentSetting.experimentStop = true;

                timeStarted += Time.deltaTime;
            }

            //Explanation

            //Block 1
            
            //Experiment End

        }

        //Set The triggers in the motion file
        playerLogTracker.SetTriggers(trigger1);//, trigger2, trigger3);
    }   

    void Loop(ref bool isFinished, float condition, int question, int questionBefore, int trial,int blockNumber, float timeVariable, bool isSeveral)
    {
     
        //Begin the PreTask
        PreTask(condition);

        //Record data for Analysis File
        if(isPreTaskDone && !isRecordingPos)
        {
            //Debug.Log("111");
            StartRecording(trial, blockNumber, condition, timeVariable);
		}
        

        //Begin the task 
        if(isRecordingPos && !isTaskDone)
        {
            //Debug.Log("222");
            Tasks(trial, blockNumber, condition, timeVariable);
        }
       

        //Stop Record Data
        if (isTaskDone && !isStopRecorded)
        {
           // Debug.Log("333");
            StopRecording(question);
        }
           

        //Begin Question 
        if(isStopRecorded && !isQuestionAnswered)
        {
           // Debug.Log("444");
            StartCoroutine(Question(question, condition, blockNumber, trial,isSeveral));
        }


        //Reset the loop
        if (isQuestionAnswered|| ExperimentInterface.Instance.noTraining )
        {
           // Debug.Log("555");
            ResetLoop(condition);
        }
        else
        {
           // Debug.Log("666");
            isFinished = false;

        }
          

    }

	// Begin Pretask for embodiment
	void TaskEmbodiment(float condition, bool isPreTaskEmbodiment)
    {
        if (!isPreTaskTrigger)
        {
			trigger1 = 0;
			playerLogTracker.SetIsSent(true);

			ActivateObjects(true, condition);

            //Activate Message
            taskInstruction.SetActive(true);

            //Shuffle order
            ShuffleTargets(targets,condition);

            //Activate the cylinder
            ActiveTargets(targets,true);

            // Set embody or non embody task
            SetParametersPreTask(condition, ExperimentInterface.Instance.PretaskMethod );

            isPreTaskTrigger = true;
        }
        else if (targets.transform.GetChild(3).GetComponent<TargetReaching>().flagEnter)
        {

            taskInstruction.SetActive(false);

			if(!isPreTaskEmbodiment)
			{
				//Calibrate red cross
				CalibrateCross(redCrossHair, 0.1095602f,0, cylinderFakeHand.transform.position );
				taskInstruction1.SetActive(true);
				redCrossHair.SetActive(true);
			}
           

            // Fake cylinder appear
            cylinderFakeHand.GetComponent<MeshRenderer>().enabled = true;

            // Real cylinder diseapear
            cylinderRealHand.GetComponent<MeshRenderer>().enabled = false;

            //Stop animation
          //  distortedTarget.animateArm = false;



            //trigger1 = 6;
           // playerLogTracker.SetIsSent(true);

            //Diseable Animate
            distortedTarget.animateArm = false;

            isPreTaskDone = true;
        }
		//else if(!isPretask)
		//	ActivateObjects(true, condition);

		//   else if(isPreTaskTrigger)
		 //  {
		//	   //Animate Hand
		//	   animateHand(condition);
		//	   Debug.Log("conditionTrigger " + condition);
		//   }


	}

	// Begin PretaskSystem shortr than the embodiment task
	void TaskEmbodimentCheck(float condition)
	{
		if (!isPreTaskTrigger)
		{
			

			ActivateObjects(true, condition);

			


			CheckSystemTask();


			isTimeDone = false;

			// Set embody or non embody task
			SetParametersPreTask(condition, ExperimentInterface.Instance.PretaskMethod );
		}
		else if (isTimeDone && !isPreTaskDone)
		{
			StopCoroutine("TimerPreTask");
			//Calibrate Red Cross
			CalibrateCross(redCrossHair, 0.1095602f,0, cylinderFakeHand.transform.position );

			taskInstruction2.SetActive(false);
			taskInstruction1.SetActive(true);
			redCrossHair.SetActive(true);


			//Deactivate the arrow
			arrowDirection.SetActive(false);

			//trigger1 = 6;
			//playerLogTracker.SetIsSent(true);

			//Diseable Animate
			distortedTarget.animateArm = false;

			isTimeDone = false;

			isPreTaskDone = true;

			
		}



	}

	void PreTask(float condition)
	{
		if( ExperimentInterface.Instance.PretaskMethod == ExperimentInterface.PretaskType.Target)
		{
			TaskEmbodiment(condition,false);
		}
		else if( ExperimentInterface.Instance.PretaskMethod == ExperimentInterface.PretaskType.NoTarget)
		{
			TaskEmbodimentCheck(condition);
		}


	}

    //Recording Data
    void StartRecording(int trial, int blockNumber, float condition, float timeVariable)
	{
		if (!ExperimentInterface.Instance.recordDataPositionsSettings.posLog.logIsOpen)
		{
			ExperimentInterface.Instance.recordDataPositionsSettings.fileNameToRecord = "subjectID_" + ExperimentInterface.Instance.subjectID + "_" + "Block" + blockNumber + "_" + "Trial" + trial + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss");
			ExperimentInterface.Instance.recordDataPositionsSettings.StartRecording();
			ExperimentInterface.Instance.trialInfo = "subjectID_" + ExperimentInterface.Instance.subjectID + "_" + "Block1" + "_" + "Trial" + trial;

			//Debug.Log("recordStart");
		}
	

            isRecordingPos = true;

    }

    //Set parameters according the condition
   void SetParameters(float condition, bool reset)
    {
       
        // Different Conditions ( Experiment 1=Embodied, 2=Non Embodied 3=Cubuoid, 4= Control , 5=Control non embody,6=Embodied + Correction) 
        if (condition == 1 || condition==6)
        {
            if (!reset)
            { 

                //case 1 : Cylinder Avatar Hand Embodied
                //RotateCylinder(cylinderFakeHand.gameObject);
				 StartCoroutine(RotateAllTheCylinders(cylinderFakeHand.gameObject,condition));
				if(condition == 1) 
				{
					trigger1 = 2;
					playerLogTracker.SetIsSent(true);
				}
				else if (condition == 6) 
				{
					trigger1 = 6;
					playerLogTracker.SetIsSent(true);

					if ( ExperimentInterface.Instance.protocole == ExperimentInterface.Protocoles.RandomClassifier) {
						randomClassifierScoreValue = randomClassifier[0][r];

						r++;
					}
					
				}
				
			}
            else
            {
                cylinderFakeHand.gameObject.transform.eulerAngles = new Vector3(cylinderFakeHand.gameObject.transform.rotation.eulerAngles.x, cylinderFakeHand.gameObject.transform.rotation.eulerAngles.y, 90);
            }
        }
        else if (condition == 2 )
        {
            if (!reset)
            {
				distortedTarget.isFixedRotation = false;
				//case 2 : Cylinder Avatar Hand Non Embodied
				//RotateCylinder(cylinderFakeHand.gameObject);
				trigger1 =3;
				playerLogTracker.SetIsSent(true);
				StartCoroutine(RotateAllTheCylinders(cylinderFakeHand.gameObject,condition));
            }
            else
            {
                cylinderFakeHand.gameObject.transform.eulerAngles = new Vector3(cylinderFakeHand.gameObject.transform.rotation.eulerAngles.x, cylinderFakeHand.gameObject.transform.rotation.eulerAngles.y,90);
            }

        }
        else if (condition == 3 )
        {
            if (!reset)
            {

                //case 3 : Cylinder AvatarCuboid Hand Non Embodied
                //RotateCylinder(cylinderFakeHand.gameObject);
                 StartCoroutine(RotateAllTheCylinders(cylinderFakeHand.gameObject,condition));

            }
            else
            {
                cylinderFakeHand.gameObject.transform.eulerAngles = new Vector3(cylinderFakeHand.gameObject.transform.rotation.eulerAngles.x, cylinderFakeHand.gameObject.transform.rotation.eulerAngles.y, 90);
            }

        }
        else if (condition==4 || condition==5)
        {

			if (condition==4 &&  !reset)
			{
				trigger1 = 4;
				playerLogTracker.SetIsSent(true);
				//rightDistortedTarget.transform.eulerAngles = new Vector3(cylinderFakeHand.gameObject.transform.rotation.eulerAngles.x, cylinderFakeHand.gameObject.transform.rotation.eulerAngles.y, 90);
				//rightDistortedTarget.transform.position =  GameObject.Find(ExperimentSetting.avatarOriginalComponent.settingsReferences.rightHand.name + "Target").transform.position;

			}
			else if (condition == 5 && !reset)
			{
				trigger1 = 5;
				playerLogTracker.SetIsSent(true);
			}

			//distortedTarget.gameObject.transform.eulerAngles = new Vector3(cylinderFakeHand.gameObject.transform.rotation.eulerAngles.x, cylinderFakeHand.gameObject.transform.rotation.eulerAngles.y, 90);

		}

	}

    //Set Embody Task
    void SetParametersPreTask(float condition, ExperimentInterface.PretaskType pretaskMethod)
    {
      
        //Embody
        if (condition==1|| condition==4 || condition==6)
        {
			if ( ExperimentInterface.Instance.optionMethod == ExperimentInterface.Option.Option2)
			BoundCylinder();
			//Cylinder follows the real cylinder
			distortedTarget.applyDistortion = false;
            distortedTarget.isEmbodied = true;


        }//Non Embody
        else if(condition==2 || condition==5)
        {

			if ( ExperimentInterface.Instance.optionMethod == ExperimentInterface.Option.Option2)
				FreeCylinder();

			if(pretaskMethod== ExperimentInterface.PretaskType.Target)
			{
				// Fake cylinder hidden
				cylinderFakeHand.GetComponent<MeshRenderer>().enabled = false;

				// Real cylinder appear
				cylinderRealHand.GetComponent<MeshRenderer>().enabled = true;
			}
         

            //Cylinder stop following the real cylinder
            distortedTarget.animateArm = true;
            distortedTarget.applyDistortion = true;
            distortedTarget.isEmbodied = false;
			distortedTarget.isFixedRotation = true;
		}

    }

  
    //Begin the task
   void Tasks(int trial,int blockNumber, float condition, float timeVariable)
    {

        if ( redCrossHair.activeSelf /*redCrossHair.GetComponent<CrossSelected>().isAnswered*/ || isOnlineInProgress)//|| questionBefore == 0))
		{
		
			//Debug.Log("timer1 "+time);
			if (time == 0f) {
			
				isTimeDone = false;

				StartCoroutine(Timer(timeVariable));

                //Trigger the Beginning of the Event
                trigger1 = 1;
                playerLogTracker.SetIsSent(true);
				ExperimentSetting.readerSocket.clientMessage = "b";
			}
            else if (time>= ( timeVariable + timeCrossValidated) && time <= (timeAfterEvent + timeVariable + timeCrossValidated) )
            {//Before Event
				



				// Set the proper Event
				if (!isSetParameter)
				{
					//Apply Distortion
					if( ExperimentInterface.Instance.protocole == ExperimentInterface.Protocoles.Offline)
					distortedTarget.applyDistortion = true;

					//Apply Distortion
					if (condition != 4 && ExperimentInterface.Instance.protocole != ExperimentInterface.Protocoles.Offline)
						distortedTarget.applyDistortion = true;

					SetParameters(condition, false);
					isSetParameter = true;

				}

				if ((ExperimentSetting.readerSocket.clientMessage == "a" || randomClassifierScoreValue>0.5f )&& condition==6)
				{
					StopCoroutine(RotateAllTheCylinders(cylinderFakeHand.gameObject,condition));

					//Apply Distortion
					distortedTarget.applyDistortion = false;
					//readerSocket.clientMessage = "b";
					//Debug.Log("ERRP!");

				}

				//Trigger the Beginning of the Event
				//trigger1 = 3;
				// playerLogTracker.SetIsSent(true);

				//record positions
				//Debug.Log("Record");
				worldPositions.Clear();
				worldPositions.Add(trackerReplayedHand.transform.position);
				worldPositions.Add(trackerOriginalHand.transform.position);
				worldPositions.Add(targets.transform.position);
				worldPositions.Add(cylinderRealHand.transform.position);
				worldPositions.Add(cylinderFakeHand.transform.position);
				//worldPositions.Add(distortedTarget.transform.position);
				worldPositions.Add(ExperimentSetting.avatarDistortedComponent.settingsReferences.pelvis.position);


				if ( ExperimentInterface.Instance.recordDataPositionsSettings.posLog.logIsOpen)
					ExperimentInterface.Instance.recordDataPositionsSettings.posLog.Logging(worldPositions, condition, trigger1);//,trigger2,trigger3);


			}
            else if (time > (timeAfterEvent + timeVariable + timeCrossValidated) )
            {//After Event
             // Start the fade
             //StartCoroutine(Fade());
             // Debug.Log("timer2 "+time);
               // SetParameters(condition, false);
               // Debug.Log("task3");
                // oculusCamera.GetComponent<OVRScreenFade>().FadeIn();
             
				//ResetAnimation(cylinderFakeHand);

              

                targets.transform.GetChild(3).GetComponent<TargetReaching>().flagEnter = false;
                redCrossHair.SetActive(false);
                taskInstruction1.SetActive(false);
				
				if( ExperimentInterface.Instance.protocole != ExperimentInterface.Protocoles.Offline) 
				{
					isOnlineInProgress = true;
				
					CheckSystemTask();
					//isPreTaskTrigger = false;
					
					if(isTimeDone) {
					

						StopCoroutine("Timer");
						StopCoroutine("TimerPreTask");
						
						//Deactivate the arrow
						arrowDirection.SetActive(false);
						taskInstruction2.SetActive(false);
						isOnlineInProgress = false;
						isSetParameter = false;
						isTaskDone = true;
						isTimeDone = false;

						// Trigger the end of the event and start of the fade 
						trigger1 = 0;
						playerLogTracker.SetIsSent(true);
					}

				} else 
				{
					StopCoroutine("Timer");
					isSetParameter = false;
					isTaskDone = true;
				}
				
               
				
				
				//blackCrossHair.GetComponent<CrossSelected>().isAnswered = false;
                //redCrossHair.GetComponent<CrossSelected>().isAnswered = false;
               // Debug.Log("timer3 " + time);

            }
        }
        
        
 
     
    }

    void StopRecording(float question)
    {
        if (question == 1)
        {
            redCrossHair.SetActive(false);
        }

		ExperimentInterface.Instance.recordDataPositionsSettings.StopRecording();
        isStopRecorded = true;
    }

    void ResetLoop(float condition)
    {
        //trigger1
        if (!isQuestion)
        {
            //trigger1 = 1;
           // playerLogTracker.SetIsSent(true);
            isQuestion = true;
        }

        if ( ExperimentInterface.Instance.noTraining )
        {
            redCrossHair.SetActive(false);
			ExperimentInterface.Instance.noTraining = false;
        }

        StopCoroutine("RotateAllTheCylinders");

		// cursor.SetActive(false);
		ExperimentInterface.Instance.timerDuration = 3;
        isR = false;
		time = 0;
        //isRecordingData = false;
       // loopDone = false;
        isRecordingPos = false;
      //  timerFinished = false;
        isQuestion = false;
     //   flag4 = false;
        //isCalibrated = false;
      //  isComeBack = false;
      //  isEnterForwardTarget1 = true;
      //  isEnterForwardTarget2 = true;
        //isEnterForwardTarget3 = true;
        ResetTrigger();
     //   isShuffledColor = false;
        isPreTaskTrigger = false;
     //   isBlackCross = true;
        taskInstruction1.SetActive(false);
		isInit = false;
		
		//cylinderFakeHand.GetComponent<Animator>().enabled=false;
		//Unapply distortion
		// distortedTarget.applyDistortion = false;

		//Reset the cylinder
		SetParameters(condition, true);

        //Stop routine
        StopAllCoroutines();

        //Reset Bool
        isPreTaskDone = false;
        isTaskDone = false;
        isStopRecorded = false;
        isQuestionAnswered = false;

        //Debug.Log("EndLoop");

        isFinished = true;
    }

    void ResetTrigger()
    {
        trigger1 = 0;
        //trigger2 = 0;
       // trigger3 = 0;
    }

    void ActivateObjects(bool active, float condition)
    {
        if(!active)
        {
            if ( calibrationManager.showAvatar )
				calibrationManager.showAvatar =false;



            if (redCrossHair.activeSelf)
                redCrossHair.SetActive(false);

            //if (blackCrossHair.activeSelf)
             //   blackCrossHair.SetActive(false);
        }

        if (active)
        {

            if ( !calibrationManager.showAvatar && condition!=3)
				calibrationManager.showAvatar = true;

            /*if (!blackCrossHair.activeSelf)
                blackCrossHair.SetActive(true);

            if (!redCrossHair.activeSelf)
                redCrossHair.SetActive(true);*/

        }

        rightObjectHeld.SetActive(active);
        leftObjectHeld.SetActive(active);
        table.SetActive(active);
        carpet.GetComponent<Renderer>().enabled = active;
		taskInstruction.SetActive(false);
		taskInstruction1.SetActive(false);
		taskInstruction2.SetActive(false);

		foreach( Renderer renderer in chair.GetComponentsInChildren<Renderer>())
	   renderer.enabled = active;
  
    }

    void ShuffleList(float[] alpha)
    {
        for (int i = 0; i < alpha.Length; i++)
        {
            float temp = alpha[i];
            int randomIndex = UnityEngine.Random.Range(i, alpha.Length);
            alpha[i] = alpha[randomIndex];
            alpha[randomIndex] = temp;
        }
    }

    void ShuffleTargets(GameObject targets , float condition )
    {
        int i = 0;

        ShuffleList(numberCylinder);

        foreach(Transform child in targets.transform)
        {
           
 
            if (child.GetChild(0).GetComponent<TextMesh>().text != "4")
            {
                child.GetChild(0).GetComponent<TextMesh>().text = numberCylinder[i].ToString();

            }

            if( ExperimentInterface.Instance.isRightHanded )
            {
                if(condition==1 || condition==4 || condition==6)
                child.GetComponent<TargetReaching>().objectHeld = cylinderFakeHand;
                else if (condition==2|| condition==5)
                    child.GetComponent<TargetReaching>().objectHeld = cylinderRealHand;
            }
            else
            {
                child.GetComponent<TargetReaching>().objectHeld = leftObjectHeld;

            }

            i++;
        }


        foreach (Transform child in targets.transform)
        {

            foreach (Transform subchild in targets.transform)
            {
                if(child.GetChild(0).GetComponent<TextMesh>().text == "1")
                {
                        child.GetComponent<TargetReaching>().previousTarget = null;

                }
                else if (child.GetChild(0).GetComponent<TextMesh>().text == "2")
                {
                    if (subchild.GetChild(0).GetComponent<TextMesh>().text == "1")
                    {
                        child.GetComponent<TargetReaching>().previousTarget = subchild.gameObject;

                    }
                }
                else if (child.GetChild(0).GetComponent<TextMesh>().text == "3")
                {
                    if (subchild.GetChild(0).GetComponent<TextMesh>().text == "2")
                    {
                        child.GetComponent<TargetReaching>().previousTarget = subchild.gameObject;

                    }
                }
                else if (child.GetChild(0).GetComponent<TextMesh>().text == "4")
                {
                    if (subchild.GetChild(0).GetComponent<TextMesh>().text == "3")
                    {
                        child.GetComponent<TargetReaching>().previousTarget = subchild.gameObject;

                    }
                }

            }
        }

      

    }

    void ActiveTargets(GameObject targets , bool active)
    {
        foreach (Transform child in targets.transform)
        {
            child.gameObject.SetActive(active);
        }
    }

    void RotateCylinder(GameObject cylinder, float condition)
    {
		//Animator anim = cylinder.GetComponent<Animator>();

		//1
		//anim.enabled = !anim.isActiveAndEnabled;
		//anim.SetTrigger("start");

		// Rotate our transform a step closer to the target's.
		//cylinder.transform.rotation = Quaternion.RotateTowards(cylinder.transform.rotation, targetCylinder.transform.rotation, -speed * Time.deltaTime);

		if ( ExperimentSetting.readerSocket.clientMessage == "a" && condition == 6) 
		{
		} 
		else 
		{
			if (Mathf.Abs(Mathf.DeltaAngle(cylinder.transform.eulerAngles.z, 90)) >= 15f && !isR) {
				// Debug.Log(
				// Debug.Log("test " + Mathf.Abs(Mathf.DeltaAngle(cylinder.transform.eulerAngles.z, 90)));
				initialCylindreRotation = cylinder.transform.eulerAngles;
				//cylinder.transform.Rotate(0, 0, -0.1f*Time.deltaTime, Space.World);
				cylinder.transform.Rotate(0, 0, -ExperimentInterface.Instance.speed * Time.deltaTime, Space.World);
			} else {

				cylinder.transform.eulerAngles = new Vector3(initialCylindreRotation.x, initialCylindreRotation.y, 90);


				isR = true;
			}


		}




	}

	void ResetAnimation(GameObject cylinder) 
	{
		Animator anim = cylinder.GetComponent<Animator>();
		anim.SetTrigger("reset");
	}

	void FreeCylinder()
	{
		cylinderRealHand.GetComponent<ObjectLinked>().objectToLinkWith = GameObject.Find(ExperimentSetting.avatarOriginalComponent.settingsReferences.rightHand.name + "Target");
		cylinderRealHand.GetComponent<ObjectLinked>().resetInitiate = true;
		cylinderRealHand.transform.parent = null;

		cylinderFakeHand.transform.parent = null;

		distortedTarget.test = true;
	}

	void BoundCylinder()
	{
		//cylinderRealHand.GetComponent<ObjectLinked>().objectToLinkWith = GameObject.Find(ExperimentSetting.ExperimentSetting.avatarOriginalComponent.settingsReferences.rightHand.name + "Target");
		//cylinderRealHand.GetComponent<ObjectLinked>().resetInitiate = true;
		cylinderRealHand.transform.parent = ExperimentSetting.avatarOriginalComponent.settingsReferences.rightHand;

		cylinderFakeHand.transform.parent = ExperimentSetting.avatarDistortedComponent.settingsReferences.rightHand;

		distortedTarget.test = false;
	}

	void CalibrateCross( GameObject arrow, float height, float height1 , Vector3 cylinderPosition)
	{
		arrow.transform.position= new Vector3( cylinderPosition.x - height1, cylinderPosition.y+ height, cylinderPosition.z);
	}

	bool DetectGaze()
	{
		RaycastHit hit;
		int layerMask = 1 << 9;

		// Does the ray intersect any objects excluding the player layer
		if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, 100f, layerMask))
		 {
			
			//Debug.Log("Did Hit");
			return true;
		}
		else {
		Debug.DrawLine(Camera.main.transform.position, Camera.main.transform.position + 100f * Camera.main.transform.forward, Color.yellow);

			//Debug.Log("Did not Hit");
			return false;
		}
	
	}

	bool IsHandRotate() 
	{
		if (!isInit) {
		
			initialHandRotation = cylinderRealHand.transform.eulerAngles;
			isInit = true;
			return false;
		}
		else
		{
			float diff = Mathf.Abs(cylinderRealHand.transform.eulerAngles.z- initialHandRotation.z);
	
			if (diff > ExperimentInterface.Instance.rotateThreshold) 
			{
				return true;
			}
			else
				return false;
		}

		


	}

	void CheckSystemTask()
	{
		//Activate Message
		taskInstruction2.SetActive(true);

	//	if (DetectGaze()) {
			if (!arrowDirection.activeSelf) {
				//Calibrate Arrow
				CalibrateCross(arrowDirection, 0.07f, -0.030f, cylinderRealHand.transform.position );
	
				//Activate the arrow
				arrowDirection.SetActive(true);
				isInit = false;
			}


		//}

		if (arrowDirection.activeSelf) {

	

			if (IsHandRotate()) {

		

				//Activate timer
				StartCoroutine(TimerPreTask(timeArrow));

				isPreTaskTrigger = true;
			}
		}
	}

	//////////////////////////////////////////////////////////////////////////////////
	// IENUMERATOR FUNCTION



	IEnumerator RotateAllTheCylinders( GameObject cylinder, float condition )
    {
       
        while(true)
        {
			RotateCylinder(cylinder,condition);

			yield return new WaitForSeconds(0.001f);
        }
        
    }

    IEnumerator Question(float question, float condition , float blockNumber, float trial, bool isSeveral)
    {
  
        if (question == 1)
        {

            if (!isQuestionAnswered)
            {

                //Questions

                //Set trigger for question
             //   trigger1 = 7;
               // playerLogTracker.SetIsSent(true);

				if (isSeveral) {
					questionManagerComponentSeveral.condition = condition;
					questionManagerComponentSeveral.fileNameToRecord = "subjectID_" + ExperimentInterface.Instance.subjectID + "_" + "Block" + blockNumber + "_" + "Trial" + trial + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss");
					questionsManagerSeveral.SetActive(true);
				}
				else 
				{
					questionManagerComponentOne.condition = condition;
					questionManagerComponentOne.fileNameToRecord = "subjectID_" + ExperimentInterface.Instance.subjectID + "_" + "Block" + blockNumber + "_" + "Trial" + trial + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss");
					questionsManagerOne.SetActive(true);
				}
                //cursor.SetActive(true);

                //Hide everything during question
                ActivateObjects(false, condition);

				if (isSeveral) {
					// wait 0.1s apres etre desactive
					yield return new WaitUntil(() => questionManagerComponentSeveral.isAnswered);
				}
				else
				{
					// wait 0.1s apres etre desactive
					yield return new WaitUntil(() => questionManagerComponentOne.isAnswered);
				}
             

                isQuestionAnswered = true;
            }

    


        }
        else if (question == 0)
        {
          //  Debug.Log("440");
            if (!isQuestionAnswered)
            isQuestionAnswered = true;
        }

       
    }


    //https://docs.unity3d.com/Manual/Coroutines.html
    /*  IEnumerator Fade()
      {
          for (float f = 1f; f >= 0; f -= 0.1f)
          {
             // Color c = renderer.material.color;
             // c.a = f;
             // renderer.material.color = c;
              yield return null;
          }
      }*/

    IEnumerator Timer(float timeVariable)
    {
        while(time <=(timeAfterEvent + timeVariable + timeCrossValidated) )
        {
           // Debug.Log("timer" + time);

            yield return new WaitForSeconds(0.01f);

            time = time +0.01f;
        }
  

    }

	IEnumerator TimerPreTask(float timeDuration)
	{
			yield return new WaitForSeconds(timeDuration);
			isTimeDone = true;

	}
	///////////////////////////////////////////////
	// LOAD CONFIG FILE

	// parse CSV file with headder and block
	public bool LoadConfig(string fileName, ref List<List<float>> conditionsList, ref List<List<float>> conditionsList2,
		int numberOfTrialsPerCondition,int  numberOfConditionBlock1, int nberBlock1, int numberOfConditionBlock2, int nberBlock2)
    {

        // open CSV file
        Debug.Log("conditionCSV: opening file " + fileName);

        CsvFileReader reader = new CsvFileReader(fileName);

        string fileContent = reader.ReadToEnd();
        reader.Close();

        float startTime = 0, endTime = 0;

        using (StringReader strReader = new StringReader(fileContent))
        {
            string line;
            int countFrame = 0;

            // skip header
            line = strReader.ReadLine();
            line= strReader.ReadLine();

            int offset = 0;
            while ((line = strReader.ReadLine()) != null)
            {
                // list of positions for this frame
                List<Vector3> mocapLine = new List<Vector3>();

                string[] mocapLineArray = line.Split(new string[] { "," }, StringSplitOptions.None);

                List<float> conditionsLine = new List<float>();
                  List<float> conditionsLine2 = new List<float>();
                // List<float> conditionsLine3 = new List<float>();





                int endBlock1 = numberOfTrialsPerCondition * numberOfConditionBlock1*nberBlock1 ;

                for (int i = 0; i < endBlock1; i++)
                {

                    float distorEntry = float.Parse(mocapLineArray[i + offset]);

                    conditionsLine.Add(distorEntry);



                }

                   int initialBlock2 = endBlock1;

                  int endBlock2 = initialBlock2 + numberOfTrialsPerCondition * numberOfConditionBlock2 * nberBlock2;


				for (int i = initialBlock2; i < endBlock2; i++)
                   {

                       float distorEntry = float.Parse(mocapLineArray[i + offset]);

                       //new coeff entry
                       conditionsLine2.Add(distorEntry);

                   }




                /*      for (int i = endBlock2; i < mocapLineArray.Length; i++)
                     {

                         float distorEntry = float.Parse(mocapLineArray[i + offset]);

                         //new coeff entry
                         conditionsLine3.Add(distorEntry);

                     }*/

                conditionsList.Add(conditionsLine);
                conditionsList2.Add(conditionsLine2);
                //  conditionsList3.Add(conditionsLine3);


                if (countFrame == 0)
                    startTime = float.Parse(mocapLineArray[0]);
                countFrame++;
                endTime = float.Parse(mocapLineArray[0]);

            }
        }



        Debug.Log("conditionCSV: " + fileName + " loaded!");
        return true;
    }

	// parse CSV file with no headder and no block kust a simple list
	public bool LoadListFromCSV(string fileName, ref List<List<float>> conditionsList, int numberOfElements)
    {

        // open CSV file
        Debug.Log("conditionCSV: opening file " + fileName);

        CsvFileReader reader = new CsvFileReader(fileName);

        string fileContent = reader.ReadToEnd();
        reader.Close();

        float startTime = 0, endTime = 0;

        using (StringReader strReader = new StringReader(fileContent))
        {
            string line;
            int countFrame = 0;

            int offset = 0;
            while ((line = strReader.ReadLine()) != null)
            {
                // list of positions for this frame
                List<Vector3> mocapLine = new List<Vector3>();

                string[] mocapLineArray = line.Split(new string[] { "," }, StringSplitOptions.None);

                List<float> conditionsLine = new List<float>();
                

                int endBlock1 = numberOfElements ;

                for (int i = 0; i < endBlock1; i++)
                {

                    float distorEntry = float.Parse(mocapLineArray[i + offset]);

                    conditionsLine.Add(distorEntry);



                }
              
                conditionsList.Add(conditionsLine);


                if (countFrame == 0)
                    startTime = float.Parse(mocapLineArray[0]);
                countFrame++;
                endTime = float.Parse(mocapLineArray[0]);

            }
        }



        Debug.Log("conditionCSV: " + fileName + " loaded!");
        return true;
    }



   


}

