﻿using System.Collections;
using System.Collections.Generic;
using RootMotion.FinalIK;
using UnityEngine;

[ExecuteInEditMode]
public class TwistRelaxer_FinalIK : MonoBehaviour {

   
    public float twistWeight = 1.0f;
    public float twistCrossfade = 0.4f;
    public Character_Generator_Settings Avatar;



    // Use this for initialization
    void Start ()
    {

     }
	
	// Update is called once per frame

	void Update () {
#if UNITY_EDITOR
        if (Avatar.settingsReferences.leftForearm.gameObject.GetComponent<TwistRelaxer>() == null)
           Avatar.settingsReferences.leftForearm.gameObject.AddComponent<TwistRelaxer>();

        if (Avatar.settingsReferences.rightForearm.gameObject.GetComponent<TwistRelaxer>() == null)
           Avatar.settingsReferences.rightForearm.gameObject.AddComponent<TwistRelaxer>();

        if (Avatar.settingsReferences.leftForearm.gameObject.GetComponent<TwistRelaxer>() != null)
        {
           Avatar.settingsReferences.leftForearm.gameObject.GetComponent<TwistRelaxer>().weight = twistWeight;
           Avatar.settingsReferences.leftForearm.gameObject.GetComponent<TwistRelaxer>().parentChildCrossfade = twistCrossfade;

        }


        if (Avatar.settingsReferences.rightForearm.gameObject.GetComponent<TwistRelaxer>() != null)
        {
           Avatar.settingsReferences.rightForearm.gameObject.GetComponent<TwistRelaxer>().weight = twistWeight;
           Avatar.settingsReferences.rightForearm.gameObject.GetComponent<TwistRelaxer>().parentChildCrossfade = twistCrossfade;

        }


    }
#endif
}
