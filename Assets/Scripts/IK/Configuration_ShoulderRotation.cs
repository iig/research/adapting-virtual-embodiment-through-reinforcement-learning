﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RootMotion.FinalIK;

public class Configuration_ShoulderRotation : MonoBehaviour {
    /// <summary>
    /// Shoulder rotator is a workaround for FBBIK not rotating the shoulder bones when pulled by hands.
    /// It get's the job done if you need it, but will take 2 solving iterations.
    /// </summary>
    [Tooltip("Weight of shoulder rotation")]
    public float weight = 1.5f;
    [Tooltip("The greater the offset, the sooner the shoulder will start rotating")]
    public float offset = 0.2f;

    private LimbIK[] iks;//todo take the right limb ik ( left arm and right arm)
    private LimbIK ik;
    //FullBodyBipedIK ik;
    private bool skip;

    public GameObject target;

    Vector3 targetOldPosition;
    Vector3 shoulderInitialPosition;
    Quaternion shoulderInitialRotation;
    Vector3 directionOld=Vector3.zero;
    Vector3 shoulderInitialDirection;
    float oldGap = 0.000f;
    Matrix4x4 test;
    //float scalarProduct=0;
    Vector3 crossProduct;
     Vector3 alignment=Vector3.one;

   // int flag1 = 0;

    void Start()
    {

        ik = GetComponent<LimbIK>();
        shoulderInitialRotation=ik.solver.bone1.transform.parent.rotation;
        shoulderInitialPosition = ik.solver.bone1.transform.position;
        target.transform.position = ik.solver.IKPosition;
        //test = TransformationMatrix(ik.solver.bone1.transform, target.transform);
        shoulderInitialDirection = ik.solver.bone1.transform.position - ik.solver.bone1.transform.parent.position;
        // You can use just LateUpdate, but note that it doesn't work when you have animatePhysics turned on for the character.
        // ik.solver.OnPostUpdate += RotateShoulders;
        // RotateShoulders();
    }

   private void LateUpdate()
    {
        //if (ik.solver.bone3.transform.position == ik.solver.IKPosition)
        {
          //  Debug.Log("equal");
          //IK est declaee mais avec une vrai target on aura pas ce pb
            target.transform.position = ik.solver.IKPosition;
        }
            

        RotateShoulder(weight, offset);

     
       
    }

    /*  private void Update()
      {
          if(flag1<2)
          {

          if (GetComponents<LimbIK>().Length > 0)
              {
                  iks = GetComponents<LimbIK>();

              foreach ( LimbIK ikComponent in iks )
                  {

                  if (ikComponent.solver.goal== AvatarIKGoal.LeftHand || ikComponent.solver.goal == AvatarIKGoal.RightHand)
                      {
                      Debug.Log("here");
                      Debug.Log(ikComponent.solver.goal.ToString());

                          ik = ikComponent;
                          // You can use just LateUpdate, but note that it doesn't work when you have animatePhysics turned on for the character.
                          ikComponent.solver.OnPostUpdate += RotateShoulders;
                      flag1++;
                      }
                  }

              }

          }
      }*/

    private void RotateShoulders()
        {
            if (ik == null) return;
            if (ik.solver.IKPositionWeight <= 0f) return;

        if (ik.solver == null) return;

        // Skipping the second update cycle
        if (skip)
            {
                skip = false;
                return;
            }

            //pour un limbIK
            RotateShoulder( weight, offset); // Rotate the left shoulder

            skip = true;
            ik.solver.Update(); // Update FBBIK again with the rotated shoulders

        }

    // Rotates a shoulder of a FBBIK character
    private void RotateShoulder(float weight, float offset)
    {
        // Get FromToRotation from the current swing direction of the shoulder to the IK target direction
        // Quaternion fromTo = Quaternion.FromToRotation(ik.solver.bone3.transform.position - ik.solver.bone1.transform.parent.position, ik.solver.IKPosition - ik.solver.bone1.transform.parent.position);
        Quaternion fromTo = Quaternion.FromToRotation(ik.solver.bone1.transform.position - ik.solver.bone1.transform.parent.position, ik.solver.IKPosition - ik.solver.bone1.transform.parent.position);
        Quaternion fromTo1 = Quaternion.FromToRotation(ik.solver.IKPosition - ik.solver.bone1.transform.parent.position, ik.solver.bone1.transform.position - ik.solver.bone1.transform.parent.position);

        // Direction to the IK target
        Vector3 toTarget = ik.solver.IKPosition - ik.solver.bone1.transform.position;

        Vector3 toEffector = ik.solver.IKPosition;
        Vector3 difference = toEffector - directionOld;
        Vector3 toLimb3 = ik.solver.IKPosition - ik.solver.bone3.transform.position;



        float scalarProduct = Vector3.Dot(toEffector, difference);

        // Length of the limb
        float limbLength = Vector3.Distance(ik.solver.bone1.transform.position, ik.solver.bone2.transform.position)
                + Vector3.Distance(ik.solver.bone2.transform.position, ik.solver.bone3.transform.position);

        // Divide IK Target direction magnitude by limb length to know how much the limb is being pulled
        float delta = (toTarget.magnitude / limbLength) - 1f; //+ offset;
        float delta1 = Mathf.Clamp(delta * weight, 0f, 1f);

        Vector3 alpha = ik.solver.bone1.transform.position - shoulderInitialPosition;
        float alpha1 = Mathf.Clamp(alpha.magnitude * weight, 0f, 1f);
        //Debug.Log(delta1);
        //float alpha1 = Mathf.Clamp(alpha * weight, 0f, 1f);

        float c = toLimb3.magnitude;
        float a = Vector3.Distance(ik.solver.bone1.transform.parent.position, ik.solver.bone1.transform.position);
        float angle = 2 * Mathf.Asin(0.5f * c / a);
        Quaternion gamma = Quaternion.AngleAxis(angle, Vector3.Cross(ik.solver.bone3.transform.position - ik.solver.bone1.transform.parent.position, ik.solver.IKPosition - ik.solver.bone1.transform.parent.position));

        // Quaternion gamma = Quaternion.Euler(Vector3.one*angle);

        Vector3 toHand = ik.solver.IKPosition - ik.solver.bone3.transform.position;
        Vector3 currentPosition = ik.solver.bone1.transform.position - shoulderInitialPosition;
        Vector3 armDirection = target.transform.position - ik.solver.bone2.transform.position;
        Vector3 targetPosition = ik.solver.IKPosition - shoulderInitialPosition;
        Vector3 handPosition = ik.solver.bone3.transform.position - shoulderInitialPosition;
        Vector3 handPositionV2 = ik.solver.bone3.transform.position - ik.solver.bone1.transform.position;
       // Vector3 targetPositionV2 = ik.solver.IKPosition- ik.solver.bone1.transform.position;
        //float projectionV2 = Vector3.Dot(targetPositionV2, handPositionV2);
        //float deltad = handPositionV2.magnitude - projectionV2;
        // float projection = Vector3.Dot(targetPosition, handPosition);
        float deltad1 = targetPosition.magnitude - limbLength;
        float deltad = handPosition.magnitude - targetPosition.magnitude  ;
       
        // float deltad =Mathf.Sqrt( targetPosition.magnitude* targetPosition.magnitude-projection*projection +(handPosition.magnitude-projection)* (handPosition.magnitude - projection));
        float d = currentPosition.magnitude;

       Vector3 shoulderDirection = ik.solver.bone1.transform.position - ik.solver.bone1.transform.parent.position;

        float shoulderVariationLength = shoulderDirection.magnitude - shoulderInitialDirection.magnitude;//(float) System.Math.Round(shoulderDirection.magnitude - shoulderInitialDirection.magnitude, 3);

      //  Debug.Log(shoulderDirection.magnitude);

      /*  if (flag1 == 1000)
        {
            crossProduct = Vector3.Cross(targetOldPosition, target.transform.position);
            alignment = new Vector3((float)System.Math.Round(crossProduct.x, 2), (float)System.Math.Round(crossProduct.y, 2), (float)System.Math.Round(crossProduct.z, 2));
            flag1 = 0;
        }*/



        // if (delta>=0.01f)
        if (deltad1 > 0)// && handPositionV2.magnitude>=(limbLength-0.001f) )
        {
            if (scalarProduct > 0.0f && alpha.magnitude < 0.08f )
            {
               // AssociateRigidBody(test, ik.solver.bone1.transform, target.transform, true, true);
                Debug.Log("positif");


                // Calculate the rotation offset for the shoulder
                // Quaternion rotationOffset = Quaternion.Lerp(ik.solver.bone1.transform.parent.rotation, ik.solver.bone1.transform.parent.rotationfromTo, 1);

                 Quaternion rotationOffset = Quaternion.Lerp(Quaternion.identity, fromTo, delta1 * ik.solver.IKPositionWeight);

                // Quaternion rotationOffset = Quaternion.Lerp(shoulderInitialRotation, ik.solver.bone1.transform.parent.rotation, delta1 * ik.solver.IKPositionWeight);

                // Rotate the shoulder
                ik.solver.bone1.transform.parent.rotation = rotationOffset * ik.solver.bone1.transform.parent.rotation;
                // Vector3 z = (ik.solver.bone1.transform.position + c * toTarget.normalized) - ik.solver.bone1.transform.parent.position;
                // Quaternion o= Quaternion.FromToRotation(ik.solver.bone1.transform.position - ik.solver.bone1.transform.parent.position, z);
                // ik.solver.bone1.transform.parent.rotation = gamma* ik.solver.bone1.transform.parent.rotation;
           
            }
            if (scalarProduct < 0.0f && (targetPosition.magnitude - handPosition.magnitude) < 0.0)// && handPositionV2.magnitude >= (limbLength - 0.005f))
            {
               // AssociateRigidBody(test, ik.solver.bone1.transform, target.transform, true, true);

                 float currentAngle = ik.solver.bone1.transform.parent.eulerAngles.magnitude - shoulderInitialRotation.eulerAngles.magnitude;

                 float coeff = currentAngle / d;


                 Quaternion newAngle = Quaternion.AngleAxis(deltad*coeff, Vector3.Cross(ik.solver.bone3.transform.position - ik.solver.bone1.transform.parent.position, ik.solver.IKPosition - ik.solver.bone1.transform.parent.position));


                ik.solver.bone1.transform.parent.rotation = newAngle * ik.solver.bone1.transform.parent.rotation;


                Debug.Log("negatif");
                // float alpha = (ik.solver.bone1.transform.parent.position.magnitude / shoulderInitialPosition.magnitude) - 1f;
                // float alpha1 = Mathf.Clamp(alpha * weight, 0f, 1f);
                // Calculate the rotation offset for the shoulder
                // Quaternion rotationOffset = Quaternion.Lerp(ik.solver.bone1.transform.parent.rotation, shoulderInitialRotation,0.1f);//(1-delta1) * ik.solver.IKPositionWeight);


                //Quaternion rotationOffset = Quaternion.Lerp(Quaternion.identity, fromTo1, delta1 * ik.solver.IKPositionWeight);
                // Quaternion rotationOffset = Quaternion.Lerp(ik.solver.bone1.transform.parent.rotation, shoulderInitialRotation, alpha1 * ik.solver.IKPositionWeight);

                // Quaternion rotationOffset = Quaternion.Lerp(Quaternion.identity, fromTo, delta1 * ik.solver.IKPositionWeight);
                // ik.solver.bone1.transform.parent.rotation = rotationOffset; // * ik.solver.bone1.transform.parent.rotation;


                //  ik.solver.bone1.transform.parent.rotation = rotationOffset * ik.solver.bone1.transform.parent.rotation
            }

        

          /*  if(flag1==0)
            {
                targetOldPosition = target.transform.position;
                //flag1 = 0;
            }
            flag1++;*/

            directionOld = toEffector;
            oldGap = toLimb3.magnitude;
        }

    }
    

        // Remove the delegate when destroyed
        void OnDestroy()
        {
            if (ik != null) ik.solver.OnPostUpdate -= RotateShoulders;
        }


        /// <summary>
        /// Gets the current swing direction of the bone in world space.
        /// </summary>
            public Vector3 swingDirectionParent(Transform bone1)
        {
            return -(bone1.parent.position - bone1.position);
        }
}

