﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//TODO: Create an utile clas where i can put all the structure i willl use to be independent from Avatar_Calibration_Generic script.

[System.Serializable]
public class References : System.Object
{
    public Transform root;
    public Transform pelvis;
    public Transform spine;
    public Transform lastSpine;
    public Transform chest; // Optional
    public Transform neck; // Optional
    public Transform head;
    public Transform leftEye;
    public Transform rightEye;
    public Transform headEnd;
    public Transform leftShoulder; // Optional
    public Transform leftUpperArm;
    public Transform leftForearm;
    public Transform leftHand;
    public Transform leftFingerBase;
    public Transform rightShoulder; // Optional
    public Transform rightUpperArm;
    public Transform rightForearm;
    public Transform rightHand;
    public Transform rightFingerBase;
    public Transform leftThigh;
    public Transform leftCalf;
    public Transform leftFoot;
    public Transform leftToes; // Optional
    public Transform rightThigh;
    public Transform rightCalf;
    public Transform rightFoot;
    public Transform rightToes; // Optional
    public List<Transform> trunk;



    /// <summary>
    /// Returns an array of all the Transforms in the definition.
    /// </summary>
    public Transform[] GetTransforms()
    {
        return new Transform[28] {
                    root, pelvis, spine,lastSpine, chest, neck, head,leftEye, rightEye, headEnd, leftShoulder, leftUpperArm, leftForearm, leftHand,leftFingerBase, rightShoulder, rightUpperArm, rightForearm, rightHand,rightFingerBase, leftThigh, leftCalf, leftFoot, leftToes, rightThigh, rightCalf, rightFoot, rightToes
                };
    }
}

[ExecuteInEditMode]
public abstract class Character_Generator_Settings : MonoBehaviour {

    //public Avatar_Calibration_Generic avatarCalibrationScript;

    public References settingsReferences;

    public int nbVertebrae;
    public bool manualScale=false;

   // [HideInInspector]
    public float ratioHeight;

    public float userHeight;
    public float initialAvatarHeight;

    //set the refrence table of the component Avatar_Calibration_Generic
    // public bool setReference=false;


    //limb of the avatar identified
    [HideInInspector]
    public bool limbIdentified = false;

    [HideInInspector]
    public bool referenceSet = false;


    public virtual void Update()
    {
        if (manualScale)
        {
            ratioHeight = userHeight / initialAvatarHeight;
            settingsReferences.root.localScale = Vector3.one * ratioHeight;
        }
       
    }


    /*  public void setReferenceAvatarCalibration () {

          if (setReference)
          {
              if (avatarCalibrationScript)
              {



                  //Root attribution
                   settingsReferences.root = settingsReferences.root;

                  // Pelvis
                   settingsReferences.pelvis = settingsReferences.pelvis;

                  // Spine
                   settingsReferences.spine = settingsReferences.spine;

                  //Last Spine
                   settingsReferences.lastSpine = settingsReferences.lastSpine;

                  //Chest
                   settingsReferences.chest = settingsReferences.chest;

                  //Neck
                   settingsReferences.neck = settingsReferences.neck;

                  //Head
                   settingsReferences.head = settingsReferences.head;

                  //Head End
                   settingsReferences.headEnd = settingsReferences.headEnd;

                  //Left Shoulder
                   settingsReferences.leftShoulder = settingsReferences.leftShoulder;

                  //Left Arm
                   settingsReferences.leftUpperArm = settingsReferences.leftUpperArm;

                  //Left ForeArm
                   settingsReferences.leftForearm = settingsReferences.leftForearm;

                  //Left Hand 
                   settingsReferences.leftHand = settingsReferences.leftHand;

                  //Left Fingers Base
                   settingsReferences.leftFingerBase = settingsReferences.leftFingerBase;

                  //Right Shoulder
                   settingsReferences.rightShoulder = settingsReferences.rightShoulder;

                  //Right Arm
                   settingsReferences.rightUpperArm = settingsReferences.rightUpperArm;

                  //Right ForeArm
                   settingsReferences.rightForearm = settingsReferences.rightForearm;

                  //Right Hand 
                   settingsReferences.rightHand = settingsReferences.rightHand;

                  //Right Fingers Base
                   settingsReferences.rightFingerBase = settingsReferences.rightFingerBase;

                  //Left Thigh
                   settingsReferences.leftThigh = settingsReferences.leftThigh;

                  //Left Calf
                   settingsReferences.leftCalf = settingsReferences.leftCalf;

                  //Left Foot
                   settingsReferences.leftFoot = settingsReferences.leftFoot;

                  //Left Toes
                   settingsReferences.leftToes = settingsReferences.leftToes;

                  //Right Thigh
                   settingsReferences.rightThigh = settingsReferences.rightThigh;

                  //Right Calf
                   settingsReferences.rightCalf = settingsReferences.rightCalf;

                  //Right Foot
                   settingsReferences.rightFoot = settingsReferences.rightFoot;

                  //Right Toes
                   settingsReferences.rightToes = settingsReferences.rightToes;

                  //Trunk
                   settingsReferences.trunk.Clear();
                  if ( settingsReferences.trunk.Count < nbVertebrae)
                  {
                      for (int i = 0; i <= nbVertebrae - 1; i++)
                           settingsReferences.trunk.Add(settingsReferences.trunk[i]);

                  }

                  referenceSet = false;
                  Debug.Log("References Set !");
              }
              else
              Debug.LogError("There is no Avatar_Calibration_Generic set.");
          }

      }*/
}
