﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

//TO DO: _save Option and load Option from file 
 //      _Done during the general loading
 //      -Remove any intervention from other scripts on the child evetring has to go through this manager to have acces

public class IK_Manager : MonoBehaviour
{
	[Header( "Settings" )]
	public IKOption IKOptions;
	public enum IKOption { None, Customized, Full }
	public bool head;
	public bool pelvis;
	public bool chest;
	public bool leftArm;
	public bool rightArm;
	public bool leftLeg;
	public bool rightLeg;
	public bool leftShoulder;
	public bool rightShoulder;
	public enum IKShoulderMethods { Accurate, Realist }
	public IKShoulderMethods shoulderIK;
	public bool isShoulderTracked;
	public List<GameObject> TargetsCustomized;
	public List<Configuration_LimbIK.TargetType> TargetsCustomizedSettings;


	[Header( "Components" )]
	public GameObject AvatarOriginal_IK;
	public GameObject AvatarDistorted_IK;
	public Character_Generator_Settings avatarOriginal;
	public Character_Generator_Settings avatarDistorted;


	//IK Component 
	Configuration_Constraints originalHeadConstraints;
	Configuration_Constraints distortedHeadConstraints;
	Configuration_Constraints originalPelvisConstraints;
	Configuration_Constraints distortedPelvisConstraints;
	Configuration_Constraints originalRightShoulderConstraints;
	Configuration_Constraints distortedRightShoulderConstraints;
	Configuration_Constraints originalLeftShoulderConstraints;
	Configuration_Constraints distortedLeftShoulderConstraints;
	Configuration_LimbIK originalLimbIK;
	Configuration_LimbIK distortedLimbIK;
	Configuration_FABRIK originalFABBRIK;
	Configuration_FABRIK distortedFABBRIK;
	TwistRelaxer_FinalIK originalTwistIK;
	TwistRelaxer_FinalIK distortedTwistIK;
	ShoulderIK originalLeftShoulderIK;
	ShoulderIK distortedLeftShoulderIK;
	ShoulderIK originalRightShoulderIK;
	ShoulderIK distortedRightShoulderIK;



	// Start is called before the first frame update
	void Start ()
    {
		if(IKOptions==IKOption.Full) 
		{
			head = true;
			pelvis = true;
			chest = true;
			leftArm = true;
			rightArm = true;
			leftLeg = true;
			rightLeg = true;

		}
		else if(IKOptions== IKOption.None ) {
			head    =false ;
			pelvis  = false;
			chest   = false;
			leftArm = false;
			rightArm= false;
			leftLeg = false;
			rightLeg= false;
		}

		if (head)
		{
			//Original Avatar
			originalHeadConstraints= AvatarOriginal_IK.AddComponent<Configuration_Constraints>();
			originalHeadConstraints.Avatar = avatarOriginal;
			originalHeadConstraints.boneType = Configuration_Constraints.BoneType.Head;
			originalHeadConstraints.positionWeight = 1;
			originalHeadConstraints.rotationWeight = 1;

			//Distorted Avatar
			distortedHeadConstraints = AvatarDistorted_IK.AddComponent<Configuration_Constraints>();
			distortedHeadConstraints.Avatar = avatarDistorted;
			distortedHeadConstraints.boneType = Configuration_Constraints.BoneType.Head;
			distortedHeadConstraints.positionWeight = 1;
			distortedHeadConstraints.rotationWeight = 1;
		}

		if ( pelvis ) {
			//Original Avatar
			originalPelvisConstraints = AvatarOriginal_IK.AddComponent<Configuration_Constraints>();
			originalPelvisConstraints.Avatar = avatarOriginal;
			originalPelvisConstraints.boneType = Configuration_Constraints.BoneType.Pelvis;
			originalPelvisConstraints.positionWeight = 1;
			originalPelvisConstraints.rotationWeight = 1;

			//Distorted Avatar
			distortedPelvisConstraints = AvatarDistorted_IK.AddComponent<Configuration_Constraints>();
			distortedPelvisConstraints.Avatar = avatarDistorted;
			distortedPelvisConstraints.boneType = Configuration_Constraints.BoneType.Pelvis;
			originalPelvisConstraints.positionWeight = 1;
			originalPelvisConstraints.rotationWeight = 1;

		}


		if ( leftShoulder ) {

			if ( shoulderIK == IKShoulderMethods.Accurate ) {
				//Original Avatar
				originalLeftShoulderConstraints = AvatarOriginal_IK.AddComponent<Configuration_Constraints>();
				originalLeftShoulderConstraints.Avatar = avatarOriginal;
				originalLeftShoulderConstraints.boneType = Configuration_Constraints.BoneType.LeftShoulder;
				originalLeftShoulderConstraints.positionWeight = 1;
				originalLeftShoulderConstraints.rotationWeight = 0;
					
				//Distorted Avatar
				distortedLeftShoulderConstraints = AvatarDistorted_IK.AddComponent<Configuration_Constraints>();
				distortedLeftShoulderConstraints.Avatar = avatarDistorted;
				distortedLeftShoulderConstraints.boneType = Configuration_Constraints.BoneType.LeftShoulder;
				distortedLeftShoulderConstraints.positionWeight = 1;
				distortedLeftShoulderConstraints.rotationWeight = 0;

			} else {	
				
				//Original Avatar
				originalLeftShoulderIK =  AvatarOriginal_IK.AddComponent<ShoulderIK>();
				originalLeftShoulderIK.Avatar = avatarOriginal;
				originalLeftShoulderIK.shoulderType = ShoulderIK.ShoulderTypes.Left;
				originalLeftShoulderIK.shoulderRotationMode = ShoulderIK.ShoulderRotationModes.Combination;
				originalLeftShoulderIK.offsetCustom = 0;
				originalLeftShoulderIK.isTracking = isShoulderTracked;



				//Distorted Avatar
				distortedLeftShoulderIK = AvatarDistorted_IK.AddComponent <ShoulderIK>();
				distortedLeftShoulderIK.Avatar = avatarDistorted;
				distortedLeftShoulderIK.shoulderType = ShoulderIK.ShoulderTypes.Left;
				distortedLeftShoulderIK.shoulderRotationMode = ShoulderIK.ShoulderRotationModes.Combination;
				distortedLeftShoulderIK.offsetCustom = 0;
				distortedLeftShoulderIK.isTracking = isShoulderTracked;
			}
		}


		if ( rightShoulder ) {
			
			if( shoulderIK == IKShoulderMethods.Accurate ) {
				//Original Avatar
				originalRightShoulderConstraints = AvatarOriginal_IK.AddComponent<Configuration_Constraints>();
				originalRightShoulderConstraints.Avatar = avatarOriginal;
				originalRightShoulderConstraints.boneType = Configuration_Constraints.BoneType.RightShoulder;
				originalRightShoulderConstraints.positionWeight = 1;
				originalRightShoulderConstraints.rotationWeight = 0;

				//Distorted Avatar
				distortedRightShoulderConstraints = AvatarDistorted_IK.AddComponent<Configuration_Constraints>();
				distortedRightShoulderConstraints.Avatar = avatarDistorted;
				distortedRightShoulderConstraints.boneType = Configuration_Constraints.BoneType.RightShoulder;
				distortedRightShoulderConstraints.positionWeight = 1;
				distortedRightShoulderConstraints.rotationWeight = 0;
			} else {
				//Original Avatar
				originalRightShoulderIK = AvatarOriginal_IK.AddComponent<ShoulderIK>();
				originalRightShoulderIK.Avatar = avatarOriginal;
				originalRightShoulderIK.shoulderType = ShoulderIK.ShoulderTypes.Right;
				originalRightShoulderIK.shoulderRotationMode = ShoulderIK.ShoulderRotationModes.Combination;
				originalRightShoulderIK.offsetCustom = 0;
				originalRightShoulderIK.isTracking = isShoulderTracked;

				//Distorted Avatar
				distortedRightShoulderIK = AvatarDistorted_IK.AddComponent<ShoulderIK>();
				distortedRightShoulderIK.Avatar = avatarDistorted;
				distortedRightShoulderIK.shoulderType = ShoulderIK.ShoulderTypes.Right;
				distortedRightShoulderIK.shoulderRotationMode = ShoulderIK.ShoulderRotationModes.Combination;
				distortedRightShoulderIK.offsetCustom = 0;
				distortedRightShoulderIK.isTracking = isShoulderTracked;
			}
		

			
			

		}

		if (leftArm || rightArm || leftLeg || rightLeg)
		{
			originalLimbIK = AvatarOriginal_IK.AddComponent<Configuration_LimbIK>();
			originalLimbIK.Avatar = avatarOriginal;

			
			distortedLimbIK = AvatarDistorted_IK.AddComponent<Configuration_LimbIK>();
			distortedLimbIK.Avatar = avatarDistorted;

			if(TargetsCustomized.Count!=0) 
			{
				int i = 0;
				if( TargetsCustomizedSettings.Count != 0) 
				{
					foreach( Configuration_LimbIK.TargetType target in TargetsCustomizedSettings )
					{
						distortedLimbIK.TargetCustomized.Add( TargetsCustomized[i]);
						distortedLimbIK.TargetCustomizedSettings.Add(target);

						i++;
					}

				} else {
					throw new Exception( "You need to set the settings of the customized targets" );
				}
			}

			if ( TargetsCustomizedSettings.Count != 0 ) {
				if ( TargetsCustomizedSettings.Count == 0 ) {
					throw new Exception( "You need to set the customized targets" );
				}
			}

			if ( TargetsCustomizedSettings.Count != TargetsCustomized.Count)
				throw new Exception( "You need to set the same number of customized targets as settings targets" );

			if ( leftArm ) {
				originalLimbIK.limbType.Add( Configuration_LimbIK.LimbType.LeftArm );
				distortedLimbIK.limbType.Add( Configuration_LimbIK.LimbType.LeftArm );
			}

			if ( rightArm ) {
				originalLimbIK.limbType.Add( Configuration_LimbIK.LimbType.RightArm );
				distortedLimbIK.limbType.Add( Configuration_LimbIK.LimbType.RightArm );
			}

			if ( leftLeg ) {
				originalLimbIK.limbType.Add( Configuration_LimbIK.LimbType.LeftLeg );
				distortedLimbIK.limbType.Add( Configuration_LimbIK.LimbType.LeftLeg );
			}

			if ( rightLeg ) {
				originalLimbIK.limbType.Add( Configuration_LimbIK.LimbType.RightLeg );
				distortedLimbIK.limbType.Add( Configuration_LimbIK.LimbType.RightLeg );
			}


			//Twist Relaxer Component
			if ( leftArm || rightArm ) 
			{
				originalTwistIK = AvatarOriginal_IK.AddComponent<TwistRelaxer_FinalIK>();
				originalTwistIK.Avatar = avatarOriginal;
				originalTwistIK.twistWeight = 1;
				originalTwistIK.twistCrossfade = 0.6f;

				distortedTwistIK = AvatarDistorted_IK.AddComponent<TwistRelaxer_FinalIK>();
				distortedTwistIK.Avatar = avatarDistorted;
				distortedTwistIK.twistWeight = 1;
				distortedTwistIK.twistCrossfade = 0.6f;
			}

		}


		if(chest) {

			originalFABBRIK = AvatarOriginal_IK.AddComponent<Configuration_FABRIK>();
			originalFABBRIK.Avatar = avatarOriginal;

			distortedFABBRIK = AvatarDistorted_IK.AddComponent<Configuration_FABRIK>();
			distortedFABBRIK.Avatar = avatarDistorted;
		}

	}

    // Update is called once per frame
    void Update()
    {
		if ( shoulderIK == IKShoulderMethods.Realist ) {

			if ( leftShoulder ) {
				//Original Avatar
				originalLeftShoulderIK.isTracking = isShoulderTracked;

				//Distorted Avatar
				distortedLeftShoulderIK.isTracking = isShoulderTracked;
			}


			if ( rightShoulder ) {
				//Original Avatar
				originalRightShoulderIK.isTracking = isShoulderTracked;

				//Distorted Avatar
				distortedRightShoulderIK.isTracking = isShoulderTracked;

			}
		}
	
	}
}
