﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//TODO : Inherite fron character_Generator_Settings abstract class

[ExecuteInEditMode]
public class RootMotion_Character_Generator_Settings : Character_Generator_Settings {


    void Start()
    {
        limbIdentified = false;

    }


    public override void Update()
    {
        //#if UNITY_EDITOR
        if (!limbIdentified)
        {

            //Root attribution
             settingsReferences.root = transform;

            // Pelvis
             settingsReferences.pelvis = transform.GetChild(0).GetChild(0);

            // Spine
             settingsReferences.spine = transform.GetChild(0).GetChild(0).GetChild(0);

            //Last Spine
             settingsReferences.lastSpine = transform.GetChild(0).GetChild(0).GetChild(0).GetChild(2).GetChild(0).GetChild(0);

            //Chest
             settingsReferences.chest = transform.GetChild(0).GetChild(0).GetChild(0).GetChild(2).GetChild(0).GetChild(0);

            //Neck
             settingsReferences.neck = transform.GetChild(0).GetChild(0).GetChild(0).GetChild(2).GetChild(0).GetChild(0).GetChild(0);

            //Head
             settingsReferences.head = transform.GetChild(0).GetChild(0).GetChild(0).GetChild(2).GetChild(0).GetChild(0).GetChild(0).GetChild(0);

            //Head End
             settingsReferences.headEnd = transform.GetChild(0).GetChild(0).GetChild(0).GetChild(2).GetChild(0).GetChild(0).GetChild(0).GetChild(3);

            //Left Shoulder
             settingsReferences.leftShoulder = transform.GetChild(0).GetChild(0).GetChild(0).GetChild(2).GetChild(0).GetChild(0).GetChild(0).GetChild(1);

            //Left Arm
             settingsReferences.leftUpperArm = transform.GetChild(0).GetChild(0).GetChild(0).GetChild(2).GetChild(0).GetChild(0).GetChild(0).GetChild(1).GetChild(0);

            //Left ForeArm
             settingsReferences.leftForearm =  transform.GetChild(0).GetChild(0).GetChild(0).GetChild(2).GetChild(0).GetChild(0).GetChild(0).GetChild(1).GetChild(0).GetChild(0);

            //Left Hand 
             settingsReferences.leftHand = transform.GetChild(0).GetChild(0).GetChild(0).GetChild(2).GetChild(0).GetChild(0).GetChild(0).GetChild(1).GetChild(0).GetChild(0).GetChild(0);

            //Left Fingers Base
             settingsReferences.leftFingerBase = transform.GetChild(0).GetChild(0).GetChild(0).GetChild(2).GetChild(0).GetChild(0).GetChild(0).GetChild(1).GetChild(0).GetChild(0).GetChild(0).GetChild(0);

            //Right Shoulder
             settingsReferences.rightShoulder =transform.GetChild(0).GetChild(0).GetChild(0).GetChild(2).GetChild(0).GetChild(0).GetChild(0).GetChild(2);

            //Right Arm
             settingsReferences.rightUpperArm = transform.GetChild(0).GetChild(0).GetChild(0).GetChild(2).GetChild(0).GetChild(0).GetChild(0).GetChild(2).GetChild(0);

            //Right ForeArm
             settingsReferences.rightForearm = transform.GetChild(0).GetChild(0).GetChild(0).GetChild(2).GetChild(0).GetChild(0).GetChild(0).GetChild(2).GetChild(0).GetChild(0);

            //Right Hand 
             settingsReferences.rightHand = transform.GetChild(0).GetChild(0).GetChild(0).GetChild(2).GetChild(0).GetChild(0).GetChild(0).GetChild(2).GetChild(0).GetChild(0).GetChild(0);

            //Right Fingers Base
             settingsReferences.rightFingerBase = transform.GetChild(0).GetChild(0).GetChild(0).GetChild(2).GetChild(0).GetChild(0).GetChild(0).GetChild(2).GetChild(0).GetChild(0).GetChild(0).GetChild(0);

            //Left Thigh
             settingsReferences.leftThigh = transform.GetChild(0).GetChild(0).GetChild(0).GetChild(0);

            //Left Calf
             settingsReferences.leftCalf = transform.GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(0);

            //Left Foot
             settingsReferences.leftFoot = transform.GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(0);

            //Left Toes
             settingsReferences.leftToes = transform.GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(0);

            //Right Thigh
             settingsReferences.rightThigh = transform.GetChild(0).GetChild(0).GetChild(0).GetChild(1);

            //Right Calf
             settingsReferences.rightCalf = transform.GetChild(0).GetChild(0).GetChild(0).GetChild(1).GetChild(0);

            //Right Foot
             settingsReferences.rightFoot = transform.GetChild(0).GetChild(0).GetChild(0).GetChild(1).GetChild(0).GetChild(0);

            //Right Toes
             settingsReferences.rightToes = transform.GetChild(0).GetChild(0).GetChild(0).GetChild(1).GetChild(0).GetChild(0).GetChild(0);



        }
//#endif
    }


}
