﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class Brian_Character_Generator_Settings : Character_Generator_Settings {


    void Start()
    {
        limbIdentified = false;
    }

	public override void Update ()
    {
        //#if UNITY_EDITOR
      if (!limbIdentified)
            {

                //Root attribution
                settingsReferences.root = transform;
                
                
                settingsReferences.pelvis = transform.GetChild(0).GetChild(0).GetChild(0).GetChild(0);
               
                
                settingsReferences.spine = transform.GetChild(0).GetChild(0).GetChild(0).GetChild(1);
                
                
                settingsReferences.lastSpine = transform.GetChild(0).GetChild(0).GetChild(0).GetChild(1).GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(0);
                
                
                settingsReferences.chest = transform.GetChild(0).GetChild(0).GetChild(0).GetChild(1).GetChild(0).GetChild(0).GetChild(0);
               
                
                settingsReferences.neck = transform.GetChild(0).GetChild(0).GetChild(0).GetChild(1).GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(0);
                
                 
                settingsReferences.head = transform.GetChild(0).GetChild(0).GetChild(0).GetChild(1).GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(0);
                
                  
                //headEnd = transform.GetChild(0).GetChild(0).GetChild(0).GetChild(2).GetChild(0).GetChild(0).GetChild(0).GetChild(3);
               
               
                settingsReferences.leftShoulder = transform.GetChild(0).GetChild(0).GetChild(0).GetChild(1).GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(2).GetChild(0).GetChild(0);
                
                   
                settingsReferences.leftUpperArm = transform.GetChild(0).GetChild(0).GetChild(0).GetChild(1).GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(2).GetChild(0).GetChild(0);
                
                 
                settingsReferences.leftForearm = transform.GetChild(0).GetChild(0).GetChild(0).GetChild(1).GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(2).GetChild(0).GetChild(0).GetChild(0);
                
                  
                settingsReferences.leftHand = transform.GetChild(0).GetChild(0).GetChild(0).GetChild(1).GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(2).GetChild(0).GetChild(0).GetChild(0).GetChild(0);
                
                     
                settingsReferences.leftFingerBase = transform.GetChild(0).GetChild(0).GetChild(0).GetChild(1).GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(2).GetChild(0).GetChild(0).GetChild(0).GetChild(0);
                
                  
                settingsReferences.rightShoulder = transform.GetChild(0).GetChild(0).GetChild(0).GetChild(1).GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(1).GetChild(0).GetChild(0);
                
                 
                settingsReferences.rightUpperArm = transform.GetChild(0).GetChild(0).GetChild(0).GetChild(1).GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(1).GetChild(0).GetChild(0);
                
                
                settingsReferences.rightForearm = transform.GetChild(0).GetChild(0).GetChild(0).GetChild(1).GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(1).GetChild(0).GetChild(0).GetChild(0);
                
                
                settingsReferences.rightHand = transform.GetChild(0).GetChild(0).GetChild(0).GetChild(1).GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(1).GetChild(0).GetChild(0).GetChild(0).GetChild(0);
                
                
                settingsReferences.rightFingerBase = transform.GetChild(0).GetChild(0).GetChild(0).GetChild(1).GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(1).GetChild(0).GetChild(0).GetChild(0).GetChild(0);
               
               
                settingsReferences.leftThigh = transform.GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(0);
               
               
                settingsReferences.leftCalf = transform.GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(0);
                
                
                settingsReferences.leftFoot = transform.GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(0);
                
                
                settingsReferences.leftToes = transform.GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(0);
                
                
                settingsReferences.rightThigh = transform.GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(1);
                
                
                settingsReferences.rightCalf = transform.GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(1).GetChild(0);
                
                
                settingsReferences.rightFoot = transform.GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(1).GetChild(0).GetChild(0);
                
                settingsReferences.rightToes = transform.GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(1).GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(0);
                
                //Trunk
                if(settingsReferences.trunk !=null)
                settingsReferences.trunk.Clear();

                settingsReferences.trunk.Add(transform.GetChild(0).GetChild(0).GetChild(0).GetChild(1));
                settingsReferences.trunk.Add(transform.GetChild(0).GetChild(0).GetChild(0).GetChild(1).GetChild(0));
                settingsReferences.trunk.Add(transform.GetChild(0).GetChild(0).GetChild(0).GetChild(1).GetChild(0).GetChild(0));
                settingsReferences.trunk.Add(transform.GetChild(0).GetChild(0).GetChild(0).GetChild(1).GetChild(0).GetChild(0).GetChild(0));
                settingsReferences.trunk.Add(transform.GetChild(0).GetChild(0).GetChild(0).GetChild(1).GetChild(0).GetChild(0).GetChild(0).GetChild(0));
                settingsReferences.trunk.Add(transform.GetChild(0).GetChild(0).GetChild(0).GetChild(1).GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(0));
                //trunk.Add(transform.GetChild(0).GetChild(0).GetChild(0).GetChild(1).GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(0));

                nbVertebrae = settingsReferences.trunk.Count;

                

                limbIdentified = true;
                
            }

      if(settingsReferences.root==null && limbIdentified)
        {
            limbIdentified = false;

        }
            ///setReferenceAvatarCalibration();


        }
       
        //#endif
    }


