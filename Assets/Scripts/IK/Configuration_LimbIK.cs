﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RootMotion.FinalIK;


public class Configuration_LimbIK : MonoBehaviour {

    public Character_Generator_Settings Avatar;


    bool flag1 = false;
    int flag2 = 0;

    public enum LimbType { LeftArm , RightArm, LeftLeg, RightLeg};
    public List<LimbType> limbType=new List<LimbType>();

    public  List<GameObject> TargetCustomized=new List<GameObject>();

    public enum TargetType { LeftArmTargetCustomized, RightArmTargetCustomized, LeftLegTargetCustomized, RightLegTargetCustomized };
    public List<TargetType> TargetCustomizedSettings=new List<TargetType>();

    public List<GameObject> BendGoalCustomized= new List<GameObject>();


    public enum BendGoalType { LeftArmBendGoalCustomized, RightArmBendGoalCustomized, LeftLegBendGoalCustomized, RightLegBendGoalCustomized };
    public List<BendGoalType> BendGoalCustomizedSettings= new List<BendGoalType>();

    //public int nbIK=2;
    List<LimbIK> LimbIK_Scripts;
   //public LimbIK[] LimbIK_Scripts;

    // Use this for initialization
    void Start() {

        LimbIK_Scripts = new List<LimbIK>();



    }

    void Update()
    {

        if (TargetCustomized.Count != TargetCustomizedSettings.Count)
        {
            Debug.LogError("Target Customized Settings needs to have the same number of element in the same order as Target Customized");
            return;
        }


        if (BendGoalCustomized.Count != BendGoalCustomizedSettings.Count)
        {
            Debug.LogError("BendGoal Customized Settings needs to have the same number of element in the same order as BendGoal Customized");
            return;
        }

        if (LimbIK_Scripts.Count >0)
        {

            if (!flag1)
            {
                for (int i = 0; i < limbType.Count; i++)
                {

                    if (limbType[i] == LimbType.LeftArm )
                    {
                        //Left arm
                        LimbIK_Scripts[i].solver.goal = AvatarIKGoal.LeftHand;


                        //leftArm
                        LimbIK_Scripts[i].solver.SetChain(Avatar.settingsReferences.leftUpperArm, Avatar.settingsReferences.leftForearm,
                            Avatar.settingsReferences.leftHand, Avatar.settingsReferences.pelvis);  
                        //leftForearm


                        //Bend Goal
                        LimbIK_Scripts[i].solver.bendModifier = IKSolverLimb.BendModifier.Goal;

						// allow movement of the bone one by an external script
						LimbIK_Scripts[i].fixTransforms= false;
					}


                    if (limbType[i] == LimbType.RightArm )
                    {
                        //Right arm
                        LimbIK_Scripts[i].solver.goal = AvatarIKGoal.RightHand;


                        LimbIK_Scripts[i].solver.SetChain(Avatar.settingsReferences.rightUpperArm, Avatar.settingsReferences.rightForearm,
                           Avatar.settingsReferences.rightHand, Avatar.settingsReferences.pelvis);


                        //Bend Goal
                        LimbIK_Scripts[i].solver.bendModifier = IKSolverLimb.BendModifier.Goal;

						// allow movement of the bone one by an external script
						LimbIK_Scripts[i].fixTransforms = false;
					}


                    if (limbType[i] == LimbType.LeftLeg  )
                    {
                        //Right arm
                        LimbIK_Scripts[i].solver.goal = AvatarIKGoal.LeftFoot;

                        LimbIK_Scripts[i].solver.SetChain(Avatar.settingsReferences.leftThigh, Avatar.settingsReferences.leftCalf,
                           Avatar.settingsReferences.leftFoot, Avatar.settingsReferences.pelvis);


                        //Bend Goal
                        LimbIK_Scripts[i].solver.bendModifier = IKSolverLimb.BendModifier.Goal;
                    }

                    if (limbType[i] == LimbType.RightLeg)
                    {
                        //Right arm
                        LimbIK_Scripts[i].solver.goal = AvatarIKGoal.RightFoot;

                        LimbIK_Scripts[i].solver.SetChain(Avatar.settingsReferences.rightThigh, Avatar.settingsReferences.rightCalf,
                         Avatar.settingsReferences.rightFoot, Avatar.settingsReferences.pelvis);

                        //Bend Goal
                        LimbIK_Scripts[i].solver.bendModifier = IKSolverLimb.BendModifier.Goal;
                    }

                }

                flag1 = true;
            }

            if (flag2< 2*limbType.Count+1)
            {

                for (int i = 0; i < limbType.Count; i++)
                {

                    if (limbType[i] == LimbType.LeftArm)
                    {
                        if (LimbIK_Scripts[i].solver.target == null)
                        {
                            if (i < TargetCustomized.Count && TargetCustomizedSettings[i] == TargetType.LeftArmTargetCustomized)
                            {
                                LimbIK_Scripts[i].solver.target = TargetCustomized[i].transform;
                                flag2++;
                            }
                            else if (GameObject.Find(Avatar.settingsReferences.leftHand.name + "Target") != null)
                            {
                                LimbIK_Scripts[i].solver.target = GameObject.Find(Avatar.settingsReferences.leftHand.name + "Target").transform;
                                flag2++;

                            }
                        }

                        if (LimbIK_Scripts[i].solver.bendGoal == null)
                        {
                            if (i < BendGoalCustomized.Count && BendGoalCustomizedSettings[i] == BendGoalType.LeftArmBendGoalCustomized)
                            {
                                LimbIK_Scripts[i].solver.bendGoal = BendGoalCustomized[i].transform;
                                flag2++;
                            }
                            else if (GameObject.Find(Avatar.settingsReferences.leftForearm.name + "Target") != null )
                            {
                                LimbIK_Scripts[i].solver.bendGoal = GameObject.Find(Avatar.settingsReferences.leftForearm.name + "Target").transform;
                                flag2++;

                            }
                        }

                  


                    }


                    if (limbType[i] == LimbType.RightArm)
                    {
                        if (LimbIK_Scripts[i].solver.target == null)
                        {
                            if (i < TargetCustomized.Count && TargetCustomizedSettings[i] == TargetType.RightArmTargetCustomized)
                            {
                                LimbIK_Scripts[i].solver.target = TargetCustomized[i].transform;
                                flag2++;
                            }
                            else if (GameObject.Find(Avatar.settingsReferences.rightHand.name + "Target") != null && LimbIK_Scripts[i].solver.target == null)
                            {
                                LimbIK_Scripts[i].solver.target = GameObject.Find(Avatar.settingsReferences.rightHand.name + "Target").transform;
                                flag2++;

                            }
                        }


                        if (LimbIK_Scripts[i].solver.bendGoal == null)
                        {
                            if (i < BendGoalCustomized.Count && BendGoalCustomizedSettings[i] == BendGoalType.RightArmBendGoalCustomized)
                            {
                                LimbIK_Scripts[i].solver.bendGoal = BendGoalCustomized[i].transform;
                                flag2++;
                            }
                            else if (GameObject.Find(Avatar.settingsReferences.rightForearm.name + "Target") != null && LimbIK_Scripts[i].solver.bendGoal == null)
                            {
                                LimbIK_Scripts[i].solver.bendGoal = GameObject.Find(Avatar.settingsReferences.rightForearm.name + "Target").transform;
                                flag2++;

                            }
                        }
                    

                    }


                    if (limbType[i] == LimbType.LeftLeg)
                    {
                        if (LimbIK_Scripts[i].solver.target == null)
                        {
                            if (i < TargetCustomized.Count && TargetCustomizedSettings[i] == TargetType.LeftLegTargetCustomized)
                            {
                                LimbIK_Scripts[i].solver.target = TargetCustomized[i].transform;
                                flag2++;
                            }
                            else if (GameObject.Find(Avatar.settingsReferences.leftFoot.name + "Target") != null && LimbIK_Scripts[i].solver.target == null)
                            {
                                LimbIK_Scripts[i].solver.target = GameObject.Find(Avatar.settingsReferences.leftFoot.name + "Target").transform;
                                flag2++;

                            }
                        }

                        if (LimbIK_Scripts[i].solver.bendGoal == null)
                        {
                            if (i < BendGoalCustomized.Count && BendGoalCustomizedSettings[i] == BendGoalType.LeftLegBendGoalCustomized)
                            {
                                LimbIK_Scripts[i].solver.bendGoal = BendGoalCustomized[i].transform;
                                flag2++;
                            }
                            else if (GameObject.Find(Avatar.settingsReferences.leftCalf.name + "Target") != null && LimbIK_Scripts[i].solver.bendGoal == null)
                            {
                                LimbIK_Scripts[i].solver.bendGoal = GameObject.Find(Avatar.settingsReferences.leftCalf.name + "Target").transform;
                                flag2++;

                            }
                        }


                    }

                    if (limbType[i] == LimbType.RightLeg)
                    {
                        if (LimbIK_Scripts[i].solver.target == null)
                        {
                            if (i < TargetCustomized.Count && TargetCustomizedSettings[i] == TargetType.RightLegTargetCustomized)
                            {
                                LimbIK_Scripts[i].solver.target = TargetCustomized[i].transform;
                                flag2++;
                            }
                            else if (GameObject.Find(Avatar.settingsReferences.rightFoot.name + "Target") != null && LimbIK_Scripts[i].solver.target == null)
                            {
                                LimbIK_Scripts[i].solver.target = GameObject.Find(Avatar.settingsReferences.rightFoot.name + "Target").transform;
                                flag2++;

                            }
                        }

                        if (LimbIK_Scripts[i].solver.bendGoal == null)
                        {
                            if (i < BendGoalCustomized.Count && BendGoalCustomizedSettings[i] == BendGoalType.RightLegBendGoalCustomized)
                            {
                                LimbIK_Scripts[i].solver.bendGoal = BendGoalCustomized[i].transform;
                                flag2++;
                            }
                            else if (GameObject.Find(Avatar.settingsReferences.rightCalf.name + "Target") != null && LimbIK_Scripts[i].solver.bendGoal == null)
                            {
                                LimbIK_Scripts[i].solver.bendGoal = GameObject.Find(Avatar.settingsReferences.rightCalf.name + "Target").transform;
                                flag2++;

                            }
                        }

                    }



                }
               

            }

            


        }
        else
        {
            for (int i = 0; i < limbType.Count; i++)
            {
                LimbIK_Scripts.Add(transform.gameObject.AddComponent<RootMotion.FinalIK.LimbIK>());
            }
            // Debug.Log(LimbIK_Scripts.Count);
        }
         


    }
       



    }


       


    

