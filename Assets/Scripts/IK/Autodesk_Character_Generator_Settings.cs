﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//TODO : Inherite fron character_Generator_Settings abstract class

[ExecuteInEditMode]
public class Autodesk_Character_Generator_Settings : Character_Generator_Settings {


    void Start()
    {
        limbIdentified = false;

    }

    public override void  Update()
    {
        base.Update();
//#if UNITY_EDITOR
        if (!limbIdentified)
        {

            //Root attribution
             settingsReferences.root = transform;

            // Pelvis
             settingsReferences.pelvis = transform.Find("master").GetChild(0).GetChild(0);

            // Spine
             settingsReferences.spine = transform.Find("master").GetChild(0).GetChild(0).Find("Spine");

            //Last Spine
             settingsReferences.lastSpine = transform.Find("master").GetChild(0).GetChild(0).Find("Spine").GetChild(0).GetChild(0);

            //Chest
             settingsReferences.chest = transform.Find("master").GetChild(0).GetChild(0).Find("Spine").GetChild(0).GetChild(0).Find("Neck");

            //Neck
            settingsReferences.neck = transform.Find("master").GetChild(0).GetChild(0).Find("Spine").GetChild(0).GetChild(0).Find("Neck");

            //Head
             settingsReferences.head = transform.Find("master").GetChild(0).GetChild(0).Find("Spine").GetChild(0).GetChild(0).Find("Neck").GetChild(0);

            //Left Eyes
            settingsReferences.leftEye = transform.Find("master").GetChild(0).GetChild(0).Find("Spine").GetChild(0).GetChild(0).Find("Neck").GetChild(0).Find("LeftEye");

            //Right Eyes
            settingsReferences.rightEye = transform.Find("master").GetChild(0).GetChild(0).Find("Spine").GetChild(0).GetChild(0).Find("Neck").GetChild(0).Find("RightEye");

            //Head End
            settingsReferences.headEnd = transform.Find("master").GetChild(0).GetChild(0).Find("Spine").GetChild(0).GetChild(0).Find("Neck").GetChild(0).Find("HeadEnd");

            //Left Shoulder
             settingsReferences.leftShoulder = transform.Find("master").GetChild(0).GetChild(0).Find("Spine").GetChild(0).GetChild(0).Find("LeftShoulder");

            //Left Arm
             settingsReferences.leftUpperArm = transform.Find("master").GetChild(0).GetChild(0).Find("Spine").GetChild(0).GetChild(0).Find("LeftShoulder").GetChild(0);

            //Left ForeArm
             settingsReferences.leftForearm = transform.Find("master").GetChild(0).GetChild(0).Find("Spine").GetChild(0).GetChild(0).Find("LeftShoulder").GetChild(0).GetChild(0);

            //Left Hand 
             settingsReferences.leftHand = transform.Find("master").GetChild(0).GetChild(0).Find("Spine").GetChild(0).GetChild(0).Find("LeftShoulder").GetChild(0).GetChild(0).GetChild(0);

            //Left Fingers Base
             settingsReferences.leftFingerBase = transform.Find("master").GetChild(0).GetChild(0).Find("Spine").GetChild(0).GetChild(0).Find("LeftShoulder").GetChild(0).GetChild(0).GetChild(0).GetChild(0);

            //Right Shoulder
             settingsReferences.rightShoulder = transform.Find("master").GetChild(0).GetChild(0).Find("Spine").GetChild(0).GetChild(0).Find("RightShoulder");

            //Right Arm
             settingsReferences.rightUpperArm = transform.Find("master").GetChild(0).GetChild(0).Find("Spine").GetChild(0).GetChild(0).Find("RightShoulder").GetChild(0);

            //Right ForeArm
             settingsReferences.rightForearm = transform.Find("master").GetChild(0).GetChild(0).Find("Spine").GetChild(0).GetChild(0).Find("RightShoulder").GetChild(0).GetChild(0);

            //Right Hand 
             settingsReferences.rightHand = transform.Find("master").GetChild(0).GetChild(0).Find("Spine").GetChild(0).GetChild(0).Find("RightShoulder").GetChild(0).GetChild(0).GetChild(0);

            //Right Fingers Base
             settingsReferences.rightFingerBase = transform.Find("master").GetChild(0).GetChild(0).Find("Spine").GetChild(0).GetChild(0).Find("RightShoulder").GetChild(0).GetChild(0).GetChild(0).GetChild(0);

            //Left Thigh
             settingsReferences.leftThigh = transform.Find("master").GetChild(0).GetChild(0).Find("LeftUpLeg");

            //Left Calf
             settingsReferences.leftCalf = transform.Find("master").GetChild(0).GetChild(0).Find("LeftUpLeg").GetChild(0);

            //Left Foot
             settingsReferences.leftFoot = transform.Find("master").GetChild(0).GetChild(0).Find("LeftUpLeg").GetChild(0).GetChild(0);

            //Left Toes
             settingsReferences.leftToes = transform.Find("master").GetChild(0).GetChild(0).Find("LeftUpLeg").GetChild(0).GetChild(0).GetChild(0);

            //Right Thigh
             settingsReferences.rightThigh = transform.Find("master").GetChild(0).GetChild(0).Find("RightUpLeg");

            //Right Calf
             settingsReferences.rightCalf = transform.Find("master").GetChild(0).GetChild(0).Find("RightUpLeg").GetChild(0);

            //Right Foot
             settingsReferences.rightFoot = transform.Find("master").GetChild(0).GetChild(0).Find("RightUpLeg").GetChild(0).GetChild(0);

            //Right Toes
             settingsReferences.rightToes = transform.Find("master").GetChild(0).GetChild(0).Find("RightUpLeg").GetChild(0).GetChild(0).GetChild(0);



            //Trunk
            if (settingsReferences.trunk != null)
                settingsReferences.trunk.Clear();

            settingsReferences.trunk.Add(transform.Find("master").GetChild(0).GetChild(0).Find("Spine"));
            settingsReferences.trunk.Add(transform.Find("master").GetChild(0).GetChild(0).Find("Spine").GetChild(0));
            settingsReferences.trunk.Add(transform.Find("master").GetChild(0).GetChild(0).Find("Spine").GetChild(0).GetChild(0));

            nbVertebrae = settingsReferences.trunk.Count;



            limbIdentified = true;

        }

        if (settingsReferences.root == null && limbIdentified)
        {
            limbIdentified = false;

        }
    }

  
	
}
