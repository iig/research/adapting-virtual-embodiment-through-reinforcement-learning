﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RootMotion.FinalIK;

public class ElbowsConstraints_Manager : MonoBehaviour {

    public Character_Generator_Settings Avatar;
    public bool reset = false;

    RotationLimitHinge leftElbow;
    RotationLimitHinge rightElbow;

    public enum ConstraintType { twist , rotation}

    public ConstraintType constraint=ConstraintType.rotation;

	// Use this for initialization
	void Start () {

        leftElbow = Avatar.settingsReferences.leftForearm.GetComponent<RotationLimitHinge>();
        rightElbow = Avatar.settingsReferences.rightForearm.GetComponent<RotationLimitHinge>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        if(constraint == ConstraintType.rotation )
        {
            leftElbow.axis = Vector3.up;
            leftElbow.useLimits = true;
            leftElbow.min = -180;
            leftElbow.max = 0;


            rightElbow.axis = Vector3.up;
            rightElbow.useLimits = true;
            rightElbow.min = -180;
            rightElbow.max = 0;
        }
		else if(constraint == ConstraintType.twist)
        {
            /* leftElbow.axis = Vector3.right;
             leftElbow.useLimits = true;
             leftElbow.min = -90;
             leftElbow.max = 90;


             rightElbow.axis = Vector3.right;
             rightElbow.useLimits = true;
             rightElbow.min = -90;
             rightElbow.max = 90;*/

            leftElbow.enabled=false;
            rightElbow.enabled=false;
        }

        if(reset)
        {
            leftElbow.enabled = true;
            rightElbow.enabled = true;
        }
    }
}
