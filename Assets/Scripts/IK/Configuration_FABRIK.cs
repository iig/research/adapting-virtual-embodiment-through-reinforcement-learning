﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Configuration_FABRIK : MonoBehaviour {

    // private int plane1ChainIndex = -1;
    // private int plane1NodeIndex = -1;

    public RootMotion.FinalIK.FABRIK fABRIK;
    public Character_Generator_Settings Avatar;
    List<Transform> spine;
    int nbSpines;

   // bool flag1 = false;


    // Use this for initialization
    void Start()
    {

        spine = new List<Transform>();

        if (transform.GetComponent<RootMotion.FinalIK.FABRIK>() == null)
        {
            fABRIK = transform.gameObject.AddComponent< RootMotion.FinalIK.FABRIK>();
        }
        else
            fABRIK = transform.GetComponent<RootMotion.FinalIK.FABRIK>();

        fABRIK.solver.IKPosition = fABRIK.solver.IKPosition * 100f;

        //spine.Add(Avatar.settingsReferences.pelvis);


        foreach (Transform vertebrae in Avatar.settingsReferences.trunk)
        {
            spine.Add(vertebrae);
        }

        spine.Add(Avatar.settingsReferences.neck);

        nbSpines = spine.Count;
      
        fABRIK.solver.SetChain(spine.ToArray(), Avatar.settingsReferences.root);

        fABRIK.solver.IKPositionWeight = 1.0f;
        fABRIK.solver.maxIterations = 1;
        fABRIK.solver.useRotationLimits =false;


        // fABRIK.solver.IKPosition = fABRIK.solver.IKPosition * 1.00001f;
    }

    // Update is called once per frame
    void Update()
    {
        if (fABRIK.solver.target==null)
        {

            if (GameObject.Find(spine[nbSpines -1].name + "Target") != null)
            {
                GameObject.Find(spine[nbSpines - 1].name + "Target").transform.position= GameObject.Find(spine[nbSpines - 1].name + "Target").transform.position* 1.00001f;
                fABRIK.solver.target = GameObject.Find(spine[nbSpines - 1].name + "Target").transform;
                fABRIK.solver.target.position = fABRIK.solver.target.position * 1.00001f;

                
            }

        }

    }

}


/*
        * Mapping the spine to the hip and chest planes
        * */
/*   public void WritePose(IKSolverFullBody solver)
   {
       Vector3 firstPosition = spine[0].GetPlanePosition(solver);
       Vector3 rootPosition = solver.GetNode(spine[rootNodeIndex].chainIndex, spine[rootNodeIndex].nodeIndex).solverPosition;
       Vector3 lastPosition = spine[spine.Length - 1].GetPlanePosition(solver);

       // If we have more than 3 bones, use the FABRIK algorithm
       if (useFABRIK)
       {
           Vector3 offset = solver.GetNode(spine[rootNodeIndex].chainIndex, spine[rootNodeIndex].nodeIndex).solverPosition - spine[rootNodeIndex].transform.position;

           for (int i = 0; i < spine.Length; i++)
           {
               spine[i].ikPosition = spine[i].transform.position + offset;
           }

           // Iterating the FABRIK algorithm
           for (int i = 0; i < iterations; i++)
           {
               ForwardReach(lastPosition);
               BackwardReach(firstPosition);
               spine[rootNodeIndex].ikPosition = rootPosition;
           }
       }
       else
       {
           // When we have just 3 bones, we know their positions already
           spine[0].ikPosition = firstPosition;
           spine[rootNodeIndex].ikPosition = rootPosition;
       }

       spine[spine.Length - 1].ikPosition = lastPosition;

       // Mapping the spine bones to the solver
       MapToSolverPositions(solver);
   }*/

/* public Vector3 GetPlanePosition(IKSolverFullBody solver)
 {
     return solver.GetNode(plane1ChainIndex, plane1NodeIndex).solverPosition + (GetTargetRotation(solver) * planePosition);
     //return planeNode1.solverPosition + (targetRotation * planePosition);
 }*/

/*  public IKSolver.Node GetNode(int chainIndex, int nodeIndex)
  {
      return chain[chainIndex].nodes[nodeIndex];
  }*/
