﻿using UnityEngine;
using System.Collections;
using RootMotion.FinalIK;

namespace RootMotion.FinalIK {

	/// <summary>
	/// Shoulder rotator is a workaround for FBBIK not rotating the shoulder bones when pulled by hands.
	/// It get's the job done if you need it, but will take 2 solving iterations.
	/// </summary>
	public class ShoulderRotator_TestFinal : MonoBehaviour {
        public GameObject shoulder;
        public GameObject hand;



		[Tooltip("Weight of shoulder rotation")]
		public float weight = 1.5f;
		[Tooltip("The greater the offset, the sooner the shoulder will start rotating")]
		public float offset = 0.2f;

		private LimbIK ik;
		private bool skip;

		void Start() {
			ik = GetComponent<LimbIK>();

			// You can use just LateUpdate, but note that it doesn't work when you have animatePhysics turned on for the character.
			//ik.solver.OnPostUpdate += RotateShoulders;
		}
		/*  private void LateUpdate()
		  {
			  shoulder.transform.rotation = Quaternion.Euler(new Vector3(30f, 30f, 30f)) * shoulder.transform.rotation;
		  }*/
		private void LateUpdate () {

			RotateShoulders();
		}

		private void RotateShoulders () {
			if (ik == null) return;
			if (ik.solver.IKPositionWeight <= 0f) return;
            
			// Skipping the second update cycle
			if (skip) {
				skip = false;
				return;
			}
            RotateShoulder(ik, weight, offset); // Rotate the left shoulder
            //RotateShoulder(FullBodyBipedChain.RightArm, weight, offset); // Rotate the right shoulder
           // shoulder.transform.rotation = Quaternion.Euler(new Vector3(1f, 1f, 1f)) * shoulder.transform.rotation;
            skip = true;
			ik.solver.Update(); // Update FBBIK again with the rotated shoulders
            
        }

		// Rotates a shoulder of a FBBIK character
		private void RotateShoulder(LimbIK ik, float weight, float offset) {
            // Get FromToRotation from the current swing direction of the shoulder to the IK target direction
            //Quaternion fromTo = Quaternion.FromToRotation(GetParentBoneMap(chain).swingDirection, ik.solver.GetEndEffector(chain).position - GetParentBoneMap(chain).transform.position);
            Quaternion fromTo = Quaternion.FromToRotation(ik.solver.bone1.transform.position - ik.solver.bone1.transform.parent.position, ik.solver.target.position - ik.solver.bone1.transform.parent.position) ;
            
            // Direction to the IK target
            //Vector3 toTarget = ik.solver.GetEndEffector(chain).position - ik.solver.GetLimbMapping(chain).bone1.position;
            Vector3 toTarget = ik.solver.target.position - ik.solver.bone1.transform.position;
			//Debug.Log(ik.solver.IKPosition);
			///Vector3 toTarget = ik.solver.GetLimbMapping(chain).bone3.position - ik.solver.GetLimbMapping(chain).bone1.position;
			// Length of the limb
			float limbLength = ik.solver.bone1.length + ik.solver.bone2.length;  

            // Divide IK Target direction magnitude by limb length to know how much the limb is being pulled
            float delta = (toTarget.magnitude / limbLength) - 1f;// + offset;
            Debug.Log(delta);
            delta = Mathf.Clamp(delta * weight, 0f, 1f);
            //Debug.Log(delta);

            // Calculate the rotation offset for the shoulder
            //Quaternion rotationOffset = Quaternion.Lerp(Quaternion.identity, fromTo, delta * ik.solver.GetEndEffector(chain).positionWeight * ik.solver.IKPositionWeight);
            Quaternion rotationOffset = Quaternion.Lerp(Quaternion.identity, fromTo, delta );
			// Debug.Log(ik.solver.GetEndEffector(chain).position);
			// Rotate the shoulder
			ik.solver.bone1.transform.parent.rotation = rotationOffset * ik.solver.bone1.transform.parent.rotation;
            // shoulder.transform.rotation = Quaternion.Euler(new Vector3(360,30,360)) * shoulder.transform.rotation;
            //Debug.Log(shoulder.transform.rotation);
           // shoulder.transform.rotation = Quaternion.Euler(new Vector3(1f, 1f, 1f)) * shoulder.transform.rotation;
        }

		// Get the shoulder BoneMap
	/*	private IKMapping.BoneMap GetParentBoneMap(FullBodyBipedChain chain) {
			return ik.solver.GetLimbMapping(chain).GetBoneMap(IKMappingLimb.BoneMapType.Parent);
		}*/

		// Remove the delegate when destroyed
		void OnDestroy() {
			if (ik != null) ik.solver.OnPostUpdate -= RotateShoulders;
		}
	}
}
