﻿using UnityEngine;
using System.Collections;


    /// <summary>
    /// Contains and manages a set of constraints.
    /// </summary>
    [System.Serializable]
    public class Configuration_Constraints : MonoBehaviour
    {

        #region Main Interface
        public Character_Generator_Settings Avatar;
    public enum BoneType { Pelvis , Head , LeftShoulder,RightShoulder };
    public BoneType boneType;

        /// <summary>
        /// The transform.
        /// </summary>
        [HideInInspector]
        public Transform transformCust;
        /// <summary>
        /// The target.
        /// </summary>
        public Transform target;
        /// <summary>
        /// The position offset.
        /// </summary>
        public Vector3 positionOffset;
        /// <summary>
        /// The position to lerp to by positionWeight
        /// </summary>
        [HideInInspector]
        public Vector3 position;
        /// <summary>
        /// The weight of lerping to position
        /// </summary>
        [Range(0f, 1f)]
        public float positionWeight;
        /// <summary>
        /// The rotation offset.
        /// </summary>
        public Vector3 rotationOffset;
        /// <summary>
        /// The rotation to slerp to by rotationWeight
        /// </summary>
        [HideInInspector]
        public Vector3 rotation;
        /// <summary>
        /// The weight of slerping to rotation
        /// </summary>
        [Range(0f, 1f)]
        public float rotationWeight;

        /// <summary>
        /// Determines whether this instance is valid.
        /// </summary>
        public bool IsValid()
        {
            return transformCust != null;
        }

        /// <summary>
        /// Initiate to the specified transform.
        /// </summary>
        public void Initiate(Transform transform)
        {
            this.transformCust = transform;
            this.position = transform.position;
            this.rotation = transform.eulerAngles;
        }

    public void Start()
    {
        if (boneType == BoneType.Pelvis)
            Initiate(Avatar.settingsReferences.pelvis);

        if (boneType == BoneType.Head)
            Initiate(Avatar.settingsReferences.head);

		if ( boneType == BoneType.LeftShoulder )
			Initiate( Avatar.settingsReferences.leftUpperArm);

		if ( boneType == BoneType.RightShoulder )
			Initiate( Avatar.settingsReferences.rightUpperArm );
	}

    /// <summary>
    /// Updates the constraints.
    /// </summary>
    public void LateUpdate()
        {
            if (!IsValid()) return;

        //Target
        if(target == null)
        {
            if (boneType == BoneType.Pelvis && GameObject.Find(Avatar.settingsReferences.pelvis.name + "Target")!=null)
                target = GameObject.Find(Avatar.settingsReferences.pelvis.name + "Target").transform;

            if (boneType == BoneType.Head && GameObject.Find(Avatar.settingsReferences.head.name + "Target") != null)
                target = GameObject.Find(Avatar.settingsReferences.head.name + "Target").transform;

			if ( boneType == BoneType.LeftShoulder && GameObject.Find( Avatar.settingsReferences.leftUpperArm.name + "Target" ) != null )
				target = GameObject.Find( Avatar.settingsReferences.leftUpperArm.name + "Target" ).transform;

			if ( boneType == BoneType.RightShoulder && GameObject.Find( Avatar.settingsReferences.rightUpperArm.name + "Target" ) != null )
				target = GameObject.Find( Avatar.settingsReferences.rightUpperArm.name + "Target" ).transform;
		}

            // Position
        if (target != null) position = target.position;
            transformCust.position += positionOffset;
            if (positionWeight > 0f) transformCust.position = Vector3.Lerp(transformCust.position, position, positionWeight);

            // Rotation
            if (target != null) rotation = target.eulerAngles;
            transformCust.rotation = Quaternion.Euler(rotationOffset) * transformCust.rotation;
            if (rotationWeight > 0f) transformCust.rotation = Quaternion.Slerp(transformCust.rotation, Quaternion.Euler(rotation), rotationWeight);
        }

        #endregion Main Interface
    }
