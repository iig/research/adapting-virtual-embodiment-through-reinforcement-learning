﻿using UnityEngine;
using System.Collections;
using RootMotion.FinalIK;



	/// <summary>
	/// Shoulder IK is a workaround for LimbIK not rotating the shoulder bones when pulled by hands.
	/// Made by Thibault Porssut
	/// </summary>
	public class ShoulderIK : MonoBehaviour {


		public enum ShoulderTypes { Left, Right };
		public ShoulderTypes shoulderType;
		public enum ShoulderRotationModes { Alkashi,Tracking,Combination, ShoulderDistortion,FromToMine };
		public ShoulderRotationModes shoulderRotationMode;
		public Character_Generator_Settings Avatar;
		public Transform targetShoulder;
		public bool isTracking;



		public float offsetCustom = 0.3f;

		[Tooltip( "Weight of shoulder rotation" )]
		public float weight = 1.5f;
		[Tooltip( "The greater the offset, the sooner the shoulder will start rotating" )]
		public float offset = 0.2f;


		private LimbIK ik;
		private LimbIK[] ikList=new LimbIK[0];
		private Quaternion initialRotation;
		private Quaternion initialLocalPosition;
		private bool isSet;
		Configuration_Constraints shoulderConstraints;


	void Start () {
			ik = GetComponent<LimbIK>();

		if ( ik != null ) {
			initialRotation = ik.solver.bone1.transform.parent.rotation;

			// You can use just LateUpdate, but note that it doesn't work when you have animatePhysics turned on for the character.
			ik.solver.OnPostUpdate += RotateShoulder;

		}
	
		}

		private void Update () {

		if ( ikList.Length == 0 ) {
			ikList = GetComponents<LimbIK>();
			return;
		} 
			

		if ( !isSet && ikList != null ) {

			foreach(LimbIK ikComponent in ikList) {

				if(shoulderType==ShoulderTypes.Left) {

					if(ikComponent.solver.goal == AvatarIKGoal.LeftHand) {
						ik = ikComponent;
					}
				} else {

					if ( ikComponent.solver.goal == AvatarIKGoal.RightHand ) {
						ik = ikComponent;
					}
				}
			}

		    if(ik!=null) {

				initialRotation = ik.solver.bone1.transform.parent.rotation;
				// You can use just LateUpdate, but note that it doesn't work when you have animatePhysics turned on for the character.
				ik.solver.OnPostUpdate += RotateShoulder;

				isSet = true;
			}
			
		}
		


	}

		private void LateUpdate () {

		if ( isSet ) {

			// Need to set the script after RootMotion script in script execution order if you choose this option
			//RotateShoulder();
		}


			}


		// Rotates a shoulder of a FBBIK character
		private void RotateShoulder ( ) {

			switch ( shoulderRotationMode ) {
				

				case ShoulderRotationModes.FromToMine:
					// Get FromToRotation from the current swing direction of the shoulder to the IK target direction
					Quaternion fromTo = Quaternion.FromToRotation( ik.solver.bone1.transform.position - ik.solver.bone1.transform.parent.position, ik.solver.target.position - ik.solver.bone1.transform.parent.position );

					// Direction to the IK target
					Vector3 toTarget = ik.solver.target.position - ik.solver.bone1.transform.position;

					// Length of the limb
					float limbLength = Vector3.Distance( ik.solver.bone1.transform.position, ik.solver.bone2.transform.position ) + Vector3.Distance( ik.solver.bone2.transform.position, ik.solver.bone3.transform.position ); // ik.solver.bone1.length + ik.solver.bone2.length;

					// Divide IK Target direction magnitude by limb length to know how much the limb is being pulled
					float delta = (toTarget.magnitude / limbLength) - 1f + offset;
					delta = Mathf.Clamp( delta * weight, 0f, 1f );

					// Calculate the rotation offset for the shoulder
					Quaternion rotationOffset = Quaternion.Lerp( Quaternion.identity, fromTo, delta );

					// Rotate the shoulder
					ik.solver.bone1.transform.parent.rotation = rotationOffset * ik.solver.bone1.transform.parent.rotation;
					break;

				case ShoulderRotationModes.ShoulderDistortion:


					// Direction to the IK target
					Vector3 toTarget1 = ik.solver.target.position - ik.solver.bone1.transform.position;


					// Difference between the IK target and the hand position
					Vector3 targetDiff = ik.solver.target.position - ik.solver.bone3.transform.position;



					// Get FromToRotation from the current swing direction of the shoulder to the IK target direction
					//	Quaternion fromTo1 = Quaternion.FromToRotation( ik.solver.bone3.transform.position - ik.solver.bone1.transform.parent.position, ik.solver.target.position - ik.solver.bone1.transform.parent.position );


					// Length of the limb
					float limbLength1 = Vector3.Distance( ik.solver.bone1.transform.position, ik.solver.bone2.transform.position ) + Vector3.Distance( ik.solver.bone2.transform.position, ik.solver.bone3.transform.position ); // ik.solver.bone1.length + ik.solver.bone2.length;

				// Divide IK Target direction magnitude by limb length to know how much the limb is being pulled
				//	float delta1 = (toTarget1.magnitude / limbLength1) - 1;

				// Divide IK Target direction magnitude by limb length to know how much the limb is being pulled
				float delta1 = 1 - (toTarget1.magnitude / limbLength1);
				delta1 = Mathf.Clamp( delta1, 0f, 1f );


				//	if ( delta1 > 0.0f ) {
				// Translate the shoulder
				ik.solver.bone1.transform.position = ik.solver.bone1.transform.position + targetDiff;

				// Translate the shoulder
				ik.solver.bone1.transform.position = ik.solver.bone1.transform.position - targetDiff * delta1;



				break;

				case ShoulderRotationModes.Alkashi:


					// Get FromToRotation from the current swing direction of the shoulder to the IK target direction
					Quaternion fromTo2 = Quaternion.FromToRotation( ik.solver.bone3.transform.position - ik.solver.bone1.transform.parent.position, ik.solver.target.position - ik.solver.bone1.transform.parent.position );
					
				// Rotate the shoulder
					ik.solver.bone1.transform.parent.rotation = fromTo2 * ik.solver.bone1.transform.parent.rotation;

				
					// Direction to the IK target
					Vector3 toTarget2 = ik.solver.target.position - ik.solver.bone1.transform.position;

					// Length of the limb
					float limbLength2 = Vector3.Distance( ik.solver.bone1.transform.position, ik.solver.bone2.transform.position ) + Vector3.Distance( ik.solver.bone2.transform.position, ik.solver.bone3.transform.position ); // ik.solver.bone1.length + ik.solver.bone2.length;

					// Divide IK Target direction magnitude by limb length to know how much the limb is being pulled
					float delta2 = 1 - (toTarget2.magnitude / limbLength2) + offsetCustom; ;
					delta2 = Mathf.Clamp( delta2, 0f, 1f );
				
					//Cancel the distortion of the shoulder
					if ( Mathf.Abs( Quaternion.Angle( initialRotation, ik.solver.bone1.transform.parent.rotation ) ) > 0 ) {
						delta2 = Mathf.Clamp( delta2, 0f, 1f );
						float angleEuler = Quaternion.Angle( ik.solver.bone1.transform.parent.rotation, initialRotation ) * (delta2);

						Quaternion rotationOffset1 = Quaternion.RotateTowards( ik.solver.bone1.transform.parent.rotation, initialRotation, angleEuler );

						ik.solver.bone1.transform.parent.rotation = rotationOffset1;		
					}

					break;

				case ShoulderRotationModes.Tracking:

					shoulderConstraints = gameObject.AddComponent<Configuration_Constraints>();

					if(shoulderType==ShoulderTypes.Left)
					shoulderConstraints.boneType = Configuration_Constraints.BoneType.LeftShoulder;
					else
					shoulderConstraints.boneType = Configuration_Constraints.BoneType.RightShoulder;

					shoulderConstraints.positionWeight = 1;


				break;
				
				case ShoulderRotationModes.Combination:

				Quaternion fromTo3 = Quaternion.identity;

				if (  shoulderType == ShoulderTypes.Left  && GameObject.Find( Avatar.settingsReferences.leftUpperArm.name + "Target" ) != null )
					targetShoulder = GameObject.Find( Avatar.settingsReferences.leftUpperArm.name + "Target" ).transform;

				if ( shoulderType == ShoulderTypes.Right  && GameObject.Find( Avatar.settingsReferences.rightUpperArm.name + "Target" ) != null )
					targetShoulder = GameObject.Find( Avatar.settingsReferences.rightUpperArm.name + "Target" ).transform;


				if ( isTracking  && targetShoulder!=null) {
					// Get FromToRotation from the current swing direction of the shoulder to the IK target direction
						 fromTo3 = Quaternion.FromToRotation( ik.solver.bone1.transform.position - ik.solver.bone1.transform.parent.position, targetShoulder.transform.position - ik.solver.bone1.transform.parent.position );
					
					// Rotate the shoulder
					ik.solver.bone1.transform.parent.rotation = fromTo3 * ik.solver.bone1.transform.parent.rotation;

					return;
				} 

				// Get FromToRotation from the current swing direction of the shoulder to the IK target direction
					fromTo3 = Quaternion.FromToRotation( ik.solver.bone3.transform.position - ik.solver.bone1.transform.parent.position, ik.solver.target.position - ik.solver.bone1.transform.parent.position );


				// Rotate the shoulder
				ik.solver.bone1.transform.parent.rotation = fromTo3 * ik.solver.bone1.transform.parent.rotation;



				// Direction to the IK target
				Vector3 toTarget3 = ik.solver.target.position - ik.solver.bone1.transform.position;

				// Length of the limb
				float limbLength3 = Vector3.Distance( ik.solver.bone1.transform.position, ik.solver.bone2.transform.position ) + Vector3.Distance( ik.solver.bone2.transform.position, ik.solver.bone3.transform.position ); // ik.solver.bone1.length + ik.solver.bone2.length;

				// Divide IK Target direction magnitude by limb length to know how much the limb is being pulled
				float delta3 = 1 - (toTarget3.magnitude / limbLength3) + offsetCustom; ;
				delta2 = Mathf.Clamp( delta3, 0f, 1f );

				//Cancel the distortion of the shoulder
				if ( Mathf.Abs( Quaternion.Angle( initialRotation, ik.solver.bone1.transform.parent.rotation ) ) > 0 ) {
					delta2 = Mathf.Clamp( delta2, 0f, 1f );
					float angleEuler = Quaternion.Angle( ik.solver.bone1.transform.parent.rotation, initialRotation ) * (delta2);

					Quaternion rotationOffset1 = Quaternion.RotateTowards( ik.solver.bone1.transform.parent.rotation, initialRotation, angleEuler );

					ik.solver.bone1.transform.parent.rotation = rotationOffset1;
				}


				break;

		}
		}

		
}
