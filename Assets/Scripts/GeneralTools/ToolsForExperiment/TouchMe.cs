﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchMe : MonoBehaviour {

    [HideInInspector]
    public CrossSelected question; // set at startup by the question

    public Transform rightHandDistorted; // set at startup by the question
    public Transform leftHandDistorted; // set at startup by the question

    [HideInInspector]
    public bool isRightHanded;

    private bool isSelected = false;
    Transform objectTouched;

    private void OnEnable()
    {
        isSelected = true;

        if(isRightHanded)
        {
            objectTouched = rightHandDistorted;
        }
        else
        {
            objectTouched = leftHandDistorted;
        }

        question = gameObject.GetComponent<CrossSelected>();

    }

    void Update()
    {
        if (!isSelected && isTouchMe(objectTouched))
        {
            isSelected = true;
            question.OnAnswerIn(gameObject);
        }
        else if (isSelected && !isTouchMe(objectTouched))
        {
            isSelected = false;
            question.OnAnswerOut(gameObject);
        }
    }

    bool isTouchMe(Transform objectTouched)
    {
        float distanceFromCross = Vector3.Distance(transform.position, objectTouched.position);
        return objectTouched != null && distanceFromCross <= 0.01f;
    }
}
