﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorProjected : MonoBehaviour {

    RaycastHit hit;
    GameObject target;

    Vector3 formerPosition;
    Quaternion formerRotation;

    public GameObject parent;

    // Use this for initialization
    void Start () {

   //faire des doubles murs avec les bons forward 
   // regarder la rotation et la position plus en detail
   // faire un layer
	}
	
	// Update is called once per frame
	void Update () {

        int layerMask = 1 << 11;


   

        if (Physics.Raycast(transform.parent.position, transform.parent.forward, out hit, Mathf.Infinity, layerMask))
        {

            transform.GetComponent<MeshRenderer>().enabled = true;
            // transform.localPosition= new Vector3 (transform.localPosition.x, transform.localPosition.y, hit.transform.position.z);
            transform.position = new Vector3(hit.point.x, hit.point.y, hit.point.z);
            transform.rotation = Quaternion.LookRotation(hit.transform.forward);

            formerPosition = transform.position;
            formerRotation = transform.rotation;
        }
        else
        {
            // gameObject.SetActive(false);
            transform.GetComponent<MeshRenderer>().enabled = false;
            //transform.position =formerPosition;
            //transform.rotation = formerRotation;
        }
	}
}
