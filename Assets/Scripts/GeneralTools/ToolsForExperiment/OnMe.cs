﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnMe : MonoBehaviour {

	[HideInInspector]
	public CrossSelected question; // set at startup by the question
	[HideInInspector]
	public Transform rayCaster; // set at startup by the question

	private bool isSelected = false;



	private void OnEnable()
	{
		isSelected = true;

		if (rayCaster == null)
			rayCaster = Camera.main.transform;

		question = gameObject.GetComponent<CrossSelected>();

	}

	private void Start()
	{

		if (rayCaster == null)
			rayCaster = Camera.main.transform;
	}

	void Update()
	{

		RaycastHit hitinfo;
		Physics.Raycast(rayCaster.position, rayCaster.forward, out hitinfo, 1000f, 11);

		if (!isSelected && isOnMe(hitinfo)) {
			isSelected = true;
			question.OnAnswerIn(gameObject);
		} else if (isSelected && !isOnMe(hitinfo)) {
			isSelected = false;
			question.OnAnswerOut(gameObject);
		}
	}

	bool isOnMe(RaycastHit hitinfo)
	{
		return hitinfo.collider != null && hitinfo.collider.transform.Equals(gameObject.transform);
	}

	//void OnDrawGizmos()
	//{
	//	if (rayCaster == null)
	//		rayCaster = Camera.main.transform;

	//	Gizmos.color = Color.blue;
	//	Gizmos.DrawLine(rayCaster.position, rayCaster.position + 100f * rayCaster.forward);
	//}
}