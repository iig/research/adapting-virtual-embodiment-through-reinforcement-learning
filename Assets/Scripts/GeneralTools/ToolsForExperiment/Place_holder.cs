﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Place_holder : MonoBehaviour {

    public GameObject VRHeadset;
	public bool isPositionDisable = true;
	public bool isRotationDisable = true;
	// Use this for initialization
	void Start () {
		if(isPositionDisable) 
		{
			UnityEngine.XR.InputTracking.disablePositionalTracking = true;
			VRHeadset.transform.position = new Vector3( 0, 0, 0 );

		}
    
		if(isRotationDisable)
		{
			UnityEngine.XR.XRDevice.DisableAutoXRCameraTracking( VRHeadset.GetComponent<Camera>(), true );
			UnityEngine.XR.InputTracking.disablePositionalTracking = false;
			VRHeadset.transform.rotation = Quaternion.Euler( new Vector3( 0, 0, 0 ) );
		}
	}
}
