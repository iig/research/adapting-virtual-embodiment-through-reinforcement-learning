﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public  class CalibrationTable : MonoBehaviour {

    public KeyCode calibrationButton;
    public KeyCode saveButton;
    public KeyCode loadButton;
    public GameObject mainTableLeg;
    public GameObject referenceCylinder;
    public GameObject referenceTable;
    public GameObject targets;
    public GameObject crosses;
    public  GameObject calibrationTableFeedback;
    public GameObject messageInstruction;
	public GameObject table;

    public static bool isLoad; 


    float offset = 0;
	bool isTableCalibrated;


	// Use this for initialization
	void Start () {
		
	}
    private void OnEnable()
    {
        calibrationTableFeedback.GetComponent<MeshRenderer>().enabled = true;
        messageInstruction.SetActive(true);
    }

    // Update is called once per frame
    void Update () {
		


      
		//SAVE AND MAKE APPEAR THE REST
		if (Input.GetKeyDown(saveButton))
        {

        }


        // LOAD TABLE AND MAKE APPEAR THE REST
        if (Input.GetKeyUp(loadButton))
        {
            calibrationTableFeedback.GetComponent<MeshRenderer>().enabled = false;
            messageInstruction.SetActive(false);
			table.SetActive(true);
            isLoad = true;
        }
    }

	private void LateUpdate () {
		if(isLoad && !isTableCalibrated) {

			offset = referenceCylinder.transform.position.y - referenceTable.transform.position.y;

			mainTableLeg.transform.position = new Vector3( mainTableLeg.transform.position.x,
				mainTableLeg.transform.position.y + offset, mainTableLeg.transform.position.z );


			targets.transform.position = new Vector3( targets.transform.position.x,
				targets.transform.position.y + offset, targets.transform.position.z );

			crosses.transform.position = new Vector3( crosses.transform.position.x,
			crosses.transform.position.y + offset, crosses.transform.position.z );

			isTableCalibrated = true;
		}
	}
}
