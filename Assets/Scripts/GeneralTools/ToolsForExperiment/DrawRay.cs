﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawRay : MonoBehaviour {

	public float time = 0;
	private LineRenderer lr;
	public GameObject ray;
	// Set some positions
	Vector3[] positions = new Vector3[3];

	[HideInInspector]
	public Transform rayCaster; // set at startup by the question
								// Use this for initialization
	void Start () {
		//lr = GetComponent<LineRenderer>();

	
	}
	
	// Update is called once per frame
	void Update () {
		if (rayCaster == null)
			rayCaster = Camera.main.transform;

		Debug.DrawLine(rayCaster.position, rayCaster.position + 100f * rayCaster.forward, Color.blue, time);

		ray.transform.localScale = new Vector3(ray.transform.localScale.x, 100f, ray.transform.localScale.z);

		//positions[0] = rayCaster.position;
		//positions[1] = rayCaster.position + 100f * rayCaster.forward;
		//positions[2] = new Vector3(2.0f, -2.0f, 0.0f);
		//lr.positionCount = positions.Length;
		//lr.SetPositions(positions);


	}

	void OnDrawGizmos()
	{
		
	}
}
