﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedColor : MonoBehaviour {

    public float speedThreshold=0;

    float speed = 0;
    bool isBack = false;
    float timer = 0;
    float threeFrames = 0;
    Renderer sphereRenderer;
    Vector3 lastPosition;


	// Use this for initialization
	void Start () {
    
        sphereRenderer = GetComponent<Renderer>();
        speedThreshold = 1000;
    }
	
	// Update is called once per frame
	void Update () {

       // speed = sphereRigidbody.velocity.magnitude;
       

        speed = (transform.parent.transform.position - lastPosition).magnitude / threeFrames;

        timer += Time.deltaTime;
        threeFrames = 3 * Time.deltaTime;

       
        if (timer >= threeFrames)
        {
            lastPosition = transform.parent.transform.position;
        }

        if(speed >= speedThreshold)
        {
           
            //sphereRenderer.material.color = Color.red;
            sphereRenderer.material.SetColor("_Color", Color.red);
            isBack = true;
        }
        else if(speed <= speedThreshold && isBack)
        {
            sphereRenderer.material.SetColor("_Color", Color.black);
            isBack = false;
        }

    }
}
