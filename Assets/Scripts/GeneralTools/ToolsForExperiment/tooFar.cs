﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tooFar : MonoBehaviour
{
	public GameObject reference;
	public string trackerName;
	public float tolerance;
	public KeyCode calibrationButton;

    Transform[] children;
	GameObject tracker;
	float initialChestDistance;
	bool isCalibrated;
	public float threshold=99999f;

	public static bool isSet;

	private void Awake () {

		children = GetComponentsInChildren<Transform>();

		foreach (Transform child in children) {

			if(child.name!=transform.name) {
				child.gameObject.SetActive( false );

			}
			
		}

	}


	void Start()
    {

		tracker=GameObject.Find( trackerName );
    }

    // Update is called once per frame
    void Update()
    {

		if ( Input.GetKeyDown( calibrationButton )  && tracker != null && !isCalibrated )
		{

			initialChestDistance = Mathf.Abs( reference.transform.position.z - tracker.transform.position.z );
			threshold = tolerance + initialChestDistance;
			isCalibrated = true;

		}

		if ( tracker!=null &&( Mathf.Abs( reference.transform.position.z - tracker.transform.position.z )) > threshold) {

			isSet = true;
			foreach ( Transform child in children ) {

				if ( child.name != transform.name )
					child.gameObject.SetActive( true );
			}
		}	
		else {

			isSet = false;
			foreach ( Transform child in children ) {

				if ( child.name != transform.name )
					child.gameObject.SetActive( false );
			}
		}

		
    }
}
