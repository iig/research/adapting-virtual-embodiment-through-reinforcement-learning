﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetCanvasActive : MonoBehaviour
{
	[HideInInspector]
	public bool setMessageReachingTarget;
	[HideInInspector]
	public bool setMessageLookAtHand;
	[HideInInspector]
	public bool setMessageComeback;
	[HideInInspector]
	public bool messageComebackActive;
	[HideInInspector]
	public bool messageReachingTargetActive;
	[HideInInspector]
	public bool setmessageTrialInvalid;

	public GameObject messageReachingTarget;
	public GameObject messageComeback;
	public GameObject messageLookHand;
	public GameObject messageTrialInvalid;
	public GameObject greenScreen;
	public GameObject redScreen;
	public GameObject orangeScreen;

	Transform[] children;

	public float trialInvalidTimeOut;
	private float trialInvalidTime;
	[SerializeField]
	[Range( 0, 5 )]
	public float messageShowingTimeLimit;
	private float messageShowingTime = 0f;
	private float startTime = 0f;

	private bool messageWasPut = false;

	private void Awake () {

		children = GetComponentsInChildren<Transform>();

		foreach ( Transform child in children ) {

			if ( child.name != transform.name ) {
				child.gameObject.SetActive( false );

			}

		}

	}
	// Start is called before the first frame update
	void Start()
    {
        trialInvalidTime = 0f;
	}

	// Update is called once per frame
	void FixedUpdate()
    {
		if (setMessageLookAtHand) {
			messageLookHand.SetActive(true);
			redScreen.SetActive(true);

			StartCoroutine( Timer( messageShowingTimeLimit ) );

			if ( !messageWasPut ) {
				startTime = Time.time;
				messageWasPut = true;
			}

			//messageShowingTime += Time.fixedDeltaTime;

			//if ( messageShowingTime >= messageShowingTimeLimit ) {

			//	messageLookHand.SetActive( false );
			//	redScreen.SetActive( false );
			//	setMessageLookAtHand = false;
			//	messageShowingTime = 0f;
			//}
		} else {

			if ( messageWasPut ) {
				Debug.Log( "message look hand time: " + (Time.time - startTime) );
				startTime = 0f;
				messageWasPut = false;
			}

			if (setmessageTrialInvalid) {
				messageTrialInvalid.SetActive(true);
				orangeScreen.SetActive(true);

				StartCoroutine( Timer2( trialInvalidTimeOut ) );
				//trialInvalidTime += Time.fixedDeltaTime;

				//if (trialInvalidTime >= trialInvalidTimeOut) {
				//	messageTrialInvalid.SetActive(false);
				//	orangeScreen.SetActive(false);
				//	trialInvalidTime = 0f;
				//	setmessageTrialInvalid = false;
				//}
			} else {

				if (messageComebackActive && !messageReachingTargetActive) {


					if (setMessageComeback) {

						messageComeback.SetActive(true);
						greenScreen.SetActive(true);

					} else {
						messageComeback.SetActive(false);
						messageReachingTarget.SetActive(false);
						greenScreen.SetActive(false);
					}
				}

				if (messageReachingTargetActive && !messageComebackActive) {

					if (setMessageReachingTarget) {

						messageReachingTarget.SetActive(true);
						redScreen.SetActive(true);

					} else {
						messageComeback.SetActive(false);
						messageReachingTarget.SetActive(false);
						redScreen.SetActive(false);
					}
				}
			}
		}

	}

	IEnumerator Timer (float time) {
		//	float timeTest =  Time.time;
		yield return new WaitForSecondsRealtime( time );
		//print(- timeTest + Time.time );
		messageLookHand.SetActive( false );
		redScreen.SetActive( false );
		setMessageLookAtHand = false;

	}

	IEnumerator Timer2(float time) {
		yield return new WaitForSecondsRealtime( time );
		messageTrialInvalid.SetActive( false );
		orangeScreen.SetActive( false );
		setmessageTrialInvalid = false;
	}
}
