﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target_Calibration : MonoBehaviour {

    //public GameObject bodyPartDistorted;
    //public float calibPosition;


    public Autodesk_Character_Generator_Settings avatarOriginal;
    public Autodesk_Character_Generator_Settings avatarDistorted;
	public KeyCode calibrateButton;


    [Range(0, 2)]
    public float D0 = 1;
	public float offsetHeightY=0;
	public float offsetHeightX = 0;

	// How much of boday length
	// [Range(0, 2)]
	//public float D1 = 1;

	//float armLength = 0;
	// public float backLength = 0;
	//  bool flag = true;

	[Header("Experiment Variable")]
    public bool isRightHanded = true;
    public bool isCalibrated;
	public bool isCentered;

   // private bool runTwoPhaseExperiment;

    void Start()
    {
        //runTwoPhaseExperiment = DynamicDistortionWithErrPExperiment.ExperimentController_ReinforcementLearning.instance.runTwoPhaseExperiment;
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(calibrateButton) || isCalibrated)
        {

         //   if (runTwoPhaseExperiment)
         //   {
         //       setPositionAccordingToBackLength();
        //    } else
         //   {
                setPositionAccordingToArmLength();

			if ( isCentered )
				centeredAccordingThePelvis();
		 //   }


			isCalibrated = false;
        }


    }

    float getArmLength()
    {
        float armLength = 0;
        if (isRightHanded)
        {
            armLength = Vector3.Distance(avatarDistorted.settingsReferences.rightForearm.transform.position, avatarDistorted.settingsReferences.rightUpperArm.transform.position) +
                Vector3.Distance(avatarDistorted.settingsReferences.rightHand.transform.position, avatarDistorted.settingsReferences.rightForearm.transform.position);
            transform.position = avatarDistorted.settingsReferences.rightUpperArm.transform.position;
        }
        else
        {
            armLength = Vector3.Distance(avatarDistorted.settingsReferences.leftForearm.transform.position, avatarDistorted.settingsReferences.leftUpperArm.transform.position) +
             Vector3.Distance(avatarDistorted.settingsReferences.leftHand.transform.position, avatarDistorted.settingsReferences.leftForearm.transform.position);
            transform.position = avatarDistorted.settingsReferences.leftUpperArm.transform.position;
        }

        return armLength;
    }

    void setPositionAccordingToArmLength()
    {
        float armLength = getArmLength();
		transform.position = transform.position + new Vector3( offsetHeightX, offsetHeightY, -armLength * D0 );// + new Vector3(0.09f, 0, 0);
    }

	void centeredAccordingThePelvis() {

		transform.position= new Vector3(avatarDistorted.settingsReferences.pelvis.position.x, transform.position.y, transform.position.z) ;
	}

  /*  void setPositionAccordingToBackLength()
    {
        Vector3 hipsPosition = avatarDistorted.settingsReferences.pelvis.transform.position;
        backLength = Vector3.Distance(avatarDistorted.settingsReferences.neck.transform.position, hipsPosition);

        float y = avatarDistorted.settingsReferences.neck.transform.position.y - D1 * backLength;
        float z = avatarDistorted.settingsReferences.rightUpperArm.transform.position.z - getArmLength() * D0;
        float x = hipsPosition.x;

        transform.position = new Vector3(x, y, z);
    }*/
}
