﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrossSelected : MonoBehaviour {

    public Transform rayCaster;
    [Range(0.1f, 2.0f)]
    public float animationSpeed = 0.1f;
 

    public bool isAnswered { get; private set; }


    public float frameScale = 0.24f;
    //public float frameDownFactor = -0.31f;

 
    public GameObject progressBar;
    public KeyCode validateCross;
	public bool isKeepValidateColor;
    private Transform currentProgressBar;

    void Start()
    {
        OnEnable();
    }

    void OnEnable()
    {
        if (rayCaster == null)
            rayCaster = Camera.main.transform;

          progressBar.transform.localScale = Vector3.zero;
          isAnswered = false;
    }

    void OnDisable()
    {
        isAnswered = false;
    }

    void Update()
    {
        Debug.DrawLine(rayCaster.position, rayCaster.position + 100f * rayCaster.forward, Color.yellow);
		

		if (!isAnswered && currentProgressBar != null)
        {
            // currentAnswer.transform.Find(frameName).GetComponent<Renderer>().sharedMaterial = highlightMaterial;
          


            if (currentProgressBar.localScale.x >= frameScale)
            {
                    isAnswered = true;

				if(!isKeepValidateColor)
				currentProgressBar.localScale = Vector3.zero;
		


			}
			else
			{
				currentProgressBar.localScale += Time.deltaTime * animationSpeed * Vector3.one;
			}
        }
        else if (Input.GetKeyDown(validateCross))
        {
            isAnswered = true;
        }

    }


    public void OnAnswerIn(GameObject answer)
    {
        if (!isAnswered)
        {
            currentProgressBar =progressBar.transform;
        }
    }

    /// <summary>
    /// Called by the answer when the cursor goes out of it.
    /// </summary>
    public void OnAnswerOut(GameObject answer)
    {
        if (!isAnswered)
        {
            

            if (currentProgressBar != null)
                currentProgressBar.localScale = Vector3.zero;

            currentProgressBar = null;
        }
    }




    
}
