﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Timers;


public class TargetReaching : MonoBehaviour {

    public GameObject objectHeld;
    public GameObject previousTarget;
    public float tolerance=0.005f;
    public KeyCode validateTarget;
	public bool isTimer;
	public bool isVisualTimer;
	[Range(0,10)]
	public float time;

    [HideInInspector]
    public bool flagEnter=false;
	[HideInInspector]
	public bool targetInside;
	public GameObject currentProgressBar;


	static float timerBar = 0;
    float distance=Mathf.Infinity;
	bool validate;
	Coroutine timerCor;
	bool timerIsRunning;
	float timeTest = 0;

	private static System.Timers.Timer aTimer;

	// Use this for initialization
	void Start () {
		// Create a timer with a two second interval.
		aTimer = new System.Timers.Timer();
		aTimer.Interval = 1;
		// Hook up the Elapsed event for the timer. 
		aTimer.Elapsed += OnTimedEvent;
		aTimer.AutoReset = true;
		aTimer.Stop();
	}


    private void OnEnable()
    {
        flagEnter =false;
		validate = false;
		targetInside = false;
		if(isVisualTimer)
		currentProgressBar.transform.GetComponent<Image>().fillAmount = 0;

	}
    // Update is called once per frame
    void FixedUpdate () {

		if ( objectHeld != null ) {
			distance = Vector3.Distance( objectHeld.transform.position, transform.position );
		} else
			Debug.LogError("Need Object Held for target reaching script");


		if((Input.GetKeyDown( KeyCode.O ) || Input.GetKeyDown( validateTarget )) && gameObject.activeSelf && (previousTarget == null || !previousTarget.activeSelf))
		{
			validate = true;
		}



			if ((distance <= tolerance || Input.GetKeyDown(KeyCode.O) || Input.GetKeyDown(validateTarget)) && gameObject.activeSelf && (previousTarget == null || !previousTarget.activeSelf)&& !tooFar.isSet)
            {
			    if(validate || !isTimer &&!isVisualTimer){
				gameObject.SetActive( false );
				flagEnter = true;
				
			} else if  ( isTimer &&  !timerIsRunning ) {


				timerCor=StartCoroutine( Timer());
				
			}
			else if(isVisualTimer) {
				
				timerCircle();
			}

			targetInside = true;

                 // Debug.Log("ENTER "+ distance );
            }else {

				if ( isTimer && !validate  && timerIsRunning ) {

				StopCoroutine( timerCor );
				timerIsRunning = false;
				}

				if(isVisualTimer)
				aTimer.Stop();

			targetInside = false;
		}
        
	}

	IEnumerator Timer() {

		timerIsRunning = true;
	//	float timeTest =  Time.time;
		yield return new WaitForSecondsRealtime( time ) ;
		//print(- timeTest + Time.time );
		gameObject.SetActive( false );
		flagEnter = true;
		timerIsRunning = false;


	}

	void timerCircle()
	{

		if(timerBar==0 && flagEnter == false ) 
		{
			SetTimer();
			//timeTest = Time.time;
			timerBar = +0.0001f;
			//Debug.Log("1");
		}
			



		if ( timerBar < time + 0.1f && flagEnter==false ) {
			//timerBar += Time.fixedDeltaTime;
			aTimer.Start();
			//float angle = (Mathf.Atan2( -timerBar/time, timerBar/time ) * 180f / Mathf.PI + 180f) / 360f;
			float angle = (timerBar / time); //* 180f / Mathf.PI + 180f) / 360f;
			currentProgressBar.transform.GetComponent<Image>().fillAmount = angle;

			

			//currentProgressBar.transform.GetComponent<Image>().color = Color.Lerp( Color.green, Color.red, angle );
		} else if ( flagEnter ==false && timerBar >time) {
			flagEnter = true;
			timerBar = 0;
			currentProgressBar.transform.GetComponent<Image>().fillAmount = 0;
			aTimer.Stop();
			
			//print(-timeTest + Time.time );
		}
	}
	
	
	private static void SetTimer () {

		aTimer.Start();
		aTimer.Enabled = true;
		
	}

	private static void OnTimedEvent(object sender, ElapsedEventArgs elapsedEventArg)
	{
		timerBar =timerBar+0.001f;
		//Debug.Log( "6= " + timerBar );
	}
}
