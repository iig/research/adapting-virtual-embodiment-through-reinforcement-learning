﻿using System.Collections;
using System.IO;
using System;
using System.Collections.Generic;
using UnityEngine;
using ReadWriteCsv;

public class Target_Trajectory_NonBiological : MonoBehaviour {

    // input source
    public enum EllipseSource { LIVE = 0, FILE = 1 };
	public EllipseSource ellipseSource;
	// update function to use
	public enum updateCallback { UPDATE, LATE_UPDATE, FIXED_UPDATE };
	public updateCallback callback = updateCallback.UPDATE;
	public string filePathToLoad;
    public float semi_major_axis;
    public float semi_minor_axis;
    public Vector3 Center;
    public float speedLive;

    [Range(0, 10)]
    public int speedFile;

    // we initialise at the value we want the ball begin
    public float parameter_t;

    public float intialLiveDeparture = 0;

    [Range(1, 7)]
    public int intialFileDeparture = 0;
    //public float calibPosition= Mathf.PI / 2;
    public bool positif=false;
    public bool negatif=false;
   // public bool reinitialiseAtEachStop;

    float timeSpent;
    float positionEllipseY;
    float positionEllipseX;
    bool flagStart=false;
    bool isLoaded;
    float sign=1;
    float increment = 0.01f;
    float interval=0.0f;
    List<float> coeffData=new List<float>();
    public int i = 0;
    public int iInitial = 0;
    public int iFormer;
    public float intialDeparture;
    float time = 0.02f;
    float speedTest=0;
    float speedTestMean = 0;
    List<float> speedTests =new List<float>();
    Vector3 ellipsePositions=Vector3.zero;
    public int oneCycle = 0;

    [Header("Experiment Variable")]
    public bool isStartedMoving;
    public bool isMoving;
    public bool limited_distance = false;
    public float distance = 2 * Mathf.PI;
    public int nberCycles = 2;
    public float pause;
    public bool isStopped;
    public bool isInitialDepartureCoherent;
    public int speedMultiplier = 10;
    public bool isRightHanded;

    // Use this for initialization
    void Start ()
    {
        timeSpent = 0.0f;

        if (!isRightHanded)
        {
            Center.y = -Center.y;

            if (intialFileDeparture - 3>0)
            {
        
                intialFileDeparture = intialFileDeparture - 4;
           

            }
            else
                intialFileDeparture = intialFileDeparture + 4;

        }
      

        if (ellipseSource == EllipseSource.FILE && !isLoaded)
            LoadCoeff(filePathToLoad);

        if (ellipseSource == EllipseSource.LIVE)
            intialDeparture = intialLiveDeparture;
        else
        {
            int position = coeffData.Count/ (12*8);
            i = position * intialFileDeparture;
            iInitial = i;
            intialDeparture = coeffData[i];
        }

        parameter_t = intialDeparture;

        positionEllipseX = Center.y + semi_minor_axis * Mathf.Sin(parameter_t );
        positionEllipseY = Center.x + semi_major_axis * Mathf.Cos(parameter_t );

        transform.localPosition = new Vector3(positionEllipseX, transform.localPosition.y, positionEllipseY);



        distance = (2 * Mathf.PI) * nberCycles;

        if (isInitialDepartureCoherent)
        {
            distance = intialDeparture + distance;
        }

     
        oneCycle=coeffData.Count / 12;

       
    }


	void FixedUpdate () {
		if ( callback == updateCallback.FIXED_UPDATE )
			EllipseMovement(  );
	}
	void LateUpdate () {
		if ( callback == updateCallback.LATE_UPDATE )
			EllipseMovement(  );
	}
	void Update () {
		if ( callback == updateCallback.UPDATE )
			EllipseMovement(  );
	}

	void EllipseMovement ()
    {
       

        if (ellipseSource == EllipseSource.FILE && !isLoaded)
            LoadCoeff(filePathToLoad);

        if (Input.GetKeyDown(KeyCode.N) || Input.GetKeyDown(KeyCode.Joystick1Button2) || isStartedMoving)
        {
            flagStart = !flagStart;

            distance = (2* Mathf.PI) * nberCycles;

            if (isInitialDepartureCoherent)
            {
                distance = intialDeparture + distance;
            }


            if (ellipseSource == EllipseSource.LIVE)
                intialDeparture = intialLiveDeparture;
            else
            {
                int position = coeffData.Count / (12 * 8);
                i = position * intialFileDeparture;
                iInitial = i;
                intialDeparture = coeffData[i];
            }
     

            parameter_t = intialDeparture;


            /*if (reinitialiseAtEachStop)
            {
                positionEllipseX = Center.y + semi_minor_axis * Mathf.Sin(intialDeparture);
                positionEllipseY = Center.x + semi_major_axis * Mathf.Cos(intialDeparture);
                parameter_t = intialDeparture;
            }
            else if (parameter_t < distance)
            {*/
                positionEllipseX = Center.y + semi_minor_axis * Mathf.Sin(intialDeparture);
                positionEllipseY = Center.x + semi_major_axis * Mathf.Cos(intialDeparture);
            //parameter_t = intialDeparture;
            // }
            transform.localPosition = new Vector3(positionEllipseX, transform.localPosition.y, positionEllipseY);

            isStartedMoving = false;
            isMoving = flagStart;


        

        }

        if (flagStart)
        {

            iFormer = i;
            //  if (timeSpent > Time.deltaTime)
            if (timeSpent >= time)
            {
                //  float timeTest = timeSpent;
            

                if (limited_distance == false)
                {
                    if (positif)
                        if (ellipseSource == EllipseSource.LIVE)
                            parameter_t += speedLive;
                        else if (i < coeffData.Count && i >= 0)
                        {
                            parameter_t = coeffData[i];
                            i += speedFile*speedMultiplier;

                        }
                        else
                            i = 0;

                    if (negatif)
                        if (ellipseSource == EllipseSource.LIVE)
                            parameter_t -= speedLive;
                        else if (i < coeffData.Count && i >= 0)
                        {
                            parameter_t = coeffData[coeffData.Count - i - 1];
                            i += speedFile* speedMultiplier;
                        }
                        else
                            i = 0;

                    timeSpent = 0.0f;
                }
                else if (parameter_t < distance)
                {
                    if (positif)
                        if (ellipseSource == EllipseSource.LIVE)
                            parameter_t += speedLive;
                        else if (i < coeffData.Count && i >= 0)
                        {
                            parameter_t = coeffData[i];
                            i += speedFile* speedMultiplier;
                        }
                        else
                            i = 0;

                    if (negatif)
                        if (ellipseSource == EllipseSource.LIVE)
                            parameter_t -= speedLive;
                        else if (i < coeffData.Count && i >= 0)
                        {
                            parameter_t = coeffData[coeffData.Count - i - 1];
                            i += speedFile* speedMultiplier;
                        }
                        else
                            i = 0;

                    timeSpent = 0.0f;
                }
                else
                {
                    if (timeSpent < pause)
                    {

                        timeSpent += Time.deltaTime;
                        return;
                    }
                    else
                    {
                        flagStart = false;
                        isMoving = false;

                        //   foreach(float speed in speedTests)
                        //{
                        //    speedTestMean += speed / speedTests.Count;
                        // }

                        //Debug.Log(speedTestMean);
                        parameter_t = intialDeparture;
                        i = 0;
                        iFormer = 0;
                    }

                }


                positionEllipseX = Center.y + semi_minor_axis*Mathf.Sin(parameter_t);
                positionEllipseY = Center.x + semi_major_axis * Mathf.Cos(parameter_t);

                // Vector3 test = transform.localPosition;
                ellipsePositions.x = positionEllipseX;
                ellipsePositions.y = transform.localPosition.y;
                ellipsePositions.z = positionEllipseY;
                //transform.localPosition = new Vector3(positionEllipseX, transform.localPosition.y, positionEllipseY);

                transform.localPosition = ellipsePositions;

                // speedTest = (transform.localPosition - test).magnitude / timeTest;

                // speedTests.Add(speedTest);

               

            }

            timeSpent += Time.deltaTime;
        }
        

        

    }

    // parse CSV file 
    public  bool LoadCoeff(string fileName)
    {

        // open CSV file
        Debug.Log("EllipseCSV: opening file " + fileName);

        CsvFileReader reader = new CsvFileReader(fileName);

        string fileContent = reader.ReadToEnd();
        reader.Close();

        float startTime = 0, endTime = 0;

        using (StringReader strReader = new StringReader(fileContent))
        {
            string line;
            int countFrame = 0;

            int offset = 0;
            while ((line = strReader.ReadLine()) != null)
            {
                // list of positions for this frame
                List<Vector3> mocapLine = new List<Vector3>();

                string[] mocapLineArray = line.Split(new string[] { "," }, StringSplitOptions.None);
                //if (((mocapLineArray.Length-offset)%4)!=0)
                //	return false;

     
                for (int i = 0; i < mocapLineArray.Length; i++)
                {

                    float coeffEntry = float.Parse(mocapLineArray[i + offset]);

                    //new coeff entry
                    coeffData.Add(coeffEntry);
       
                }

                if (countFrame == 0)
                    startTime = float.Parse(mocapLineArray[0]);
                countFrame++;
                endTime = float.Parse(mocapLineArray[0]);

            }
        }

        // set reproduction parameters
        /* totalFrames = mocapData.Count;
         stopFrame = totalFrames;
         frameRate = totalFrames / (endTime - startTime);
         // sampling delta time (in seconds)
         samplingDTime = 1.0f / frameRate;*/

        isLoaded = true;
        Debug.Log("EllipseCSV: " + fileName + " loaded!");
        return true;
    }


}
