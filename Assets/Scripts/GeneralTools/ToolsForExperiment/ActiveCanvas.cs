﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActiveCanvas : MonoBehaviour
{

	public KeyCode buttonToSetScreenWithTimer;
	public KeyCode buttonToSetScreenWithNoTimer;

	public bool setCanvasWithTimer;
	public bool setCanvasWithNoTimer;
	//Transform[] children;
	Image screen;
	public float timeToWait=1;

	//bool isAutomatic;



	private void Awake () {

		screen = GetComponentInChildren<Image>();

	}

	// Start is called before the first frame update
	void Start()
    {
		//StartCoroutine(timerSetScreen(timeToWait));
		//children = GetComponentsInChildren<Transform>();

		/*foreach ( Transform child in children ) {

			if ( child.name != transform.name ) {
				child.gameObject.SetActive( false );

			}

		}*/
	}

    // Update is called once per frame
    void Update()
    {
		/*if( isAutomatic ) {

			if ( Camera.current != Camera.main )
				screen.enabled = true;
			else
				screen.enabled = false;
		}*/
	


		if ((Input.GetKeyDown( buttonToSetScreenWithTimer) && Camera.main.targetDisplay != 1) ) {

			screen.enabled = true;
			StartCoroutine( timerSetScreen( timeToWait ) );
			//setCanvasWithTimer = !setCanvasWithTimer;
		}

		if (( Input.GetKeyDown( buttonToSetScreenWithNoTimer) && Camera.main.targetDisplay != 1 ) || setCanvasWithNoTimer ) {

			screen.enabled = !screen.enabled;
			setCanvasWithNoTimer = !setCanvasWithNoTimer;
		}

		if ( Camera.main.targetDisplay == 1 && setCanvasWithTimer ) {
			screen.enabled = true;
			StartCoroutine( timerSetScreen( timeToWait ) );
			setCanvasWithTimer = false;
		}
		else if ( Camera.main.targetDisplay != 1 )
			setCanvasWithTimer = true;

	}

	IEnumerator timerSetScreen(float time) {

		yield return new WaitForSecondsRealtime(time);
		screen.enabled = false;

	}
}
