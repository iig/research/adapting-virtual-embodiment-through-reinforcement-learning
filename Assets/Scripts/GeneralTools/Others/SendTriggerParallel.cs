﻿
using System;
using System.Reflection;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Collections;
// unity
using UnityEngine;

public class SendTriggerParallel: MonoBehaviour {

	//[DllImport( @"C:\windows\system32\inpoutx64.dll" )]C:\Projects\tporssut\ExtensionMovementDistortion\Assets\Plugins\Others\
	[DllImport( @".\Assets\Plugins\Others\inpoutx64.dll" )]
	public static extern void DlPortWritePortUshort ( int adress, int value );

	//Initialization of public variables
	//LPT address
	int address = 0xCFF8; //53240 in decimal

	//Value to be sent
	int value = 1;
	//Width of pulse in seconds
	public static float PulseWidthS = 0.00001f;
		
	// ### internal settings, do not remove
//	public static MonoBehaviour add_component_to_gameobject ( GameObject host ) {
	//		return auto_add( host, MethodBase.GetCurrentMethod().DeclaringType );
	//	}
		
	// ### Ex component flow functions, uncomment to use
		// # main functions
	public bool initialize () { return true; }
	public void start_experiment () {
		
		//These values are taken from the initialization, they are constant during the whole execution.
		//address = init_config().get<int>( "address" );
		
		//PulseWidthS = init_config().get<float>( "PulseWidthS" );
			
	}
	public void Trigger ( int triggerValue ) {

		//value change depending on the condition
		//value = current_config().get<int>( "value" );

		// DlPortWritePortUshort(address, value);
		//ExVR.Coroutines().start( SendPulse() );

		//StartCoroutine(SendPulse());
		//ExVR.Log().message( "Parallel trigger sent " + value );

		StartCoroutine( SendTrigger(triggerValue) );

	}

	// public void update() {}
	public void stop_routine () {
		
		// DlPortWritePortUshort(address, 0);
	}

	public void SetAdress(int addressSet) {

		address = addressSet;
	}

	IEnumerator SendPulse () {
			DlPortWritePortUshort( address, value );



			yield return new WaitForSeconds( PulseWidthS );
		
		DlPortWritePortUshort( address, 0 );
	}

	IEnumerator SendTrigger (int triggerValue) {
		DlPortWritePortUshort( address, triggerValue );
		//Debug.Log("send" +triggerValue );
		yield return new WaitForSeconds( PulseWidthS );

		DlPortWritePortUshort( address, 0 );

		yield return 0;
	}



}

