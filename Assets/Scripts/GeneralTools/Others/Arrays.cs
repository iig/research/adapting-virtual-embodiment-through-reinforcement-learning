﻿using System;
using System.Collections;

namespace Utils
{
    class Arrays
    {
        public static T[] Shuffle<T>(T[] OriginalArray)
        {
            var matrix = new SortedList();
            var r = new Random();

            for (var x = 0; x <= OriginalArray.GetUpperBound(0); x++)
            {
                var i = r.Next();
                while (matrix.ContainsKey(i)) { i = r.Next(); }
                matrix.Add(i, OriginalArray[x]);
            }

            var OutputArray = new T[OriginalArray.Length];
            matrix.Values.CopyTo(OutputArray, 0);
            return OutputArray;
        }

        public static string PrettyPrint<T>(T[] array)
        {
            string toString = "";
            for (int i = 0; i < array.Length; i++)
                toString += array[i].ToString() + " ";
            toString.Trim();
            return toString;
        }
    }
}
