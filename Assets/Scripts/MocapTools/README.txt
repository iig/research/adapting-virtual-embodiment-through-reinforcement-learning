Henrique Galvan Debarba - 05/03/14

MocapInput controls the source of mocap data (VRPN streaming, .c3d file or .mkr.csv file)
and whether to debug this data

It contains

scripts:

-MocapPlayer.cs: controls the reproduction of preloaded mocap data

-C3DPlayer.cs: extends MocapPlayer.cs loading C3D files to memory

-CSVPlayer.cs: extends MocapPlayer.cs loading mkr.csv files to memory

-FilePlayerGUI.cs: GUI to control the execution of a MocapPlayer object

-MarkersDebug.cs: draws current frame of markers position and visibility

-MarkerText.cs: draws the marker name

-VRPN_Client.cs: bridge the VRPN_client_DLL to get stream from VRPN_Server_EXE

-Phasespace folder: controls the direct input of phasespace data (provided by phasespace)

-MocapInputController.cs: controls the data input 
VRPN_Client_DLL: supports the stream of VRPN data from the VRPN_Server_EXE
and .c3d files (must set the .C3D file path and name in the Connection string field)
File: supports reading from files .mkr.csv and .C3D (limited support)
Phasespace: direct acquisition from the phasespace computer
This script also calls the IK update in the AvatarController

-PosePlayer.cs: Reads and reproduces a .pose.csv file in an avatar.

