﻿/**************************************************************************
 * PlayerLog_Tracker.cs controls the motion capture source and reproduction
 * mocap recording path and name, it loads .tkr.csv 
 * which is a simple ASCII that can be created using LogTracker.cs 
 * Written by Thibault Porssut
 * Last update: 06/07/18
 * *************************************************************************/


using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Modularity;

namespace Modularity
{
    // the classes bellow are only used to make the inspector for MocapInputController
    // more bearable with so many setting options

    [System.Serializable]
    public class FileInitSettings
    {
        public bool initAtStartup = false;
        public string folderPath;
        public string fileToLoad = ".\\adjusted-id-schubert-day-1-exp-1-trial-1OK.mkr16Hz.csv";
        public CSVPlayer_Tracker mocapPlayer { get; protected set; }
        // !!!??? TODO check what happens with this initialization code when I change initAtStartup value
        /*FileInitSettings(){
			if (initAtStartup)
				Init ();
		}*/
        public void Init( ref Dictionary<int, string> m_tkrIDtoNames )
        {
            // open csv file
            if (fileToLoad.EndsWith(".csv"))
                mocapPlayer = new CSVPlayer_Tracker();
            else
                Debug.LogError("Wrong Format , the file cannot be read");

            mocapPlayer.LoadMocap(folderPath+fileToLoad, ref m_tkrIDtoNames );
        }
        public bool IsReady()
        {
            if (mocapPlayer == null)
                return false;
            return true;
        }

        public void DestroyMocapPlayer()
        {
            mocapPlayer = null;
        }
    };

    [System.Serializable]
    public class DelayMocap
    {
        // delay mocap? keep marker+deltatime buffers and time controllers
        public bool delayMocap = false;
        // buffer for delayed mocap
        public Queue<double[,]> mocapBufferPos = new Queue<double[,]>();
        public Queue<double[,]> mocapBufferRot = new Queue<double[,]>();
        public Queue<float> deltaTimeBuffer = new Queue<float>();
        public float delayTime = 1.5f;
        private float keptInterval = 0;


        public void ResetKeptInterval()
        {
            keptInterval = 0;
        }

        public void UpdateDelay(float deltaT, ref double[,] positionsTracker , ref double[,] rotationsTracker)
        {
            // manage delay buffer, it is updated always so it is not 
            // necessary to wait for it to be filled after enabling
            mocapBufferPos.Enqueue(positionsTracker);
            mocapBufferRot.Enqueue(rotationsTracker);
            deltaTimeBuffer.Enqueue(deltaT);
            keptInterval += deltaT;
            if (delayMocap)
            {
                if (keptInterval > delayTime)
                {
                    positionsTracker = mocapBufferPos.Dequeue();
                    rotationsTracker = mocapBufferRot.Dequeue();
                    keptInterval -= deltaTimeBuffer.Dequeue();
                }
            }
            else
            {
                if (keptInterval > delayTime)
                {
                    positionsTracker = mocapBufferPos.Dequeue();
                    rotationsTracker = mocapBufferRot.Dequeue();
                    keptInterval -= deltaTimeBuffer.Dequeue();

                   
                }
            }

        }

    };
    // mocap recording path and name, it loads  .tkr.csv
    // .tkr.csv is a simple ASCII that can be created using LogTracker.cs
    public class PlayerLog_Tracker : MonoBehaviour
    {
        // update function to use
        public enum updateCallback { UPDATE, LATE_UPDATE, FIXED_UPDATE };
        public updateCallback callback = updateCallback.UPDATE;

        // input source
        public enum MocapSource { LIVE = 0, FILE = 1, NONE=2};
        public MocapSource m_mocapSource;
        public bool isCustomTrackers;
        public string[] trackersCustoms;


        //Recording parameters
        [Header("Recording Parameters")]
        [SerializeField]
        public string logFolderPath="";
        public string fileName = "";
        //public string logFilePath = "";
        public bool isRecording = false;
        float rate;
        public float recordRate = 1.0f; //the number of sample per second
        public double[,] traker_positions;
        GameObject[] trackers;//TO be filled 
        List<Tuple<int, string>> TrackerToNumber = new List<Tuple<int, string>>();
        float numberOfCoordinatesVector = 3;
        float numberOfCoordinatesQuaternion = 4;

        //Playing parameters
        [Header("Playing Parameters")]
       // public string trackerIDToNameMapping = ".Assets\\Scripts\\MocapPlayer\\Files\\marker_minimal.txt";
        // debug markers - make unity transform markers and draw them as spheres
        public TrackersDebug tkrDebug;
        public bool drawTrackersTransform
        {
            get { return drawTrackers; }
            set { drawTrackers = value; tkrDebug.SetVisible(drawTrackers); }
        }

        [SerializeField]
        public bool instantiateDebugTrackers = true;
        private bool drawTrackers=true;
        public bool updateTrackers = true;

        //Smoothing parameters
        public bool isSmooth;
        public float timer=10;
        public float playerRate =0;
        float rate2;
        public float errorTolerance=1f;
       [Range(0.00001f, 1f)]
        public float smoothingFactor = 0.05f;
        [HideInInspector]
        public bool play;
        public bool isTriggersSent;

		//ARRAY TO STORE
		Vector3 currentTkrPos;
		Quaternion currentTkrRot;
		int totalTrackers = 0;


        Dictionary<int, string> m_tkrIDtoNames = new  Dictionary<int, string>();
        int[] trackerIDs;

        bool flag=false;
        bool flag1 = false;
   

        // current frame
        //public double[,] frame;// {get; protected set;}//= null;
        public double[,] positionsTrackers;
        public double[,] rotationsTrackers;
		public bool[] visibFrame; //{get; protected set;}// = null;
        Vector3 newPos;
        Quaternion newRot;
        float differencePos;
        int trigger1 = 0;
        //int trigger2 = 0;
        //int trigger3 = 0;
   



        // delay mocap manager
        public DelayMocap delayOptions ;

        // options to load mocap from file
        //public string fileToLoad;
        public FileInitSettings fileOptions;

        LogTrackers tkrsLog = new LogTrackers();

       
        // Initialization
        void Start()
        {

            tkrsLog.SetIsTriggerSent(isTriggersSent);

            //Create the marker from the file to replay them
            if (fileOptions.initAtStartup)
            {
                

                // setup marker transforms
               // LoadTrackerNames(trackerIDToNameMapping, ref m_tkrNames);

                // init input devices
                fileOptions.Init( ref  m_tkrIDtoNames );

                if(instantiateDebugTrackers)
                {
                    // init Markers 
                    tkrDebug = new TrackersDebug();

                    tkrDebug.Init();
                    foreach (KeyValuePair<int, string> pair in m_tkrIDtoNames)
                        tkrDebug.AddTracker(pair.Value);
                    
					tkrDebug.SetVisible(drawTrackersTransform);

                }

				positionsTrackers = new double[m_tkrIDtoNames.Count, (int) numberOfCoordinatesVector];
				rotationsTrackers = new double[m_tkrIDtoNames.Count, (int) numberOfCoordinatesQuaternion];
				visibFrame = new bool[m_tkrIDtoNames.Count];// = null;
			}

	

			trackers = new GameObject[trackersCustoms.Length];
			//marker_positions = new double[no_of_markers, 3]; 



			/* if (phasespaceOptions.initAtStartup)
                 phasespaceOptions.Init();*/

		}

        // load marker ID <-> name relations
        //TODO lire CSV pour recuperer les nom des trackers
        void LoadTrackerNames(string inFilePath, ref List<Tuple<int, string>> tkrnames)
        {
            //mkrnames = new List<Tuple<int, string>>();
            System.IO.StreamReader reader = new System.IO.StreamReader(inFilePath);
            string line;

            while ((line = reader.ReadLine()) != null)
            {
                string[] id_value = line.Split(';');
                if (id_value[0] != null)
                {
                    string[] tkrNameID = id_value[0].Split(' ');
                    if (tkrNameID.Length >= 2)
                    {
                        tkrNameID[1] = tkrNameID[1].Replace("\t", "");
                        Tuple<int, string> tkrNameIDTuple = new Tuple<int, string>(System.Convert.ToInt32(tkrNameID[0]), tkrNameID[1]);
                        tkrnames.Add(tkrNameIDTuple);
                        m_tkrIDtoNames.Add(System.Convert.ToInt32(tkrNameID[0]), tkrNameID[1]);
                    }
                }

            }
            // marker IDs contains part of the mkrnams info (marker numbering in the correct order)
            trackerIDs = new int[tkrnames.Count];
            for (int i = 0; i < trackerIDs.Length; i++)
            {
                trackerIDs[i] = tkrnames[i].First;
            }

            reader.Close();
        }

        void ResetTrackerNames(ref Dictionary<int, string> m_tkrIDtoNames )
        {

            m_tkrIDtoNames.Clear();
           
        }


        public void FrameUpdate(float deltaT)
        {
            // current frame
            //double[,] frame = new double[m_mkrNames.Count, 3];// {get; protected set;}//= null;
            //positionsTrackers = new double[m_tkrNames.Count, (int)numberOfCoordinatesVector];
           // rotationsTrackers = new double[m_tkrNames.Count, (int)numberOfCoordinatesQuaternion];
           // visibFrame = new bool[m_tkrNames.Count];// = null;
            bool validFrame = true;
          //  int totalTrackers = m_tkrNames.Count;

		



			// MOCAP from file //TO DO adapt the player and uncomment change the function to accept two table frame
			if (m_mocapSource == MocapSource.FILE)
            {
                
               if (!fileOptions.IsReady())
                {
                   

                    delayOptions.mocapBufferPos.Clear();
                    delayOptions.mocapBufferRot.Clear();
                    delayOptions.deltaTimeBuffer.Clear();
                    delayOptions.ResetKeptInterval();

                    //Clear data
                    ResetTrackerNames( ref m_tkrIDtoNames );

                    // setup marker transforms
                   // LoadTrackerNames(trackerIDToNameMapping, ref m_tkrNames);

                    // init input devices
                    fileOptions.Init( ref m_tkrIDtoNames );

                    
                    //reset smoothing
                    //isSmooth = true;

                    if (instantiateDebugTrackers)
                    {
                        // init Markers 
                        tkrDebug = new TrackersDebug();

                        tkrDebug.Init();
                        foreach (KeyValuePair<int, string> pair in m_tkrIDtoNames)
                            tkrDebug.AddTracker(pair.Value);
                        tkrDebug.SetVisible(drawTrackersTransform);

                    }

                }

               if(play)
                {
                    CSVPlayer_Tracker mp = fileOptions.mocapPlayer;

                    // frame update
                    mp.repSpeed = playerRate;
                    validFrame = mp.CycleFrames(deltaT, ref rotationsTrackers, ref positionsTrackers, ref visibFrame);
                    totalTrackers = mp.totalPoints;

                    // Update the maker drawn from the file loaded
                    if (validFrame)
                    {
                        delayOptions.UpdateDelay(deltaT, ref positionsTrackers, ref rotationsTrackers);

                        // convert frame pos to Vector3 format, used by MarkersDebug to set markers representation position
                        if (updateTrackers)
                        {
                            //TO DO : Smoothing FUNCTION

                            // rate2 += Time.deltaTime;

                            //  if (rate2 >= playerRate)
                            // {
                            //  rate2 = 0;

                            for (int i = 0; i < totalTrackers; i++)
                            {
								//  int index = m_tkrNames[i].First;
								int index = i;



								///TO DO : read the file according a fixed rate (the same as the recording preferabiliy)







								//if marker is not visible its position is not updated
								if (!visibFrame[index])
                                {
                                    if (instantiateDebugTrackers)
                                    {
                                        tkrDebug.TrackerNotVisible(m_tkrIDtoNames[index]);
                                    }
                                    else
                                    {
                                        //Todo: change color of the tracker to red
                                    }

                                }
                                else
                                {

                                    // - on z parameter is HARD CODED !!!!!! so mocap and markers match
                                    // a 180 on the avatar was also needed to match ...
                                    // the reason has something to do with right/left handed coord system (mocap is right h, unity left h)
                                    currentTkrPos = new Vector3((float)positionsTrackers[index, 0], (float)positionsTrackers[index, 1], (float)positionsTrackers[index, 2]);
                                    currentTkrRot = new Quaternion((float)rotationsTrackers[index, 0], (float)rotationsTrackers[index, 1], (float)rotationsTrackers[index, 2], (float)rotationsTrackers[index, 3]);


                                    //to do : garder la valeur precedente


                                    differencePos = Vector3.Distance(GameObject.Find(m_tkrIDtoNames[index]).transform.position, currentTkrPos);

                                    // Debug.Log("diff"+ differencePos);

                                    if (instantiateDebugTrackers)
                                    {

                                        if ((/*(Time.frameCount < 10) || */(differencePos > errorTolerance)) && isSmooth)
                                        {
                                            //Smoothing between two positions and rotation during replay
                                            newPos = Vector3.Slerp(GameObject.Find(m_tkrIDtoNames[index]).transform.position, currentTkrPos, smoothingFactor);
                                            newRot = Quaternion.Slerp(GameObject.Find(m_tkrIDtoNames[index]).transform.rotation, currentTkrRot, smoothingFactor);
											tkrDebug.SetTracker( m_tkrIDtoNames[index], newPos, newRot );
											
											// tkrDebug.SetTracker(m_tkrIDtoNames[index], currentTkrPos, currentTkrRot);
										}
                                        else
                                        {
											//No smoothing we just assign the position and rotation from the file
											tkrDebug.SetTracker( m_tkrIDtoNames[index], currentTkrPos, currentTkrRot );

											//tkrDebug.SetTracker(m_tkrIDtoNames[index], newPos, newRot);
										}

                                    }
                                    else
                                    {

                                        if ((/*(Time.frameCount < 10) || */(differencePos > errorTolerance)) && isSmooth)
                                        {
                                            //Smoothing between two positions and rotation during replay
                                            newPos = Vector3.Slerp(GameObject.Find(m_tkrIDtoNames[index]).transform.position, currentTkrPos, smoothingFactor);
                                            GameObject.Find(m_tkrIDtoNames[index]).transform.position = newPos;

                                            newRot = Quaternion.Slerp(GameObject.Find(m_tkrIDtoNames[index]).transform.rotation, currentTkrRot, smoothingFactor);
                                            GameObject.Find(m_tkrIDtoNames[index]).transform.rotation = newRot;


                                          //  Debug.Log("smoothing");

                                        }
                                        else
                                        {
                                            //No smoothing we just assign the position and rotation from the file
                                            GameObject.Find(m_tkrIDtoNames[index]).transform.position = currentTkrPos;
                                            GameObject.Find(m_tkrIDtoNames[index]).transform.rotation = currentTkrRot;
                                            //Debug.Log("Nosmoothing");

                                            isSmooth = false;

                                        }

                                        // GameObject.Find(m_tkrIDtoNames[index]).transform.position = currentTkrPos;
                                        // GameObject.Find(m_tkrIDtoNames[index]).transform.rotation = currentTkrRot;

                                    }

                                }

                            }

                            // }
                        }
                    }
                }
           
            }
            // VRPN stream // Store the data but does not do anything with them if we do not record  
            else if (m_mocapSource == MocapSource.LIVE)
            {
                /*if (!VRPNOptions.IsReady())
                    VRPNOptions.Init(markerIDToNameMapping);
                VRPN.Client vrpn = VRPNOptions.vrpn_client;
                vrpn.Update();*/

                //We create the table of markers present in the scene and we fill the different table with it.
                // Allow to record only some trackers
                if(isCustomTrackers && !flag1)
                {
                    //trackers = new GameObject[trackersCustoms.Length];
                   // Debug.Log("custom");
                    for (int i = 0; i < trackersCustoms.Length; i++)
                    {
                       // Debug.Log("custom1");
                        trackers[i] = GameObject.Find(trackersCustoms[i]);
                    }
                    //Debug.Log("custom2");
                    flag1 = true;
                }
                else if (GameObject.FindGameObjectsWithTag("RigidBody_Transform").Length == 0 && !isCustomTrackers)
                {
                   
                    return;
                }
                else if(!flag && !isCustomTrackers)
                {
                    trackers = GameObject.FindGameObjectsWithTag("RigidBody_Transform");

					positionsTrackers = new double[trackers.Length, (int) numberOfCoordinatesVector];
					rotationsTrackers = new double[trackers.Length, (int) numberOfCoordinatesQuaternion];
					visibFrame = new bool[trackers.Length];

			

					flag = true;
                }
                else if ( trackers.Length != GameObject.FindGameObjectsWithTag("RigidBody_Transform").Length && !isCustomTrackers)
                {
                    trackers = GameObject.FindGameObjectsWithTag("RigidBody_Transform");

					positionsTrackers = new double[trackers.Length, (int) numberOfCoordinatesVector];
					rotationsTrackers = new double[trackers.Length, (int) numberOfCoordinatesQuaternion];
					visibFrame = new bool[trackers.Length];

			

				}


                // We create the table
                // copy positions (so a buffer can be kept) in the scene 
               // positionsTrackers= new double[trackers.Length,(int) numberOfCoordinatesVector];
               // rotationsTrackers= new double[trackers.Length, (int) numberOfCoordinatesQuaternion];
               // visibFrame = new bool[trackers.Length];
                totalTrackers = trackers.Length;


                //Debug.Log(trackers.Length);

                //We fill the previous table 
                for (int i = 0; i < trackers.Length; i++)
                {
                    string trackerName = trackers[i].name;
                    //Tuple<int,string> index = new Tuple<int, string>(i, Trackers[i].name);
                    //TrackerToNumber.Add(index);

                    visibFrame[i] = true;

                    //marker_positions = new double[no_of_markers, 3]; just to remember its definition

                   // string nameMarker= m_mkrNames[i].Second;
                   // int number = 0;

               
                    for (int j = 0; j < numberOfCoordinatesVector; j++)
                        positionsTrackers[i, j] = trackers[i].transform.position[j];

                    for (int j = 0; j < numberOfCoordinatesQuaternion; j++)
                        rotationsTrackers[i, j] = trackers[i].transform.rotation[j];

                    if (positionsTrackers[i, 0] != positionsTrackers[i, 0])
                        visibFrame[i] = false;
                }
            }
             
          
            //We record the information (position and ID) in a csv.
            if (isRecording)
                {

                    if (!tkrsLog.logIsOpen)
                        StartRecording(logFolderPath + fileName, true);

                    rate += Time.deltaTime;

                //record a ccording a fixed rate
                    if( rate>= recordRate)
                   {
                    Record(positionsTrackers, rotationsTrackers, visibFrame, trigger1);//,trigger2, trigger3);

                    rate = 0;
                    }
                   


                }
            else
                StopRecording();
            

        }

        void FixedUpdate()
        {
            if (callback == updateCallback.FIXED_UPDATE)
                FrameUpdate(Time.fixedDeltaTime);
        }
        void LateUpdate()
        {
            if (callback == updateCallback.LATE_UPDATE)
                FrameUpdate(Time.fixedDeltaTime);
        }
        void Update()
        {
            if (callback == updateCallback.UPDATE)
                FrameUpdate(Time.fixedDeltaTime);
        }

       /* public List<Tuple<int, string>> GetNamesList()
        {
            return m_tkrNames;
        }*/

      /*  void OnDestroy()
        {
            // disconnect from OWL server
            phasespaceOptions.Disconnect();
        }*/

        public string StartRecording(string markerLogName = "test", bool writeDirectToFile = false)
        {
                return tkrsLog.OpenLog(trackers, markerLogName, writeDirectToFile);
          
        }

        public void Record (double[,] listOfMarkersPosition, double[,] listOfMarkersRotation, bool[] markersVisibility, int trigger1)//, int trigger2, int trigger3)
        {
            if (tkrsLog.logIsOpen)
                tkrsLog.LoggingTrackersFrame(listOfMarkersPosition, listOfMarkersRotation, markersVisibility, trigger1);//, trigger2, trigger3);
        }
         
        public void StopRecording()
        {
            if (tkrsLog.logIsOpen)
                tkrsLog.CloseLog();
        }

        void OnDestroy()
        {
            StopRecording();

            if(isTriggersSent)
            tkrsLog.ClosePort();
        }
        
        public void  SetTriggers(int trigger1Sent )//, int trigger2Sent, int trigger3Sent)
        {
            trigger1 = trigger1Sent;
           // trigger2 = trigger2Sent;
           // trigger3 = trigger3Sent;

        

        }

        public void InitializeSerialPort(string portName, int baudRate)
        {
            tkrsLog.OpenSerialPort(portName, baudRate);
        }

        public void SetIsSent(bool flag)
        {
            tkrsLog.SetIsSentToPort(flag);

        }


    }

}



