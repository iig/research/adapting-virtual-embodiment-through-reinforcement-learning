﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ReadWriteCsv;
using System.IO;
//using System.IO.Ports;
using System;


[System.Serializable]
public class RecordPositionSettings {

	[HideInInspector]
	public bool isRecording;

	[HideInInspector]
	public bool recordFlag;

	public LogDynamicDistortionPosition posLog = new LogDynamicDistortionPosition();
	public string folderPathToRecord = ".\\adjusted-id-schubert-day-1-exp-1-trial-1OK.mkr16Hz.csv";

	[HideInInspector]
	public string fileNameToRecord = "";

	bool writeDirectToFile = true;


	public string StartRecording () {
		return posLog.OpenLog( folderPathToRecord + fileNameToRecord, writeDirectToFile );
	}
	public void StopRecording () {
		if ( posLog.logIsOpen )
			posLog.CloseLog();
	}

	/* public void InitializeSerialPort(string portName,int baudRate)
	 {
		  posLog.OpenSerialPort(portName,baudRate);
	 }*/

};



public class LogDynamicDistortionPosition  {

         
    // list of log entries
    private List<CsvRow> markerLogList = new List<CsvRow>();
    public bool logIsOpen { get; set; }
    string fileFormat = ".pos.csv";
    CsvRow currentLog = new ReadWriteCsv.CsvRow();

    float startTime;
    CsvFileWriter m_writer;
    bool directToFile = false;
    string logName = "";

    // Networking for EEG Trigger
   // private static SerialPort serialPort= new SerialPort();
    //byte[] input = new byte[4];

    public LogDynamicDistortionPosition()
    {
        logIsOpen = false;
        directToFile = false;
    }



    //Log the data in the CSV file to the corresponding collumn if they have been created (log opened)
    public void Logging( List<Vector3> listOfPositions, float condition, float distortionValue ,int trigger1=0)//, int trigger2, int trigger3)
    {
        if (!logIsOpen || listOfPositions == null )
            return;

        //directToFile recommended because the other can drop your performance heavily with the garbage collecter
        if (directToFile)
            currentLog.Clear();
        else
            currentLog = new ReadWriteCsv.CsvRow();

        currentLog.Add((Time.time - startTime).ToString("F7"));
        currentLog.Add(Time.time.ToString("F7"));

 
            for (int i = 0; i < listOfPositions.Count; ++i)
            {
                currentLog.Add(listOfPositions[i].x.ToString("F4"));
                currentLog.Add(listOfPositions[i].y.ToString("F4"));
                currentLog.Add(listOfPositions[i].z.ToString("F4"));
            }

        currentLog.Add(condition.ToString("F4"));

		currentLog.Add( distortionValue.ToString( "F4" ) );

		//Record the trigger in the CSV file
		currentLog.Add(trigger1.ToString("F4"));
       // currentLog.Add(trigger2.ToString("F4"));
       // currentLog.Add(trigger3.ToString("F4"));

        //Record the triggers for the EEG
      /*  input[0]=Convert.ToByte(trigger1);
        input[1]=Convert.ToByte(trigger2);
        input[2]=Convert.ToByte(trigger3);

        serialPort.Write(input, 0, 3);*/

       // Debug.Log("Sent=POS");

        if (directToFile)
        {
            //Write the trigger in the CSV file
            m_writer.WriteRow(currentLog);

            //Send the triggers to the EEG
           // serialPort.Write(input, 0, 3);

           // Debug.Log("Sent="+input.Length);
        } 
        else
            markerLogList.Add(currentLog);
    }

    //Create the Head Title for each Collumn of your CSV 
    public string OpenLog(string markerLogName = "markers", bool writeDirectToFile = false)
    {
        logName = FixName(markerLogName);

        directToFile = writeDirectToFile;
        if (directToFile)
            m_writer = new CsvFileWriter(logName);

        logIsOpen = true;
      //  Debug.Log("positions data log is open");

        CsvRow currentLog = new ReadWriteCsv.CsvRow();

        currentLog.Add("Time (s)");
        currentLog.Add("SystemTime (s)");

        string name1 = "Distorted Hand World Position (m)";
        currentLog.Add(name1+ ".X");
        currentLog.Add(name1 + ".Y");
        currentLog.Add(name1 + ".Z");

		string name2 = "Distorted Elbow World Position (m)";
		currentLog.Add( name2 + ".X" );
		currentLog.Add( name2 + ".Y" );
		currentLog.Add( name2 + ".Z" );

		string name3 = "Distorted Shoulder World Position (m)";
		currentLog.Add( name3 + ".X" );
		currentLog.Add( name3 + ".Y" );
		currentLog.Add( name3 + ".Z" );

		string name4 = "Original Hand World Position (m)";
        currentLog.Add(name4 + ".X");
        currentLog.Add(name4 + ".Y");
		currentLog.Add( name4 + ".Z" );

		string name5 = "Original Elbow World Position (m)";
		currentLog.Add( name5 + ".X" );
		currentLog.Add( name5 + ".Y" );
		currentLog.Add( name5 + ".Z" );

		string name6 = "Original Shoulder World Position (m)";
		currentLog.Add( name6 + ".X" );
		currentLog.Add( name6 + ".Y" );
		currentLog.Add( name6 + ".Z" );

		string name7 = "Target Reference World Position (m)";
        currentLog.Add(name7 + ".X");
        currentLog.Add(name7 + ".Y");
        currentLog.Add(name7 + ".Z");

        string name8 = "Original Cylinder World Position (m)";
        currentLog.Add(name8 + ".X");
        currentLog.Add(name8 + ".Y");
        currentLog.Add(name8 + ".Z");


        string name9 = "Distorted Cylinder World Position (m)";
        currentLog.Add(name9 + ".X");
        currentLog.Add(name9 + ".Y");
        currentLog.Add(name9 + ".Z");


        string name10 = "Hips World Position (m)";
        currentLog.Add(name10 + ".X");
        currentLog.Add(name10 + ".Y");
        currentLog.Add(name10 + ".Z");

		string name11 = "Head World Position (m)";
		currentLog.Add( name11 + ".X" );
		currentLog.Add( name11 + ".Y" );
		currentLog.Add( name11 + ".Z" );

		string name12 = "Head World Rotation (deg)";
		currentLog.Add( name12 + ".X" );
		currentLog.Add( name12 + ".Y" );
		currentLog.Add( name12 + ".Z" );

		currentLog.Add("Condition");
		currentLog.Add( "Distortion Value" );
		currentLog.Add("State");
       // currentLog.Add("Virtual Hand Threshold");
       // currentLog.Add("Original Hand Threshold");


        if (directToFile)
            m_writer.WriteRow(currentLog);
        else
            markerLogList.Add(currentLog);

        startTime = Time.time;

        return logName;
    }

    // add suffix and prevents files from overriding each other
    string FixName(string logName)
    {
        int i = 0;
        
        while (File.Exists(logName + fileFormat))
        {
			logName = logName.Remove(logName.Length - 1);
            logName += i.ToString();
			i++;
		}

        return logName + fileFormat;
    }

    // close log
    public void CloseLog()
    {
        logIsOpen = false;

        if (!directToFile)
        {
            // write log to CSV file
            Debug.Log("saving data position log " + logName);
            using (CsvFileWriter writer = new CsvFileWriter(logName))
            {
                foreach (CsvRow logEntry in markerLogList)
                {
                    writer.WriteRow(logEntry);
                }
                writer.Close();
            }


        //   Debug.Log("data position log saved " + logName);

            // clear log data
            markerLogList.Clear();
            System.GC.Collect();
        }
        else
        {
            m_writer.Close();
			Debug.Log( "saving data position log " + logName );
		}

      //  Debug.Log("data position log is closed");
    }



}
