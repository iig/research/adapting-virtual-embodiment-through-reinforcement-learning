﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ReadWriteCsv;
using UnityEngine;

//[System.Serializable]
//public class RecordRLSettings
//{
//	[SerializeField]
//	public string folderPath;

//	public LogRLDistortion rlLogger = new LogRLDistortion();

//	public void StartRecording(int subjectId, string type, string filename, bool writeToFile)
//	{
//		rlLogger.OpenLog(subjectId, type, folderPath + filename, writeToFile);
//	}
//	public void StopRecording()
//	{
//		if (rlLogger.logIsOpen)
//			rlLogger.CloseLog();
//	}

//};

public enum RLType
{
	UCB, UCB2
}

public class LogRLDistortion
{
	// list of log entries
	private List<CsvRow> RlLogList;
	public bool logIsOpen { get; set; }
	string fileFormat;
	bool header;
	string filename;
	bool directToFile;
	CsvFileWriter csvWriter;
	string folderPath = ".\\ExternalAsset\\ExperimentData\\RL\\";

	int subjectId;
	string type;

	public LogRLDistortion()
	{
		logIsOpen = false;
		header = true;
		RlLogList = new List<CsvRow>();
		fileFormat = ".RL.csv";
	}

	// Put header
	public void OpenLog(int subjectId, string filename, bool directToFile, string type)
	{
		this.subjectId = subjectId;
		this.filename = folderPath + filename + fileFormat;
		this.directToFile = directToFile;
		this.type = type;

		logIsOpen = true;

		csvWriter = new CsvFileWriter(this.filename);

		// Construct header
		CsvRow header = new ReadWriteCsv.CsvRow();
		header.Add("SubjectId");
		header.Add("Type"); // UCB or UCB2
		header.Add("Trial");
		header.Add("Gain");
		if (this.type == RLType.UCB2.ToString()) {
			header.Add("Radius");
		}
		header.Add("Detected");

		if (directToFile) {
			csvWriter.WriteRow(header);
		} else {
			RlLogList.Add(header);
		}

	}

	// Log the noTrial, distortionGain, if the distortion has been detected
	public void Logging(int trial, float gain, bool detected, float radius = -1f)
	{
		if (!logIsOpen)
			return;

		CsvRow currentLog = new ReadWriteCsv.CsvRow();

		currentLog.Add(subjectId.ToString());
		currentLog.Add(type.ToString());
		currentLog.Add(trial.ToString());
		currentLog.Add(gain.ToString());
		if (this.type == RLType.UCB2.ToString()) {
			currentLog.Add(radius.ToString());
		}
		currentLog.Add(detected.ToString());

		if (directToFile)
			csvWriter.WriteRow(currentLog);
		else
			RlLogList.Add(currentLog);

	}

	public void LogConvergence(float gain, float radius=-1)
	{
		if (!logIsOpen)
			return;


		CsvRow currentLog = new ReadWriteCsv.CsvRow();
		if (this.type == RLType.UCB.ToString()) {
			currentLog.Add("Convergence gain: " + gain.ToString());
		} else {
			currentLog.Add("Convergence gain: " + gain.ToString() + " radius: " + radius);
		}

		if (directToFile)
			csvWriter.WriteRow(currentLog);
		else
			RlLogList.Add(currentLog);
	}

	public void CloseLog()
	{
		logIsOpen = false;
		Debug.Log("Saving RL log " + filename);

		if (!directToFile) {
			foreach (CsvRow row in RlLogList) {
				csvWriter.Write(row);
			}

			RlLogList.Clear();
			System.GC.Collect(); // collect allocated mem in heap
		}

		csvWriter.Close();

	}

}

