﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ReadWriteCsv;
using System.IO;

public class LogDynamicDistortionQuestion {

         
    // list of log entries
    private List<CsvRow> markerLogList = new List<CsvRow>();
    public bool logIsOpen { get; set; }
    string fileFormat = ".qn.csv";

    float startTime;
    CsvFileWriter m_writer;
    CsvRow currentLog = new ReadWriteCsv.CsvRow();
    bool directToFile = false;
    string logName = "";

    public LogDynamicDistortionQuestion()
    {
        logIsOpen = false;
        directToFile = false;
    }



    //Log the data in the CSV file to the corresponding column if they have been created (log opened)
    public void Logging( List<string> answersList, float condition, int pairNumber=0)
    {
        if (!logIsOpen || answersList == null )
            return;


        //directToFile recommended because the other can drop your performance heavily with the garbage collecter
        if (directToFile)
            currentLog.Clear();
        else
            currentLog = new ReadWriteCsv.CsvRow();

        currentLog.Add(condition.ToString("F4"));
		currentLog.Add( pairNumber.ToString( "F4" ) );



		for (int i = 0; i < answersList.Count; ++i)
            {
                currentLog.Add(answersList[i]);
              
            }



        if (directToFile)
            m_writer.WriteRow(currentLog);
        else
            markerLogList.Add(currentLog);
    }

    //Create the Head Title for each Column of your CSV 
    public string OpenLog(string markerLogName, List<string> questionsList, bool writeDirectToFile = false)
    {
        logName = FixName(markerLogName);

        directToFile = writeDirectToFile;
        if (directToFile)
            m_writer = new CsvFileWriter(logName);

        logIsOpen = true;
        //Debug.Log("questions log is open");

        CsvRow currentLog = new ReadWriteCsv.CsvRow();


        currentLog.Add("Condition");
		currentLog.Add( "Pair Number" );

		for (int i = 0; i < questionsList.Count; ++i)
        {
            currentLog.Add(questionsList[i]);

        }



        if (directToFile)
            m_writer.WriteRow(currentLog);
        else
            markerLogList.Add(currentLog);

        startTime = Time.time;

        return logName;
    }

    // add suffix and prevents files from overriding each other
    string FixName(string logName)
    {
		int i = 0;

		while ( File.Exists( logName + fileFormat ) ) {
			logName = logName.Remove( logName.Length - 1 );
			logName += i.ToString();
			i++;
		}
		return logName + fileFormat;
    }

    // close log
    public void CloseLog()
    {
        logIsOpen = false;

        if (!directToFile)
        {
            // write log to CSV file
           // Debug.Log("saving data questions log " + logName);
            using (CsvFileWriter writer = new CsvFileWriter(logName))
            {
                foreach (CsvRow logEntry in markerLogList)
                {
                    writer.WriteRow(logEntry);
                }
                writer.Close();
            }


           // Debug.Log("data questions log saved " + logName);

            // clear log data
            markerLogList.Clear();
            System.GC.Collect();
        }
        else
        {
            m_writer.Close();
        }

       // Debug.Log("data questions log is closed");
    }

}
