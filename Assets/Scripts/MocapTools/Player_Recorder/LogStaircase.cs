﻿using System;
using System.IO;
using System.Collections.Generic;
using ReadWriteCsv;
using UnityEngine;

public class LogStaircase
{
	// list of log entries
	private List<CsvRow> StaircaseLogList;
	public bool logIsOpen { get; set; }
	string fileFormat;
	string filename;
	//bool directToFile;
	CsvFileWriter csvWriter;
	string folderPath = ".\\ExternalAsset\\ExperimentData\\RL\\";
    CsvRow headerRow;

    int subjectId;
	string type;

	public LogStaircase()
	{
		logIsOpen = false;
		StaircaseLogList = new List<CsvRow>();
		fileFormat = ".Staircase.csv";
	}

	// Put header
	public void OpenLog(int subjectId)
	{
		this.subjectId = subjectId;
		//this.filename = folderPath + filename + fileFormat;
		this.type = "Staircase";

		logIsOpen = true;

		//csvWriter = new CsvFileWriter(this.filename);

        // Construct header
        headerRow = new ReadWriteCsv.CsvRow();
        headerRow.Add("SubjectId");
        headerRow.Add("Type"); // UCB or staircase
        headerRow.Add("Trial");
        headerRow.Add("Gain");
        headerRow.Add("Detected");
        headerRow.Add("IsTurn");

		StaircaseLogList.Add(headerRow);
	}

	// Log the noTrial, distortionGain, if the distortion has been detected
	public void Logging(string prev, string current, int trial, float gain, bool detected, bool isTurn)
	{
		if (!logIsOpen)
			return;

        string prevFilename = folderPath + prev + fileFormat;
        string currentFilename = folderPath + current + fileFormat;

        this.filename = currentFilename;

        CsvRow currentLog = new ReadWriteCsv.CsvRow();

		currentLog.Add(subjectId.ToString());
		currentLog.Add(this.type);
		currentLog.Add(trial.ToString());
		currentLog.Add(gain.ToString());
		currentLog.Add(detected.ToString());
		currentLog.Add(isTurn.ToString());

        StaircaseLogList.Add(currentLog);
        WriteToFile();

        if (File.Exists(prevFilename))
        {
            // Delete it
            File.Delete(prevFilename);
        }
    }

    public void LogConvergence(string prev, string current, float gain, bool converged)
	{
		if (!logIsOpen)
			return;

        string prevFilename = folderPath + prev + fileFormat;
        string currentFilename = folderPath + current + fileFormat;
        this.filename = currentFilename;

        CsvRow currentLog = new ReadWriteCsv.CsvRow();
		if (converged) {
			currentLog.Add("Convergence value: " + gain.ToString());
		} else {
			currentLog.Add("Did not converge");
		}

		StaircaseLogList.Add(currentLog);
        WriteToFile();

        if (File.Exists(prevFilename))
        {
            // Delete it
            File.Delete(prevFilename);
        }

    }

	public void WriteToFile()
	{
		Debug.Log("Saving staircase log " + filename);
        csvWriter = new CsvFileWriter(filename);

        //if (!directToFile) {
        foreach (CsvRow row in StaircaseLogList) {
			csvWriter.WriteRow(row);
		}

			//StaircaseLogList.Clear();
			//System.GC.Collect(); // collect allocated mem in heap
		//}

		csvWriter.Close();

	}

    public void CloseLog()
    {
        logIsOpen = false;
        StaircaseLogList.Clear();
        System.GC.Collect(); // collect allocated mem in heap
    }

}

