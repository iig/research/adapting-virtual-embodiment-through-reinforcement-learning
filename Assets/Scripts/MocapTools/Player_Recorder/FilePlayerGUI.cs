﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Graphical interface that handles the playback function calls of a mocapPlayer object (File input).
/// </summary>
public class FilePlayerGUI : MonoBehaviour {
	
	Rect repControlRect = new Rect(20, 20, 300, 90);
	bool pause = false;
	float currentFrame = 0.0F;

	[Tooltip("Reference to a MocapInputController playing a recording in the FILE mode")]
	public IIG.MocapInputController m_inputController;

	MocapPlayer m_mocapPlayer;

	// Use this for initialization
	void Start () {
		if (m_inputController == null)
			m_inputController = this.GetComponent<IIG.MocapInputController> (); 
		if (m_inputController == null) {
			Debug.LogError ("[FilePlayerGUI: MocapInputController reference is missing!]");
			return;
		}
		m_mocapPlayer = m_inputController.fileOptions.mocapPlayer;
	}
	

	void OnGUI(){
		if (m_mocapPlayer == null)
			return;

		if (m_mocapPlayer == null){
			m_mocapPlayer = m_inputController.fileOptions.mocapPlayer;
		}

		repControlRect = GUI.Window(0, repControlRect, ReproductionWindow, "reproduction");

	}


	void ReproductionWindow(int windowID) {
		
		// backward double backward repspeed
		if (GUI.Button(new Rect(10, 25, 60, 30), "RW")){
			if (m_mocapPlayer.repSpeed > 0.0f){
				m_mocapPlayer.repSpeed = -2;
			} else {
				m_mocapPlayer.repSpeed *= 2;
			}
		}

		if (pause){
			// play set repspeed to 1
			if (GUI.Button(new Rect(80, 25, 60, 30), "Play")){
				m_mocapPlayer.repSpeed = 1;
				pause = false;
			}
		} else {
			// pause set repspeed to 0
			if (GUI.Button(new Rect(80, 25, 60, 30), "Pause")){
				m_mocapPlayer.repSpeed = 0;
				pause = true;
			}
		}

		// fastforward duplicate repspeed
		if (GUI.Button(new Rect(150, 25, 60, 30), "FF")){
			if (m_mocapPlayer.repSpeed < 0.0f){
				m_mocapPlayer.repSpeed = 2;
			} else {
				m_mocapPlayer.repSpeed *= 2;
			}
		}

		// repeat button reset time to 0 when done
		m_mocapPlayer.repeat = GUI.Toggle(new Rect(230, 30, 60, 25), m_mocapPlayer.repeat, "Repeat");

		// slider jumps to any point of the recording
		currentFrame = GUI.HorizontalSlider(new Rect(10, 65, 280, 25), (float)m_mocapPlayer.GetCurrentFrameNbr(), (float)m_mocapPlayer.startFrame, (float)m_mocapPlayer.stopFrame);
		if ((int)currentFrame != (int)m_mocapPlayer.GetCurrentFrameNbr()) {
			m_mocapPlayer.SetCurrentFrameNbr((int)currentFrame);
		}
			
		GUI.DragWindow();
	}
}
