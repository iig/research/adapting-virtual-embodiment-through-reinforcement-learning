/**************************************************************************
 * CSVPlayer.cs especialization of MocapPlayer.cs which load mocaps in CSV
 * Written by Henrique Galvan Debarba
 * Last update: 03/03/14
 * *************************************************************************/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using ReadWriteCsv;

namespace Modularity
{

    public class CSVPlayer_Tracker : MocapPlayer_Tracker
    {

        // parse CSV file with prerecorded mocap
        public override bool LoadMocap(string fileName, ref Dictionary<int, string> m_tkrIDtoNames )
        {
            // check if there is any loaded mocap, dump it if that is the case
            if (mocapIsLoaded)
                DumpMocap();

            // open CSV file
            Debug.Log("CSVPlayer: opening file " + fileName);

            CsvFileReader reader = new CsvFileReader(fileName);
            // TODO check if the file was loaded successfully
            //Debug.Log("Error: Unable to open file " + fileName);
            //return false;

            string fileContent = reader.ReadToEnd();
            reader.Close();

            float startTime = 0, endTime = 0;

            using (StringReader strReader = new StringReader(fileContent))
            {
                string line;
                int countFrame = 0;
                // skip header read the first for nothing
                line = strReader.ReadLine();// the header

				// Replace alias file by reading the name of the tracker directly from the csv file
				int offset = 2;
				string[] mocapLineArrayHeader = line.Split( new string[] { "," }, StringSplitOptions.None );
				totalPoints = (mocapLineArrayHeader.Length - offset) / 8;

				for ( int i = 0; i < totalPoints; i++ ) 
				{
					m_tkrIDtoNames[i] = mocapLineArrayHeader[8 * i + 2];
				}



				
                while ((line = strReader.ReadLine()) != null)
                {
                    // list of positions for this frame
                    List<Vector3> mocapLinePos = new List<Vector3>();

                    // list of rotations for this frame
                    List<Quaternion> mocapLineRot = new List<Quaternion>();

                    string[] mocapLineArray = line.Split(new string[] { "," }, StringSplitOptions.None);
                    //if (((mocapLineArray.Length-offset)%4)!=0)
                    //	return false;

                   // totalPoints = (mocapLineArray.Length - offset) / 8;
                    double[,] mocLinePos = new double[totalPoints, 3];
                    double[,] mocLineRot = new double[totalPoints, 4];
                    bool[] visibLine = new bool[totalPoints];
                    for (int i = 0; i < totalPoints; i++)
                    {
                        if (mocapLineArray[8 * i + 3] == "NA")
                        {
                            mocapLineArray[8 * i + 3] = "NaN";
                            mocapLineArray[8 * i + 4] = "NaN";
                            mocapLineArray[8 * i + 5] = "NaN";

                            mocapLineArray[8 * i + 6] = "NaN";
                            mocapLineArray[8 * i + 7] = "NaN";
                            mocapLineArray[8 * i + 8] = "NaN";
                            mocapLineArray[8 * i + 9] = "NaN";
                        }

                        Vector3 posEntry = new Vector3(float.Parse(mocapLineArray[8 * i + offset + 1]),
                                                       float.Parse(mocapLineArray[8 * i + offset + 2]),
                                                       float.Parse(mocapLineArray[8* i + offset + 3]));

                        Quaternion rotEntry = new Quaternion(float.Parse(mocapLineArray[8 * i + offset + 4]),
                                                       float.Parse(mocapLineArray[8 * i + offset + 5]),
                                                       float.Parse(mocapLineArray[8 * i + offset + 6]), float.Parse(mocapLineArray[8 * i + offset + 7]));

                        visibLine[i] = (1 == float.Parse(mocapLineArray[8 * i + offset]));
                        //new position entry
                        mocapLinePos.Add(posEntry);
                        mocapLineRot.Add(rotEntry);
                        mocLinePos[i, 0] = posEntry.x;
                        mocLinePos[i, 1] = posEntry.y;
                        mocLinePos[i, 2] = posEntry.z;

                        mocLineRot[i, 0] = rotEntry.x;
                        mocLineRot[i, 1] = rotEntry.y;
                        mocLineRot[i, 2] = rotEntry.z;
                        mocLineRot[i, 3] = rotEntry.w;
                    }

                    if (countFrame == 0)
                        startTime = float.Parse(mocapLineArray[0]);
                    countFrame++;
                    endTime = float.Parse(mocapLineArray[0]);

                    // new frame of positions and visibility
                    mocapDataRot.Add(mocLineRot);
                    mocapDataPos.Add(mocLinePos);
                    mocapVisibility.Add(visibLine);
                }
            }

            // set reproduction parameters
            totalFrames = mocapDataPos.Count;
            stopFrame = totalFrames;
            frameRate = totalFrames / (endTime - startTime);
            // sampling delta time (in seconds)
            samplingDTime = 1.0f / frameRate;

            mocapIsLoaded = true;
            Debug.Log("CSVPlayer: " + fileName + " loaded!");
            return true;
        }
    }
}
