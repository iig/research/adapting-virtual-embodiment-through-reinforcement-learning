﻿/**************************************************************************
 * LogMarkers.cs allows to log a time stamp, marker positions, and whether
 * they were visible at that given instant into a mkr.csv file
 * Written by Henrique Galvan Debarba
 * Last update: 28/02/14
 * *************************************************************************/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ReadWriteCsv;
using System.IO;

public class LogMarkers {

	// list of log entries
	private List<CsvRow> markerLogList = new List<CsvRow>();
	public bool logIsOpen {get; set;}
	string fileFormat = ".mkr.csv";

    CsvRow currentLog = new ReadWriteCsv.CsvRow();
    float startTime;
	CsvFileWriter m_writer;
	bool directToFile = false;
	string logName = "";

	public LogMarkers(){
		logIsOpen = false;
		directToFile = false;
	}
	
	// log current pose if log is open
	public void LogMarkersFrame(double[,] listOfMarkers, bool[] markersVisibility){


        if (!logIsOpen || listOfMarkers==null || listOfMarkers.GetLength(0)!=markersVisibility.Length)
			return;



        //directToFile recommended because the other can drop your performance heavily with the garbage collecter
        if (directToFile)
            currentLog.Clear();
        else
            currentLog = new ReadWriteCsv.CsvRow();


        currentLog.Add((Time.time-startTime).ToString("F7"));
		currentLog.Add(Time.time.ToString("F7"));


        for (int i = 0; i <listOfMarkers.GetLength(0); ++i)
		{		
			currentLog.Add(((markersVisibility[i])?1:0).ToString());
			for(int j = 0; j <listOfMarkers.GetLength(1); ++j)
			{
				currentLog.Add(listOfMarkers[i,j].ToString("F4"));
			}
		}
		if (directToFile)
			m_writer.WriteRow(currentLog);
		else
			markerLogList.Add(currentLog);
	}
	
	// open log
	public string OpenLog(List<Tuple<int, string>> nameList, string markerLogName = "markers", bool writeDirectToFile = false){	
		logName = FixName (markerLogName);

		directToFile = writeDirectToFile;
		if (directToFile)
			m_writer = new CsvFileWriter (logName);

		logIsOpen = true;
		Debug.Log("markers log is open");

		CsvRow currentLog = new ReadWriteCsv.CsvRow();
		currentLog.Add("Time (s)");
		currentLog.Add("SystemTime (s)");

		for(int i = 0; i <nameList.Count; ++i)
		{		
			currentLog.Add(nameList[i].Second);
			currentLog.Add(nameList[i].Second + ".X");
			currentLog.Add(nameList[i].Second + ".Y");
			currentLog.Add(nameList[i].Second + ".Z");
		}

		if (directToFile)
			m_writer.WriteRow(currentLog);
		else
			markerLogList.Add(currentLog);

		startTime = Time.time;

		return logName;
	}

	// add suffix and prevents files from overriding each other
	string FixName (string logName){
		int i=0;
		logName += i.ToString();
		while (File.Exists(logName + fileFormat)){
			i++;
			logName = logName.Remove(logName.Length - 1);
			logName += i.ToString();
		}
		return logName + fileFormat;	
	}

	// close log
	public void CloseLog(){
		logIsOpen = false;

		if (!directToFile) {
			// write log to CSV file
			Debug.Log ("saving avatar pose log " + logName);
			using (CsvFileWriter writer = new CsvFileWriter(logName)) {
				foreach (CsvRow logEntry in markerLogList) {
					writer.WriteRow (logEntry);
				}
				writer.Close ();
			}
		

			Debug.Log ("avatar pose log saved " + logName);

			// clear log data
			markerLogList.Clear ();
			System.GC.Collect ();
		} else {
			m_writer.Close();
		}

		Debug.Log("avatar pose log is closed");
	}	

}