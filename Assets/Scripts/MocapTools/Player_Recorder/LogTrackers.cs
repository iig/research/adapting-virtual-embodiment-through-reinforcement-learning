﻿/**************************************************************************
 * LogMarkers.cs allows to log a time stamp, marker positions, and whether
 * they were visible at that given instant into a mkr.csv file
 * Written by Henrique Galvan Debarba
 * Last update: 28/02/14
 * *************************************************************************/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ReadWriteCsv;
using System.IO;
using System.IO.Ports;
using System;

namespace Modularity
{

    public class LogTrackers
    {

        // list of log entries
        private List<CsvRow> markerLogList = new List<CsvRow>();
        public bool logIsOpen { get; set; }
        string fileFormat = ".tkr.csv";
        CsvRow currentLog = new ReadWriteCsv.CsvRow();
        bool isTriggerSent;
        bool isSentToPort;

        float startTime;
        CsvFileWriter m_writer;
        bool directToFile = false;
        string logName = "";


        // Networking for EEG Trigger
        private static SerialPort serialPort = new SerialPort();
        byte[] input = new byte[4];


        public LogTrackers()
        {
            logIsOpen = false;
            directToFile = false;
        }

        // log current pose if log is open
        public void LoggingTrackersFrame(double[,] listOfMarkersPosition,double[,] listOfMarkersRotation, bool[] markersVisibility, int trigger1)//,int trigger2, int trigger3)
        {
            if (!logIsOpen || listOfMarkersPosition == null || listOfMarkersPosition.GetLength(0) != markersVisibility.Length)
                return;

            //directToFile recommended because the other can drop your performance heavily with the garbage collecter
            if(directToFile)
            currentLog.Clear();
            else
            currentLog = new ReadWriteCsv.CsvRow();

            currentLog.Add((Time.time - startTime).ToString("F7"));
            currentLog.Add(Time.time.ToString("F7"));

            for (int i = 0; i < listOfMarkersPosition.GetLength(0); ++i)
            {
                currentLog.Add(((markersVisibility[i]) ? 1 : 0).ToString());
                for (int j = 0; j < listOfMarkersPosition.GetLength(1); ++j)
                {
                    currentLog.Add(listOfMarkersPosition[i, j].ToString("F4"));
                }

                for (int j = 0; j < listOfMarkersRotation.GetLength(1); ++j)
                {
                    currentLog.Add(listOfMarkersRotation[i, j].ToString("F4"));
                }
            }


            if (isTriggerSent)
            {
                currentLog.Add(trigger1.ToString("F4"));
                //currentLog.Add(trigger2.ToString("F4"));
                //currentLog.Add(trigger3.ToString("F4"));



                //Record the triggers for the EEG
                input[0] = Convert.ToByte(trigger1);
               // input[0] = Convert.ToByte(trigger2);
               // input[0] = Convert.ToByte(trigger3);
             
                // input[1] = Convert.ToByte(trigger2);
                // input[2] = Convert.ToByte(trigger3);


            }

            

            if (directToFile)
            {
                if(isTriggerSent && isSentToPort && serialPort.IsOpen)
                {
                   // Debug.Log("SentBytesT1" + trigger1.ToString("F4"));
                  
                    //Send the triggers to the EEG
                    serialPort.Write(input, 0, 1);
                    isSentToPort = false;
                }
                m_writer.WriteRow(currentLog);
            }
            else
                markerLogList.Add(currentLog);

            
        }

        // open log
        public string OpenLog(GameObject[] nameList, string markerLogName = "markers", bool writeDirectToFile = false)
        {
            logName = FixName(markerLogName);

            directToFile = writeDirectToFile;
            if (directToFile)
                m_writer = new CsvFileWriter(logName);

            logIsOpen = true;
            Debug.Log("trackers log is open");

            CsvRow currentLog = new ReadWriteCsv.CsvRow();
            currentLog.Add("Time (s)");
            currentLog.Add("SystemTime (s)");



            for (int i = 0; i < nameList.Length; ++i)
            {
                currentLog.Add(nameList[i].name);
                currentLog.Add(nameList[i].name+ ".X");
                currentLog.Add(nameList[i].name + ".Y");
                currentLog.Add(nameList[i].name+ ".Z");
                currentLog.Add(nameList[i].name + "Q.X");
                currentLog.Add(nameList[i].name + "Q.Y");
                currentLog.Add(nameList[i].name + "Q.Z");
                currentLog.Add(nameList[i].name + "Q.W");
            }

            if (isTriggerSent)
            {
                currentLog.Add("State");
              //  currentLog.Add("Threshold");
               // currentLog.Add("Virtual Hand Threshold");
               // currentLog.Add("Original Hand Threshold");
            }

            if (directToFile)
                m_writer.WriteRow(currentLog);
            else
                markerLogList.Add(currentLog);

            startTime = Time.time;

            return logName;
        }

        // add suffix and prevents files from overriding each other
        string FixName(string logName)
        {
			int i = 0;

			while ( File.Exists( logName + fileFormat ) ) {
				logName = logName.Remove( logName.Length - 1 );
				logName += i.ToString();
				i++;
			}
			return logName + fileFormat;
        }

        // close log
        public void CloseLog()
        {
            logIsOpen = false;

            if (!directToFile)
            {
                // write log to CSV file
                Debug.Log("saving avatar pose log " + logName);
                using (CsvFileWriter writer = new CsvFileWriter(logName))
                {
                    foreach (CsvRow logEntry in markerLogList)
                    {
                        writer.WriteRow(logEntry);
                    }
                    writer.Close();
                }


                Debug.Log("avatar pose log saved " + logName);

                // clear log data
                markerLogList.Clear();
                System.GC.Collect();
            }
            else
            {
                m_writer.Close();
            }

            Debug.Log("avatar pose log is closed");
        }

        public void OpenSerialPort(string portName, int baudRate)
        {
            // PortName should be confirmed before experiments
            serialPort.PortName = portName;
            serialPort.BaudRate = baudRate;

            if (!serialPort.IsOpen)
            {
                try
                {
                    serialPort.Open();
                   // Debug.Log("Open= " + serialPort.IsOpen);
                    System.Threading.Thread.Sleep(100);
                }
                catch
                {
                }
            }
        }

        public void SetIsTriggerSent(bool isTriggerSentBool)
        {

            isTriggerSent = isTriggerSentBool;
        }

        public void ClosePort()
        {
            if (isTriggerSent && serialPort.IsOpen)
            {
                input[0] = Convert.ToByte(0);


                serialPort.Write(input, 0, 1);
                serialPort.Close();
            }
            
        }

        public void SetIsSentToPort(bool isSent)
        {
            isSentToPort = isSent;
        }
    }
}