/**************************************************************************
 * MarkersDebug.cs debug markers positions
 * Written by Henrique Galvan Debarba
 * Last update: 03/03/14
 * *************************************************************************/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ReadWriteCsv;


namespace Modularity
{



    public class TrackersDebug
    {

        //Dictionary<string, GameObject> m_markersDebugNameKey;
        Dictionary<string, int> m_nameToIndexMap = new Dictionary<string, int>();
        List<GameObject> trackerGOArray = new List<GameObject>();

        // materials that will be used in visible/notvisible markers
        public Material matVisible;
        public Material matNotVisible;

        // sphere primitive has by standard the size of 1 
       // float scaleFactor = 0.025f;

        // std game object to put the markers, so that they are not in the root of the hierarchy
        public GameObject trackerDebugParent = new GameObject();

        // prefab object
        public string tkrGOPrefabName = "PhaseSpace_Tracker";
        GameObject trackerPrefab;

        public void Init()
        {
            //	m_markersDebugIDKey.Clear ();
            //	m_markersDebugNameKey.Clear ();
            m_nameToIndexMap.Clear();
            trackerGOArray.Clear();

            // set marker debug parent name
            trackerDebugParent.name = "Trackers debug";
            // create materials to be used on marker representations
            if (matVisible == null)
            {
                //matVisible = new Material (Shader.Find ("Transparent/DiffuseBackFace"));
                matVisible = new Material(Shader.Find("Standard"));
                matVisible.color = new Color(0, 0.8f, 0, 0.5f);//Color.green;
            }
            if (matNotVisible == null)
            {
                //matNotVisible = new Material (Shader.Find ("Transparent/DiffuseBackFace"));
                matNotVisible = new Material(Shader.Find("Standard"));
                matNotVisible.color = new Color(0.8f, 0, 0, 0.5f);//Color.red;
            }
            // allocation
            //markerGOArray = new GameObject [markers.Count];
            //m_markersDebug = new Dictionary<int, GameObject> ();

            trackerPrefab = Resources.Load(tkrGOPrefabName, typeof(GameObject)) as GameObject;
            if (trackerPrefab == null)
                Debug.LogError("TrackersDebug.cs: marker GO prefab not found " + tkrGOPrefabName);
        }

        public void Init(List<Tuple<int, string>> trackers)
        {
            Init();
            AddTrackers(trackers);
        }

        public void Init(List<string> trackers)
        {
            Init();
            AddTrackers(trackers);
        }

        public void AddTrackers(List<Tuple<int, string>> trackers)
        {
            // instantiate marker prefab
            foreach (Tuple<int, string> tkr in trackers)
            {
                AddTracker(tkr);
            }
        }
        public void AddTrackers(List<string> trackers)
        {
            // instantiate marker prefab
            foreach (string tkr in trackers)
            {
                AddTracker(tkr);
            }
        }
        // insertion with (string) number key
        public GameObject AddTracker(Tuple<int, string> tracker)
        {
            int index;
            if (m_nameToIndexMap.TryGetValue(tracker.First.ToString(), out index)) // Returns true.
            {
                //if (m_nameToIndexMap [marker.First.ToString ()] != null) {
                Debug.Log("TrackersDebug.cs: tracker " + tracker.First + " already exists");
                return trackerGOArray[index];//GetMarker(marker.First.ToString ());
            }

            GameObject newTracker = GameObject.Instantiate(trackerPrefab) as GameObject;

            newTracker.GetComponent<Renderer>().material = matNotVisible;
            newTracker.transform.parent = trackerDebugParent.transform;
            newTracker.name = tracker.Second;

            trackerGOArray.Add(newTracker);
            m_nameToIndexMap.Add(tracker.First.ToString(), trackerGOArray.Count - 1);

            return newTracker;
        }

        // insertion with name key
        public GameObject AddTracker(string name)
        {
            int index;
            if (m_nameToIndexMap.TryGetValue(name, out index)) // Returns true.
            {
                //if (m_nameToIndexMap [name] != null){			
                Debug.Log("TrackersDebug.cs: marker " + name + " already exists");
                return trackerGOArray[index];//GetMarker (name);
            }

			if(GameObject.Find(name)!=null && GameObject.Find( name ).tag=="RigidBody_Transform" )
			{
				GameObject tracker = GameObject.Find( name );


				trackerGOArray.Add( tracker );

				m_nameToIndexMap.Add( name, trackerGOArray.Count - 1 );

				return tracker;
			}

            GameObject newMarker = GameObject.Instantiate(trackerPrefab) as GameObject;
            // assume it is not visible inittialy
            newMarker.GetComponent<Renderer>().material = matNotVisible;
            // set parent to a standard GO
            newMarker.transform.parent = trackerDebugParent.transform;
            newMarker.name = name;

            trackerGOArray.Add(newMarker);
            m_nameToIndexMap.Add(name, trackerGOArray.Count - 1);

            return newMarker;
        }



        // make all markers (in)visible
        public void SetVisible(bool isVisible)
        {
            for (int i = 0; i < trackerGOArray.Count; i++)
            {
				if ( trackerGOArray[i].GetComponent<MeshRenderer>() != null)
                trackerGOArray[i].GetComponent<MeshRenderer>().enabled = isVisible;

                for (int j = 0; j < trackerGOArray[i].transform.childCount; j++)
                {
					if ( trackerGOArray[i].transform.GetChild( j ).GetComponent<MeshRenderer>() != null )
						trackerGOArray[i].transform.GetChild(j).GetComponent<MeshRenderer>().enabled = isVisible;
                }
            }
        }


        public void SetMarkersMap(int[] keys, Vector3[] mkrPosArray, bool[] mkrVisibArray)
        {
            if (keys.Length != mkrPosArray.Length || keys.Length != mkrVisibArray.Length)
                return;

            for (int i = 0; i < mkrPosArray.Length; i++)
            {
                SetMarkerMap(keys[i].ToString(), mkrPosArray[i], mkrVisibArray[i]);
            }
        }

        public void SetMarkerMap(string mapKey, Vector3 mkrPos, bool mkrVisib)
        {
            //int index = m_nameToIndexMap [mapKey];
            int index;
            if (!m_nameToIndexMap.TryGetValue(mapKey, out index)) // Returns true.
            {
                //if (index == null) {
                Debug.LogWarning("TrackersDebug.cs: tracker key " + mapKey + " not found");
                return;
            }

            GameObject currentGO = trackerGOArray[index];
            currentGO.transform.position = mkrPos;
            if (mkrVisib)
            {
                currentGO.GetComponent<Renderer>().material = matVisible;
                currentGO.tag = "TrackerVisible";
            }
            else
            {
                currentGO.GetComponent<Renderer>().material = matNotVisible;
                currentGO.tag = "TrackerNotVisible";
            }

        }


        // set marker position, material and tag
        public void SetTracker(string mapKey, Vector3 tkrPos, Quaternion tkrRot)
        {
            int index;
            if (!m_nameToIndexMap.TryGetValue(mapKey, out index)) // Returns true.
            {
                //if (m_nameToIndexMap[mapKey] == null){
                //	Debug.LogWarning("MarkersDebug.cs: marker key " + mapKey + " not found");
                return;
            }
            SetTracker(m_nameToIndexMap[mapKey], tkrPos, tkrRot);
        }

        public void TrackerNotVisible(string mapKey)
        {
            int index;
            if (!m_nameToIndexMap.TryGetValue(mapKey, out index)) // Returns true.
            {
                //if (m_nameToIndexMap[mapKey] == null){
                //	Debug.LogWarning("MarkersDebug.cs: marker key " + mapKey + " not found");
                return;
            }
            TrackerNotVisible(m_nameToIndexMap[mapKey]);
        }

        // set markers attributes at once
        public void SetTrackers(Vector3[] trackPosArray, Quaternion[] trackQuatArray, bool[] visibArray)
        {
            if (trackPosArray.Length != trackerGOArray.Count || trackPosArray.Length != visibArray.Length)
            {
                Debug.Log("TrackersDebug.cs: array lenghts doesn't match");
                return;
            }

            // update markers debug visibility and pos
            for (int i = 0; i < trackPosArray.Length; i++)
            {
                trackerGOArray[i].transform.position = trackPosArray[i];
                trackerGOArray[i].transform.rotation= trackQuatArray[i];

                if (visibArray[i])
                {
                    trackerGOArray[i].GetComponent<Renderer>().material = matVisible;
                    trackerGOArray[i].tag = "RigidBody_Transform";
                    //trackerGOArray[i].tag = "markerVisible";
                }
                else
                {
                    trackerGOArray[i].GetComponent<Renderer>().material = matNotVisible;
                    trackerGOArray[i].tag = "RigidBody_Transform";
                    // trackerGOArray[i].tag = "markerNotVisible";
                }
            }

        }


        // set marker position and visible
        public void SetTracker(int index, UnityEngine.Vector3 tkrPos , Quaternion tkrRot)
        {
            if (index >= trackerGOArray.Count)
            {
                Debug.Log("TrackersDebug.cs: index " + index + " is bigger than total trackers: " + trackerGOArray.Count);
                return;
            }
            //if (markerGOArray[index].GetComponent<Renderer>().enabled)
			if ( trackerGOArray[index].GetComponent<Renderer>()!=null)
            trackerGOArray[index].GetComponent<Renderer>().material = matVisible;
            //  trackerGOArray[index].tag = "trackerVisible";
            trackerGOArray[index].tag = "RigidBody_Transform";
            trackerGOArray[index].transform.position = tkrPos;
            trackerGOArray[index].transform.rotation = tkrRot;
        }

        public void TrackerNotVisible(int index)
        {
            if (index >= trackerGOArray.Count)
            {
                Debug.Log("TrackersDebug.cs: index " + index + " is bigger than total trackers: " + trackerGOArray.Count);
                return;
            }
            //if (markerGOArray[index].GetComponent<Renderer>().enabled)
            trackerGOArray[index].GetComponent<Renderer>().material = matNotVisible;
            trackerGOArray[index].tag = "RigidBody_Transform";
            //trackerGOArray[index].tag = "trackerNotVisible";
        }

        public GameObject GetTracker(string name)
        {
            if (m_nameToIndexMap[name] < trackerGOArray.Count)
                return trackerGOArray[m_nameToIndexMap[name]];
            else
                return null;
        }
        public GameObject GetTracker(int index)
        {
            if (index < trackerGOArray.Count)
                return trackerGOArray[index];
            else
                return null;
        }

        public void Destroy()
        {
            foreach (GameObject tkr in trackerGOArray)
            {
                GameObject.Destroy(tkr);
            }
            GameObject.Destroy(trackerDebugParent);
            trackerGOArray.Clear();
            m_nameToIndexMap.Clear();
        }

    }
}