﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class HeadTracking : MonoBehaviour {

    public enum trackingObject { Marker , Tracker, GameObject};
    public trackingObject Type;

    public string rigidBodyName="HEAD";
	public GameObject gameObjectToFollow;
    public bool rotationEnable = false;
    public bool positionEnable = false;
   // public bool correctionEnable = false;
    //public bool positionHMD = false;

    
   GameObject tracker;
    GameObject marker;
   bool flag=false;
    
	// Use this for initialization
	void Awake () {

        //UnityEngine.VR.InputTracking.disablePositionalTracking = positionHMD;

    }
	
	// Update is called once per frame
	void Update () {


            if (Type==trackingObject.Tracker)
            {
                if (tracker==null)
                {
                    GameObject[] allTrackers = GameObject.FindGameObjectsWithTag("RigidBody_Transform");
				//Debug.Log( gameObject.name );
                    foreach (GameObject Tracker in allTrackers)
                    {
                        if (Tracker.name == rigidBodyName )
                        {
                            tracker = Tracker;
                           
                        }

                    }

                }
               else
                {
				if (positionEnable) {
					transform.position = tracker.transform.position;
					//Debug.DrawRay(transform.position, Vector3.forward *10, Color.yellow);

				}

				if (rotationEnable)
                        transform.rotation = tracker.transform.rotation;

                }
            }
            else if ( !(Type== trackingObject.GameObject))
            {
                if (!flag)
                {
                    GameObject[] allMarkers = GameObject.FindGameObjectsWithTag("markerVisible");

                foreach (GameObject Marker in allMarkers)
                    {
             
                    if (Marker.name == rigidBodyName )
                        {
                            marker = Marker;
                            flag = true;



                    }

                }

                }
                 else
                {
				if (positionEnable) 
					transform.position = marker.transform.position;
			

                    if (rotationEnable)
                        transform.rotation = marker.transform.rotation;


                }

            }else {
			if ( positionEnable )
				transform.position = gameObjectToFollow.transform.position;


			if ( rotationEnable )
				transform.rotation = gameObjectToFollow.transform.rotation;

		}

      




            
              

          /*  if (correctionEnable)
                transform.eulerAngles = tracker.transform.rotation.eulerAngles - gameObject.transform.GetChild(0).transform.rotation.eulerAngles;*/




        


    }
}
