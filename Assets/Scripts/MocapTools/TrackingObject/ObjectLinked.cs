﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectLinked : MonoBehaviour {


    public GameObject objectToLinkWith;
    public GameObject referenceScale;
    public bool updatePosition = true;
    public bool updateRotation = true;
    public bool updateScale = true;
    public bool initiateAtWake = true;

   // [HideInInspector]
    public bool resetInitiate = false;

    Calibration calibrationMethods;

    [HideInInspector]
    public bool flag;
    Matrix4x4 matrixTransformation;

  //  float time = 0f;

    // Use this for initialization
    //TO DO: change with onenable and see if it breaks something
    void Awake () {

        calibrationMethods = new Calibration();

        if (initiateAtWake)
        {
            matrixTransformation = calibrationMethods.TransformationMatrix(transform, objectToLinkWith.transform);
            flag = true;
        }
           

        if (updateScale)
        {
            if(referenceScale==null)
            transform.localScale = objectToLinkWith.transform.lossyScale;
            else
            referenceScale.transform.localScale = objectToLinkWith.transform.lossyScale;
        }
        

    }
	
	// Update is called once per frame
	void Update () {


        if (resetInitiate)
        {
            
            matrixTransformation = calibrationMethods.TransformationMatrix(transform, objectToLinkWith.transform);

            resetInitiate = false;

            
            flag = true;
        }
            
        if(flag)
        calibrationMethods.AssociateRigidBody(matrixTransformation, transform, objectToLinkWith.transform, updateRotation, updatePosition);

        if (updateScale)
        {
            if (referenceScale == null)
                transform.localScale = objectToLinkWith.transform.lossyScale;
            else
            {
                referenceScale.transform.localScale = objectToLinkWith.transform.lossyScale;

               // referenceScale.transform.localScale = new Vector3( objectToLinkWith.transform.lossyScale.x, 10*objectToLinkWith.transform.lossyScale.y, objectToLinkWith.transform.lossyScale.z);
            }
               
        }
    }
}
