﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Changed by Thibault
/// Do we need to correct also according to x and z ?
/// For the momment works only on at the beginning with the headset facing the good direction
/// TO DO: USE A RIGIBODY TRACKED PROPERLY ALIGNED WITH OUR GAZE AS CORRECT REFERENCE
/// </summary>

    namespace IIG
{


    public class RotationCorrectionModified : MonoBehaviour
    {
        public Transform correct;
        public Transform drifting;

        public bool initOnly = true;

        Quaternion lastDelta;
        Quaternion correction = Quaternion.identity;
        Quaternion currentDelta;

      // smoothing factor should be very small, so that the correction
      // to the sensor drift is unnoticeable
      [Range(0.00001f, 1f)]
        public float smoothingFactor = 0.005f;


        void LateUpdate()
        {
            if ( Time.frameCount < 10)
            {
                /* var newRot = correct.rotation;
                 newRot = Quaternion.Euler(0f, newRot.eulerAngles.y, 0f);
                 drifting.parent.rotation = newRot;*/
                if(correct!=null && drifting!=null)
                {
                    Quaternion newRot = (correct.rotation) * Quaternion.Inverse(drifting.rotation);
                    newRot = Quaternion.Euler(0f, newRot.eulerAngles.y, 0f);
                    drifting.parent.rotation = newRot;

                    correction = newRot;
                }
                else
                {
                    Debug.Log("Need to attribute the gameobjects to the script");
                }
              


            }
            else if ( !initOnly )
            {
                lastDelta = correction;
                // orientation difference between oculus and calibrated coordinate system
                currentDelta = (correct.rotation) * Quaternion.Inverse(Quaternion.Inverse(lastDelta) * drifting.rotation);


                correction = Quaternion.Slerp(correction, currentDelta, smoothingFactor);
                correction = Quaternion.Euler(0f, correction.eulerAngles.y, 0f);

#if THIS_IS_NOT_WORKING
        var newRot = Quaternion.Slerp(drifting.rotation, correct.rotation, smoothingFactor);
        newRot = Quaternion.Euler(0f, newRot.eulerAngles.y, 0f);
        drifting.rotation = newRot;
#else
                drifting.parent.rotation = correction;
                //this.transform.rotation = correction;
#endif
            }
        }
    }
}
