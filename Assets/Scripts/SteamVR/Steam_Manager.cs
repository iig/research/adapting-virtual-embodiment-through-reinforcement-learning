﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using System.Text;

// TODO : Load  lighthouse unique references correspondance to their arbitary ID from a config file


public  class Steam_Manager : Singleton<Steam_Manager>
	{
	public static uint[] trackersUniqueIDNumber = new uint[16];
	public static uint[] trackersUniqueIDRole = new uint[16];
	public enum MethodIdentificationTracker {UniqueNumber,Role,Both};
	public  MethodIdentificationTracker methodIdentificationTracker;
	int totalNb = 0;
	public int totalDevicesConnected;
    // Start is called before the first frame update
    void Start()
    {
		SteamVR_Events.DeviceConnected.Listen( OnDeviceConnected );
		
	}

    // Update is called once per frame
    void Update()
    {
		

		if ( totalNb < totalDevicesConnected ) {

		
			for( uint i =1 ; i<=16; i++ ) 
			{

				uint index = i;
				ETrackedPropertyError error = new ETrackedPropertyError();
				StringBuilder uniqueNumber = new StringBuilder();
				StringBuilder role = new StringBuilder( ((int) 64) );
				OpenVR.System.GetStringTrackedDeviceProperty( index, ETrackedDeviceProperty.Prop_SerialNumber_String, uniqueNumber, OpenVR.k_unMaxPropertyStringSize, ref error );
				OpenVR.System.GetStringTrackedDeviceProperty( index, ETrackedDeviceProperty.Prop_ControllerType_String, role, OpenVR.k_unMaxPropertyStringSize, ref error );
				uint LeftHand=OpenVR.System.GetTrackedDeviceIndexForControllerRole(ETrackedControllerRole.LeftHand);
				uint RightHand = OpenVR.System.GetTrackedDeviceIndexForControllerRole( ETrackedControllerRole.RightHand );

				var probablyUniqueDeviceSerial = uniqueNumber.ToString();
				var probablyRole = role.ToString();

				
	
				//This method uses the reference number of each tracker you can get through the binding or Unity or steamVR setting
				if (methodIdentificationTracker==MethodIdentificationTracker.UniqueNumber || methodIdentificationTracker == MethodIdentificationTracker.Both ) {

					switch ( probablyUniqueDeviceSerial ) {
						case "LHR-6FE46329":
							trackersUniqueIDNumber[0] = index;
							totalNb++;
							break;
						case "LHR-B04A9D59":
							trackersUniqueIDNumber[1] = index;
							totalNb++;
							break;
						case "LHR-FE66A922":
							trackersUniqueIDNumber[2] = index;
							totalNb++;
							break;
						case "LHR-840303BC":
							trackersUniqueIDNumber[3] = index;
							totalNb++;
							break;
						case "LHR-020C53C7":
							trackersUniqueIDNumber[4] = index;
							totalNb++;
							break;
						case "LHR-68AE20FA":
							trackersUniqueIDNumber[5] = index;
							totalNb++;
							break;
						case "LHR-2267D321":
							trackersUniqueIDNumber[6] = index;
							totalNb++;
							break;
						case "LHR-E9D1323F":
							trackersUniqueIDNumber[7] = index;
							totalNb++;
							break;
						case "":
							break;

					}
				} // This method uses the official binding system names from steamVR for the Vive Trackers
				
				if ( methodIdentificationTracker == MethodIdentificationTracker.Role || methodIdentificationTracker == MethodIdentificationTracker.Both) 
				{

					switch ( probablyRole ) {
						case "vive_tracker_handed":
							if(index == LeftHand )
							trackersUniqueIDRole[0] = index;
							else if(index==RightHand)
								trackersUniqueIDRole[1] = index;
							totalNb++;
							break;
						case "vive_tracker_left_elbow":
							trackersUniqueIDRole[2] = index;
							totalNb++;
							break;
						case "vive_tracker_right_elbow":
							trackersUniqueIDRole[3] = index;
							totalNb++;
							break;
						case "vive_tracker_left_shoulder":
							trackersUniqueIDRole[4] = index;
							totalNb++;
							break;
						case "vive_tracker_right_shoulder":
							trackersUniqueIDRole[5] = index;
							totalNb++;
							break;
						case "vive_tracker_left_foot":
							trackersUniqueIDRole[6] = index;
							totalNb++;
							break;
						case "vive_tracker_right_foot":
							trackersUniqueIDRole[7] = index;
							totalNb++;
							break;
						case "vive_tracker_left_knee":
							trackersUniqueIDRole[8] = index;
							totalNb++;
							break;
						case "vive_tracker_right_knee":
							trackersUniqueIDRole[9] = index;
							totalNb++;
							break;
						case "vive_tracker_left_chest":
							trackersUniqueIDRole[10] = index;
							totalNb++;
							break;
						case "vive_tracker_right_waist":
							trackersUniqueIDRole[11] = index;
							totalNb++;
							break;
						case "vive_controller":
							trackersUniqueIDRole[12] = index;
							totalNb++;
							break;
						case "":
							break;

					}

				}


			}



		} else
			totalNb = totalDevicesConnected;


	}

	private void OnDeviceConnected ( int index, bool connected )
	{

		ETrackedPropertyError error = new ETrackedPropertyError();
		StringBuilder uniqueNumber = new StringBuilder();
		StringBuilder role = new StringBuilder( ((int) 64) );
		OpenVR.System.GetStringTrackedDeviceProperty( (uint) index, ETrackedDeviceProperty.Prop_SerialNumber_String, uniqueNumber, OpenVR.k_unMaxPropertyStringSize, ref error );
		OpenVR.System.GetStringTrackedDeviceProperty( (uint) index, ETrackedDeviceProperty.Prop_ControllerType_String, role, OpenVR.k_unMaxPropertyStringSize, ref error );


		var probablyUniqueDeviceSerial = uniqueNumber.ToString();
		var probablyRole = role.ToString();

		if (connected) {

			totalDevicesConnected++;
			Debug.Log( string.Format( "{0} is  connected, device {2} Ref: {1}", probablyRole, probablyUniqueDeviceSerial, index ) );
		}

		if(!connected) {

			totalNb--;
			Debug.Log(string.Format("{0} is  disconnected, device {2} Ref: {1}", probablyRole, probablyUniqueDeviceSerial,index) );
		}


	}


}
