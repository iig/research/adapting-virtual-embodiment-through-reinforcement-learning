﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CorrectOriginSteamVR : MonoBehaviour
{
	public KeyCode correctOrigin = KeyCode.O;

	public GameObject steamTracker;
	public float offsetZ=0;

	Vector3 positionToCorrect;

	// Start is called before the first frame update
	void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(correctOrigin)) {

			positionToCorrect=steamTracker.transform.GetChild(0).position-new Vector3(0,0,-offsetZ);

			transform.position =new Vector3( -positionToCorrect.x, -positionToCorrect.y,-positionToCorrect.z);
		}
    }
}
