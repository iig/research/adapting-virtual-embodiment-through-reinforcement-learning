﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

[RequireComponent( typeof( SteamVR_TrackedObject ),typeof(SteamVR_RenderModel) )]
public class SetSteamTracker : MonoBehaviour
{
	public enum TrackersID:int {None, One, Two, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Eleven, Twelve, Thirteen, Fourteen, Fifteen, Sixteen };
	public TrackersID trackerID;
	public enum TrackersRole : int { None, LeftHand, RightHand, LeftElbow, RightElbow, LeftShoulder, RightShoulder, LeftFoot, RightFoot, LefttKnee, RightKnee, Chest, Waist, Controller };
	public TrackersRole trackerRole;
	//public enum MethodIdentificationTracker { UniqueNumber, Role };
	public bool isHandlebyTheManager = true;
	// enum methodTracker {UniqueNumber, Role };
	//public Steam_Manager.MethodIdentificationTracker methodIdentificationTracker;
	public Steam_Manager.MethodIdentificationTracker methodIdentificationTracker;
	SteamVR_TrackedObject steamVRTrackedObject;
	SteamVR_RenderModel steamVRRenderModel;
	


	// Start is called before the first frame update
	void Start()
    {
		steamVRRenderModel = gameObject.GetComponent<SteamVR_RenderModel>();
		steamVRTrackedObject = gameObject.GetComponent<SteamVR_TrackedObject>();
		steamVRRenderModel.enabled = false;

	}
	    
    // Update is called once per frame
    void Update()
    {

		if ( isHandlebyTheManager ) {
			if ( Steam_Manager.Instance.methodIdentificationTracker != Steam_Manager.MethodIdentificationTracker.Both )
				methodIdentificationTracker = Steam_Manager.Instance.methodIdentificationTracker;
			else
				isHandlebyTheManager = false;
		}
	

		if( steamVRTrackedObject.index == SteamVR_TrackedObject.EIndex.None) {
		
			if( methodIdentificationTracker == Steam_Manager.MethodIdentificationTracker.UniqueNumber) 
			{
				switch ( trackerID) {
					case TrackersID.One:
						if( Steam_Manager.trackersUniqueIDNumber[0] != 0 )
						{
								steamVRTrackedObject.SetDeviceIndex( (int) Steam_Manager.trackersUniqueIDNumber[0] );
								steamVRRenderModel.SetDeviceIndex( (int) Steam_Manager.trackersUniqueIDNumber[0] );
						}
						break;
					case TrackersID.Two:
						if ( Steam_Manager.trackersUniqueIDNumber[1] != 0 ) {
								steamVRTrackedObject.SetDeviceIndex( (int) Steam_Manager.trackersUniqueIDNumber[1] );
								steamVRRenderModel.SetDeviceIndex( (int) Steam_Manager.trackersUniqueIDNumber[1] );
							}
						break;
					case TrackersID.Three:
						if ( Steam_Manager.trackersUniqueIDNumber[2] != 0 ) 
						{
								steamVRTrackedObject.SetDeviceIndex( (int) Steam_Manager.trackersUniqueIDNumber[2] );
								steamVRRenderModel.SetDeviceIndex( (int) Steam_Manager.trackersUniqueIDNumber[2] );
						}
						break;
					case TrackersID.Four:
						if ( Steam_Manager.trackersUniqueIDNumber[3] != 0 ) 
						{
								steamVRTrackedObject.SetDeviceIndex( (int) Steam_Manager.trackersUniqueIDNumber[3] );
								steamVRRenderModel.SetDeviceIndex( (int) Steam_Manager.trackersUniqueIDNumber[3] );
						}
						break;
					case TrackersID.Five:
						if ( Steam_Manager.trackersUniqueIDNumber[4] != 0 ) 
						{
								steamVRTrackedObject.SetDeviceIndex( (int) Steam_Manager.trackersUniqueIDNumber[4] );
								steamVRRenderModel.SetDeviceIndex( (int) Steam_Manager.trackersUniqueIDNumber[4] );
						}
						break;
					case TrackersID.Six:
							if ( Steam_Manager.trackersUniqueIDNumber[5] != 0 ) 
							{
								steamVRTrackedObject.SetDeviceIndex( (int) Steam_Manager.trackersUniqueIDNumber[5] );
								steamVRRenderModel.SetDeviceIndex( (int) Steam_Manager.trackersUniqueIDNumber[5] );
							}
						break;
					case TrackersID.Seven:
							if ( Steam_Manager.trackersUniqueIDNumber[6] != 0 )
							{
							
								steamVRTrackedObject.SetDeviceIndex( (int) Steam_Manager.trackersUniqueIDNumber[6] );
								steamVRRenderModel.SetDeviceIndex( (int) Steam_Manager.trackersUniqueIDNumber[6] );
							}
						break;
					case TrackersID.Eight:
							if ( Steam_Manager.trackersUniqueIDNumber[7] != 0 ) {
								steamVRTrackedObject.SetDeviceIndex( (int) Steam_Manager.trackersUniqueIDNumber[7] );
						steamVRRenderModel.SetDeviceIndex( (int) Steam_Manager.trackersUniqueIDNumber[7] );
							}
						break;
					case TrackersID.Nine:
							if ( Steam_Manager.trackersUniqueIDNumber[8] != 0 ) {
								steamVRTrackedObject.SetDeviceIndex( (int) Steam_Manager.trackersUniqueIDNumber[8] );
						steamVRRenderModel.SetDeviceIndex( (int) Steam_Manager.trackersUniqueIDNumber[8] );
							}
						break;
					case TrackersID.Ten:
							if ( Steam_Manager.trackersUniqueIDNumber[9] != 0 ) {
								steamVRTrackedObject.SetDeviceIndex( (int) Steam_Manager.trackersUniqueIDNumber[9] );
						steamVRRenderModel.SetDeviceIndex( (int) Steam_Manager.trackersUniqueIDNumber[9] );
							}
						break;
					case TrackersID.Eleven:
							if ( Steam_Manager.trackersUniqueIDNumber[10] != 0 ) {
								steamVRTrackedObject.SetDeviceIndex( (int) Steam_Manager.trackersUniqueIDNumber[10] );
						steamVRRenderModel.SetDeviceIndex( (int) Steam_Manager.trackersUniqueIDNumber[10] );
							}
						break;
					case TrackersID.Twelve:
							if ( Steam_Manager.trackersUniqueIDNumber[11] != 0 ) {
								steamVRTrackedObject.SetDeviceIndex( (int) Steam_Manager.trackersUniqueIDNumber[11] );
						steamVRRenderModel.SetDeviceIndex( (int) Steam_Manager.trackersUniqueIDNumber[11] );
							}
						break;
					case TrackersID.Thirteen:
							if ( Steam_Manager.trackersUniqueIDNumber[12] != 0 ) {
								steamVRTrackedObject.SetDeviceIndex( (int) Steam_Manager.trackersUniqueIDNumber[12] );
						steamVRRenderModel.SetDeviceIndex( (int) Steam_Manager.trackersUniqueIDNumber[12] );
							}
						break;
					case TrackersID.Fourteen:
							if ( Steam_Manager.trackersUniqueIDNumber[13] != 0 ) {
								steamVRTrackedObject.SetDeviceIndex( (int) Steam_Manager.trackersUniqueIDNumber[13] );
						steamVRRenderModel.SetDeviceIndex( (int) Steam_Manager.trackersUniqueIDNumber[13] ); }
						break;
					case TrackersID.Fifteen:
							if ( Steam_Manager.trackersUniqueIDNumber[14] != 0 ) {
								steamVRTrackedObject.SetDeviceIndex( (int) Steam_Manager.trackersUniqueIDNumber[14] );
						steamVRRenderModel.SetDeviceIndex( (int) Steam_Manager.trackersUniqueIDNumber[14] ); }
						break;
					case TrackersID.Sixteen:
							if ( Steam_Manager.trackersUniqueIDNumber[15] != 0 ) {
								steamVRTrackedObject.SetDeviceIndex( (int) Steam_Manager.trackersUniqueIDNumber[15] );
						steamVRRenderModel.SetDeviceIndex( (int) Steam_Manager.trackersUniqueIDNumber[15] );
							}
						break;
					case TrackersID.None:
							break;

				}
			}
			else if( methodIdentificationTracker == Steam_Manager.MethodIdentificationTracker.Role ) {

				switch ( trackerRole) {
					case TrackersRole.LeftHand:
						if ( Steam_Manager.trackersUniqueIDRole[0] != 0 ) {
							steamVRTrackedObject.SetDeviceIndex( (int) Steam_Manager.trackersUniqueIDRole[0] );
							steamVRRenderModel.SetDeviceIndex( (int) Steam_Manager.trackersUniqueIDRole[0] );
						}
						break;
					case TrackersRole.RightHand:
						if ( Steam_Manager.trackersUniqueIDRole[1] != 0 ) {
							steamVRTrackedObject.SetDeviceIndex( (int) Steam_Manager.trackersUniqueIDRole[1] );
							steamVRRenderModel.SetDeviceIndex( (int) Steam_Manager.trackersUniqueIDRole[1] );
						}
						break;
					case TrackersRole.LeftElbow:
						if ( Steam_Manager.trackersUniqueIDRole[2] != 0 ) {
							steamVRTrackedObject.SetDeviceIndex( (int) Steam_Manager.trackersUniqueIDRole[2] );
							steamVRRenderModel.SetDeviceIndex( (int) Steam_Manager.trackersUniqueIDRole[2] );
						}
						break;
					case TrackersRole.RightElbow:
						if ( Steam_Manager.trackersUniqueIDRole[3] != 0 ) {
							steamVRTrackedObject.SetDeviceIndex( (int) Steam_Manager.trackersUniqueIDRole[3] );
							steamVRRenderModel.SetDeviceIndex( (int) Steam_Manager.trackersUniqueIDRole[3] );
						}
						break;
					case TrackersRole.LeftShoulder:
						if ( Steam_Manager.trackersUniqueIDRole[4] != 0 ) {
							steamVRTrackedObject.SetDeviceIndex( (int) Steam_Manager.trackersUniqueIDRole[4] );
							steamVRRenderModel.SetDeviceIndex( (int) Steam_Manager.trackersUniqueIDRole[4] );
						}
						break;
					case TrackersRole.RightShoulder:
						if ( Steam_Manager.trackersUniqueIDRole[5] != 0 ) {
							steamVRTrackedObject.SetDeviceIndex( (int) Steam_Manager.trackersUniqueIDRole[5] );
							steamVRRenderModel.SetDeviceIndex( (int) Steam_Manager.trackersUniqueIDRole[5] );
						}
						break;
					case TrackersRole.LeftFoot:
						if ( Steam_Manager.trackersUniqueIDRole[6] != 0 ) {
							steamVRTrackedObject.SetDeviceIndex( (int) Steam_Manager.trackersUniqueIDRole[6] );
							steamVRRenderModel.SetDeviceIndex( (int) Steam_Manager.trackersUniqueIDRole[6] );
						}
						break;
					case TrackersRole.RightFoot:
						if ( Steam_Manager.trackersUniqueIDRole[7] != 0 ) {
							steamVRTrackedObject.SetDeviceIndex( (int) Steam_Manager.trackersUniqueIDRole[7] );
							steamVRRenderModel.SetDeviceIndex( (int) Steam_Manager.trackersUniqueIDRole[7] );
						}
						break;
					case TrackersRole.LefttKnee:
						if ( Steam_Manager.trackersUniqueIDRole[8] != 0 ) {
							steamVRTrackedObject.SetDeviceIndex( (int) Steam_Manager.trackersUniqueIDRole[8] );
							steamVRRenderModel.SetDeviceIndex( (int) Steam_Manager.trackersUniqueIDRole[8] );
						}
						break;
					case TrackersRole.RightKnee:
						if ( Steam_Manager.trackersUniqueIDRole[9] != 0 ) {
							steamVRTrackedObject.SetDeviceIndex( (int) Steam_Manager.trackersUniqueIDRole[9] );
							steamVRRenderModel.SetDeviceIndex( (int) Steam_Manager.trackersUniqueIDRole[9] );
						}
						break;
					case TrackersRole.Chest:
						if ( Steam_Manager.trackersUniqueIDRole[10] != 0 ) {
							steamVRTrackedObject.SetDeviceIndex( (int) Steam_Manager.trackersUniqueIDRole[10] );
							steamVRRenderModel.SetDeviceIndex( (int) Steam_Manager.trackersUniqueIDRole[10] );
						}
						break;
					case TrackersRole.Waist:
						if ( Steam_Manager.trackersUniqueIDRole[11] != 0 ) {
							steamVRTrackedObject.SetDeviceIndex( (int) Steam_Manager.trackersUniqueIDRole[11] );
							steamVRRenderModel.SetDeviceIndex( (int) Steam_Manager.trackersUniqueIDRole[11] );
						}
						break;
					case TrackersRole.Controller:
						if ( Steam_Manager.trackersUniqueIDRole[12] != 0 ) {
							steamVRTrackedObject.SetDeviceIndex( (int) Steam_Manager.trackersUniqueIDRole[12] );
							steamVRRenderModel.SetDeviceIndex( (int) Steam_Manager.trackersUniqueIDRole[12] );
						}
						break;
					case TrackersRole.None:
						break;
				}
		    }
			else
				throw new Exception( "You cannot use the both option for a tracker if you use both in the manager you have to uncheck handle by the manager" );

		}   }
}
