﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;


/// <summary>
/// This is the script you need to modify if you change system for the controller
/// Switch On the controller at last to avoid role confusion ( handness) between Vive tracker and controller if not just turn off the controller and turn them on again
/// When steamVR is turning on you need to initialize the controller with a lighthouse and then you can disconnect the lighthouse until you close steamVR 
/// </summary>

public class ControllerExperiment_SteamVR : ControllerExperiment {
	public SteamVR_ActionSet actionSet;
	public SteamVR_Input_Sources role;
	public SteamVR_Action_Boolean action;



	private void Awake () {
		
		
	}

	// Start is called before the first frame update
	void Start()
    {
		//Necessary to be able to activate an actionset ( why???)
		var render = SteamVR_Render.instance;
		actionSet.Activate();

	}

    // Update is called once per frame
    void Update()
    {
		if ( action.GetStateDown( role ) ) //; experiment_ValidationQuestion.stateDown)
		{
			isButtonDown = true;
			isButtonUp = false;
			isButton = false;
		
		} 
		else if ( action.GetStateUp( role ) ) {
			isButtonDown =false;
			isButtonUp = true;
			isButton = false;

		
		}
		else if ( action.GetState( role ) ) {
			isButtonDown = false;
			isButtonUp = false;
			isButton = true;

		}
		else {
			isButtonUp = false;
	
		}

	}
}
