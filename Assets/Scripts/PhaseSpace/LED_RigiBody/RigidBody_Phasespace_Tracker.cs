﻿/**************************************************************************
 * RBTransform.cs defines a coordinate system based on at least 3 tracked 
 * points attached to a rigid body
 * Written by Samuel Gruner and Henrique Galvan Debarba
 * Last update: 05/03/14
 * *************************************************************************/


using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using System.Linq;

//Sidney modif  
//using IIG.Experiment;

//[Serializable]
public class CoordSystemTracker {
	[XmlIgnore]
    public VRPN.Client VRPN_Client;

    [XmlArray("markerNames")]
    [XmlArrayItem("markerName")]
    public string[] markerNames;
	
    //[XmlAttribute("rotOffset")]
	public Quaternion rotOffset;

    //[XmlAttribute("posOffset")]
    public Vector3 posOffset;

	//bool usePrediction = true;
	//LineRotationDESPredictor[] mkrPredictors = new LineRotationDESPredictor[3];
    [XmlIgnore]
	public Vector3DESPredictor[] mkrPredictors = new Vector3DESPredictor[3];
	Vector3[] initPos = new Vector3[3];
	Vector3[] projLength = new Vector3[3];
	float[] radius = new float[3];
	QuaternionDESPredictor qPredictor = new QuaternionDESPredictor(0.4f);


	//int countMissing = 0;

	int[] timeMissing = {0,0,0};

	float damping = 0.95f;
    
	/* public void InitCoordSystem (VRPN.Client vrpnClient, string[] markerNames, Vector3 pOffset, Quaternion rOffset){
        if (markerNames.Length != 3)
        {
            Debug.LogError("Cannot track rigid body out of " + markerNames.Length + "markers, must be exactly 3 (thant's the way Henrique made it).");
            return;
        }

        VRPN_Client = vrpnClient;
        this.markerNames = markerNames;
        rotOffset = rOffset;
		posOffset = pOffset;

        initPos = new Vector3[markerNames.Length];
        projLength = new Vector3[markerNames.Length];
        mkrPredictors = new Vector3DESPredictor[markerNames.Length];
        radius = new float[markerNames.Length];

        for (int i = 0; i < markerNames.Length; i++)
        {
            VRPN_Client.GetMarkerPosition(markerNames[i], out initPos[i]);
            mkrPredictors[i] = new Vector3DESPredictor(0.4f);
        }
        
        for (int i = 0; i < markerNames.Length; i++)
        {
            projLength[i] = Vector3.Project(initPos[i] - initPos[(i + 1) % markerNames.Length], initPos[(i + 2) % markerNames.Length] - initPos[(i + 1) % markerNames.Length]);
            radius[i] = (initPos[i] - (initPos[(i + 1) % markerNames.Length] + projLength[i])).magnitude;
        }

        //initPos [0] = mkr1.position; 		initPos [1] = mkr2.position; 		initPos [2] = mkr3.position;
        //projLength [0] = Vector3.Project (initPos [0] - initPos [1], initPos [2] - initPos [1]);
        //projLength [1] = Vector3.Project (initPos [1] - initPos [2], initPos [0] - initPos [2]);
        //projLength [2] = Vector3.Project (initPos [2] - initPos [0], initPos [1] - initPos [0]);
        //mkrPredictors[0] = new Vector3DESPredictor(0.4f);
        //mkrPredictors[1] = new Vector3DESPredictor(0.4f);
        //mkrPredictors[2] = new Vector3DESPredictor(0.4f);
        //radius[0] = (initPos[0] - (initPos[1] + projLength[0])).magnitude;
        //radius[1] = (initPos [1] - (initPos [2] + projLength[1])).magnitude; 
        //radius[2] = (initPos [2] - (initPos [0] + projLength[2])).magnitude; 
    }
    //*/

    public void CreateCoordSystem (VRPN.Client vrpnClient, string[] markerNames, Vector3 reference, Quaternion refQuat)
    {
        if (markerNames.Length != 3)
        {
            Debug.LogError("Cannot track rigid body out of " + markerNames.Length + "markers, must be exactly 3 (thant's the way Henrique made it).");
            return;
        }

        VRPN_Client = vrpnClient;
        this.markerNames = markerNames;

        initPos = new Vector3[markerNames.Length];
        projLength = new Vector3[markerNames.Length];
        mkrPredictors = new Vector3DESPredictor[markerNames.Length];
        radius = new float[markerNames.Length];

        for (int i = 0; i < markerNames.Length; i++)
        {
            VRPN_Client.GetMarkerPosition(markerNames[i], out initPos[i]);
            mkrPredictors[i] = new Vector3DESPredictor(0.4f);
        }

        rotOffset = Quaternion.Inverse (refQuat) * ComputeRotation (initPos[0], initPos[1], initPos[2]);//rotate the rigid body initial ( convert its coordinate in the local coordinate of the markers)
		Vector3 meanPos = (initPos[0] + initPos[1] + initPos[2]) / 3;
		posOffset = reference - meanPos;
		posOffset = Quaternion.Inverse (refQuat) * posOffset; //translate the rigibody initial (convert its position in the local coordinates of the markers)

        for (int i = 0; i < markerNames.Length; i++)
        {
            projLength[i] = Vector3.Project(initPos[i] - initPos[(i + 1) % markerNames.Length], initPos[(i + 2) % markerNames.Length] - initPos[(i + 1) % markerNames.Length]);
            radius[i] = (initPos[i] - (initPos[(i + 1) % markerNames.Length] + projLength[i])).magnitude;
        }

        //initPos [0] = mkr1.position; 		initPos [1] = mkr2.position; 		initPos [2] = mkr3.position;
        //projLength [0] = Vector3.Project (initPos [0] - initPos [1], initPos [2] - initPos [1]);
		//projLength [1] = Vector3.Project (initPos [1] - initPos [2], initPos [0] - initPos [2]);
		//projLength [2] = Vector3.Project (initPos [2] - initPos [0], initPos [1] - initPos [0]);
		//mkrPredictors[0] = new Vector3DESPredictor(0.4f);
		//mkrPredictors[1] = new Vector3DESPredictor(0.4f);
		//mkrPredictors[2] = new Vector3DESPredictor(0.4f);
		//radius[0] = (initPos [0] - (initPos [1] + projLength[0])).magnitude; 
		//radius[1] = (initPos [1] - (initPos [2] + projLength[1])).magnitude; 
		//radius[2] = (initPos [2] - (initPos [0] + projLength[2])).magnitude; 
	}
	
	Quaternion ComputeRotation(Vector3 pos1, Vector3 pos2, Vector3 pos3){
		
        //1st it converts the three markers system in an orthonormal system.
        // version matching w/ eray implementation
		Vector3 vec1 = (pos3 - pos2).normalized;
		Vector3 vec3 = (pos2 - pos1);//.normalized;
		//Vector3 vec1 = (markers[2].position - markers[1].position).normalized;
		//Vector3 vec3 = (markers [1].position - markers [0].position);//.normalized;
		Vector3 vec2 = Vector3.Cross (vec3, vec1).normalized;
		vec3 = Vector3.Cross (vec1,vec2);
	
        //2nd 
		// orientation matrix based on coord system - it express the rotation from world to the above coord system		
		Matrix4x4 trayOrientationMat = Matrix4x4.identity;
		trayOrientationMat.m00 = vec1.x;
		trayOrientationMat.m01 = vec1.y;
		trayOrientationMat.m02 = vec1.z;
		trayOrientationMat.m10 = vec2.x;
		trayOrientationMat.m11 = vec2.y;
		trayOrientationMat.m12 = vec2.z;
		trayOrientationMat.m20 = vec3.x;
		trayOrientationMat.m21 = vec3.y;
		trayOrientationMat.m22 = vec3.z;
		
		// inverse of the orientation TODO ?? inverse should be == to transpose
		return Quaternion.Inverse (Extensions.QuaternionFromMatrix(trayOrientationMat));
	}
	
	public bool MarkersAreVisible(){
        foreach(string markerName in markerNames)
            if (!VRPN_Client.IsMarkerVisible(markerName))
                return false;

		return true;
	}


	// http://stackoverflow.com/questions/6571202/closest-point-on-circle-in-3d-whats-missing
	// Return a point on the circle with center C, unit normal n and radius r
	// that's closest to the point P. (If all points are closest, return any.)
	Vector3 PointCircleClosest(Vector3 P, Vector3 C, Vector3 n, float r){
		// Translate problem to C-centred coordinates.
		n = n.normalized;
		Vector3 locP = P - C;

		// Project P onto the plane containing the circle.
		Vector3 Q = locP - n * Vector3.Dot (n, locP);

		// If Q is at the centre, all points on the circle are equally close.
		if (Q.sqrMagnitude == 0) {
			Q = Perpendicular (n);
		}

		// Now the nearest point lies on the line through the origin and Q.
		Vector3 R = /*locP -*/ Q * (r / Q.magnitude);

		// Return to original coordinate system.
		return R + C;
	}

	// Return an arbitrary vector that's perpendicular to n.
	Vector3 Perpendicular (Vector3 n){
		if (Mathf.Abs(n[0]) < Mathf.Abs(n[1]))
			return Vector3.Cross(n, Vector3.left);
		else
			return Vector3.Cross(n, Vector3.up);
	}

    /// <summary>
    /// Give the rotation and the position of the rigidbody (at least 3 markers)
    /// </summary>
    public float GetPosAndRot (out Vector3 position ,out Quaternion rotation, bool predictMkr/*, bool predictCS*/){
        //Vector3[] mkrsP = {markers [0].position, markers [1].position, markers [2].position};
        Vector3[] markerPositions = new Vector3[markerNames.Length];
        for (int i = 0; i < markerNames.Length; i++)
        {
            VRPN_Client.GetMarkerPosition(markerNames[i], out markerPositions[i]);

            //Debug.Log(markerPositions[i]);
        }
            
            

        float reliable = 0;
		if (predictMkr) {
			//List<int> missing = new List<int>();
			Dictionary<int, int> missing = new Dictionary<int, int>();
			List<int> notMissing = new List<int>();
			for (int i = 0; i < 3; i++){
				if (!VRPN_Client.IsMarkerVisible(markerNames[i]))
                {
					// array with time the marker is missing
					timeMissing[i]++;
					// makes sure that this key is unique
					while (missing.ContainsKey(timeMissing[i]))
						timeMissing[i] ++;
					// list of missing values
					missing.Add(timeMissing[i],i);
				}else{
					timeMissing[i] = 0;
					notMissing.Add(i);
				}
			}

			reliable = 1.0f - missing.Count * 0.3333f;

			// Acquire keys and sort them.
			List<int> keys = missing.Keys.ToList();
			keys.Sort();
			int pivot = -1;
			// for markers that are missing
			for (int j = keys.Count-1; j >=0 ; j--){
				int i = missing[keys[j]];

				Vector3 predicted = mkrPredictors[i].Predict(damping);
				// last missing marker predictor, project point int o circle
				if (j == 0 && mkrPredictors[i].initialized){
					int ii =  (i+1)%3;
					int iii =  (i+2)%3;
					markerPositions[i] = PointCircleClosest(predicted, markerPositions[ii] + (markerPositions[iii] - markerPositions[ii]).normalized * projLength [i].magnitude,
					                              markerPositions[iii] - markerPositions[ii], radius[i]);
					//mkrsP[i] = mkrsP[ii] + (mkrsP[iii] - mkrsP[ii]).normalized * projLength [i].magnitude + mkrPredictors[i].Predict (mkrsP[iii] - mkrsP[ii]);
				}
				// if a second marker is missing, project it into a sphere
				if (j == 1 && mkrPredictors[i].initialized){
					if (notMissing.Count != 0)
						pivot = notMissing[0]; 
					markerPositions[i] = markerPositions[pivot]+ (predicted - markerPositions[pivot]).normalized * (initPos[pivot] - initPos[i]).magnitude;
				}
				// if a third marker is missing, predict it
				if (j == 2 && mkrPredictors[i].initialized){
					markerPositions[i] = predicted;
					pivot = i;
				}
				mkrPredictors[i].UpdateModel(markerPositions[i]);
			}

			// for markers that are not missing: update its model and get its position
			for (int j = 0; j < notMissing.Count; j++){
				int i = notMissing[j];
				mkrPredictors[i].UpdateModel(markerPositions[i]);
				//projLength [i] = Vector3.Project (mkrsP[i] - mkrsP[ii], mkrsP[iii] - mkrsP[ii]);
				//mkrPredictors[i].UpdateModel (mkrsP[i] - (mkrsP[ii] + projLength [i]));
				if (mkrPredictors[i].initialized){
					markerPositions[i] = mkrPredictors[i].Predict(0);
					//mkrsP[i] = mkrsP[ii] + (mkrsP[iii] - mkrsP[ii]).normalized * projLength [i].magnitude + mkrPredictors[i].Predict (mkrsP[iii] - mkrsP[ii], 0);	
				}

			}
		} else {
			reliable = MarkersAreVisible () ? 1.0f : 0.0f;
		}

		//rotation = ComputeRotation (markerPositions[0], markerPositions[1], markerPositions[2]) * Quaternion.Inverse (rotOffset);
		Vector3 meanPos = (markerPositions[0] + markerPositions[1] + markerPositions[2]) / 3;
        //position = meanPos + (rotation * posOffset);

        rotation = ComputeRotation(markerPositions[0], markerPositions[1], markerPositions[2]);
        position = meanPos;

		return reliable;
	}
	
}

public class RigidBody_Phasespace_Tracker : MonoBehaviour {
	
	public enum UpdateCallback {UPDATE, LATE_UPDATE, FIXED_UPDATE};
	public UpdateCallback callback = UpdateCallback.LATE_UPDATE;
    public bool isTrackerReady=true;
   // [Tooltip("This means that the script will additionally check in the experiment setup whether it should calibrate")]
   // public bool experimentCalibrarble = false;
    public KeyCode calibrateKey = KeyCode.C;
    public KeyCode saveKey = KeyCode.S;

    [Header("Tracking options")]
    public bool updatePosition = true;
    public bool updateRotation = true;
	public bool usePrediction = false;
    public bool updatePositionCustom = true;
    public bool updateRotationCustom = true;
    public GameObject lefHandOriginalTracker;
    public GameObject rightHandOriginalTracker;
    public string[] trackersCustomUpdate;
	//public Transform controlledTr;
    public GameObject phasespaceTracker;
    public List<GameObject> phasespaceRigidbody = new List<GameObject>();

    [Header("Tracking")]
    public string [] markerNames;

    //TODO : be able to work from a file 
    public IIG.MocapInputController mocapInputController = null;

    [Header("Saving")]
    public bool loadSavedCalibration = true;
	public string persistenceFileName = "";

	public bool initialized { get; private set; }
    public List<CoordSystemTracker> coordSystems = new List<CoordSystemTracker>();

	//bool usePrediction = true;
	QuaternionDESPredictor qPredictor = new QuaternionDESPredictor(0.5f);
	Vector3DESPredictor pPredictor = new Vector3DESPredictor(0.5f);
	//int countMissing = 0;
    bool goPosition=true ;
    bool goRotation=true;
	
	void Start() {
		//if (controlledTr == null)
			//controlledTr = this.transform;

        if (persistenceFileName == "")
        {
            persistenceFileName = this.gameObject.name;
            persistenceFileName = "./" + persistenceFileName + "_saved.xml";
        }
        /*THIBAULT
		if (loadSavedCalibration)
			Load();*/
	}

    /// <summary>
    /// Copies back the values of a loaded state object into this instance.
    /// </summary>
    /// <param name="copy">The RB tracker state to be copied.</param>
    /*THIBAULT
    private void LoadCalibration(RigidBodyTrackerState copy)
    {
        markerNames = copy.markerNames;
        coordSystems = copy.coordSystems;

        foreach (CoordSystemTracker cs in coordSystems)
        {
            cs.VRPN_Client = mocapInputController.VRPNOptions.vrpn_client;

            for (int i = 0; i < cs.mkrPredictors.Length; i++)
                cs.mkrPredictors[i] = new Vector3DESPredictor(0.4f);
        }

        initialized = true;
    }*/

    /// <summary>
    /// Performs RB calibration.
    /// </summary>
    public void InitializeCS()
    {
        
        coordSystems.Clear();
       
      //  if( markerNames.Length%3 == 0)
      
        //TO DO fILL makerNames table according to the makerfile ( directly looking for the marker tag in the scene or from the file that read it ) 
            for (int i = 0; i < markerNames.Length; i++)
            {

           
                var first = i;
                var second = (i + 1) % markerNames.Length;
                var third = (i + 2) % markerNames.Length;


          /*  if (markerNames[third]==null)//markerNames.Length <(i+2) )
            {
                Debug.Log("olaaaa");
                GameObject newRigidbody2 = Instantiate(phasespaceTracker, transform.position, transform.rotation);
                newRigidbody2.name = markerNames[first];
                newRigidbody2.transform.parent = transform;
                phasespaceRigidbody.Add(newRigidbody2);
                break;
            }*/
            string[] threeMarkerNames = { markerNames[first], markerNames[second], markerNames[third] };

            CoordSystemTracker newCS = new CoordSystemTracker();

                GameObject newRigidbody = Instantiate(phasespaceTracker, transform.position, transform.rotation);
                newRigidbody.name = markerNames[first];
                newRigidbody.transform.parent = transform;


                newCS.CreateCoordSystem(mocapInputController.VRPNOptions.vrpn_client, threeMarkerNames, newRigidbody.transform.position, newRigidbody.transform.rotation);


                phasespaceRigidbody.Add(newRigidbody);

                coordSystems.Add(newCS);

                i = third;

            }
      


        if (coordSystems.Count > 0)
			initialized = true;
	}
    /*THIBAULT
    private void Save()
    {
        RigidBodyTrackerState rigidBodyTrackerState = new RigidBodyTrackerState(this);
        rigidBodyTrackerState.Save(persistenceFileName);
    }*/

    /*THIBAULT
    private void Load()
    {
        var loaded = RigidBodyTrackerState.Load(persistenceFileName);
        LoadCalibration(loaded);
        initialized = true;
    }*/

	void UpdateTransform(){

        if ((Input.GetKeyDown(calibrateKey) && !initialized) || (!initialized && Input.GetKeyDown(KeyCode.Joystick1Button1)) ||
            (!initialized && isTrackerReady))
        {
            InitializeCS();
            isTrackerReady = false;
        }
			

       /*THIBAULT
       if (Input.GetKeyDown(saveKey))
            Save();*/

        if (!initialized)
			return;

       

		for (int i = 0; i < coordSystems.Count; i++) {
			Quaternion ithCSRot;
			Vector3 ithCSPos;
			float reliable = coordSystems[i].GetPosAndRot(out ithCSPos, out ithCSRot, usePrediction);
            //Debug.Log("Tracker Name"+ phasespaceRigidbody[i].transform .name+ "position =" + ithCSPos + "Rotation=" + ithCSRot);


            if (reliable > 0){
                if (!float.IsNaN(ithCSPos.x))
                {
                    if (updatePosition)
                    {
                 
                        if(!updatePositionCustom && (trackersCustomUpdate.Length > 0) )
                        {
                            
                            for (int h = 0; h < trackersCustomUpdate.Length; h++)
                            {
                             
                                if (phasespaceRigidbody[i].name == trackersCustomUpdate[h])
                                {
                                    goPosition = false;
                                }

                            }
                        }
                   

                        if(goPosition)
                        {
                            pPredictor.UpdateModel(ithCSPos);
                            phasespaceRigidbody[i].transform.position = ithCSPos;

                            if (phasespaceRigidbody[i].name == "LHAN")
                            {
                                lefHandOriginalTracker.transform.position = ithCSPos;
                            }

                            if (phasespaceRigidbody[i].name == "RHAN")
                            {
                                rightHandOriginalTracker.transform.position = ithCSPos;
                            }

                        }
                        else
                        {
                           
                            //update another tracker to keep the trajectory
                            if (phasespaceRigidbody[i].name == "LHAN")
                            {
                                pPredictor.UpdateModel(ithCSPos);
                                lefHandOriginalTracker.transform.position = ithCSPos;

                            }

                            if (phasespaceRigidbody[i].name == "RHAN")
                            {
                                pPredictor.UpdateModel(ithCSPos);
                                rightHandOriginalTracker.transform.position = ithCSPos;
                           
                            }

                        }
                        goPosition = true;

                    }
                    //Debug.Log(ithCSPos);
                    if (updateRotation)
                    {
                        if(!updateRotationCustom && (trackersCustomUpdate.Length > 0))
                        {
                            for (int h = 0; h < trackersCustomUpdate.Length; h++)
                            {
                                if (phasespaceRigidbody[i].name == trackersCustomUpdate[h])
                                {
                                    goRotation = false;
                                }

                            }

                        }
                      

                        if (goRotation)
                        {
                            qPredictor.UpdateModel(ithCSRot);
                            phasespaceRigidbody[i].transform.rotation = ithCSRot;

                            if (phasespaceRigidbody[i].name == "LHAN")
                            {
                                lefHandOriginalTracker.transform.rotation = ithCSRot;

                            }

                            if (phasespaceRigidbody[i].name == "RHAN")
                            {
                                rightHandOriginalTracker.transform.rotation = ithCSRot;
                            }
                        }
                        else
                        {
   
                            //update another tracker to keep the trajectory
                            if (phasespaceRigidbody[i].name == "LHAN")
                            {
                                qPredictor.UpdateModel(ithCSRot);
                                lefHandOriginalTracker.transform.rotation = ithCSRot;



                            }

                            if (phasespaceRigidbody[i].name == "RHAN")
                            {
           
                                qPredictor.UpdateModel(ithCSRot);
                                rightHandOriginalTracker.transform.rotation = ithCSRot;
                            }

                        }

                        goRotation = true;
                    }
 
                }
                else if (coordSystems[i].MarkersAreVisible())
                {
                    Debug.Log(phasespaceRigidbody[i].name);
                    coordSystems[i].CreateCoordSystem(mocapInputController.VRPNOptions.vrpn_client, coordSystems[i].markerNames, phasespaceRigidbody[i].transform.position, phasespaceRigidbody[i].transform.rotation);
                }
                else if( coordSystems.Count% 2 != 0)
                {
                    if(i == coordSystems.Count - 1)
                    {
                        Vector3 meanPosition = Vector3.zero;
                        int nbInt = 0;

                        for (int j = markerNames.Length - 3; j < markerNames.Length; j++)
                        {
                            Vector3 markerPosition = Vector3.zero;
                            mocapInputController.VRPNOptions.vrpn_client.GetMarkerPosition(markerNames[j], out markerPosition);


                            if (!float.IsNaN(markerPosition.x))
                                meanPosition = meanPosition + markerPosition;

                            nbInt = +1;
                        }

                        meanPosition = meanPosition / (nbInt);

                        if (!float.IsNaN(meanPosition.x))
                        {
                            phasespaceRigidbody[phasespaceRigidbody.Count - 1].transform.position = meanPosition;
                            phasespaceRigidbody[phasespaceRigidbody.Count - 1].GetComponent<Renderer>().material.color = new Color(0, 0, 0);
                        }
                    }
               
                   


                }
                 



            }
        }

     /*   if( markerNames.Length%3 != 0)
        {
            Vector3 meanPosition = Vector3.zero;

            for(int i=0; i<markerNames.Length %3;i++ )
            {
                Vector3 markerPosition = Vector3.zero;
                mocapInputController.VRPNOptions.vrpn_client.GetMarkerPosition(markerNames[markerNames.Length-1-i], out markerPosition);
                meanPosition = meanPosition+markerPosition;
            }

            meanPosition = meanPosition /(markerNames.Length % 3);
            phasespaceRigidbody[phasespaceRigidbody.Count-1].transform.position = meanPosition;
        }*/
    }

    void FixedUpdate(){
		if (callback == UpdateCallback.FIXED_UPDATE)
			UpdateTransform ();
	}

	void LateUpdate(){
		if (callback == UpdateCallback.LATE_UPDATE)
			UpdateTransform ();
	}

	void Update(){
		if (callback == UpdateCallback.UPDATE)
			UpdateTransform ();
	}
}
/*THIBAULT
[XmlRoot("CoordinateSystemCollection")]
public class RigidBodyTrackerState
{
    public string[] markerNames;

    [XmlArray("CoordinateSystems")]
    [XmlArrayItem("CoordinateSystem")]
    public List<CoordSystemTracker> coordSystems = new List<CoordSystemTracker>();

    //    public RigidBodyTrackerState(bool updateRotation, bool usePrediction, string[] markerNames, List<CoordSystemTracker> coordSystems)
    
    public RigidBodyTrackerState(RigidBodyTracker toCopy)
    {
        this.markerNames = toCopy.markerNames;
        this.coordSystems = toCopy.coordSystems;
    }

    
public RigidBodyTrackerState() { }

        
    public void Save(string path)
    {
        var serializer = new XmlSerializer(typeof(RigidBodyTrackerState));
        using (var stream = new FileStream(path, FileMode.Create))
        {
            serializer.Serialize(stream, this);
        }
    }

    public static RigidBodyTrackerState Load(string path)
    {
        var serializer = new XmlSerializer(typeof(RigidBodyTrackerState));
        using (var stream = new FileStream(path, FileMode.Open))
        {
            return serializer.Deserialize(stream) as RigidBodyTrackerState;
        }
    }
}*/
