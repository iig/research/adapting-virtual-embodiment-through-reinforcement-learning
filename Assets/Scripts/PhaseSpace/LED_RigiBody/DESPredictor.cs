﻿using UnityEngine;
using System.Collections;

public class FloatDESPredictor {
	public bool initialized { get; protected set;}
	float lastSpt;
	float lastSpt2;
	public float alpha = 0.4f;
	protected int timeIntervals = 0;

	public FloatDESPredictor(){ initialized = false;}

	public FloatDESPredictor(float newalpha){
		alpha = newalpha;
		initialized = false;
	}

	public void UpdateModel (float current, float newalpha){
		alpha = newalpha;
		UpdateModel (current);
	}
	// update the double exponential smoothing predictor variables
	public void UpdateModel (float current){
		if (!initialized) {
			lastSpt = lastSpt2 = current;
			initialized = true;
		}

		// compute new exponential smoothing variables
		float spt = alpha * current + (1 - alpha) * lastSpt;
		float spt2 = alpha * spt + (1 - alpha) * lastSpt2;

		// keep these new values
		lastSpt = spt;
		lastSpt2 = spt2;
		timeIntervals = 0;
	}

	public float StepPredict (){
		timeIntervals++;
		return Predict (timeIntervals);
	}

	// predict the outcome for # time intervals
	public float Predict (int deltaTime){
		if (!initialized)
			return 0;

		float fraction = (alpha * deltaTime) / (1 - alpha);
		return (2 + fraction) * lastSpt - (1 + fraction) * lastSpt2;
	}

	// predict the outcome for #.# time intervals
	public float Predict (float deltaTime){
		if (!initialized)
			return 0;

		// interpolate between two time intervals
		float floorDT = Mathf.Floor (deltaTime);

		float phi = Predict ((int)Mathf.Ceil (deltaTime));
		float plo = Predict ((int)floorDT);

		return (phi - plo) * (deltaTime - floorDT) + plo;
	}
}


public class Vector3DESPredictor {
	public bool initialized { get; protected set;}
	Vector3 lastSpt;
	Vector3 lastSpt2;
	public float alpha = 0.4f;
	int timeIntervals = 0;

	public Vector3DESPredictor(float newalpha){
		initialized = false;
		alpha = newalpha;
	}

	// update alpha and the double exponential smoothing predictor variables
	public void UpdateModel (Vector3 current, float newalpha){
		alpha = newalpha;
		UpdateModel (current);
	}

	// update the double exponential smoothing predictor variables
	public void UpdateModel (Vector3 current){
		if (!initialized) {
			lastSpt = lastSpt2 = current;
			initialized = true;
		}
		// compute new exponential smoothing variables
		Vector3 spt = Vector3.Lerp (lastSpt, current, alpha);
		Vector3 spt2 = Vector3.Lerp (lastSpt2, spt, alpha);
		
		// keep these new values
		lastSpt = spt;
		lastSpt2 = spt2;
		timeIntervals = 0;
	}

	public Vector3 StepPredict (){
		timeIntervals++;
		return Predict (timeIntervals);
	}
	// predict the outcome for # time intervals
	public Vector3 Predict (int deltaTime){
		if (!initialized)
			return Vector3.zero;

		float fraction = (alpha * deltaTime) / (1 - alpha);
		return (2 + fraction) * lastSpt - (1 + fraction) * lastSpt2;
	}
	
	// predict the outcome for #.# time intervals
	public Vector3 Predict (float deltaTime){
		if (!initialized)
			return Vector3.zero;

		// interpolate between two time intervals
		float floorDT = Mathf.Floor (deltaTime);
		
		Vector3 phi = Predict ((int)Mathf.Ceil (deltaTime));
		Vector3 plo = Predict ((int)floorDT);
		
		return (phi - plo) * (deltaTime - floorDT) + plo;
	}
}

public class QuaternionDESPredictor {
	bool initialized = false;
	Quaternion lastSpt;
	Quaternion lastSpt2;
	public float alpha = 0.4f;
	int timeIntervals = 0;

	public QuaternionDESPredictor(float newalpha){
		alpha = newalpha;
	}

	// update alpha and the double exponential smoothing predictor variables
	public void UpdateModel (Quaternion current, float newalpha){
		alpha = newalpha;
		UpdateModel (current);
	}
	
	// update the double exponential smoothing predictor variables
	public void UpdateModel (Quaternion current){
		if (!initialized) {
			lastSpt = lastSpt2 = current;
			initialized = true;
		}
		// compute new exponential smoothing variables
		Quaternion spt = Quaternion.Lerp (lastSpt, current, alpha);
		Quaternion spt2 = Quaternion.Lerp (lastSpt2, spt, alpha);
		
		// keep these new values
		lastSpt = spt;
		lastSpt2 = spt2;
		timeIntervals = 0;
	}

	public Quaternion StepPredict (){
		timeIntervals++;
		return Predict (timeIntervals);
	}

	// predict the outcome for # time intervals
	public Quaternion Predict (int deltaTime){
		if (!initialized)
			return Quaternion.identity;
		float fraction = (alpha * deltaTime) / (1 - alpha);
		float x = (2 + fraction) * lastSpt.x - (1 + fraction) * lastSpt2.x;
		float y = (2 + fraction) * lastSpt.y - (1 + fraction) * lastSpt2.y;
		float z = (2 + fraction) * lastSpt.z - (1 + fraction) * lastSpt2.z;
		float w = (2 + fraction) * lastSpt.w - (1 + fraction) * lastSpt2.w;
		float magn = Mathf.Sqrt(x*x + y*y + z*z + w*w);
		return new Quaternion(x/magn, y/magn, z/magn, w/magn);
	}

}



public class LineRotationDESPredictor : FloatDESPredictor {
	Vector3[] lastVector = new Vector3[5];

	public LineRotationDESPredictor(){ initialized = false;}
	
	public LineRotationDESPredictor(float newalpha){
		alpha = newalpha;
		initialized = false;
	}

	// update alpha and the double exponential smoothing predictor variables
	public void UpdateModel (Vector3 current, float newalpha){
		alpha = newalpha;
		UpdateModel (current);
	}

	// update alpha and the double exponential smoothing predictor variables
	public void UpdateModel (Vector3 current){
		// need two samples as change in angle is the derivative of rotation
		if (!initialized) {
			Debug.Log("lastVecUpdated");
			for (int i = 0; i < lastVector.Length; i++)
				lastVector[i] = current;
			initialized = true;
			return;
		}
		float angle = Vector3.Angle (lastVector[0], current);
		UpdateModel (angle);
		for (int i = lastVector.Length-1; i > 0; i--)
			lastVector[i-1] = lastVector[i];
		lastVector[lastVector.Length-1] = current;
	}

	public Vector3 Predict (Vector3 axis){
		if (!initialized)
			return Vector3.zero;
		float deltaAngle = StepPredict ();
		Vector3 towards = Vector3.Cross (axis.normalized, lastVector[0].normalized);
		lastVector[0] = Vector3.Cross (towards.normalized, axis.normalized) * lastVector[0].magnitude;
		towards = Vector3.RotateTowards (lastVector[0], towards, Mathf.Deg2Rad * deltaAngle, 0);
		return towards;
	}
	public Vector3 Predict (Vector3 axis, int deltaTime){
		if (!initialized)
			return Vector3.zero;
		float deltaAngle = Predict (deltaTime);
		Vector3 towards = Vector3.Cross (axis.normalized, lastVector[0].normalized);
		lastVector[0] = Vector3.Cross (towards.normalized, axis.normalized) * lastVector[0].magnitude;
		towards = Vector3.RotateTowards (lastVector[0], towards, Mathf.Deg2Rad * deltaAngle, 0).normalized * lastVector[0].magnitude ;
		return towards;
	}

}

public class DESPredictor{


};

