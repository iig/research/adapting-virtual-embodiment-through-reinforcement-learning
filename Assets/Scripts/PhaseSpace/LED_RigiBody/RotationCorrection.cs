﻿using UnityEngine;
using System.Collections;

/// <summary>
/// This script is not working at all.
/// The HMD's orientation cannot be changed here.
/// </summary>
public class RotationCorrection : MonoBehaviour {

	public Transform correct;
	public Transform drifting;
    public Transform trackingSpace;

    public bool initOnly = false;
    public bool drawRays = false;

    Quaternion lastDelta;
	Quaternion correction = Quaternion.identity;

	// smoothing factor should be very small, so that the correction
	// to the sensor drift is unnoticeable
	[Range(0.00001f,1f)]
	public float smoothingFactor = 0.005f;

	void LateUpdate ()
    {

        if (initOnly && Time.frameCount == 10)
        {
            var rotation = Quaternion.Inverse(Quaternion.RotateTowards(drifting.rotation, correct.rotation, 1000f));
            rotation = Quaternion.Euler(0f, rotation.eulerAngles.y, 0f);
            trackingSpace.rotation *= rotation;
        }
        else if (!initOnly)
        {
            lastDelta = correction;
            // orientation difference between oculus and calibrated coordinate system
            Quaternion currentDelta = (correct.rotation) * Quaternion.Inverse(Quaternion.Inverse(lastDelta) * drifting.localRotation);// * );


            correction = Quaternion.Slerp(correction, currentDelta, smoothingFactor);
            correction = Quaternion.Euler(0f, correction.eulerAngles.y, 0f);

#if THIS_IS_NOT_WORKING
        var newRot = Quaternion.Slerp(drifting.rotation, correct.rotation, smoothingFactor);
        newRot = Quaternion.Euler(0f, newRot.eulerAngles.y, 0f);
        drifting.rotation = newRot;
#else
            trackingSpace.rotation = correction;
            //this.transform.rotation = correction;
#endif
        }

        if (drawRays)
        {
            Debug.DrawRay(correct.position, correct.forward, Color.green);
            Debug.DrawRay(drifting.position, drifting.forward, Color.magenta);
            Debug.DrawRay(trackingSpace.position, trackingSpace.forward, Color.blue);
        }
    }
}
