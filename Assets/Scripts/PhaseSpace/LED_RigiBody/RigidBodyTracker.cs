﻿/**************************************************************************
 * RBTransform.cs defines a coordinate system based on at least 3 tracked 
 * points attached to a rigid body
 * Written by Samuel Gruner and Henrique Galvan Debarba
 * Modified by Thibault Porssut
 * Last update: 21/05/19
 * *************************************************************************/


using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using System.Linq;
//using IIG.Experiment;

//[Serializable]
namespace IIG
{



    public class CoordSystemTracker
    {
        [XmlIgnore]
        public VRPN.Client VRPN_Client;

        [XmlArray("markerNames")]
        [XmlArrayItem("markerName")]
        public string[] markerNames;
	

		//[XmlAttribute("rotOffset")]
		public Quaternion rotOffset;

        //[XmlAttribute("posOffset")]
        public Vector3 posOffset;

        //bool usePrediction = true;
        //LineRotationDESPredictor[] mkrPredictors = new LineRotationDESPredictor[3];
        [XmlIgnore]
        public Vector3DESPredictor[] mkrPredictors = new Vector3DESPredictor[3];
        Vector3[] initPos = new Vector3[3];



       public bool CreateCoordSystem(VRPN.Client vrpnClient, string[] markerNames, Vector3 reference, Quaternion refQuat,bool isDebug)
        {
            if (markerNames.Length != 3)
            {
                Debug.LogError("Cannot track rigid body out of " + markerNames.Length + "markers, must be exactly 3 (thant's the way Henrique made it).");
                return false;
            }
           
            VRPN_Client = vrpnClient;
            this.markerNames = markerNames;

            initPos = new Vector3[markerNames.Length];
 

            for (int i = 0; i < markerNames.Length; i++)
            { 
				if (isDebug)
					GetMarkerPositionDebug( markerNames[i], out initPos[i] );
				else
					VRPN_Client.GetMarkerPosition( markerNames[i], out initPos[i] );
            }

            rotOffset = Quaternion.Inverse(refQuat) * ComputeRotation(initPos[0], initPos[1], initPos[2]);
            Vector3 meanPos = (initPos[0] + initPos[1] + initPos[2]) / 3;
            posOffset = reference - meanPos;
            posOffset = Quaternion.Inverse(refQuat) * posOffset;

			return QualityCoordSystem( initPos[0], initPos[1], initPos[2]) ;

		}

        Quaternion ComputeRotation(Vector3 pos1, Vector3 pos2, Vector3 pos3)
        {
            // version matching w/ eray implementation
            Vector3 vec1 = (pos3 - pos2).normalized;
            Vector3 vec3 = (pos2 - pos1);//.normalized;
                                         //Vector3 vec1 = (markers[2].position - markers[1].position).normalized;
                                         //Vector3 vec3 = (markers [1].position - markers [0].position);//.normalized;
            Vector3 vec2 = Vector3.Cross(vec3, vec1).normalized;
            vec3 = Vector3.Cross(vec1, vec2);

			float t1 = Vector3.Dot( vec1, vec2 );
			float t2 = Vector3.Dot( vec1, vec3 );
			float t3 = Vector3.Dot( vec2, vec3 );
			

			// orientation matrix based on coord system - it express the rotation from world to the above coord system		
			Matrix4x4 trayOrientationMat = Matrix4x4.identity;
            trayOrientationMat.m00 = vec1.x;
            trayOrientationMat.m01 = vec1.y;
            trayOrientationMat.m02 = vec1.z;
            trayOrientationMat.m10 = vec2.x;
            trayOrientationMat.m11 = vec2.y;
            trayOrientationMat.m12 = vec2.z;
            trayOrientationMat.m20 = vec3.x;
            trayOrientationMat.m21 = vec3.y;
            trayOrientationMat.m22 = vec3.z;

            // inverse of the orientation TODO ?? inverse should be == to transpose
            return Quaternion.Inverse(Extensions.QuaternionFromMatrix(trayOrientationMat));
        }

		bool MarkersAreVisible()
        {
            foreach (string markerName in markerNames)
                if (!VRPN_Client.IsMarkerVisible(markerName))
                    return false;

            return true;
        }



		public bool GetPosAndRot(out Vector3 position, out Quaternion rotation, bool isDebug)
        {
            Vector3[] markerPositions = new Vector3[markerNames.Length];
            for (int i = 0; i < markerNames.Length; i++)
			{
				if(isDebug)
					GetMarkerPositionDebug( markerNames[i], out markerPositions[i] );
				else
				VRPN_Client.GetMarkerPosition( markerNames[i], out markerPositions[i] );


			}
                

			
            rotation = ComputeRotation(markerPositions[0], markerPositions[1], markerPositions[2]) * Quaternion.Inverse(rotOffset);
            Vector3 meanPos = (markerPositions[0] + markerPositions[1] + markerPositions[2]) / 3;
            position = meanPos + (rotation * posOffset);

			if ( isDebug )
				return DebugMarkersAreVisible() ? true : false; 
			else
				return MarkersAreVisible() ? true : false;
        }

		bool QualityCoordSystem ( Vector3 pos1, Vector3 pos2, Vector3 pos3 ) 		 {
			
			// version matching w/ eray implementation
			Vector3 vec1 = (pos3 - pos2).normalized;
			Vector3 vec2 = (pos2 - pos1).normalized;
			//Vector3 vec1 = (markers[2].position - markers[1].position).normalized;
			//Vector3 vec3 = (markers [1].position - markers [0].position);//.normalized;
			Vector3 vec3 = Vector3.Cross( vec2, vec1 );

			float qualityScore = Vector3.Magnitude( vec3 );


			if ( qualityScore < 0.5f ) {
				Debug.Log( "rejected" );
				return false;
			} else {
				Debug.Log( "accepted" );
				return true;
			}


		}

		void GetMarkerPositionDebug ( string markerName, out Vector3 initPos) 
		{
			initPos=GameObject.Find(markerName).transform.position;
		}

		bool DebugMarkersAreVisible () {
			foreach ( string markerName in markerNames )
				if ( GameObject.Find( markerName ).tag == "markerNotVisible" )
					return false;

			return true;
		}

	
	}

    public class RigidBodyTracker : MonoBehaviour
    {

        public enum UpdateCallback { UPDATE, LATE_UPDATE, FIXED_UPDATE };
        public UpdateCallback callback = UpdateCallback.LATE_UPDATE;
        [Tooltip("This means that the script will additionally check in the experiment setup whether it should calibrate")]
        public bool experimentCalibrarble = false;
        public KeyCode calibrateKey = KeyCode.C;
        public KeyCode saveKey = KeyCode.S;
		public KeyCode occlusionMarkerDebugOnlyKey = KeyCode.O;


		[Header("Tracking options")]
        public bool updateRotation = true;
		public bool updatePosition = true;
		public bool redundancy = true;
        public Transform controlledTr;

        [Header("Tracking")]
        public string[] markerNames;
		public string[] markerNamesDebugOnlyOccluded;
		public IIG.MocapInputController mocapInputController = null;

        [Header("Saving")]
        public bool loadSavedCalibration = true;
        public string persistenceFileName = "";

        public bool initialized { get; private set; }
        public List<CoordSystemTracker> coordSystems = new List<CoordSystemTracker>();

		public GameObject prefabDebug;
		public List<GameObject> listDebug = new List<GameObject>();
		public bool isDebug;

		List<Quaternion> allRotationFromCoordsys=new List<Quaternion>();
		List<Vector3> allPositionFromCoordsys = new List<Vector3>();
		Quaternion Rot=Quaternion.identity;
		Vector3 Pos=Vector3.zero;

		/*Vector4 cumulative =  Vector4.zero;
		Vector3 vectorAdded = Vector4.zero;
		Quaternion RotTemp = Quaternion.identity;
		Vector3 PosTemp = Vector3.zero;
		Quaternion ithCSRot = Quaternion.identity;
		Vector3 ithCSPos = Vector4.zero;*/
		Quaternion RotTemp = Quaternion.identity;
		Vector3 PosTemp = Vector3.zero;
		Vector4 cumulative = Vector4.zero;
		Vector3 vectorAdded = Vector4.zero;

		void Start()
        {
            if (controlledTr == null)
                controlledTr = this.transform;

            if (persistenceFileName == "")
            {
                persistenceFileName = this.gameObject.name;
                persistenceFileName = "./" + persistenceFileName + "_saved.xml";
            }

            if (loadSavedCalibration)
                Load();
        }

        /// <summary>
        /// Copies back the values of a loaded state object into this instance.
        /// </summary>
        /// <param name="copy">The RB tracker state to be copied.</param>
        private void LoadCalibration(RigidBodyTrackerState copy)
        {
            markerNames = copy.markerNames;
            coordSystems = copy.coordSystems;


            foreach (CoordSystemTracker cs in coordSystems)
            {
                cs.VRPN_Client = mocapInputController.VRPNOptions.vrpn_client;


                for (int i = 0; i < cs.mkrPredictors.Length; i++)
                    cs.mkrPredictors[i] = new Vector3DESPredictor(0.4f);
            }

            initialized = true;
        }

        /// <summary>
        /// Performs RB calibration.
        /// </summary>
        public void InitializeCS()
        {
			coordSystems.Clear();

			if (redundancy) {
				List<int> e = new List<int>();
				Combination(3, markerNames.Length-1, e);

				for (int i = 0; i < e.Count; i++) {
					var first = i;
					var second = (i + 1) % e.Count;
					var third = (i + 2) % e.Count;
					//print(e[third]);
					//print(markerNames[e[third]]);
					string[] threeMarkerNames = { markerNames[e[first]], markerNames[e[second]], markerNames[e[third]] };

					CoordSystemTracker newCS = new CoordSystemTracker();
					bool isReliableCoordSystem = newCS.CreateCoordSystem(mocapInputController.VRPNOptions.vrpn_client, threeMarkerNames, controlledTr.position, controlledTr.rotation,isDebug);
					
					if(isReliableCoordSystem) 
					{
						coordSystems.Add( newCS );
						if ( isDebug )
							listDebug.Add( Instantiate( prefabDebug ));

					}
					

					i = third;
				}
			} else {

				for (int i = 0; i < markerNames.Length; i++) {
					var first = i;
					var second = (i + 1) % markerNames.Length;
					var third = (i + 2) % markerNames.Length;

					string[] threeMarkerNames = { markerNames[first], markerNames[second], markerNames[third] };

					CoordSystemTracker newCS = new CoordSystemTracker();
					bool isReliableCoordSystem = newCS.CreateCoordSystem(mocapInputController.VRPNOptions.vrpn_client, threeMarkerNames, controlledTr.position, controlledTr.rotation,isDebug);

					if ( isReliableCoordSystem )
						coordSystems.Add(newCS);
				}
			}


            if (coordSystems.Count > 0)
                initialized = true;
        }

		void Combination(int r, int n, List<int> e) {

			int[] a = new int[r];

			// initialize first combination
			for (int l = 0; l < r; l++) {
				a[l] = l;
				e.Add(a[l]);
			}

			int i = r - 1; // Index to keep track of maximum unsaturated element in array
						   // a[0] can only be n-r+1 exactly once - our termination condition!
			while (a[0] < n - r + 1) {

				// If outer elements are saturated, keep decrementing i till you find unsaturated element
				while (i > 0 && a[i] == n - r + i) {
					i--;
				}
				//print(a[i]); // pseudo-code to print array as space separated numbers

				a[i]++;
				// Reset each outer element to prev element + 1
				while (i < r - 1) {
					a[i + 1] = a[i] + 1;
					i++;
				}


				for (int l = 0; l < a.Length; l++) {
					//print(a[l])
					e.Add(a[l]);
				}


			}

		}
		public void OccludeDebugMarkers ( bool isOccluded ) {
			if ( isOccluded ) {
				foreach ( string markerName in markerNamesDebugOnlyOccluded ) {
					GameObject.Find( markerName ).tag = "markerNotVisible";
					GameObject.Find( markerName ).GetComponent<Renderer>().material.color = Color.red;
				}
			} else {
				foreach ( string markerName in markerNamesDebugOnlyOccluded ) {
					GameObject.Find( markerName ).tag = "markerVisible";
					GameObject.Find( markerName ).GetComponent<Renderer>().material.color = Color.blue;
				}


			}
		}

		public void GetTrackerPosAndRot( out Vector3 Pos, out Quaternion Rot, bool isDebug) 
		{
			/*cumulative=Vector4.zero;
			 vectorAdded = Vector4.zero;
			RotTemp = Quaternion.identity;
			 PosTemp = Vector3.zero;*/

		
	
		

			allPositionFromCoordsys.Clear();
			allRotationFromCoordsys.Clear();


			for ( int i = 0; i < coordSystems.Count; i++ ) {

				Quaternion ithCSRot = Quaternion.identity;
				Vector3 ithCSPos = Vector4.zero;

				bool reliable = coordSystems[i].GetPosAndRot( out ithCSPos, out ithCSRot, isDebug );


				if ( reliable ) {
					if ( isDebug ) {
						listDebug[i].transform.position = ithCSPos;
						listDebug[i].transform.rotation = ithCSRot;
					}


					allRotationFromCoordsys.Add( ithCSRot );
					allPositionFromCoordsys.Add( ithCSPos );
					//pPredictor.UpdateModel(ithCSPos);

				} //else
				  //	print(reliable);
			}

			cumulative = Vector4.zero;
			vectorAdded = Vector3.zero;

			for ( int i = 0; i < allRotationFromCoordsys.Count;i++) 
			{
				RotTemp = AverageQuaternion( ref cumulative, allRotationFromCoordsys[i], allRotationFromCoordsys[0], i+1 );
				PosTemp = AverageVector( ref vectorAdded ,allPositionFromCoordsys[i],i+1);

				
			}

			Rot = RotTemp;
			Pos = PosTemp;


		}

		private void Save()
        {
            RigidBodyTrackerState rigidBodyTrackerState = new RigidBodyTrackerState(this);
            rigidBodyTrackerState.Save(persistenceFileName);
		
        }

        private void Load()
        {
            var loaded = RigidBodyTrackerState.Load(persistenceFileName);
            LoadCalibration(loaded);
            initialized = true;
        }


        void UpdateTransform()
        {

            if (Input.GetKeyDown(calibrateKey))
                InitializeCS();

            if (Input.GetKeyDown(saveKey))
                Save();

			if ( Input.GetKeyDown( occlusionMarkerDebugOnlyKey ) )
				OccludeDebugMarkers( true );
			else if ( Input.GetKeyUp( occlusionMarkerDebugOnlyKey ) )
				OccludeDebugMarkers( false );

			if (!initialized)
                return;

			GetTrackerPosAndRot( out Pos, out Rot, isDebug );


			if ( updatePosition ) {
				controlledTr.position =Pos;
			}

			if ( updateRotation ) {
				//qPredictor.UpdateModel(ithCSRot);
				controlledTr.rotation = Rot;
			}



		}

        void FixedUpdate()
        {
            if (callback == UpdateCallback.FIXED_UPDATE)
                UpdateTransform();
        }

        void LateUpdate()
        {
            if (callback == UpdateCallback.LATE_UPDATE)
                UpdateTransform();
        }

        void Update()
        {
            if (callback == UpdateCallback.UPDATE)
                UpdateTransform();
        }


		//METHOD from https://wiki.unity3d.com/index.php/Averaging_Quaternions_and_Vectors
		//and based on a simplified procedure described in this document: http://ntrs.nasa.gov/archive/nasa/casi.ntrs.nasa.gov/20070017872_2007014421.pdf 
		//Get an average (mean) from more then two quaternions (with two, slerp would be used).
		//Note: this only works if all the quaternions are relatively close together.
		//Usage: 
		//-Cumulative is an external Vector4 which holds all the added x y z and w components.
		//-newRotation is the next rotation to be added to the average pool
		//-firstRotation is the first quaternion of the array to be averaged
		//-addAmount holds the total amount of quaternions which are currently added
		//This function returns the current average quaternion
		public static Quaternion AverageQuaternion ( ref Vector4 cumulative, Quaternion newRotation, Quaternion firstRotation, int addAmount ) {

			float w = 0.0f;
			float x = 0.0f;
			float y = 0.0f;
			float z = 0.0f;

			//Before we add the new rotation to the average (mean), we have to check whether the quaternion has to be inverted. Because
			//q and -q are the same rotation, but cannot be averaged, we have to make sure they are all the same.
			if ( !AreQuaternionsClose( newRotation, firstRotation ) ) {

				newRotation = InverseSignQuaternion( newRotation );
			}

			//Average the values
			float addDet = 1f / (float) addAmount;
			cumulative.w += newRotation.w;
			w = cumulative.w * addDet;
			cumulative.x += newRotation.x;
			x = cumulative.x * addDet;
			cumulative.y += newRotation.y;
			y = cumulative.y * addDet;
			cumulative.z += newRotation.z;
			z = cumulative.z * addDet;

			//note: if speed is an issue, you can skip the normalization step
			//return NormalizeQuaternion( x, y, z, w );

			return new Quaternion( x, y, z, w );
		}

		public static Quaternion NormalizeQuaternion ( float x, float y, float z, float w ) {

			float lengthD = 1.0f / (w * w + x * x + y * y + z * z);
			w *= lengthD;
			x *= lengthD;
			y *= lengthD;
			z *= lengthD;

			return new Quaternion( x, y, z, w );
		}

		//Changes the sign of the quaternion components. This is not the same as the inverse.
		public static Quaternion InverseSignQuaternion ( Quaternion q ) {

			return new Quaternion( -q.x, -q.y, -q.z, -q.w );
		}

		//Returns true if the two input quaternions are close to each other. This can
		//be used to check whether or not one of two quaternions which are supposed to
		//be very similar but has its component signs reversed (q has the same rotation as
		//-q)
		public static bool AreQuaternionsClose ( Quaternion q1, Quaternion q2 ) {

			float dot = Quaternion.Dot( q1, q2 );

			if ( dot < 0.0f ) {

				return false;
			} else {

				return true;
			}
		}


		//METHOD from https://wiki.unity3d.com/index.php/Averaging_Quaternions_and_Vectors
		//and based on a simplified procedure described in this document:"http://wiki.unity3d.com/index.php?title=Averaging_Quaternions_and_Vectors&oldid=16679"
		public static Vector3 AverageVector ( ref Vector3 addedVector, Vector3 singleVector , int addAmount ) 
		{
			addedVector.x+= singleVector.x;
			addedVector.y+= singleVector.y;
			addedVector.z+= singleVector.z;

		
			return  addedVector / (float) addAmount;
			//return new Vector3( addedVector.x / (float) addAmount, addedVector.y / (float) addAmount, addedVector.z / (float) addAmount );

		}


	}





	[XmlRoot( "CoordinateSystemCollection" )]
	public class RigidBodyTrackerState {
		public string[] markerNames;

		[XmlArray( "CoordinateSystems" )]
		[XmlArrayItem( "CoordinateSystem" )]
		public List<CoordSystemTracker> coordSystems = new List<CoordSystemTracker>();

		//    public RigidBodyTrackerState(bool updateRotation, bool usePrediction, string[] markerNames, List<CoordSystemTracker> coordSystems)
		public RigidBodyTrackerState ( RigidBodyTracker toCopy ) {
			this.markerNames = toCopy.markerNames;
			this.coordSystems = toCopy.coordSystems;
		}

		public RigidBodyTrackerState () { }

		public void Save ( string path ) {
			var serializer = new XmlSerializer( typeof( RigidBodyTrackerState ) );
			using ( var stream = new FileStream( path, FileMode.Create ) ) {
				serializer.Serialize( stream, this );
			}
		}

		public static RigidBodyTrackerState Load ( string path ) {
			var serializer = new XmlSerializer( typeof( RigidBodyTrackerState ) );
			using ( var stream = new FileStream( path, FileMode.Open ) ) {
				return serializer.Deserialize( stream ) as RigidBodyTrackerState;
			}
		}

	}
}