﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class Extensions
{
	// creates a left handed CS gizmo - X, Y and Z lines
	public static GameObject CSGizmo(float scale = 1, float length = 0.2f, float width = 0.02f){
		length *= scale;
		width *= scale;
		Color [] colors = {Color.red, Color.green, Color.blue};
		string [] names = {"X", "Y", "Z"};
		Vector3 [] positions = new [] {	Vector3.right, Vector3.up, Vector3.forward};

		GameObject parentGO = new GameObject ("CS_gizmo");

		for (int i = 0; i<3; i++) {
			GameObject axis_GO = new GameObject (names[i]);
			LineRenderer axis_LR = axis_GO.AddComponent<LineRenderer> ();
            axis_LR.startWidth = width;
            axis_LR.endWidth = width;
            axis_LR.SetPosition (1, positions[i] * length);
			axis_LR.material.color = colors[i];
			axis_LR.material.shader = Shader.Find("Unlit/Color");
			//axis_LR.receiveShadows = false;
			//axis_LR.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
			axis_LR.useWorldSpace = false;
			axis_GO.transform.parent = parentGO.transform;
		}

		return parentGO;
	}

	public static Transform Search(Transform target, string name){
		if (target.name == name) return target;
		
		for (int i = 0; i < target.childCount; ++i){
			var result = Search(target.GetChild(i), name);
			
			if (result != null) return result;
		}
		return null;
	}
	

	public static void SearchAll(Transform target, string name, ref List<Transform> result){
		if (target.name == name) result.Add(target);
		
		for (int i = 0; i < target.childCount; ++i)
		{	
			SearchAll(target.GetChild(i), name, ref result);	
		}
	}


	public static Quaternion QuaternionFromMatrix(Matrix4x4 m) {
		// Adapted from: http://www.euclideanspace.com/maths/geometry/rotations/conversions/matrixToQuaternion/index.htm
		Quaternion q = new Quaternion();
		q.w = Mathf.Sqrt( Mathf.Max( 0, 1 + m[0,0] + m[1,1] + m[2,2] ) ) / 2; 
		q.x = Mathf.Sqrt( Mathf.Max( 0, 1 + m[0,0] - m[1,1] - m[2,2] ) ) / 2; 
		q.y = Mathf.Sqrt( Mathf.Max( 0, 1 - m[0,0] + m[1,1] - m[2,2] ) ) / 2; 
		q.z = Mathf.Sqrt( Mathf.Max( 0, 1 - m[0,0] - m[1,1] + m[2,2] ) ) / 2; 
		q.x *= Mathf.Sign( q.x * ( m[2,1] - m[1,2] ) );
		q.y *= Mathf.Sign( q.y * ( m[0,2] - m[2,0] ) );
		q.z *= Mathf.Sign( q.z * ( m[1,0] - m[0,1] ) );
		return q;
	}
}