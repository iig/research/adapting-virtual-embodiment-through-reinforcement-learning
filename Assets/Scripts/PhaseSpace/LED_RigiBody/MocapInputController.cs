/**************************************************************************
 * MocapInputController.cs controls the motion capture source and reproduction
 * Written by Henrique Galvan Debarba
 * Last update: 28/02/14
 * *************************************************************************/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace IIG{
	// the classes bellow are only used to make the inspector for MocapInputController
	// more bearable with so many setting options

	[System.Serializable]
	public class VRPNInitSettings{
		public bool initAtStartup = false;
		public string connectionString = "Tracker0@localhost";
		public int frameSkip = 8;
		public int frameSkipIncrement = 8;
		public VRPN.Client vrpn_client { get; private set;}
		bool initialized = false;
		/*VRPNInitSettings(){
			if (initAtStartup)
				Init ();
		}*/
		public void Init (string markerIDToNameMapping){
			//frameSkip = frameSkipIncrement;
			vrpn_client = new VRPN.Client (markerIDToNameMapping, connectionString);
			vrpn_client.Set_Frame_Increment (frameSkip);
			vrpn_client.Init ();
			initialized = true;
		}

		public void FastBackward () {
			frameSkip -= frameSkipIncrement;
			vrpn_client.Set_Frame_Increment (frameSkip);
		}
		public void Pause() {
			frameSkip = 0;
			vrpn_client.Set_Frame_Increment (frameSkip);
		}
		public void Play() {
			frameSkip = frameSkipIncrement;
			vrpn_client.Set_Frame_Increment (frameSkip);
		}
		public void FastForward() {
			frameSkip += frameSkipIncrement;
			vrpn_client.Set_Frame_Increment (frameSkip);
		}
		public bool IsReady(){
			if (!initialized)
				return false;
			return true;
		}
	};

	[System.Serializable]
	public class FileInitSettings{
		public bool initAtStartup = false;
		public string folderPathToLoad = ".\\adjusted-id-schubert-day-1-exp-1-trial-1OK.mkr16Hz.csv";
        public string fileNameToLoad;
        public MocapPlayer mocapPlayer  {get; protected set;} 
		// !!!??? TODO check what happens with this initialization code when I change initAtStartup value
		/*FileInitSettings(){
			if (initAtStartup)
				Init ();
		}*/
		public void Init(){
			// open c3D file
			if (fileNameToLoad.EndsWith(".c3d"))
				mocapPlayer = new C3DPlayer();
			else
				mocapPlayer = new CSVPlayer();
			
			mocapPlayer.LoadMocap(folderPathToLoad + fileNameToLoad);
		}
		public bool IsReady(){
			if (mocapPlayer == null)
				return false;
			return true;
		}
	};

	[System.Serializable]
	public class PhasespaceInitSettings{
		public bool initAtStartup = false;
		[HideInInspector]
		public OWLWrapper OWL = new OWLWrapper();
		public string deviceName = "iciigphasespacex2.epfl.ch";
		public bool slaveMode = true;

		public void Init(){
			if (IsReady ())
				return;
			if (OWL.Connect (deviceName, slaveMode)) {
				if (!slaveMode) {
					// create default point tracker
					int n = 128;
					int [] leds = new int[n];
					for (int i = 0; i < n; i++)
						leds [i] = i;
					OWL.CreatePointTracker (0, leds);
				}
				
				// start streaming
				OWL.Start ();
			}
		}
		public bool IsReady(){
			if (OWL.Connected())
				return true;
			return false;
		}
		public void Disconnect(){
			if (OWL.Connected ())
				OWL.Disconnect ();
		}
	};

	[System.Serializable]
	public class DelayMocap{
		// delay mocap? keep marker+deltatime buffers and time controllers
		public bool delayMocap = false;
		// buffer for delayed mocap
		public Queue<double[,]> mocapBuffer = new Queue<double[,]>();
		public Queue<float> deltaTimeBuffer = new Queue<float>();
		public float delayTime = 1.5f;
		private float keptInterval = 0;



		public void UpdateDelay(float deltaT, ref double[,] frame){
			// manage delay buffer, it is updated always so it is not 
			// necessary to wait for it to be filled after enabling
			mocapBuffer.Enqueue(frame);
			deltaTimeBuffer.Enqueue(deltaT);
			keptInterval += deltaT;
			if (delayMocap){
				if (keptInterval > delayTime){
					frame = mocapBuffer.Dequeue();
					keptInterval -= deltaTimeBuffer.Dequeue();
				}
			}else{
				if (keptInterval > delayTime){
					mocapBuffer.Dequeue();
					keptInterval -= deltaTimeBuffer.Dequeue();
				}
			}	

		}

	};

    [System.Serializable]
    public class RecordInitSettings
    {
        public bool isRecording;

        [HideInInspector]
        public bool recordFlag;

        public LogMarkers m_mkrsLog = new LogMarkers();
        public string folderPathToRecord = ".\\adjusted-id-schubert-day-1-exp-1-trial-1OK.mkr16Hz.csv";
        public string fileNameToRecord = "";
        bool writeDirectToFile = true;

        public float rate;
        public float recordRate = 1.0f; //the number of sample per second

        public string StartRecording(List<Tuple<int, string>> NamesList)
        {
            return m_mkrsLog.OpenLog(NamesList, folderPathToRecord + fileNameToRecord, writeDirectToFile);
        }
        public void StopRecording()
        {
            if (m_mkrsLog.logIsOpen)
                m_mkrsLog.CloseLog();
        }

    };

    public class MocapInputController : MonoBehaviour {

		// update function to use
		public enum updateCallback {UPDATE, LATE_UPDATE, FIXED_UPDATE};
		public updateCallback callback = updateCallback.UPDATE;


		// mocap recording path and name, it loads C3D and .mkr.csv
		// .mkr.csv is a simple ASCII that can be created using LogMarker.cs
		public string markerIDToNameMapping = ".\\calibrationFiles\\marker_minimal.txt";

		// input source
		public enum MocapSource {VRPN=0, FILE=1, PHASESPACE=2};
		public MocapSource m_mocapSource;



		// debug markers - make unity transform markers and draw them as spheres
		public MarkersDebug mkrDebug;
		public bool drawMarkersTransform {
			get{return drawMarkers;} 
			set{drawMarkers=value; mkrDebug.SetVisible(drawMarkers);}
		}
		[SerializeField]
		private bool drawMarkers;
		public bool updateMarkers = true;
		public KeyCode occlusionKey = KeyCode.O;
		public List<int> markerIDOccluded ;



		// avatar controller, which receives mocap input from this class
		//public AvatarController avtController = null;

		// TODO !!!??? why do I have two structures???? are both used?
		List<Tuple<int,string>> m_mkrNames = new  List<Tuple<int,string>> ();
		Dictionary<int,string> m_mkrIDtoNames = new Dictionary<int, string>();
		int [] markerIDs;

		// delay mocap manager
		public DelayMocap delayOptions;


		// options to init local VRPN client
		// VRPN settings
		//public string VRPN_connection_string;	
	
		public VRPNInitSettings VRPNOptions;

		// options to load mocap from file
		//public string fileToLoad;
		public FileInitSettings fileOptions;

		// connection to phasespace owl server
		public PhasespaceInitSettings phasespaceOptions;

        //options for recording the MocapData
        public RecordInitSettings recordOptions;

        // current frame
        //public double[,] frame;// {get; protected set;}//= null;
        //public bool [] visibFrame {get; protected set;}// = null;

        


        // Initialization
        void Awake() {

			// init Markers debug
			mkrDebug = new MarkersDebug();

		
			// setup marker transforms
			LoadMakerNames (markerIDToNameMapping, ref m_mkrNames);
			mkrDebug.Init ();
			foreach (KeyValuePair<int, string> pair in m_mkrIDtoNames)
				mkrDebug.AddMarker (pair.Value);
			mkrDebug.SetVisible(drawMarkersTransform);

			// init input devices
			if (VRPNOptions.initAtStartup)
				VRPNOptions.Init(markerIDToNameMapping);

			if (fileOptions.initAtStartup)
				fileOptions.Init();

			if (phasespaceOptions.initAtStartup)
				phasespaceOptions.Init();

			

		}


        // load marker ID <-> name relations
        void LoadMakerNames(string inFilePath, ref List<Tuple<int,string>> mkrnames){
			//mkrnames = new List<Tuple<int, string>>();
			System.IO.StreamReader reader = new System.IO.StreamReader(inFilePath);
			string line;

			while((line = reader.ReadLine()) != null)
			{
				string[] id_value = line.Split(';');
				if (id_value[0] != null){
					string[] mkrNameID = id_value[0].Split(' ');
					if (mkrNameID.Length >= 2){
						mkrNameID[1] = mkrNameID[1].Replace("\t","");
						Tuple<int, string> mkrNameIDTuple = new Tuple<int, string>(System.Convert.ToInt32(mkrNameID[0]), mkrNameID[1]);
						mkrnames.Add(mkrNameIDTuple);
						m_mkrIDtoNames.Add(System.Convert.ToInt32(mkrNameID[0]), mkrNameID[1]);
					}
				}

			}
			// marker IDs contains part of the mkrnams info (marker numbering in the correct order)
			markerIDs = new int[mkrnames.Count];
			for (int i = 0; i < markerIDs.Length; i++){
				markerIDs[i] = mkrnames[i].First;
			}

			reader.Close();
		}

		public void FrameUpdate(float deltaT){
				// current frame
			double[,] frame = new double[m_mkrNames.Count,3];// {get; protected set;}//= null;
			bool [] visibFrame = new bool[m_mkrNames.Count];// = null;
			bool validFrame = true;
			int totalMarkers = m_mkrNames.Count;
			
			// MOCAP from file
			if (m_mocapSource==MocapSource.FILE){
				if (!fileOptions.IsReady())
					fileOptions.Init();
				MocapPlayer mp = fileOptions.mocapPlayer;

				// frame update
				validFrame = mp.CycleFrames(deltaT, ref frame, ref visibFrame);
				totalMarkers = mp.totalPoints;

     



            }
            // VRPN stream
            else if (m_mocapSource==MocapSource.VRPN){
				if (!VRPNOptions.IsReady())
					VRPNOptions.Init(markerIDToNameMapping);
				VRPN.Client vrpn = VRPNOptions.vrpn_client;
				vrpn.Update();
				
				// copy positions (so a buffer can be kept)
				visibFrame = new bool[vrpn.marker_positions.GetLength(0)];
				totalMarkers = vrpn.marker_ids.Length;
				for (int i = 0; i < vrpn.marker_positions.GetLength(0); i++){

                    int index = vrpn.marker_ids[i];
					visibFrame[index]= true;

					for (int j = 0; j < vrpn.marker_positions.GetLength(1); j++)
						frame[index,j] = vrpn.marker_positions[i,j];

                    if (frame[index,0]!=frame[index,0])
						visibFrame[index] = false;

                    //if (recordOptions.m_mkrsLog.logIsOpen)
                       // recordOptions.m_mkrsLog.LogMarkersFrame(frame, visibFrame);
                }

			
			}
			// MOCAP from file
			if (m_mocapSource==MocapSource.PHASESPACE){
				if (!phasespaceOptions.IsReady())
					phasespaceOptions.Init();
				OWLWrapper ps = phasespaceOptions.OWL;

				int count = 0;
				bool latest = false;
				while (!latest && count < 512){
					count++;
					latest = ps.GetMarkers(ref frame, ref visibFrame);
					//if (recordOptions.m_mkrsLog.logIsOpen)
                       // recordOptions.m_mkrsLog.LogMarkersFrame(frame, visibFrame);
				}

			}

            // Update the maker drawn from the file loaded
            if (validFrame)
            {
                delayOptions.UpdateDelay(deltaT, ref frame);

                // convert frame pos to Vector3 format, used by MarkersDebug to set markers representation position
                if (updateMarkers)
                {
				

					for (int i = 0; i < totalMarkers; i++)
                    {
                        int index = m_mkrNames[i].First;

                        //if marker is not visible its position is not updated
                        if ( !VRPNOptions.vrpn_client.IsMarkerVisible( index) )// (!visibFrame[index])
                        {
                            mkrDebug.MarkerNotVisible(m_mkrIDtoNames[index]);
                        }
                        else
                        {
                            // - on z parameter is HARD CODED !!!!!! so mocap and markers match
                            // a 180 on the avatar was also needed to match ...
                            // the reason has something to do with right/left handed coord system (mocap is right h, unity left h)
                            //Vector3 currentMkr = new Vector3((float)frame[index,0],(float)frame[index,1],-(float)frame[index,2]);
                            Vector3 currentMkr = new Vector3(-(float)frame[index, 0], (float)frame[index, 1], (float)frame[index, 2]);
                            mkrDebug.SetMarker(m_mkrIDtoNames[index], currentMkr);
                        }
                    }

				
					/*else if (Input.GetKeyUp(occlusionKey) && visibFrame[m_mkrNames[markerIDOccluded].First])
					{
						Vector3 currentMkr = new Vector3(-(float)frame[m_mkrNames[markerIDOccluded].First, 0], (float)frame[m_mkrNames[markerIDOccluded].First, 1], (float)frame[m_mkrNames[markerIDOccluded].First, 2]);
						mkrDebug.SetMarker(m_mkrIDtoNames[m_mkrNames[markerIDOccluded].First], currentMkr);
					}*/

				}

                /*if (avtController!= null){
                    avtController.MocapUpdateIK(markerIDs, frame);
                }*/

            }

			if ( Input.GetKey( occlusionKey ) ) {

				VRPNOptions.vrpn_client.markerOccluded = markerIDOccluded;
				VRPNOptions.vrpn_client.hideMarker = true;

				//Debug.Log("occludedpress");

				/*for ( int i = 0; i < markerIDOccluded.Count; i++ ) {
					VRPNOptions.vrpn_client.SetMarkerPosition( markerIDOccluded[i], new Vector3( float.NaN, float.NaN, float.NaN ) );
				}*/

				/*for (int i = 0; i < markerIDOccluded.Count; i++) {
					int index = m_mkrNames[i].First;
					//mkrDebug.MarkerNotVisible(m_mkrIDtoNames[m_mkrNames[markerIDOccluded].First]);
					visibFrame[index] = false;
				}*/

			} else
				VRPNOptions.vrpn_client.hideMarker = false;

			if (recordOptions.isRecording)
            {
                if (!recordOptions.m_mkrsLog.logIsOpen)
                    recordOptions.StartRecording(GetNamesList());


                recordOptions.rate += Time.deltaTime;

                if (recordOptions.rate >= recordOptions.recordRate && recordOptions.m_mkrsLog.logIsOpen)
                {
                    recordOptions.rate = 0;
                    recordOptions.m_mkrsLog.LogMarkersFrame(frame, visibFrame);
                }
                    

                
            }
            else
                recordOptions.StopRecording();

        }

		void FixedUpdate(){
			if (callback == updateCallback.FIXED_UPDATE)
				FrameUpdate (Time.fixedDeltaTime);
		}
		void LateUpdate(){
			if (callback == updateCallback.LATE_UPDATE)
				FrameUpdate (Time.fixedDeltaTime);
		}
		void Update(){
			if (callback == updateCallback.UPDATE)
				FrameUpdate (Time.fixedDeltaTime);
		}

		public List<Tuple<int,string>> GetNamesList(){
			return m_mkrNames;
		}

		void OnDestroy()
		{
            recordOptions.StopRecording();

            // disconnect from OWL server
           phasespaceOptions.Disconnect();

            //vrpn
            if (m_mocapSource == MocapSource.VRPN ) {
				VRPNOptions.vrpn_client.Stop_streaming();
				Destroy(gameObject);
				
			}
                

      

        }

	


	}
}