﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IIG
{
    public class VrpnClientWrapper : MonoBehaviour
    {
        public bool getLiveData = false;
        public string mocap_marker_alias_file_path;
        public string VRPN_connection_string;
        public int frameIncrement = 8;
        public int startFrameNo = 1;
        public int currentFrame = 1;
        public VRPN.Client vrpn_client;
        public bool movementTrace=false; 

        [HideInInspector]
        public bool renderMarkers = false;
        public bool freezeMarkers = false;


        bool flag=true;


        // Use this for initialization
        void Awake()
        {
            if (getLiveData)
                vrpn_client = new VRPN.Client(mocap_marker_alias_file_path, "Tracker0@localhost");
                
            
                
            else
                vrpn_client = new VRPN.Client(mocap_marker_alias_file_path, VRPN_connection_string);
                

                

            vrpn_client.Set_Frame_No(startFrameNo);
            vrpn_client.Set_Frame_Increment(frameIncrement);
            vrpn_client.Init();

            // clean the buffers
            for (int i = 0; i < 10; ++i)
                vrpn_client.Update();
        }

        // Update is called once per frame
        void Update()
        {
            
            if (movementTrace)
            {
                    vrpn_client.InitMarkerRendering();
            }

            if (renderMarkers && flag)
            {
                    vrpn_client.InitMarkerRendering();

                flag = false;

            }
            else if ( !renderMarkers && !flag)
            {
                vrpn_client.StopMarkerRendering();
                flag = true;
            }
                    


            vrpn_client.Update();
            currentFrame = vrpn_client.currentFrameNo;

            if (renderMarkers && !freezeMarkers)
                vrpn_client.Render_Markers();

          /*  if (Input.GetKeyDown(KeyCode.M))
            {
                if (renderMarkers)
                    vrpn_client.StopMarkerRendering();
                else
                    vrpn_client.InitMarkerRendering();
                renderMarkers = !renderMarkers;
            }

            vrpn_client.Update();

            if (renderMarkers)
                vrpn_client.Render_Markers();*/
        }
    }
}