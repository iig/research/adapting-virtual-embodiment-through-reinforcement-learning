/*
 * AUTHOR: 
 * Eray Molla
 * 
 * DATE: 
 * 19/08/2014
 * 
 * DESCRIPTION: 
 * This source file is responsible for accessing the functionalities of
 * VRPN_Client_DLL file. It encapsulates the the .dll file so
 * that the other modules don't see the details.
 * */

using UnityEngine;
using System;
using System.Collections;
using System.Runtime.InteropServices;     // DLL support
using System.Collections.Generic;

namespace VRPN
{
	public class Client
	{
		public double[,] marker_positions;
		public int[] marker_ids;
        public Dictionary<int, string> marker_ids_to_alias;
        public Dictionary<string, int> marker_alias_to_ids;
        public int no_of_markers;
		public int no_of_frames;
		public int currentFrameNo;
		public int frameIncrement;
		public GameObject[] marker_spheres;
		public bool hideMarker;
		public List<int> markerOccluded =new List<int>();
		
		[DllImport("VRPN_Client_DLL", CallingConvention=CallingConvention.Cdecl)]
		private static extern int create_VRPN_Tracker(string marker_alias_file_name, string tracker_name);
		
		[DllImport("VRPN_Client_DLL", CallingConvention=CallingConvention.Cdecl)]
		private static extern void init();
		
		[DllImport("VRPN_Client_DLL", CallingConvention=CallingConvention.Cdecl)]
		private static extern void stop_streaming();
		
		[DllImport("VRPN_Client_DLL", CallingConvention=CallingConvention.Cdecl)]
		private static extern void destroy();
		
		[DllImport("VRPN_Client_DLL", CallingConvention=CallingConvention.Cdecl)]
		private static extern void update();
		
		[DllImport("VRPN_Client_DLL", CallingConvention=CallingConvention.Cdecl)]
		private static extern void get_marker_positions([Out] double[,] marker_positions);
		
		[DllImport("VRPN_Client_DLL", CallingConvention=CallingConvention.Cdecl)]
		private static extern int test();
		
		[DllImport("VRPN_Client_DLL", CallingConvention=CallingConvention.Cdecl)]
		private static extern int get_marker_ids([Out] int[] marker_ids);
		
		[DllImport("VRPN_Client_DLL", CallingConvention=CallingConvention.Cdecl)]
		private static extern int get_no_of_markers();

		[DllImport("VRPN_Client_DLL", CallingConvention=CallingConvention.Cdecl)]
		private static extern int get_nb_frames();
		
		[DllImport("VRPN_Client_DLL", CallingConvention=CallingConvention.Cdecl)]
		private static extern void set_frame_no(int frame_no);
		
		[DllImport("VRPN_Client_DLL", CallingConvention=CallingConvention.Cdecl)]
		private static extern void set_frame_increment(int frame_no);
		
		public Client (string marker_alias_file_name, string tracker_name)
		{
			if(create_VRPN_Tracker(marker_alias_file_name, tracker_name) == 1)
			{
				no_of_markers = get_no_of_markers();
				no_of_frames = get_nb_frames();
				marker_ids = new int[no_of_markers];
				get_marker_ids(marker_ids);
				marker_positions = new double[no_of_markers, 3];
				currentFrameNo = 0;
				frameIncrement = 1;

                marker_ids_to_alias = new Dictionary<int, string>(no_of_markers);
                marker_alias_to_ids = new Dictionary<string, int>(no_of_markers);
                LoadMakerNames(marker_alias_file_name, ref marker_ids_to_alias, ref marker_alias_to_ids);
			}
			else
				throw new System.ArgumentException("VRPN Client could not be created. Make sure that there is no other client");
		}

        // load marker ID <-> name relations
        private void LoadMakerNames(string aliasFilePath, ref Dictionary<int, string> ids_to_alias, ref Dictionary<string, int> alias_to_ids)
        {
            //mkrnames = new List<Tuple<int, string>>();
            System.IO.StreamReader reader = new System.IO.StreamReader(aliasFilePath);
            string line;

            while ((line = reader.ReadLine()) != null)
            {
                string[] id_value = line.Split(';');
                if (id_value[0] != null)
                {
                    string[] mkrNameID = id_value[0].Split(' ');
                    if (mkrNameID.Length >= 2)
                    {
                        mkrNameID[1] = mkrNameID[1].Replace("\t", "");
                        Tuple<int, string> mkrNameIDTuple = new Tuple<int, string>(System.Convert.ToInt32(mkrNameID[0]), mkrNameID[1]);
                        ids_to_alias.Add(System.Convert.ToInt32(mkrNameID[0]), mkrNameID[1]);
                        alias_to_ids.Add(mkrNameID[1], System.Convert.ToInt32(mkrNameID[0]));
                        //Debug.Log("Marker " + mkrNameID[1] + " is marker #" + System.Convert.ToInt32(mkrNameID[0]));
                    }
                }

            }

            reader.Close();
        }

        ~Client()
		{
			destroy();
		}
		
		public void Init()
		{
			init();
		}

		public void Destroy()
		{
			destroy();

		}
		
		public void Stop_streaming()
		{
			stop_streaming();
        }

        public void Update()
        {
			currentFrameNo += frameIncrement;
			if(currentFrameNo >= no_of_frames)
				Set_Frame_No(0);
			update();
			get_marker_positions(marker_positions);
			/*for(int i = 0; i < this.no_of_markers; ++i)
			{
				marker_positions[i, 0] *= 0.66;
				marker_positions[i, 1] *= 0.66;
				marker_positions[i, 2] *= 0.66;
			}*/
		}
		
		// sets the the frame no that will be read from the c3d file
		public void Set_Frame_No(int frame_no)
		{
			currentFrameNo = frame_no;
			set_frame_no(frame_no);
		}
		
		// sets the frame_no increment where frame_no += increment
		public void Set_Frame_Increment(int frame_increment)
		{
			set_frame_increment(frame_increment);
			frameIncrement = frame_increment;
		}

		public void Render_Markers()
		{
			for(int i = 0; i < no_of_markers; ++i)
			{
                //if (marker_positions[i, 0] == marker_positions[i, 0])
                if (IsMarkerVisible(i))
                {
					marker_spheres[i].transform.position = new Vector3((float)(-marker_positions[i,0]), 
															 		   (float)(marker_positions[i,1]), 
															           (float)(marker_positions[i,2]));

					if(marker_spheres[i].GetComponent<Renderer>().material.color == Color.red)
						marker_spheres[i].GetComponent<Renderer>().material.color = Color.blue;
				}
				else
					marker_spheres[i].GetComponent<Renderer>().material.color = Color.red;
			}
		}

		public void InitMarkerRendering()
		{
			marker_spheres = new GameObject[no_of_markers];
			
			for(int i = 0; i < no_of_markers; ++i)
			{
				marker_spheres[i] = GameObject.CreatePrimitive(PrimitiveType.Sphere);
				marker_spheres[i].transform.localScale = new Vector3((float)0.02, (float)0.02, (float)0.02);
				marker_spheres[i].GetComponent<Renderer>().material.color = new Color(0, 0, 1);
				marker_spheres[i].name = marker_ids_to_alias[marker_ids[i]];
                marker_spheres[i].tag = "phasespace_Marker";//Thibault Changement

            }
		}

		public void StopMarkerRendering()
		{
			for(int i = 0; i < no_of_markers; ++i)
				GameObject.Destroy(marker_spheres[i]);
			marker_spheres = null;
		}

		public bool SetMarkerPosition( int markerID, Vector3 position) 
		{
			if ( markerID < no_of_markers ) {

				marker_positions[markerID, 0] =position.x;
				marker_positions[markerID, 1] = position.y;
				marker_positions[markerID, 2] = position.z;
				Debug.Log( "occludedpress2" );

			} else {
				Debug.LogWarning( "ID " + markerID + " is too big, cannot find marker." );
				position = Vector3.zero;
			}

			return markerID < no_of_markers;
		}

        public bool GetMarkerPosition(int markerID, out Vector3 position)
        {
            if (markerID < no_of_markers)
            {
                position = new Vector3((float)(-marker_positions[markerID, 0]),
                                    (float)(marker_positions[markerID, 1]),
                                    (float)(marker_positions[markerID, 2]));
            }
            else
            {
                Debug.LogWarning("ID " + markerID + " is too big, cannot find marker.");
                position = Vector3.zero;
            }

            return markerID < no_of_markers;
        }

        public bool GetMarkerPosition(string markerName, out Vector3 position)
        {
            int id;
            if (marker_alias_to_ids.TryGetValue(markerName, out id))
            {
                return GetMarkerPosition(id, out position);
            }
            else
            {
                Debug.LogWarning("Marker " + markerName + " cannot be found in VRPN client list.");
                position = Vector3.zero;
                return false;
            }
        }

        public bool IsMarkerVisible(int markerID)
        {
			if (hideMarker)
			{	
				for (int i = 0; i < markerOccluded.Count; i++)
				{
					if (markerID==markerOccluded[i]) 
					{

						//Debug.Log( "occludedpress3" );
						return false;
					}
					
				}
			
			}


			return marker_positions[markerID, 0] == marker_positions[markerID, 0]; // WTF?
        }

        public bool IsMarkerVisible(string markerName)
        {
            int id;
            if (marker_alias_to_ids.TryGetValue(markerName, out id))
            {
                return IsMarkerVisible(id);
            }
            else
            {
                Debug.LogWarning("Marker " + markerName + " cannot be found in VRPN client list. " +
                                    "Will be considered as not visible.");

                return false;
            }
        }
    }
}

