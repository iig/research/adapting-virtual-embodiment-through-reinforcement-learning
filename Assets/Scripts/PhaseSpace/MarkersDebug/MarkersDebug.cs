/**************************************************************************
 * MarkersDebug.cs debug markers positions
 * Written by Henrique Galvan Debarba
 * Last update: 03/03/14
 * *************************************************************************/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ReadWriteCsv;



public class MarkersDebug {

	//Dictionary<string, GameObject> m_markersDebugNameKey;
	Dictionary<string, int> m_nameToIndexMap = new Dictionary<string, int>();
	List<GameObject> markerGOArray = new List<GameObject>();

	// materials that will be used in visible/notvisible markers
	public Material matVisible;
	public Material matNotVisible;
	
	// sphere primitive has by standard the size of 1 
	//float scaleFactor = 0.025f;

	// std game object to put the markers, so that they are not in the root of the hierarchy
	public GameObject markerDebugParent = new GameObject();

	// prefab object
	public string mkrGOPrefabName = "MarkerDebugPrefab";
	GameObject markerPrefab;

	public void Init(){
	//	m_markersDebugIDKey.Clear ();
	//	m_markersDebugNameKey.Clear ();
		m_nameToIndexMap.Clear ();
		markerGOArray.Clear ();

		// set marker debug parent name
		markerDebugParent.name = "Markers debug";
		// create materials to be used on marker representations
		if (matVisible == null) {
			//matVisible = new Material (Shader.Find ("Transparent/DiffuseBackFace"));
			matVisible = new Material (Shader.Find ("Standard"));
			matVisible.color = new Color(0,0.8f,0,0.5f);//Color.green;
		}
		if (matNotVisible == null) {
			//matNotVisible = new Material (Shader.Find ("Transparent/DiffuseBackFace"));
			matNotVisible = new Material (Shader.Find ("Standard"));
			matNotVisible.color = new Color(0.8f,0,0,0.5f);//Color.red;
		}
		// allocation
		//markerGOArray = new GameObject [markers.Count];
		//m_markersDebug = new Dictionary<int, GameObject> ();
		
		markerPrefab = Resources.Load(mkrGOPrefabName, typeof(GameObject)) as GameObject;
		if (markerPrefab == null)
			Debug.LogError("MarkersDebug.cs: marker GO prefab not found " + mkrGOPrefabName);
	}
		
	public void Init(List<Tuple<int, string>> markers){
		Init ();
		AddMarkers (markers);
	}

	public void Init(List<string> markers){
		Init ();
		AddMarkers (markers);
	}

	public void AddMarkers(List<Tuple<int, string>> markers){
		// instantiate marker prefab
		foreach (Tuple<int, string> mkr in markers) {
			AddMarker(mkr);
		}
	}
	public void AddMarkers(List<string> markers){
		// instantiate marker prefab
		foreach (string mkr in markers) {
			AddMarker(mkr);
		}
	}
	// insertion with (string) number key
	public GameObject AddMarker(Tuple<int, string> marker){
		int index;
		if (m_nameToIndexMap.TryGetValue(marker.First.ToString (), out index)) // Returns true.
		{
		//if (m_nameToIndexMap [marker.First.ToString ()] != null) {
			Debug.Log("MarkersDebug.cs: marker " + marker.First + " already exists");
			return markerGOArray[index];//GetMarker(marker.First.ToString ());
		}
		
		GameObject newMarker = GameObject.Instantiate(markerPrefab) as GameObject;
		
		newMarker.GetComponent<Renderer>().material =  matNotVisible;
		newMarker.transform.parent = markerDebugParent.transform;
		newMarker.name = marker.Second;

		markerGOArray.Add(newMarker);
		m_nameToIndexMap.Add(marker.First.ToString(), markerGOArray.Count-1);

		return newMarker;
	}

	// insertion with name key
	public GameObject AddMarker(string name){
		int index;
		if (m_nameToIndexMap.TryGetValue(name, out index)) // Returns true.
		{
		//if (m_nameToIndexMap [name] != null){			
			Debug.Log("MarkersDebug.cs: marker " + name + " already exists");
			return markerGOArray[index];//GetMarker (name);
		}

		GameObject newMarker = GameObject.Instantiate(markerPrefab) as GameObject;
		// assume it is not visible inittialy
		newMarker.GetComponent<Renderer>().material =  matNotVisible;
		// set parent to a standard GO
		newMarker.transform.parent = markerDebugParent.transform;
		newMarker.name = name;

		markerGOArray.Add(newMarker);
		m_nameToIndexMap.Add(name, markerGOArray.Count-1);

		return newMarker;
	}



	// make all markers (in)visible
	public void SetVisible(bool isVisible){
		for (int i =0; i<markerGOArray.Count; i++){
			markerGOArray[i].GetComponent<Renderer>().enabled = isVisible;
			for (int j=0; j < markerGOArray[i].transform.childCount; j++){
				markerGOArray[i].transform.GetChild(j).GetComponent<Renderer>().enabled = isVisible;
			}
		}
	}


	public void SetMarkersMap (int [] keys, Vector3 [] mkrPosArray, bool [] mkrVisibArray){
		if (keys.Length != mkrPosArray.Length || keys.Length != mkrVisibArray.Length)
						return;

		for (int i =0; i<mkrPosArray.Length; i++){
			SetMarkerMap(keys[i].ToString(), mkrPosArray[i], mkrVisibArray[i]);
		}
	}

	public void SetMarkerMap (string mapKey, Vector3 mkrPos, bool mkrVisib){
		//int index = m_nameToIndexMap [mapKey];
		int index;
		if (!m_nameToIndexMap.TryGetValue(mapKey, out index)) // Returns true.
		{
		//if (index == null) {
			Debug.LogWarning("MarkersDebug.cs: marker key " + mapKey + " not found");
			return;
		}

		GameObject currentGO = markerGOArray[index];
		currentGO.transform.position = mkrPos;
		if (mkrVisib){
			currentGO.GetComponent<Renderer>().material =  matVisible;
			currentGO.tag = "markerVisible";
		}
		else{
			currentGO.GetComponent<Renderer>().material =  matNotVisible;
			currentGO.tag = "markerNotVisible";
		}

	}


	// set marker position, material and tag
	public void SetMarker(string mapKey, Vector3 mkrPos){
		int index;
		if (!m_nameToIndexMap.TryGetValue(mapKey, out index)) // Returns true.
		{
		//if (m_nameToIndexMap[mapKey] == null){
		//	Debug.LogWarning("MarkersDebug.cs: marker key " + mapKey + " not found");
			return;
		}
		SetMarker(m_nameToIndexMap[mapKey], mkrPos);
	}
	
	public void MarkerNotVisible(string mapKey){
		int index;
		if (!m_nameToIndexMap.TryGetValue(mapKey, out index)) // Returns true.
		{
		//if (m_nameToIndexMap[mapKey] == null){
		//	Debug.LogWarning("MarkersDebug.cs: marker key " + mapKey + " not found");
			return;
		}		
		MarkerNotVisible (m_nameToIndexMap [mapKey]);
	}

	// set markers attributes at once
	public void SetMarkers(Vector3 [] markPosArray, bool [] visibArray){
		if (markPosArray.Length != markerGOArray.Count || markPosArray.Length != visibArray.Length){
			Debug.Log("MarkersDebug.cs: array lenghts doesn't match");
			return;
		}

		// update markers debug visibility and pos
		for (int i =0; i<markPosArray.Length; i++){
			markerGOArray[i].transform.position = markPosArray[i];

			if (visibArray[i]){
				markerGOArray[i].GetComponent<Renderer>().material =  matVisible;
				markerGOArray[i].tag = "markerVisible";
			}
			else{
				markerGOArray[i].GetComponent<Renderer>().material =  matNotVisible;
				markerGOArray[i].tag = "markerNotVisible";
			}
		}

	}


	// set marker position and visible
	public void SetMarker(int index, UnityEngine.Vector3 mkrPos){
		if (index>=markerGOArray.Count){
			Debug.Log("MarkersDebug.cs: index " + index + " is bigger than total markers: " + markerGOArray.Count);
			return;
		}
		//if (markerGOArray[index].GetComponent<Renderer>().enabled)
			markerGOArray[index].GetComponent<Renderer>().material =  matVisible;
		markerGOArray[index].tag = "markerVisible";
		markerGOArray[index].transform.position = mkrPos;
	}
	
	public void MarkerNotVisible(int index){
		if (index>=markerGOArray.Count){
			Debug.Log("MarkersDebug.cs: index " + index + " is bigger than total markers: " + markerGOArray.Count);
			return;
		}
		//if (markerGOArray[index].GetComponent<Renderer>().enabled)
			markerGOArray[index].GetComponent<Renderer>().material =  matNotVisible;
		markerGOArray[index].tag = "markerNotVisible";
	}

	public GameObject GetMarker(string name){
		if (m_nameToIndexMap [name] < markerGOArray.Count)
			return markerGOArray [m_nameToIndexMap [name]];
		else
			return null;
	}
	public GameObject GetMarker(int index){
		if (index < markerGOArray.Count)
			return markerGOArray [index];
		else
			return null;
	}

	public void Destroy(){
		foreach (GameObject mkr in markerGOArray) {
			GameObject.Destroy (mkr);		
		}
		GameObject.Destroy (markerDebugParent);
		markerGOArray.Clear ();
		m_nameToIndexMap.Clear ();
	}

}
