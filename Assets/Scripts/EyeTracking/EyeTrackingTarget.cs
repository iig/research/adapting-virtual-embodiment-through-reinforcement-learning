﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EyeTrackingTarget : MonoBehaviour
{
	bool isViewing;
	SetCanvasActive message;
	float notViewingTime = 0f;
	//float messageShowingTime = 0f;
	[SerializeField]
	[Range(0, 600)]
	float notViewingTimeLimit;
	//[SerializeField]
	//[Range(0, 5)]
	//float messageShowingTimeLimit;
	[SerializeField]
	GameObject messagePanel;
	Text txt;

    public string notViewTimeTxt = "";

	bool isActive;

	//bool trialWasInvalid;
	bool currentTrialIsInvalid;
	//public bool waningMessageisActive;

	float startTime = 0f;
	float startTime2 = 0f;
	bool timeSet = false;
	bool timeset2 = false;

	// Start is called before the first frame update
	void Start()
    {
		message = Camera.main.GetComponentInChildren<SetCanvasActive>();
		isActive = false;
		isViewing = false;
		notViewingTime = 0f;
		//messageShowingTime = 0f;
		currentTrialIsInvalid = false;
		//trialWasInvalid = false;
		txt = messagePanel.GetComponent<Text>();
		Debug.Log("Original text: " + txt.text);
		//waningMessageisActive = false;
	}


    // Update is called once per frame
    void FixedUpdate()
    {
		notViewTimeTxt = "Not viewing time: " + notViewingTime;

		if ( isActive && !isViewing) {
			if (notViewingTime >= notViewingTimeLimit) {
				message.setMessageLookAtHand = true;
				currentTrialIsInvalid = true;
				//messageShowingTime += Time.fixedDeltaTime;

				StartCoroutine( ResetEyeNotViewing() );

				if (timeSet) {
				Debug.Log( "Eye not viewing: " + (Time.time - startTime) + " notViewingTime: " + notViewingTime );
				timeSet = false;
				}

				if (!timeset2 ) {
					timeset2 = true;
					startTime2 = Time.time;
				}
				//waningMessageisActive = true;
				//Debug.Log("Current Trial is invalid");

				//if ( !message.setMessageLookAtHand) {
				//	Debug.Log( "Message showing time: " + (Time.time - startTime2) );
				//	notViewingTime = 0f;
				//	timeset2 = false;
				//	startTime2 = Time.time;
				//}

				//if ( messageShowingTime >= messageShowingTimeLimit) {
				//	Debug.Log( "Message showing time: " + messageShowingTime );
				//	Reset();
				//	//waningMessageisActive = false;

				//}
			} else {
				if ( !timeSet ) {
					startTime = Time.time;
					timeSet = true;
				}
				notViewingTime += Time.fixedDeltaTime;
				//txt.text = "Not viewing time: " + notViewingTime;

			}
		} else {
			notViewingTime = 0f;
		}
	}

	IEnumerator ResetEyeNotViewing() {
		yield return new WaitUntil( () => !message.setMessageLookAtHand );
		notViewingTime = 0f;
	}

	// Trial is invalid if for 1s he doesn't look at the hand
	public bool invalidateTrial()
	{
		return currentTrialIsInvalid;
	}

	public void setActive(bool active)
	{
		this.isActive = active;
		// If last trial was invalid
		//trialWasInvalid = currentTrialIsInvalid;
		currentTrialIsInvalid = false;
		// Restart each time we activate / deactivate
		//Reset();
	}

	public void Reset()
	{
		//notViewingTime = 0f;
		//messageShowingTime = 0f;
		//message.setMessageLookAtHand = false;
	}

	private void OnTriggerEnter(Collider other)
	{
		if (other.tag == "EyeGaze") {
			isViewing = true;
			//Debug.Log(this.name + " Enter Trigger: " + other.name + "notViewingTime: " + notViewingTime);
		}
	}

	private void OnTriggerExit(Collider other)
	{
		if (other.tag == "EyeGaze") {
			isViewing = false;
			//Debug.Log(this.name + " Enter Trigger: " + other.name + "notViewingTime: " + notViewingTime);
		}
	}

    private void OnCollisionEnter(Collision other)
    {
        if (other.transform.tag == "EyeGaze")
        {
            isViewing = true;
            //Debug.Log(this.name + " Enter Trigger: " + other.name + "notViewingTime: " + notViewingTime);
        }
    }

    private void OnCollisionExit(Collision other)
    {
        if (other.transform.tag == "EyeGaze")
        {
            isViewing = false;
            //Debug.Log(this.name + " Enter Trigger: " + other.name + "notViewingTime: " + notViewingTime);
        }
    }

}
