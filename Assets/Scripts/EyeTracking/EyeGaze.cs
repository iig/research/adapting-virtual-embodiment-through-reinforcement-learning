﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EyeGaze : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log(this.name + " Enter Trigger: " + other.name);
    }

    private void OnTriggerExit(Collider other)
    {
        Debug.Log(this.name + " Enter Trigger: " + other.name);
    }

    private void OnCollisionEnter(Collision other)
    {
        Debug.Log(this.name + " Enter Collision: " + other.transform.name);
    }

    private void OnCollisionExit(Collision other)
    {
        Debug.Log(this.name + " Exit Collision: " + other.transform.name);
    }
}
