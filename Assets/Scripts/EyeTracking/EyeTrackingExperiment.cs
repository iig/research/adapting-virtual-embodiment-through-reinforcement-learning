﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Assertions;
using UnityEngine;
using ViveSR.anipal.Eye;

public class EyeTrackingExperiment : Singleton<EyeTrackingExperiment>
{
	public bool eyeCalibrationLaunched;
	public enum eyeTrackingMethods{None, ThreadMethod, DirectMethod }
	public eyeTrackingMethods eyeTrackingMethod;
	public bool enabledCursorRenderer;
	public GameObject cursorGaze;
	public ThreadReader threadreader;
	public EyeThreader eyeThreader;
	public SRanipal_Eye_Framework SRanipalFramework;

	private MeshRenderer cursorRenderer;

	private FocusInfo FocusInfo;

	

	// Start is called before the first frame update
	void Start()
    {
		cursorRenderer= cursorGaze.GetComponent<MeshRenderer>();
    }

    // Update is called once per frame
    void Update() {

		SetScript();

		if ( eyeTrackingMethod == eyeTrackingMethods.DirectMethod ) {

			if ( SRanipal_Eye_Framework.Status != SRanipal_Eye_Framework.FrameworkStatus.WORKING &&
			   SRanipal_Eye_Framework.Status != SRanipal_Eye_Framework.FrameworkStatus.NOT_SUPPORT ) return;
		}

		if ( enabledCursorRenderer )
			cursorRenderer.enabled = true;
		else
			cursorRenderer.enabled = false;

			cursorGaze.transform.position = gazePosition();

		
		if(eyeCalibrationLaunched) {

			eyeCalibrationLaunched = !SRanipal_Eye.LaunchEyeCalibration();
		}

	


	}

	private void SetScript () {

		if ( eyeTrackingMethod == eyeTrackingMethods.ThreadMethod ) {
			
			if( !threadreader.isActiveAndEnabled ) {
				threadreader.enabled = true;
				eyeThreader.enabled = true;
			}
			
		
		} else {
			if ( threadreader.isActiveAndEnabled ) {
				threadreader.enabled = false;
			eyeThreader.enabled = false;
			}
		}

		if ( eyeTrackingMethod == eyeTrackingMethods.None ) {
			if ( SRanipalFramework.isActiveAndEnabled )
				SRanipalFramework.enabled = false;
		} else {
			if ( !SRanipalFramework.isActiveAndEnabled )
				SRanipalFramework.enabled = true;
		}


	}

	Vector3 gazePosition() {

        // Only hit layer for EyeTarget
        int layerMask = 1 << 26;

		if ( eyeTrackingMethod == eyeTrackingMethods.ThreadMethod ) {

		
			RaycastHit hit;

		if ( Physics.Raycast( Camera.main.transform.position, threadreader.GetGazeDirection(), out hit,Mathf.Infinity, layerMask) )
		{
			//Debug.Log( hit.transform.name );
			return hit.point;
			//return hit.transform.position;
		
		}
		else {

			return Vector3.zero;
		}

		}
		else if( eyeTrackingMethod == eyeTrackingMethods.DirectMethod ) {


			Vector3 GazeOriginCombinedLocal, GazeDirectionCombinedLocal;
		//	Ray GazeRay;

			if ( SRanipal_Eye.GetGazeRay( GazeIndex.COMBINE, out GazeOriginCombinedLocal, out GazeDirectionCombinedLocal ) ) { } else return Vector3.zero;
		//	if ( SRanipal_Eye.Focus( GazeIndex.COMBINE, out GazeRay, out FocusInfo )) { } else return Vector3.zero;

		//		 GazeDirectionCombinedLocal = GazeRay.direction;
			Vector3 GazeDirectionCombined = Camera.main.transform.TransformDirection( GazeDirectionCombinedLocal );

			RaycastHit hit;

        

            if ( Physics.Raycast( Camera.main.transform.position, GazeDirectionCombined, out hit, Mathf.Infinity, layerMask ) ) {

				return hit.point;
				//return hit.transform.position;

			} else {

				return Vector3.zero;
			}

		}
		else
			return Vector3.zero;

	}
}
