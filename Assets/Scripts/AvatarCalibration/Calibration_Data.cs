﻿/// <summary>
///This is the class to store the data calibrations like the limb length
///and the matrix transformation.
/// </summary>

using System.Collections.Generic;
using System.Xml.Serialization;
using System.IO;
using UnityEngine;


[XmlRoot("calibration")]
public class Calibration_Data
{
    public float height;
    //public Vector3 scale;

    [XmlArray("rigid_body_handlers")]
    [XmlArrayItem("rigid_body_handler")]
    public List<Rigid_Body_Handler> rigidBodyHandler = new List<Rigid_Body_Handler>();

   // [XmlArray("clavicle_reconstructors")]
  //  [XmlArrayItem("clavicle_reconstructor")]
   // public List<Clavicle_Reconstructor> clavicleReconstructors;

    public Calibration_Data() { }

    public Calibration_Data(List<Rigid_Body_Handler> rigidBodyHandler)
    {
        this.rigidBodyHandler = rigidBodyHandler;
    }

    public void Clear()
    {
        this.rigidBodyHandler.Clear();
    }

    /*......SAVE AND LOAD FUNCTION.....*/
    public void Save(string path)
    {
        var serializer = new XmlSerializer(typeof(Calibration_Data));
        using (var stream = new FileStream(path, FileMode.Create))
        {
            serializer.Serialize(stream, this);
            //stream.Close();
        }
    }

     public Calibration_Data Load(string path)
     {
         var serializer = new XmlSerializer(typeof(Calibration_Data));
         using (var stream = new FileStream(path, FileMode.Open))
         {
             return serializer.Deserialize(stream) as Calibration_Data;
            //stream.Close();
        }
     }


}

public class Rigid_Body_Handler
{
    //name of the target
    public string name;
   
    //name of the joint parent ( start)
    public string limb;

    public int type;

    //direction of the limb ( local position of the joint ( child/end) )
    public Vector3 direction;

    //lenght of the limb
    public float length;
    public float radius;

    //Store the name of the tracker with the matrix association tracker/target (name of the tracker)
    [XmlArray("attached_marker_cses")]
    [XmlArrayItem("marker_cs")]
    public List<Marker_CS> attachedMarkerCSes;

    // Store of the Skin local positions for the limb (joint parent/start)
    [XmlArray("attached_points")]
    [XmlArrayItem("point")]
    public List<Point> attachedPoints;

    //Store the name of the joint with the matrix association target/joint (name of the joint ( child/end))
    [XmlArray("attached_bodies")]
    [XmlArrayItem("attached_body")]
    public List<Attached_Body> attachedBodies;

    //In the case of the phasespace save the position of the trackers ( not use here because I generate the maker on runtime)
    [XmlArray("attached_markers_in_local")]
    [XmlArrayItem("point")]
    public List<Point> attachedMarkersInLocal;

    public Rigid_Body_Handler() { }

    public Rigid_Body_Handler(string name, string limb, int type, Vector3 direction, int length, int radius)
    {
        this.name=name;
        this.limb = limb;
        this.type = type;
        this.direction = direction;
        this.length = length;
        this.radius = radius;
    }
}

public class Marker_CS
{
    public string name; 
    public float[] loc_dir;

    public Marker_CS() { }

    public Marker_CS(string name, float[] loc_dir )
    {
        this.name = name;

        for (int j = 0; j < loc_dir.Length;j++)
        {
            this.loc_dir[j] = loc_dir[j];
        }

    }

}


public class Point
{
    public string name; 
    public Vector3 local_coordinate;

    public Point() { }

    public Point( string name, Vector3 local_coordinate)
    {
        this.name = name;
        this.local_coordinate = local_coordinate;
    }
}

public class Attached_Body
{
    public string name; 
    public float[] loc_dir ;

    public Attached_Body() { }

    public Attached_Body(string name, float[] loc_dir)
    {
        this.name = name;

        for (int j = 0; j < loc_dir.Length; j++)
        {
            this.loc_dir[j] = loc_dir[j];
        }

    }
}

/*public class Clavicle_Reconstructor
{
    [XmlAttribute("type")]
    public int type; 

    [XmlArray("markers")]
    [XmlArrayItem("marker")]
    public List<Marker> markers;

    public float[,] vt1_to_clav_loc_dir = new float[4,4];

    public Clavicle_Reconstructor() { }

    public Clavicle_Reconstructor(int type, float[,] loc_dir)
    {
        this.type = type;

        for (int j = 0; j < 4; j++)
        {
            for (int i = 0; i < 4; i++)
            {
                this.vt1_to_clav_loc_dir[i, j] = vt1_to_clav_loc_dir[i, j];

            }

        }

    }

}*/

public class Marker
{
    [XmlAttribute("x")]
    public float x; 

    [XmlAttribute("y")]
    public float y; 

    [XmlAttribute("z")]
    public float z; 

    public Marker() { }

    public Marker(float x, float y, float z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

}