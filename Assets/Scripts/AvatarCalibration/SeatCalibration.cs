﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeatCalibration : MonoBehaviour {

	public GameObject avatarOriginal;
	public GameObject avatarDistorted;
	//public ExperimentController experimentController;
	public GameObject seat;
	public GameObject TopSeat;
	public GameObject DownSeat;
	public KeyCode calibrateSeat = KeyCode.H;
	//public KeyCode stopLiveCalibration = KeyCode.U;
	public enum CalibrationMode { Chair, Avatar, Both };
	public CalibrationMode calibrationMode;
	public KeyCode changeMode = KeyCode.LeftShift;
	public KeyCode increaseRotation = KeyCode.Alpha0;
	public KeyCode decreaseRotation = KeyCode.Alpha9;
	public enum TranslationAxis { X, Y, Z};
	public TranslationAxis translationAxis;
	public KeyCode increasePosition = KeyCode.Alpha8;
	public KeyCode decreasePosition= KeyCode.Alpha7;
	public float incrementRotation = 5;
	public float incrementPosition = 0.01f;

	Character_Generator_Settings avatarOriginalComponent;
	Character_Generator_Settings avatarDistortedComponent;

	Quaternion initialSeatRotation;
	Quaternion offsetRotation;
	Quaternion offsetRotationBis;

	Vector3 initialSeatPosition;
	Vector3 offsetPosition;

	//bool liveOption = false;

	int i = 0;

	private void Awake() {

		avatarOriginalComponent = avatarOriginal.GetComponent<Character_Generator_Settings>();
		avatarDistortedComponent = avatarDistorted.GetComponent<Character_Generator_Settings>();

		initialSeatRotation = TopSeat.transform.localRotation;
		initialSeatPosition = DownSeat.transform.position;

	}
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {


		if (Input.GetKeyDown(calibrateSeat) || calibrationMode == CalibrationMode.Both ) {

			BendAvatar();
			TranslateAvatar();
		}

	//	if (Input.GetKeyDown(stopLiveCalibration))
		//	liveOption = false;

		if (Input.GetKeyDown(increaseRotation))
			IncreaseRotation();


		if (Input.GetKeyDown(decreaseRotation))
			DecreaseRotation();

		if (Input.GetKeyDown(increasePosition))
			IncreasePosition();


		if (Input.GetKeyDown(decreasePosition))
			DecreasePosition();

		if (Input.GetKeyDown(changeMode))
			ChangeMode();


		
	}


	void BendAvatar()
	{
		//offsetRotation = initialSeatRotation*Quaternion.Inverse(TopSeat.transform.localRotation);
		offsetRotation = TopSeat.transform.localRotation * Quaternion.Inverse(initialSeatRotation);
		//offsetRotation = Quaternion.Lerp(TopSeat.transform.localRotation, TopSeat.transform.localRotation, 1);
		//offsetRotationBis = Quaternion.Slerp(TopSeat.transform.localRotation, initialSeatRotation, 1);

		//print("Normal= "+offsetRotation.eulerAngles);
		//print("Inverse= "+Quaternion.Inverse( offsetRotationBis).eulerAngles);

		//avatarOriginalComponent.settingsReferences.spine.localEulerAngles= new Vector3(avatarOriginalComponent.settingsReferences.spine.localEulerAngles.x+ offsetRotation,
		//	avatarOriginalComponent.settingsReferences.spine.localEulerAngles.y,
		//	avatarOriginalComponent.settingsReferences.spine.localEulerAngles.z);

		avatarDistortedComponent.settingsReferences.rightShoulder.localRotation = avatarDistortedComponent.settingsReferences.rightShoulder.localRotation * offsetRotation;
		avatarDistortedComponent.settingsReferences.leftShoulder.localRotation = avatarDistortedComponent.settingsReferences.leftShoulder.localRotation*offsetRotation;
		avatarDistortedComponent.settingsReferences.spine.localRotation = offsetRotation *avatarDistortedComponent.settingsReferences.spine.localRotation ;

		avatarOriginalComponent.settingsReferences.rightShoulder.localRotation = avatarOriginalComponent.settingsReferences.rightShoulder.localRotation * offsetRotation;
		avatarOriginalComponent.settingsReferences.leftShoulder.localRotation = avatarOriginalComponent.settingsReferences.leftShoulder.localRotation * offsetRotation;
		avatarOriginalComponent.settingsReferences.spine.localRotation = offsetRotation * avatarOriginalComponent.settingsReferences.spine.localRotation;


		initialSeatRotation = TopSeat.transform.localRotation;
	}

	void TranslateAvatar() {

		offsetPosition = -initialSeatPosition + DownSeat.transform.position;
		//print(offsetPosition);

		avatarDistortedComponent.settingsReferences.pelvis.position =  avatarDistortedComponent.settingsReferences.pelvis.position + offsetPosition;
		avatarOriginalComponent.settingsReferences.pelvis.position = avatarOriginalComponent.settingsReferences.pelvis.position + offsetPosition;

		initialSeatPosition = DownSeat.transform.position;


	}

	void IncreaseRotation() {

		Quaternion offsetRotation = Quaternion.Euler(new Vector3(-incrementRotation,0,0));

		if(calibrationMode==CalibrationMode.Avatar) {
			
		//	liveOption = false;
			avatarDistortedComponent.settingsReferences.rightShoulder.localRotation = avatarDistortedComponent.settingsReferences.rightShoulder.localRotation * offsetRotation;
			avatarDistortedComponent.settingsReferences.leftShoulder.localRotation = avatarDistortedComponent.settingsReferences.leftShoulder.localRotation * offsetRotation;
			avatarDistortedComponent.settingsReferences.spine.localRotation = offsetRotation * avatarDistortedComponent.settingsReferences.spine.localRotation;

			avatarOriginalComponent.settingsReferences.rightShoulder.localRotation = avatarOriginalComponent.settingsReferences.rightShoulder.localRotation * offsetRotation;
			avatarOriginalComponent.settingsReferences.leftShoulder.localRotation = avatarOriginalComponent.settingsReferences.leftShoulder.localRotation * offsetRotation;
			avatarOriginalComponent.settingsReferences.spine.localRotation = offsetRotation * avatarOriginalComponent.settingsReferences.spine.localRotation;

		} else if (calibrationMode == CalibrationMode.Chair) {
			//liveOption = false;
			TopSeat.transform.localRotation = TopSeat.transform.localRotation * offsetRotation;

		} else {
		//	liveOption = true;
			TopSeat.transform.localRotation = TopSeat.transform.localRotation * offsetRotation;
		}

	}


	void DecreaseRotation() {

		Quaternion offsetRotation = Quaternion.Euler(new Vector3(incrementRotation, 0, 0));

		if (calibrationMode == CalibrationMode.Avatar) {

			//liveOption = false;
			avatarDistortedComponent.settingsReferences.rightShoulder.localRotation = avatarDistortedComponent.settingsReferences.rightShoulder.localRotation * offsetRotation;
			avatarDistortedComponent.settingsReferences.leftShoulder.localRotation = avatarDistortedComponent.settingsReferences.leftShoulder.localRotation * offsetRotation;
			avatarDistortedComponent.settingsReferences.spine.localRotation = offsetRotation * avatarDistortedComponent.settingsReferences.spine.localRotation;

			avatarOriginalComponent.settingsReferences.rightShoulder.localRotation = avatarOriginalComponent.settingsReferences.rightShoulder.localRotation * offsetRotation;
			avatarOriginalComponent.settingsReferences.leftShoulder.localRotation = avatarOriginalComponent.settingsReferences.leftShoulder.localRotation * offsetRotation;
			avatarOriginalComponent.settingsReferences.spine.localRotation = offsetRotation * avatarOriginalComponent.settingsReferences.spine.localRotation;
		}
		else if(calibrationMode == CalibrationMode.Chair) 
		{
			//liveOption = false;
			TopSeat.transform.localRotation = TopSeat.transform.localRotation * offsetRotation;
	
		}
		else
		{
		//	liveOption = true;
			TopSeat.transform.localRotation = TopSeat.transform.localRotation * offsetRotation;
		}
		

	}

	void ChangeMode()
	{
		

		calibrationMode =(CalibrationMode) i;
		i++;

		if (i >= 3)
			i = 0;
	}

	void IncreasePosition()
	{
		float X = 0;
		float Y = 0;
		float Z = 0;
		if(translationAxis== TranslationAxis.X) 
		{
			X = incrementPosition;
		}
		else if (translationAxis == TranslationAxis.Y) 
		{
			Y = incrementPosition;
		}
		else if (translationAxis == TranslationAxis.Z) 
		{
			Z = incrementPosition;
		}

			Vector3 offsetPosition = new Vector3(X, Y, Z);

		if (calibrationMode == CalibrationMode.Avatar) {

			//liveOption = false;

			avatarDistortedComponent.settingsReferences.pelvis.position = -offsetPosition+ avatarDistortedComponent.settingsReferences.pelvis.position;

			avatarOriginalComponent.settingsReferences.pelvis.position = -offsetPosition + avatarOriginalComponent.settingsReferences.pelvis.position;

		} else if (calibrationMode == CalibrationMode.Chair) {
			//liveOption = false;

			DownSeat.transform.parent.GetComponent<HeadTracking>().positionEnable = false;
			seat.transform.position= TopSeat.transform.position- offsetPosition;

		} else {
			//liveOption = true;
			DownSeat.transform.parent.GetComponent<HeadTracking>().positionEnable = false;

			seat.transform.position= seat.transform.position -offsetPosition ;
		}

		

	}

	void DecreasePosition()
	{
		float X = 0;
		float Y = 0;
		float Z = 0;
		if (translationAxis == TranslationAxis.X) {
			X = incrementPosition;
		} else if (translationAxis == TranslationAxis.Y) {
			Y = incrementPosition;
		} else if (translationAxis == TranslationAxis.Z) {
			Z = incrementPosition;
		}

		Vector3 offsetPosition = new Vector3(X, Y, Z);

		if (calibrationMode == CalibrationMode.Avatar) {

			//liveOption = false;

			avatarDistortedComponent.settingsReferences.pelvis.position = offsetPosition + avatarDistortedComponent.settingsReferences.pelvis.position;

			avatarOriginalComponent.settingsReferences.pelvis.position = offsetPosition + avatarOriginalComponent.settingsReferences.pelvis.position;

		} else if (calibrationMode == CalibrationMode.Chair) {
		//	liveOption = false;

			DownSeat.transform.parent.GetComponent<HeadTracking>().positionEnable = false;
			seat.transform.position = TopSeat.transform.position + offsetPosition;

		} else {
		//	liveOption = true;
			DownSeat.transform.parent.GetComponent<HeadTracking>().positionEnable = false;

			seat.transform.position = seat.transform.position +offsetPosition;
		}

	}

}
