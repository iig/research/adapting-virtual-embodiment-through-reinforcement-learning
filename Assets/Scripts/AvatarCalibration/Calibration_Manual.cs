﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.IO;
using UnityEngine;

public abstract class Calibration_Manual : Calibration {


    [Header("Settings")]
    public KeyCode freezeKey = KeyCode.T;
    public KeyCode skeletonKey = KeyCode.T;
    public KeyCode avatarKey = KeyCode.T;
	public KeyCode automaticLimbCalibrationKey = KeyCode.U;

	bool calibrationWithAnAvatar = true;
    public GameObject avatarToCalibrate;
	public GameObject leftAboveShoulderReference;
	public GameObject rightAboveShoulderReference;
	public GameObject avatar_Prefab;
    public GameObject target_prefab;
    public Transform meshRoot;
    public Camera cameraCalibration;


    //These ones has to be initialised in the custom class
    [Header("Function")]
    public bool takeACapture = false;
    public enum automaticCalibrationMethod { };//TODO: implement in the custom class

    [Header("Move Avatar")]
    public bool enableAvatarMoveControl = true;
    public Vector3 translatePosition = new Vector3(0, 0, 0);
    public Vector3 rotateOrientation = new Vector3(0, 0, 0);

    [Header("Scale")]
    public GameObject limbSelected;
   // public bool isScale = false;
  //  public bool extendFullBody = true;
  //  public bool extendUpperBody = false;
   // public bool extendLowerBody = false;

    public InteractionMethod interactionMethod;


    [Header("Option")]
    public bool showRigidBody = true;
    public bool showAvatar = true;
    public bool showSkeleton = false;
    public bool isJointsTargetsAssociated = false;
    public bool isTargetsTrackerAssociated = false;

    [Header("Calibration")]
    [HideInInspector]
    public bool generateTarget = false;
    public bool generateTargetConfig = false;
    public List<Transform> config;
	public List<Transform> LimbsSelectable;
	public bool destroyAllTargets = false;
    public bool hideOrShowTargets = false;
    public bool createTarget = false;
    public bool deleteTarget = false;
    public bool moveTarget = false;
    public GameObject targetSelected;
    bool isSkeletonCalibrationDone = false;
    bool isJointsCalibrationDone = false;
    bool isTargetsCalibrated = false;

    [HideInInspector]
    public bool isMatrixLoaded=true;
	[HideInInspector]
	public bool isAutomaticCalib = false;


    //Character_Generator_Settings referenceTable;
  

    /*.....PARAMETERS FOR CYLINDER AND SPHERE...*/
    bool flagMeshColliderAdded = true;

    /*.....PARAMETERS FOR AVATAR CALIBRATION...*/
    public string loadPathBonesNameToVerticesIndex;
    public string savePathBonesNameToVerticesIndex;
    public string loadPathBonesNameToIndex;
    public string savePathBonesNameToIndex;
    public enum InteractionMethod { BonesCalibration, MeshCalibration };
    Mesh avatarMesh;
    SkinnedMeshRenderer avatarSkinnedMeshRenderer;
    Character_Generator_Settings setting;
    Vector3[] verticesSave;
    Matrix4x4[] bindPosesSave;
    public SerializableDictionary<string, List<int>> bonesNameToVerticesIndex;
    public SerializableDictionary<string, int> bonesNameToIndex;


	/*.....PARAMETERS FOR AVATAR CALIBRATION...*/
	GameObject targetParent;
    List<GameObject> allTargets;
    List<Transform> allChildren;
    Dictionary<GameObject, GameObject> targetsToJoints;
    Dictionary<GameObject, GameObject> targetsToTrackers;
    Dictionary<GameObject , Matrix4x4> matrixJoints;
    Dictionary<GameObject, Matrix4x4> matrixTracker;
	Dictionary<string, float> limbSizes=new Dictionary<string, float>() ;




	public abstract void FreezeTracker(bool isFreeze);

    /*.......UNIT TEST.......*/
    //public GameObject start;
    //public GameObject end;
    //public GameObject sphere1;
    //public Vector3 sphereSize1;
    //public const float thickness = 0.009f;



    // Use this for initialization
    public override void Start()
    {
        base.Start();

        /*.....CHECK THE SCENE......*/
        if(avatarToCalibrate == null)
        {
            if (calibrationWithAnAvatar && GameObject.FindGameObjectWithTag("Avatar") == null)
            {
                if (avatar_Prefab != null)
                {
                    avatarToCalibrate = Instantiate(avatar_Prefab);

                    Debug.LogWarning("An Avatar has been instantiated because no object with the tag Avatar has been found ");

                    if (avatarToCalibrate.GetComponent<Character_Generator_Settings>())
                        setting = avatarToCalibrate.GetComponent<Character_Generator_Settings>();
                    else
                        Debug.Log("No Character Generator Settings Attached on the Avatar");
                }
                else
                    Debug.LogWarning("No object with the tag Avatar has been found and no prefab has been set");

            }
            else
            {
                avatarToCalibrate = GameObject.FindGameObjectWithTag("Avatar");

                if (avatarToCalibrate.GetComponent<Character_Generator_Settings>())
                    setting = avatarToCalibrate.GetComponent<Character_Generator_Settings>();
                else
                    Debug.Log("No Character Generator Settings Attached on the Avatar");
            }
        }
        



        /*.....INITILISATION......*/
       
       
        setting = avatarToCalibrate.GetComponent<Character_Generator_Settings>();
        translatePosition = setting.settingsReferences.pelvis.position;
        rotateOrientation = setting.settingsReferences.pelvis.eulerAngles;

	

		 if( interactionMethod == InteractionMethod.MeshCalibration ) 
			{
				avatarSkinnedMeshRenderer = avatarToCalibrate.GetComponentInChildren<SkinnedMeshRenderer>();
				avatarMesh = avatarSkinnedMeshRenderer.sharedMesh;
				verticesSave = avatarMesh.vertices;
				bindPosesSave = avatarMesh.bindposes;
			// GenerateOrLoadBonesAndVertexIndex(avatarSkinnedMeshRenderer);
		}



		//Calibration Initialisation ( I need to clear all of them before any other loading)
		allTargets = new List<GameObject>();
        allChildren = new List<Transform>();
        targetsToJoints =new Dictionary<GameObject, GameObject>();
        targetsToTrackers=new Dictionary<GameObject, GameObject>();
        matrixJoints=new Dictionary<GameObject, Matrix4x4>();
        matrixTracker=new Dictionary<GameObject, Matrix4x4>();
		targetParent = new GameObject();
		targetParent.name = "IKTargetsGenerated"+avatarToCalibrate.name;

		/*.......UNIT TEST.......*/
		// DrawLineOnGameObject(start, end, Color.blue);
		//DrawCylinderOnGameObject( start,  end,  thickness);
		// DrawSphereOnGameObject(start, scalingFactor);
		//LimbDirectionFunction( end);
		// sphereSize1 = Vector3.one * 0.03f;

	}

    // Update is called once per frame
    public override void Update()
    {

        // Stop the marker
        if (Input.GetKeyDown(freezeKey))
            takeACapture = !takeACapture;

		FreezeTracker(takeACapture);


        //Hid or show rigidbody
        if (GameObject.FindGameObjectWithTag("RigidBody_Transform") != null)
            HideAndShowRigidBody(showRigidBody);

        if (avatarToCalibrate !=null)
        {
            TranslateAndRotate(enableAvatarMoveControl);
        }
        else
            Debug.LogError("No Avatar in the scene");

        SelectLimb(ref limbSelected);
        SelectTarget(ref targetSelected);
        //RotateLimb(limbSelected);

        if (Input.GetKeyDown(avatarKey))
            showAvatar = !showAvatar;

            HideAndShowAvatar(showAvatar);

        if (Input.GetKeyDown(skeletonKey))
            showSkeleton = !showSkeleton;

        if (showSkeleton)
            DrawSkeleton(true);

        if(limbSelected != null)
        {
            if (interactionMethod == InteractionMethod.MeshCalibration)
            {
                LimbScalingPredefined(avatarMesh, limbSelected.transform);
                LimbRotatingPredefined(avatarMesh, limbSelected.transform);
            }

            if(interactionMethod == InteractionMethod.BonesCalibration)
            {
                if(!moveTarget)
                {
                    LimbScalingPredefinedNoMesh(limbSelected.transform);
                    LimbRotatingPredefinedNoMesh(limbSelected.transform);
                }
                
            }

        }


    /*    if (Input.GetKeyDown(KeyCode.UpArrow) && Input.GetKey(KeyCode.LeftShift))
        {
            float scaleFactor = 0.002f;

            if(isScale)
            ScaleFullBody(setting.settingsReferences.root,scaleFactor);
            else
            {
                ScaleFullLimbLength(setting, (scaleFactor + 1), extendFullBody);
                ScaleUpperLimbLength(setting, (scaleFactor + 1), extendUpperBody);
                ScaleLowerLimbLength(setting, (scaleFactor + 1), extendLowerBody);


            }
        }
            

        if (Input.GetKeyDown(KeyCode.DownArrow) && Input.GetKey(KeyCode.LeftShift))
        {
            float scaleFactor = -0.002f;

            if (isScale)
            ScaleFullBody(setting.settingsReferences.root,scaleFactor);
            else
            {
                ScaleFullLimbLength(setting, (scaleFactor + 1), extendFullBody);
                ScaleUpperLimbLength(setting, (scaleFactor + 1), extendUpperBody);
                ScaleLowerLimbLength(setting, (scaleFactor + 1), extendLowerBody);


            }
           
        }*/
          

        GenerateTarget(setting.settingsReferences.pelvis.parent, ref  generateTarget);
        GenerateTargetConfig(config, ref generateTargetConfig);

        //save calibration parameters
        if ((Input.GetKeyDown(saveKey) && !isSaved)|| (save && !isSaved))
        {
            Clear();
            //generateTargetConfig = true;
            //GenerateTargetConfig(config, ref generateTargetConfig);
            isSkeletonCalibrationDone = true;
            isJointsCalibrationDone = true;
            isTargetsCalibrated = true;

            SetAvatarHeight(setting.userHeight);

            SetSkeletonLength(setting.settingsReferences.pelvis.parent, ref isSkeletonCalibrationDone);

            SetJointsMatrix(setting.settingsReferences.pelvis.parent, ref isJointsCalibrationDone);

            SetTargetsMatrix( ref isTargetsCalibrated, setting);


            isSaved = true;

            //save = !save;
        }
            

  

      

        //after save function but before load function ( TODO: independant from the order)
        base.Update();

        //load calibration parameters
        if ((Input.GetKeyDown(loadKey) && isLoaded ) || (load && isLoaded))
        {
            if (isMatrixLoaded)
                ClearList();


            GetSkeletonLength(setting.settingsReferences.pelvis, ref setting);
            GetAvatarScale(ref setting);

            if (isMatrixLoaded)
            {

                GetJointsMatrix(setting.settingsReferences.pelvis.parent, targetsToJoints, matrixJoints, allTargets, allChildren);
                GetTargetsMatrix(targetsToTrackers, matrixTracker, allTargets);


            }

            // isJointsTargetsAssociated = true;
            // isTargetsTrackerAssociated = true;

            isLoaded = false;

            load = !load;

        }

         Association(isJointsTargetsAssociated, isTargetsTrackerAssociated);




        TargetsHide(ref hideOrShowTargets);
        DestroyAllTargets(ref destroyAllTargets);

        if(createTarget)
        {
            deleteTarget = false;
            moveTarget = false;
            TargetCreate(limbSelected);
        }
        if ( deleteTarget)
        {
            createTarget = false;
            moveTarget = false;
            TargetRemove(targetSelected);
        }
        if ( moveTarget)
        {
            createTarget = false;
            deleteTarget = false;
            TargetMove(targetSelected);
        }


		if(Input.GetKeyDown(automaticLimbCalibrationKey) || isAutomaticCalib ) 
		{
			AutomaticLimbCalibration();

			isAutomaticCalib = false;
		}

        //trunkDirectionAvatar == LimbDirection.Y

        /*.......UNIT TEST.......*/
        //DrawSphere(sphere1, scalingFactor,true);
        // DrawCylinder(start, end, thickness, true);


        /* if (Input.GetKeyDown(KeyCode.Q))
         {
          GameObject[] targetV = GameObject.FindGameObjectsWithTag("Target");
         GameObject[] targetZ = GameObject.FindGameObjectsWithTag("test");


         for (int i = 0; i < targetV.Length; i++)
             {


             Matrix4x4 targetMatrixV = TransformationMatrix(targetV[i].transform, GameObject.FindGameObjectWithTag("RigidBody_Transform").transform);
             //Matrix4x4 targetMatrixV = TransformationMatrix(GameObject.FindGameObjectWithTag("RigidBody_Transform").transform, targetV[i].transform);


             AssociateRigidBody(targetMatrixV,targetZ[i].transform, GameObject.FindGameObjectWithTag("RigidBody_Transform").transform,true,true);


             }

             isTargetsCalibrated = false;
         }*/



    }


    /*......AVATAR INTERACTION.....*/

    void TranslateAndRotate(bool enableAvatarMoveControl)
    {
        if(enableAvatarMoveControl)
        {
            setting.settingsReferences.pelvis.position = translatePosition;
            setting.settingsReferences.pelvis.eulerAngles = rotateOrientation;
        }
    

    }

    void LimbRotating(Mesh avatarMesh, Transform limb, float incrementValue, Vector3 axisRotation)
    {
        //Mesh scaled
        MeshLimbRotating(avatarMesh, limb, incrementValue, axisRotation);

        //Bone scaled (joint position)
        LocalRotationLimbScaling(limb, incrementValue, axisRotation);

        //BindPose Updated
        BindPoseUpdate(avatarMesh, limb);
    }

    void LocalRotationLimbScaling(Transform bone, float scaleFactor, Vector3 axisRotation)
    {
        Vector3 incrementValue = axisRotation * scaleFactor;
        bone.transform.Rotate(incrementValue, Space.World);
    }

    void LimbRotatingPredefinedNoMesh(Transform limb)
    {
        //TO DO: rotate around always the same axis for the limb direction independant from the local direction.

        Vector3 axisRotation;
        float incrementValue = 0.05f;


        if ( Input.GetKey(KeyCode.LeftArrow) )
        {
            //this one allows to turn arround always around the same axis indepently the local coordinate system but we have some drift.
            //incrementValue=scaleFactor*LimbDirection(limb.GetChild(0).gameObject);

            //it depends of the local coordinate direction but no drift
            axisRotation = Vector3.up;
            LocalRotationLimbScaling(limb, incrementValue, axisRotation);
        }

        if ( Input.GetKey(KeyCode.RightArrow))
        {
           
                axisRotation = Vector3.up;
            LocalRotationLimbScaling(limb, -incrementValue, axisRotation);
           
        }

        if (Input.GetKey(KeyCode.DownArrow) && Input.GetKey(KeyCode.LeftControl))
        {
          
                axisRotation = Vector3.forward;
            LocalRotationLimbScaling(limb, incrementValue, axisRotation);
        }


        if (Input.GetKey(KeyCode.UpArrow) && Input.GetKey(KeyCode.LeftControl))
        {
          
            axisRotation = Vector3.forward;
            LocalRotationLimbScaling(limb, -incrementValue, axisRotation);

        }

        if (Input.GetKey(KeyCode.UpArrow) && !Input.GetKey(KeyCode.LeftControl))
        {
            if (setting.settingsReferences.leftForearm == limbSelected.transform || setting.settingsReferences.rightForearm == limbSelected.transform)
            {
                return;
            }
            axisRotation = Vector3.right;
            LocalRotationLimbScaling(limb, incrementValue, axisRotation);
        }


        if (Input.GetKey(KeyCode.DownArrow) && !Input.GetKey(KeyCode.LeftControl))
        {
            if (setting.settingsReferences.leftForearm == limbSelected.transform || setting.settingsReferences.rightForearm == limbSelected.transform)
            {
                return;
            }
            axisRotation = Vector3.right;
            LocalRotationLimbScaling(limb, -incrementValue, axisRotation);
        }
    }

    void LimbRotatingPredefined(Mesh avatarMesh, Transform limb)
    {
        //TO DO: rotate around always the same axis for the limb direction independant from the local direction.

        Vector3 axisRotation;
        float incrementValue = 0.2f;

        if (Input.GetKey(KeyCode.UpArrow) && Input.GetKey(KeyCode.LeftControl) && !Input.GetKey(KeyCode.LeftAlt))
        {
            //this one allows to turn arround always around the same axis indepently the local coordinate system but we have some drift.
            //incrementValue=scaleFactor*LimbDirection(limb.GetChild(0).gameObject);

            //it depends of the local coordinate direction but no drift
            axisRotation = Vector3.up;
            LimbRotating(avatarMesh, limb, incrementValue, axisRotation);

        }


        if (Input.GetKey(KeyCode.DownArrow) && Input.GetKey(KeyCode.LeftControl) && !Input.GetKey(KeyCode.LeftAlt))
        {
            axisRotation = Vector3.up;
            LimbRotating(avatarMesh, limb, -incrementValue, axisRotation);
        }

        if (Input.GetKey(KeyCode.LeftArrow) && Input.GetKey(KeyCode.LeftControl) && !Input.GetKey(KeyCode.LeftAlt))
        {
            axisRotation = Vector3.right;
            LimbRotating(avatarMesh, limb, incrementValue, axisRotation);
        }


        if (Input.GetKey(KeyCode.RightArrow) && Input.GetKey(KeyCode.LeftControl) && !Input.GetKey(KeyCode.LeftAlt))
        {
            axisRotation = Vector3.right;
            LimbRotating(avatarMesh, limb, -incrementValue, axisRotation);

        }

        if (Input.GetKey(KeyCode.UpArrow) && Input.GetKey(KeyCode.LeftControl) && Input.GetKey(KeyCode.LeftAlt))
        {
            axisRotation = Vector3.forward;
            LimbRotating(avatarMesh, limb, incrementValue, axisRotation);
        }


        if (Input.GetKey(KeyCode.DownArrow) && Input.GetKey(KeyCode.LeftControl) && Input.GetKey(KeyCode.LeftAlt))
        {
            axisRotation = Vector3.forward;
            LimbRotating(avatarMesh, limb, -incrementValue, axisRotation);
        }
    }

    // select the limb you want to calibrate
    // you must create a layer "Avatar" and put your avatar on it
    void SelectLimb( ref GameObject limb)
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = cameraCalibration.ScreenPointToRay(Input.mousePosition);

            int layerMask = 1 << 9; //LayerMask.GetMask("Avatar");
            //layerMask = ~layerMask;

            if (Physics.Raycast(ray, out hit , Mathf.Infinity, layerMask))
            {
               if(LimbsSelectable.Contains(hit.transform))
                {
                    Debug.Log("You selected the " + hit.transform.name); // ensure you picked right object

                    limb = hit.transform.gameObject;
                }
               else
                {
                    Debug.Log("This bone cannot be calibrated");
                }
              
            }
         /*   else
            {
                
              //  Debug.Log("Nothing selected or your avatar is not on the good layer");
                limb = null;
            }*/
                
        }

    }

    void HideAndShowAvatar( bool showAvatar)
    {
        if(!showAvatar)
        avatarToCalibrate.GetComponentInChildren<SkinnedMeshRenderer>().enabled = false;
        else
        avatarToCalibrate.GetComponentInChildren<SkinnedMeshRenderer>().enabled = true;
    }

    /*......MESH SCALING FUNCTION.....*/

    void MeshLimbScaling(Mesh avatarMesh, Transform bone, float scaleFactor, Vector3 LimbDirection)
    {

        Vector3[] verticesDeformed = avatarMesh.vertices;
        List<int> verticesIndex = new List<int>();

        int boneParentIndex = new int();


        bonesNameToVerticesIndex.TryGetValue(bone.name, out verticesIndex);
        bonesNameToIndex.TryGetValue(bone.parent.name, out boneParentIndex);


        foreach (int vertexIndex in verticesIndex)
        {
            //transform the local coordinate of the vertex into the bone´s local coordinates thanks to bind pose of the chosen bone 
            verticesDeformed[vertexIndex] = avatarMesh.bindposes[boneParentIndex] * verticesDeformed[vertexIndex];

            //translate the vertex according to the bone direction
            verticesDeformed[vertexIndex] = verticesDeformed[vertexIndex] + LimbDirection * scaleFactor;

            //revert the vertex coordinate to its inial local coordinates
            verticesDeformed[vertexIndex] = Matrix4x4.Inverse(avatarMesh.bindposes[boneParentIndex]) * verticesDeformed[vertexIndex];


        }


        avatarMesh.vertices = verticesDeformed;




    }

    void MeshLimbRotating(Mesh avatarMesh, Transform bone, float scaleFactor, Vector3 axisRotation)
    {

        Vector3[] verticesDeformed = avatarMesh.vertices;
        List<int> verticesIndex = new List<int>();

        Quaternion rotation = Quaternion.Euler(axisRotation * scaleFactor);

        bonesNameToVerticesIndex.TryGetValue(bone.name, out verticesIndex);


        foreach (int vertexIndex in verticesIndex)
        {

            //verticesDeformed[vertexIndex] = Matrix4x4.Rotate(rotation) * (verticesDeformed[vertexIndex] - root.transform.InverseTransformPoint(bone.transform.position));

            verticesDeformed[vertexIndex] = Matrix4x4.TRS(Vector3.one,rotation,Vector3.one) * (verticesDeformed[vertexIndex] - meshRoot.transform.InverseTransformPoint(bone.transform.position));

            verticesDeformed[vertexIndex] = verticesDeformed[vertexIndex] + meshRoot.transform.InverseTransformPoint(bone.transform.position);
        }


        avatarMesh.vertices = verticesDeformed;




    }

    void ResetMesh(Mesh targetMesh)
    {

        targetMesh.vertices = verticesSave;
        targetMesh.bindposes = bindPosesSave;

    }


    /*......AVATAR SCALING FUNCTION.....*/
	void AutomaticLimbCalibration ()
	{

		limbSizes.Clear();
		//Calculate the limb length
		if (ComputeLimbLength())
		{
			
			//Adjust the avatar limb
			AdjustLimbSize();

		}

	}

	bool ComputeLimbLength ( ) 
	{

		if(GameObject.Find("LSHLD")!=null && GameObject.Find( "RSHLD" ) != null ) {

			//Spine Calibration
			float distanceLeftShoulder = GameObject.Find( "LSHLD" ).transform.position.y - leftAboveShoulderReference.transform.position.y;
			float distanceRightShoulder = GameObject.Find( "RSHLD" ).transform.position.y - rightAboveShoulderReference.transform.position.y;
			//float sign = / Mathf.Abs( GameObject.Find( "LSHLD" ).transform.position.y - leftAboveShoulderReference.transform.position.y );
			//Debug.Log("sign="+sign);
			float meanSpine =  (distanceLeftShoulder + distanceRightShoulder) / 2;


			limbSizes.Add( setting.settingsReferences.spine.name, meanSpine );

			//Shoulder width
			float shoulderWidth=Vector3.Distance( GameObject.Find( "LSHLD" ).transform.position, GameObject.Find( "RSHLD" ).transform.position);
			limbSizes.Add( setting.settingsReferences.leftShoulder.name, shoulderWidth);

			if ( GameObject.Find( "LELB" ) != null && GameObject.Find( "RELB" ) != null ) 
			{
				//UpperArm
				float leftUpperArm = Vector3.Distance( GameObject.Find( "LSHLD" ).transform.position, GameObject.Find( "LELB" ).transform.position );
				float rightUpperArm = Vector3.Distance( GameObject.Find( "RSHLD" ).transform.position, GameObject.Find( "RELB" ).transform.position );
				float meanUpperArm = (leftUpperArm + rightUpperArm) / 2;
				limbSizes.Add( setting.settingsReferences.leftUpperArm.name, meanUpperArm );

			}

		}


		if ( GameObject.Find( "LELB" ) != null && GameObject.Find( "RELB" ) != null )
		{
			if ( GameObject.Find( "LHAN" ) != null && GameObject.Find( "RHAN" ) != null ) 
			{
				//Arm
				float leftArm = Vector3.Distance( GameObject.Find( "LHAN" ).transform.position, GameObject.Find( "LELB" ).transform.position );
				float rightArm = Vector3.Distance( GameObject.Find( "RHAN" ).transform.position, GameObject.Find( "RELB" ).transform.position );
				float meanArm = (leftArm + rightArm) / 2;
				limbSizes.Add( setting.settingsReferences.leftForearm.name, meanArm );
			}

		}




			return true;
	}

	void AdjustLimbSize ()
	{
		Transform previousLeftParent = leftAboveShoulderReference.transform.parent;
		Transform previousRightParent = rightAboveShoulderReference.transform.parent;
		//Change parent
		leftAboveShoulderReference.transform.parent = setting.settingsReferences.leftUpperArm.transform;
		rightAboveShoulderReference.transform.parent = setting.settingsReferences.rightUpperArm.transform;


		//Spine
		if ( limbSizes.ContainsKey(setting.settingsReferences.spine.name))
		{
			 //Direction of the scaling/limb
			Vector3 limbDirection = LimbDirection( setting.settingsReferences.lastSpine.gameObject );
			LocalPositionLimbScaling( setting.settingsReferences.lastSpine, limbSizes[setting.settingsReferences.spine.name], limbDirection);
		}

		//Shoulder width
		if ( limbSizes.ContainsKey( setting.settingsReferences.leftShoulder.name) ) {
			//Direction of the scaling/limb
			Vector3 limbDirectionLeft = LimbDirection( setting.settingsReferences.leftUpperArm.gameObject );
			Vector3 limbDirectionRight = LimbDirection( setting.settingsReferences.rightUpperArm.gameObject );
			LocalPositionLimbResizing( setting.settingsReferences.leftUpperArm, limbSizes[setting.settingsReferences.leftShoulder.name]/2, limbDirectionLeft );
			LocalPositionLimbResizing( setting.settingsReferences.rightUpperArm, limbSizes[setting.settingsReferences.leftShoulder.name] / 2, limbDirectionRight );

		}

		//UpperArm
		if ( limbSizes.ContainsKey( setting.settingsReferences.leftUpperArm.name ) ) {
			//Direction of the scaling/limb
			Vector3 limbDirectionLeft = LimbDirection( setting.settingsReferences.leftForearm.gameObject );
			Vector3 limbDirectionRight = LimbDirection( setting.settingsReferences.rightForearm.gameObject );
			LocalPositionLimbResizing( setting.settingsReferences.leftForearm, limbSizes[setting.settingsReferences.leftUpperArm.name], limbDirectionLeft );
			LocalPositionLimbResizing( setting.settingsReferences.rightForearm, limbSizes[setting.settingsReferences.leftUpperArm.name], limbDirectionRight );

		}

		//Arm
		if ( limbSizes.ContainsKey( setting.settingsReferences.leftForearm.name ) ) {
			//Direction of the scaling/limb
			Vector3 limbDirectionLeft = LimbDirection( setting.settingsReferences.leftHand.gameObject );
			Vector3 limbDirectionRight = LimbDirection( setting.settingsReferences.rightHand.gameObject );
			LocalPositionLimbResizing( setting.settingsReferences.leftHand, limbSizes[setting.settingsReferences.leftForearm.name], limbDirectionLeft );
			LocalPositionLimbResizing( setting.settingsReferences.rightHand, limbSizes[setting.settingsReferences.leftForearm.name], limbDirectionRight );

		}

		//Reset parent
		leftAboveShoulderReference.transform.parent = previousLeftParent;
		rightAboveShoulderReference.transform.parent = previousRightParent;
	}

	void LocalPositionLimbResizing ( Transform bone, float scaleFactor, Vector3 LimbDirection ) {
		//translate the joint according to the joint direction
		bone.localPosition = LimbDirection * scaleFactor;
	}

	void ScaleFullBody( Transform root , float scaleFactor)
    {
        root.transform.localScale= root.transform.localScale+Vector3.one*scaleFactor;
    }

    void ScaleFullLimbLength( Character_Generator_Settings settings, float scaleFactor, bool extendFullBody)
    {
        Transform[] allChildren = setting.settingsReferences.pelvis.GetComponentsInChildren<Transform>();


        for (int i=0; i<allChildren.Length;i++)
        {
            if(allChildren[i].parent != setting.settingsReferences.head && extendFullBody )
            allChildren[i].localPosition = allChildren[i].localPosition * scaleFactor;

        }  

    }

    void ScaleUpperLimbLength(Character_Generator_Settings settings, float scaleFactor, bool extendUpperBody)
    {
        Transform[] allChildrenSpine = setting.settingsReferences.spine.GetComponentsInChildren<Transform>();


        for (int i = 0; i < allChildrenSpine.Length; i++)
        {
            if (allChildrenSpine[i].parent != setting.settingsReferences.head && extendUpperBody)
                allChildrenSpine[i].localPosition = allChildrenSpine[i].localPosition * scaleFactor;

        }

    }

    void ScaleLowerLimbLength(Character_Generator_Settings settings, float scaleFactor, bool extendLowerBody)
    {
        Transform[] allChildrenLeftLeg = setting.settingsReferences.leftThigh.GetComponentsInChildren<Transform>();
        Transform[] allChildrenRightLeg = setting.settingsReferences.rightThigh.GetComponentsInChildren<Transform>();


        for (int i = 0; i < allChildrenLeftLeg.Length; i++)
        {
            if (extendLowerBody)
            {
                allChildrenLeftLeg[i].localPosition = allChildrenLeftLeg[i].localPosition * scaleFactor;
                allChildrenRightLeg[i].localPosition = allChildrenRightLeg[i].localPosition * scaleFactor;
            }
               

        }

    }

    void LimbScaling(Mesh avatarMesh, Transform limb, float incrementValue)
    {
        //Direction of the scaling/limb
        Vector3 limbDirection = LimbDirection(limb.gameObject);

        //Mesh scaled
        MeshLimbScaling(avatarMesh, limb, incrementValue, limbDirection);

        //Bone scaled (joint position)
        LocalPositionLimbScaling(limb, incrementValue, limbDirection);

        //BindPose Updated
        BindPoseUpdate(avatarMesh, limb);

    }

    void LocalPositionLimbScaling(Transform bone, float scaleFactor, Vector3 LimbDirection)
    {
        //translate the joint according to the joint direction
        bone.localPosition = bone.localPosition + LimbDirection * scaleFactor;
    }

    void LimbScalingPredefinedNoMesh(Transform limb)
    {
        float incrementValue = 0.0001f;
        //Direction of the scaling/limb
        Vector3 limbDirection = LimbDirection(limb.gameObject);

        if (Input.GetKey(KeyCode.Alpha1) )
        {
            LocalPositionLimbScaling(limb, incrementValue, limbDirection);
        }


        if (Input.GetKey(KeyCode.Alpha2))
        {
            LocalPositionLimbScaling(limb, -incrementValue, limbDirection);
        }

    }

    void LimbScalingPredefined(Mesh avatarMesh, Transform limb)
    {
        float incrementValue;

        if (Input.GetKey(KeyCode.UpArrow) && !Input.GetKey(KeyCode.LeftControl) && !Input.GetKey(KeyCode.LeftShift))
        {
            incrementValue = 0.001f;
            LimbScaling(avatarMesh, limb, incrementValue);
        }


        if (Input.GetKey(KeyCode.DownArrow) && !Input.GetKey(KeyCode.LeftControl) && !Input.GetKey(KeyCode.LeftShift))
        {
            incrementValue = -0.001f;
            LimbScaling(avatarMesh, limb, incrementValue);
        }

    }



    /*........TARGET......*/

    void GenerateTarget(Transform root,  ref bool generateTarget )
    {
        if (generateTarget)
        {
			
            Transform[] allChildren = root.GetChild(0).GetComponentsInChildren<Transform>();
            foreach (Transform child in allChildren)
            {
                GameObject target = Instantiate(target_prefab);
                target.name = child.name + "Target";
                target.transform.parent = child;
                target.transform.localPosition = Vector3.zero;
                target.transform.localRotation = Quaternion.Euler(Vector3.zero);
                target.transform.parent = targetParent.transform;

            }

            generateTarget = false;
        }


    }

void GenerateTargetConfig(List<Transform> config, ref bool generateTargetConfig)
{
    if (generateTargetConfig && GameObject.FindGameObjectWithTag("Target")==null)
    {

        foreach (Transform child in config)
        {
            GameObject target = Instantiate(target_prefab);
            target.name = child.name + "Target";
            target.transform.parent = child;
            target.transform.localPosition = Vector3.zero;
            target.transform.localRotation = Quaternion.Euler(Vector3.zero);
			target.transform.parent = targetParent.transform;
			}

        generateTargetConfig = false;
    }

}

    void DestroyAllTargets(ref bool destroyAllTargets)
    {

        if (destroyAllTargets)
        {
            GameObject[] allTargets= GameObject.FindGameObjectsWithTag("Target");
            foreach (GameObject target in allTargets)
            {
                GameObject.DestroyImmediate(target);
            }

            destroyAllTargets = false;
        }
    }

    // select the limb you want to calibrate
    // you must create a layer "Target" and put your avatar on it
    void SelectTarget( ref GameObject target)
    {
        if (Input.GetMouseButtonDown(0) && Input.GetKey(KeyCode.LeftControl))
        {
            RaycastHit hit;
            Ray ray = cameraCalibration.ScreenPointToRay(Input.mousePosition);

            int layerMask = 1 << 10; //LayerMask.GetMask("Avatar");
            //layerMask = ~layerMask;

            if (Physics.Raycast(ray, out hit, Mathf.Infinity, layerMask))
            {

                Debug.Log("You selected the " + hit.transform.name); // ensure you picked right object

                target = hit.transform.gameObject;
            }

        }

    }

    void TargetCreate(GameObject limbSelected)
    {

            if (limbSelected != null && GameObject.Find(limbSelected.name + "Target") ==null)
            {
                GameObject target = Instantiate(target_prefab);
                target.transform.position = limbSelected.transform.position;
                target.transform.rotation = limbSelected.transform.rotation;
                target.name = limbSelected.name + "Target";
				target.transform.parent = targetParent.transform;
		}

            limbSelected = null;
    }

    void TargetRemove(GameObject targetSelected)
    {
            if(targetSelected!=null)
               GameObject.Destroy(targetSelected);
        
    }

    void TargetMove(GameObject targetSelected)
    {
        float scaleFactor = 0.002f;

            if (targetSelected != null)
            {
                if (Input.GetKeyDown(KeyCode.UpArrow) && Input.GetKey(KeyCode.LeftControl))
                {
                    
                    targetSelected.transform.localPosition = targetSelected.transform.localPosition + Vector3.up* scaleFactor;
                  
                }

                if (Input.GetKeyDown(KeyCode.DownArrow) && Input.GetKey(KeyCode.LeftControl))
                {
                    targetSelected.transform.localPosition = targetSelected.transform.localPosition + Vector3.down * scaleFactor;

                }

                if (Input.GetKeyDown(KeyCode.LeftArrow) && Input.GetKey(KeyCode.LeftControl))
                {
                    targetSelected.transform.localPosition = targetSelected.transform.localPosition + Vector3.left * scaleFactor;

                }

                if (Input.GetKeyDown(KeyCode.RightArrow) && Input.GetKey(KeyCode.LeftControl))
                {
                    targetSelected.transform.localPosition = targetSelected.transform.localPosition + Vector3.right * scaleFactor;

                }

                if (Input.GetKeyDown(KeyCode.UpArrow) && Input.GetKey(KeyCode.LeftControl) && Input.GetKey(KeyCode.LeftAlt))
                {
                    targetSelected.transform.localPosition = targetSelected.transform.localPosition + Vector3.forward * scaleFactor;

                }

                if (Input.GetKeyDown(KeyCode.DownArrow) && Input.GetKey(KeyCode.LeftControl) && Input.GetKey(KeyCode.LeftAlt))
                {
                    targetSelected.transform.localPosition = targetSelected.transform.localPosition + Vector3.back * scaleFactor;

                }

            }

    }

    void TargetsHide(ref bool hideOrShowTarget)
    {
        if(hideOrShowTarget)
        {
            GameObject[] allTargets = GameObject.FindGameObjectsWithTag("Target");
           
            foreach(GameObject target in allTargets)
            {
                target.GetComponent<MeshRenderer>().enabled =!target.GetComponent<MeshRenderer>().enabled;
            }

            hideOrShowTarget = false;
        }
    }


    /*......DRAW SKELETTON.....*/

    void DrawBones(float thickness, bool isCollider, ref bool  flagMeshColliderAdded)
    {
        Transform[] allChildren = setting.settingsReferences.pelvis.GetComponentsInChildren<Transform>();
        bool isColliderLocal = isCollider;
      

        //child is your child transform
        for(int i=0;i<(allChildren.Length-1); i++ )
        {
            if (i == (allChildren.Length - 2))
            {
                flagMeshColliderAdded = false;

            }

            for (int j = i+1; j < (allChildren.Length - 1); j++)
                {
                    if (allChildren[i].parent == allChildren[j].parent || allChildren[i] == allChildren[j].parent)
                    {
                        DrawCylinder(allChildren[i].gameObject, allChildren[j].gameObject, thickness, isColliderLocal,flagMeshColliderAdded);

                       // if (j == (allChildren.Length - 1))
                         //   isColliderLocal = false;
                    }

                }
            
        }

    }

    void DrawJoints(Vector3 sphereSize, bool isCollider)
    {
        Transform[] allChildren = setting.settingsReferences.pelvis.parent.GetComponentsInChildren<Transform>();

        //child is your child transform
        foreach (Transform child in allChildren)
        {
            DrawSphere( child.gameObject, sphereSize, isCollider);
        }
    }

    void DrawSkeleton( bool isCollider )
    {
        float bonesThickness = 0.009f;
        Vector3 sphereSize = Vector3.one * 0.03f;


        DrawBones(bonesThickness, isCollider, ref flagMeshColliderAdded);

        DrawJoints(sphereSize, isCollider);


    }

    /*...................OTHER................*/

    void BindPoseUpdate(Mesh avatarMesh, Transform bone)
    {

        Matrix4x4[] bindPosesChanged = avatarMesh.bindposes;
        Transform[] allChildren = bone.GetComponentsInChildren<Transform>();


        //after translating a joint you need to update its bindpose matrix and its child bindpose matrix

        /*The mesh's bind pose takes the vertex from local space to world-space,
         * and then each bone's inverse bind pose takes the mesh vertex from world-space to the local space of that bone*
         * For more explanation about bindpose:
         * https://forum.unity.com/threads/some-explanations-on-bindposes.86185/
         * 
         * Def:
         * bindPosesChanged[boneIndex] = bone.worldToLocalMatrix * root.localToWorldMatrix;
         */

        //Go through all the child and the parent to update their bindPose matrix ( the child is your child transform)
        for (int i = 0; i < (allChildren.Length-1 ); i++)
        {
            int boneIndexLoop;

            bonesNameToIndex.TryGetValue(allChildren[i].name, out boneIndexLoop);

            bindPosesChanged[boneIndexLoop] = allChildren[i].worldToLocalMatrix * meshRoot.localToWorldMatrix;

            Debug.Log(allChildren[i].name);
        }

        avatarMesh.bindposes = bindPosesChanged;

    }
    //Find the bone direction
    Vector3 LimbDirection(GameObject end)
    {
        return Vector3.Normalize(end.transform.localPosition);

    }

 /*   void OnApplicationQuit()
    {
        ResetMesh(avatarMesh);
    }*/


    /*...................RIGGING INDEX................*/

    void GetVerticesIndexAttachedToABone(SkinnedMeshRenderer avatarSkinnedMeshRenderer)
    {
        Mesh avatarMesh = avatarSkinnedMeshRenderer.sharedMesh;

        for (int i = 0; i < avatarSkinnedMeshRenderer.bones.Length; i++)
        {

            string boneName = avatarSkinnedMeshRenderer.bones[i].name;

            if (avatarSkinnedMeshRenderer.bones[i].name == boneName)
            {
                List<int> boneIndex = new List<int>();

                for (int j = 0; j < avatarMesh.boneWeights.Length; j++)
                {

                    if (avatarMesh.boneWeights[j].boneIndex0 == i || avatarMesh.boneWeights[j].boneIndex1 == i ||
                         avatarMesh.boneWeights[j].boneIndex2 == i || avatarMesh.boneWeights[j].boneIndex3 == i)
                    {

                        boneIndex.Add(j);

                    }

                }

                bonesNameToVerticesIndex.Add(boneName, boneIndex);
                bonesNameToIndex.Add(boneName, i);
            }

        }


    }

    public void SaveBonesNameToVerticesIndex(string path, SerializableDictionary<string, List<int>> bonesNameToVerticesIndex)
    {
        var serializer = new XmlSerializer(typeof(SerializableDictionary<string, List<int>>));
        using (var stream = new FileStream(path, FileMode.Create))
        {
            serializer.Serialize(stream, bonesNameToVerticesIndex);
            //stream.Close();
        }
    }

    public SerializableDictionary<string, List<int>> LoadBonesNameToVerticesIndex(string path)
    {
        var serializer = new XmlSerializer(typeof(SerializableDictionary<string, List<int>>));
        using (var stream = new FileStream(path, FileMode.Open))
        {
            return serializer.Deserialize(stream) as SerializableDictionary<string, List<int>>;
            //stream.Close();
        }
    }
    public void SaveBonesNameToIndex(string path, SerializableDictionary<string, int> bonesNameToIndex)
    {
        var serializer = new XmlSerializer(typeof(SerializableDictionary<string, int>));
        using (var stream = new FileStream(path, FileMode.Create))
        {
            serializer.Serialize(stream, bonesNameToIndex);
            //stream.Close();
        }
    }

    public SerializableDictionary<string, int> LoadBonesNameToIndex(string path)
    {
        var serializer = new XmlSerializer(typeof(SerializableDictionary<string, int>));
        using (var stream = new FileStream(path, FileMode.Open))
        {
            return serializer.Deserialize(stream) as SerializableDictionary<string, int>;
            //stream.Close();
        }
    }

    public void GenerateOrLoadBonesAndVertexIndex(SkinnedMeshRenderer avatarSkinnedMeshRenderer)
    {
        //TODO : check if the file exist not only the string for the loading
        if (loadPathBonesNameToVerticesIndex != "" && loadPathBonesNameToIndex != "")
        {
            bonesNameToIndex = LoadBonesNameToIndex(loadPathBonesNameToIndex);
            bonesNameToVerticesIndex = LoadBonesNameToVerticesIndex(loadPathBonesNameToVerticesIndex);
        }
        else if (savePathBonesNameToVerticesIndex != "" && savePathBonesNameToIndex != "")
        {
            Debug.Log(" loadPathBonesNameToVerticesIndex and loadPathBonesNameToIndex  needs to be fill to be able to load");

            bonesNameToVerticesIndex = new SerializableDictionary<string, List<int>>();
            bonesNameToIndex = new SerializableDictionary<string, int>();

            GetVerticesIndexAttachedToABone(avatarSkinnedMeshRenderer);

            SaveBonesNameToIndex(savePathBonesNameToIndex, bonesNameToIndex);
            SaveBonesNameToVerticesIndex(savePathBonesNameToVerticesIndex, bonesNameToVerticesIndex);

        }
        else
        {
            Debug.Log("You need to fill the two save paths or the two load paths");
        }

    }


    /*......SET AND GET DIFFERENT DATA.....*/

    //Calibration order:

        //First
    void SetSkeletonLength( Transform root , ref bool isSkeletonCalibrationDone)
    {
        if(isSkeletonCalibrationDone)
        {
            Transform[] allChildren = root.GetComponentsInChildren<Transform>();

            for (int i = 0; i < allChildren.Length; i++)
            {
                int IdNumberofRigidbody = new int();
                NewRigidBodyData(ref IdNumberofRigidbody);

                float segmentLength = Vector3.Distance(allChildren[i].parent.position, allChildren[i].position); //Vector3.Magnitude(allChildren[i].localPosition);
                SetLimbSegmentLength(allChildren[i].parent.name, segmentLength, allChildren[i].localPosition, IdNumberofRigidbody);
            }

            isSkeletonCalibrationDone = false;

        }

    }

    //Second
    void SetJointsMatrix(Transform root , ref bool isJointsCalibrationDone)
    {
        if (isJointsCalibrationDone)
        {
            Transform[] allChildren = root.GetComponentsInChildren<Transform>();

            for (int i = 0; i < allChildren.Length; i++)
            {
                Transform targetName = GetTargetUnique(allChildren[i]);

                if (targetName!=null)
                {
                    Matrix4x4 jointMatrix = TransformationMatrix(allChildren[i], targetName);
                    

                    SetJointMatrix(allChildren[i].name, targetName.name, jointMatrix, i );

                }


            }

            isJointsCalibrationDone = false;
        }
    }

    //Third
    //targets different name as joints or targets.
    void SetTargetsMatrix( ref bool isTargetsCalibrated, Character_Generator_Settings Avatar)
    {

        if ( isTargetsCalibrated )
        {
            // float distanceMax = 0.3f;
            //float distanceMax = Mathf.Infinity;

            for (int i = 0; i < calibrationData.rigidBodyHandler.Count; i++)
            {
                GameObject target = GameObject.Find(calibrationData.rigidBodyHandler[i].name);

                Transform tracker = null;

                if (target!=null)
                    tracker = GetTrackerUnique(target.transform,Avatar);

                if( tracker !=null)
                {
                    Matrix4x4 targetMatrix = TransformationMatrix(target.transform, tracker );
                   // Matrix4x4 targetMatrix = TransformationMatrix(tracker, target.transform );

                    SetRigidBodyMatrix(tracker.name, targetMatrix, i);

                    //Debug.Log(tracker.name+"=="+ target.name + "==" + targetMatrix.ToString());
                   // Debug.Log(target.name);
                   // Debug.Log(targetMatrix);
                }

            }

            isTargetsCalibrated = false;
        }
    }

    void GetAvatarScale(ref Character_Generator_Settings AvatarCharacterSettings)
    {
        if(GetAvatarHeight()!=0)
        {
            AvatarCharacterSettings.userHeight = GetAvatarHeight();
            AvatarCharacterSettings.manualScale = true;
        }
   

    }

   Transform GetNearestTarget(Transform joint , string targetTag)
    {
        GameObject[] allTargets = GameObject.FindGameObjectsWithTag(targetTag);
        //Debug.Log(allTargets.Length);

        float distanceMax = Mathf.Infinity;
        GameObject targetChosen= null;

        foreach(GameObject target in allTargets)
        {
            float distance = Vector3.Distance(joint.position, target.transform.position);
           // Debug.Log(target);
           // Debug.Log(distance);
           // Debug.Log(joint);

            if (distance <= distanceMax)
            {
                targetChosen = target;
                distanceMax = distance;
            }
                
                
        }
       // Debug.Log(targetChosen);
        /* if (targetChosen == null)
             return null;
         else*/
        return targetChosen.transform;
        
    }

    Transform GetTargetAssociated(Transform joint, string targetTag , float distanceMax)
    {
        GameObject[] allTargets = GameObject.FindGameObjectsWithTag(targetTag);
        //Debug.Log(allTargets.Length);

        GameObject targetChosen = null;

        foreach (GameObject target in allTargets)
        {
            float distance = Vector3.Distance(joint.position, target.transform.position);
            // Debug.Log(target);
            // Debug.Log(distance);
            // Debug.Log(joint);

            if (distance <= distanceMax)
            {
                targetChosen = target;
                distanceMax = distance;
            }


        }
        // Debug.Log(targetChosen);
         if (targetChosen == null)
             return null;
         else
        return targetChosen.transform;

    }

    Transform GetTargetUnique(Transform joint)
    {
       

        GameObject targetChosen = null;

        targetChosen = GameObject.Find(joint.name+"Target");


        // Debug.Log(targetChosen);
        if (targetChosen == null)
            return null;
        else
            return targetChosen.transform;

    }

    //TODO: name of the tracker can be changed on load and not hardcoded
    Transform GetTrackerUnique(Transform target, Character_Generator_Settings Avatar)
    {
        GameObject targetChosen = null;


        GameObject[] allTrackers = GameObject.FindGameObjectsWithTag("RigidBody_Transform");

        foreach ( GameObject tracker in allTrackers )
        {
           if (target.name == Avatar.settingsReferences.head.name + "Target")
            {
                if (tracker.name == "VRHeadset")
                {
                    targetChosen = tracker;
                }
            }
            
            if (target.name == Avatar.settingsReferences.chest.name + "Target")
            {
                if (tracker.name == "CHE")
                {
                    targetChosen = tracker;
                }
            }

            if (target.name == Avatar.settingsReferences.pelvis.name + "Target")
            {
                if (tracker.name == "PSI")
                {
                    targetChosen = tracker;
                }
            }

			if ( target.name == Avatar.settingsReferences.leftUpperArm.name + "Target" ) {
				if ( tracker.name == "LSHLD" ) {
					targetChosen = tracker;
				}
			}

			if (target.name == Avatar.settingsReferences.leftForearm.name + "Target")
            {
                if (tracker.name == "LELB")
                {
                    targetChosen = tracker;
                }
            }


            if (target.name == Avatar.settingsReferences.leftHand.name + "Target")
            {
                if (tracker.name == "LHAN")
                {
                    targetChosen = tracker;
                }
            }

			if ( target.name == Avatar.settingsReferences.rightUpperArm.name + "Target" ) {
				if ( tracker.name == "RSHLD" ) {
					targetChosen = tracker;
				}
			}

			if (target.name == Avatar.settingsReferences.rightForearm.name + "Target")
            {
                if (tracker.name == "RELB")
                {
                    targetChosen = tracker;
                }
            }
 

            if (target.name == Avatar.settingsReferences.rightHand.name + "Target")
            {
                if (tracker.name == "RHAN")
                {
                    targetChosen = tracker;
                }
            }

            if (target.name == Avatar.settingsReferences.leftCalf.name + "Target")
            {
                if (tracker.name == "LKN")
                {
                    targetChosen = tracker;
                }
            }
    

            if (target.name == Avatar.settingsReferences.leftFoot.name + "Target")
            {
                if (tracker.name == "LFT")
                {
                    targetChosen = tracker;
                }
            }

            if (target.name == Avatar.settingsReferences.rightCalf.name + "Target")
            {
                if (tracker.name == "RKN")
                {
                    targetChosen = tracker;
                }
            }


            if (target.name == Avatar.settingsReferences.rightFoot.name + "Target")
            {
                if (tracker.name == "RFT")
                {
                    targetChosen = tracker;
                }
            }
       
        }

     


        // Debug.Log(targetChosen);
        if (targetChosen == null)
            return null;
        else
            return targetChosen.transform;

    }

    /*
    void GetUserScale(Transform root, bool isScale)
    {
        if(isScale)
        root.transform.localScale = GetAvatarHeight();

    }
    */

    void GetSkeletonLength(Transform root , ref Character_Generator_Settings AvatarCharacterSettings)
    {

            Transform[] allChildren = root.GetComponentsInChildren<Transform>();
        float rate = GetAvatarHeight()/ AvatarCharacterSettings.initialAvatarHeight;// ratioHeight = userHeight / initialAvatarHeight;


        for (int i = 0; i < (allChildren.Length-1); i++)
            {

                float segmentLength = Vector3.Magnitude(allChildren[i].localPosition);

                Transform[] allSubChildren = allChildren[i].GetComponentsInChildren<Transform>();

                for (int j = 1; j < allSubChildren.Length ; j++)
                {
                 if (allSubChildren[j].parent == allSubChildren[0])
                 {
                   if( GetLimbSegmentLength(allSubChildren[0].name, allSubChildren[j].name) != 0)
                   {
                        
                        
                        allSubChildren[j].localPosition = (GetLimbSegmentLength(allSubChildren[0].name, allSubChildren[j].name)/rate)* LimbDirection(allSubChildren[j].gameObject);
                        
                    }
                 }

                }
            }


    }

    void GetJointsMatrix(Transform root, Dictionary<GameObject, GameObject> targetsToJoints, Dictionary<GameObject, Matrix4x4> matrixJoints, List<GameObject> allTargets, List<Transform> allChildren)
    {
        Transform[] allChildrenTab = root.GetComponentsInChildren<Transform>();


        for (int i = 1; i < allChildrenTab.Length; i++)
            {
            string targetName="" ;
            Matrix4x4 jointMatrix = GetJointMatrix(allChildrenTab[i].name, ref targetName);

            if (targetName != "")
            {
                if (GameObject.Find(targetName) == null)
                {
                    GameObject target = Instantiate(target_prefab);
					target.transform.parent = targetParent.transform;
					target.name = targetName;
                    allTargets.Add(target);

                    

                    // targetsToJoints.Add(target,allChildren[i].gameObject);
                    // matrixJoints.Add(target, jointMatrix);

                }
                allChildren.Add(allChildrenTab[i]);
                targetsToJoints.Add(allChildrenTab[i].gameObject, GameObject.Find(targetName));


                matrixJoints.Add(allChildrenTab[i].gameObject, jointMatrix);

               AssociateRigidBody(Matrix4x4.Inverse(jointMatrix), GameObject.Find(targetName).transform, allChildrenTab[i].transform, true, true); 
            }


        }

    }



    void GetTargetsMatrix(Dictionary<GameObject, GameObject> targetsToTrackers, Dictionary<GameObject, Matrix4x4> matrixTracker, List<GameObject> allTargets)
    {
        int nbTrackers = 0;
        for (int i = 0; i < allTargets.Count; i++)
        {
           string trackerName = "";
           Matrix4x4 rigidBodyMatrix = GetRigidBodyMatrix(allTargets[i].name, ref trackerName);

            GameObject[] allTrackers = GameObject.FindGameObjectsWithTag("RigidBody_Transform");

            foreach (GameObject tracker in allTrackers)
            {
                if (tracker.name == trackerName)
                {
                    //if (GameObject.Find(trackerName) != null)
                    nbTrackers++;


                    targetsToTrackers.Add(allTargets[i], tracker);
                    matrixTracker.Add(allTargets[i], rigidBodyMatrix);
                }

            }

           // Debug.Log(allTargets[i].name +"=="+ GameObject.Find(trackerName).name + "==" + rigidBodyMatrix.ToString());


          //  AssociateRigidBody(rigidBodyMatrix, allTargets[i].transform, GameObject.Find(trackerName).transform, true, true);


        }

        if(nbTrackers==0)
            Debug.LogError("Loading Failed! Trackers requiered in the scene!");

    }


    void AssociateJointsToTargets(Dictionary<GameObject, GameObject> targetsToJoints, Dictionary<GameObject, Matrix4x4> matrixJoints, List<Transform> allChildren)
    {
        //table of target 
        // regarder les trackers grace aux targets
 
        for (int i=0; i< allChildren.Count; i++)
        {
            Matrix4x4 matrixJoint;
            GameObject target;
            targetsToJoints.TryGetValue(allChildren[i].gameObject, out target);
            matrixJoints.TryGetValue(allChildren[i].gameObject, out matrixJoint);
            AssociateRigidBody(matrixJoint, allChildren[i], target.transform, true, true);

        }
        
    }

    void AssociateTargetsToTrackers(Dictionary<GameObject, GameObject> targetsToTrackers, Dictionary<GameObject, Matrix4x4> matrixTracker, List<GameObject> allTargets)
    {
        
        //table of target 
        // regarder les trackers grace aux targets
        for (int i = 0; i < allTargets.Count; i++)
        {
 
            Matrix4x4 matrixTrackers;
            GameObject tracker;
         
            targetsToTrackers.TryGetValue(allTargets[i], out tracker);
            matrixTracker.TryGetValue(allTargets[i], out matrixTrackers);



            if (tracker != null)
            {
                //Debug.Log(i);
                //Debug.Log(matrixTrackers);
                //Debug.Log(allTargets[i]);
                //Debug.Log(tracker);

                AssociateRigidBody(matrixTrackers, allTargets[i].transform, tracker.transform, true, true);

            }
            else if(targetsToTrackers ==null)
                Debug.LogError("Tracker list empty!");
        }
    }

    void Association(bool isJointsTargetsAssociated, bool isTargetsTrackerAssociated)
    {
        if (isJointsTargetsAssociated)
            AssociateJointsToTargets(targetsToJoints, matrixJoints, allChildren);

        if (isTargetsTrackerAssociated)
            AssociateTargetsToTrackers(targetsToTrackers, matrixTracker,allTargets);

    }

    /*.................OTHER........*/

    void ClearList()
    {
        allTargets.Clear();
        allChildren.Clear();
        targetsToJoints.Clear();
        targetsToTrackers.Clear();
        matrixJoints.Clear();
         matrixTracker.Clear();

        GameObject[] allTargetInTheScene = GameObject.FindGameObjectsWithTag("Target");
        foreach( GameObject target in allTargetInTheScene)
        {
            GameObject.DestroyImmediate(target);
        }



    }
}
