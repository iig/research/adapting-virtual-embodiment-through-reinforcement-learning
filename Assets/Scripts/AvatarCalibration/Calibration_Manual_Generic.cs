﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using IIG;

public class Calibration_Manual_Generic: Calibration_Manual
{
     GameObject[] trackers;
	bool initialUpdatePosition;
	bool initialUpdateRotation;

	// [Header("Option")]
	// public bool showMarker=true;


	//RigidBodyTracker[] rigidBodyTrackerComponent;
	//IIG.MocapInputController mocapInputControllerComponent;

	public override void  Start()
    {
        base.Start();

		trackers = GameObject.FindGameObjectsWithTag( "RigidBody_Transform" );
		if(GameObject.Find("VRHeadset").GetComponent<Place_holder>()!=null) 
		{
			initialUpdatePosition = GameObject.Find( "VRHeadset" ).GetComponent<Place_holder>().isPositionDisable;
			initialUpdateRotation = GameObject.Find( "VRHeadset" ).GetComponent<Place_holder>().isRotationDisable;
		}

      /*  if (phasespaceManager.GetComponent<RigidBody_Phasespace_Tracker>() != null)
        {
            rigidBodyPhasespaceTrackerComponent = phasespaceManager.GetComponent<RigidBodyTracker>();
        }
        else
            Debug.LogError("No  RigidBody_Phasespace_Tracker attached to the phasespaceManager ");

        if (phasespaceManager.GetComponent<IIG.MocapInputController>() != null)
        {
            mocapInputControllerComponent = phasespaceManager.GetComponent<IIG.MocapInputController>();
        }
        else
            Debug.LogError("No  MocapInputController attached to the phasespaceManager ");*/
    }

    public override void Update()
    {
        base.Update();

    /*    if (mocapInputControllerComponent != null)
        { 
            
            HideOrShowMarker(showMarker);
        }
        else
            Debug.LogError("No  VrpnClientWrapper attached to the phasespaceManager ");*/
    }

    //this function will freeze the marker and consequently the rigidbody of the phasespace 
    // in their current rotation and position
    public override void FreezeTracker( bool isFreeze)
    {
        if (trackers != null)
        {
            if(isFreeze)
            {
				foreach ( GameObject tracker in trackers)
				{
					if ( tracker.GetComponent<MonoBehaviour>() != null ) 
					{
						//tracker.GetComponent<RigidBodyTracker>().updateRotation = false;
						//tracker.GetComponent<RigidBodyTracker>().updatePosition = false;
						foreach ( MonoBehaviour childCompnent in tracker.GetComponentsInChildren<MonoBehaviour>() ) {

							if ( tracker.name == "VRHeadset" ) {
								tracker.GetComponent<Place_holder>().isPositionDisable = true;
								tracker.GetComponent<Place_holder>().isRotationDisable = true;
							} else {
								childCompnent.enabled = false;
							}	
							
						}
							

					}

				}
                //mocapInputControllerComponent.updateMarkers = false;
            }
            else
            {
				foreach ( GameObject tracker in trackers ) {

					if( tracker.GetComponent<MonoBehaviour>() !=null) {
						//tracker.GetComponent<RigidBodyTracker>().updateRotation = true;
						//tracker.GetComponent<RigidBodyTracker>().updatePosition = true;
						foreach ( MonoBehaviour childCompnent in tracker.GetComponentsInChildren<MonoBehaviour>() ) {

							if ( tracker.name == "VRHeadset" ) {
								tracker.GetComponent<Place_holder>().isPositionDisable = initialUpdatePosition;
								tracker.GetComponent<Place_holder>().isRotationDisable = initialUpdateRotation;
							} else {
								childCompnent.enabled = true;
							}

						}
					}
				

				}
				// mocapInputControllerComponent.updateMarkers = true;
			}
        
        }
        else
            Debug.LogError("No  Tracker in the scene ");
    }


  /*  public void HideOrShowMarker( bool showMarker)
    {


        mocapInputControllerComponent.drawMarkersTransform = showMarker;
        


    }*/

 


}
