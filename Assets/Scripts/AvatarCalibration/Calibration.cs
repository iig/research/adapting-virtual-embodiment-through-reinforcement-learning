﻿/// <summary>
///This script already filled, allows to save the data calibration in the proper format. 
///Computation of the limb length and the rigidbody matrice.
/// </summary>
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using UnityEngine;


public class Calibration : MonoBehaviour
{

    [Header("Settings")]
    public string pathOfSavingFiles;
    public string pathOfLoadingFiles;
    public string nameSaveFiles;
    public string nameLoadFiles;

    [HideInInspector]
    public bool isSaved = false;
    public bool isLoaded=false;

    public KeyCode saveKey = KeyCode.S;
    public KeyCode loadKey = KeyCode.Return;
    public bool save;
    public bool load;

    public Calibration_Data calibrationData = new Calibration_Data();

    /*.....PARAMETERS FOR CYLINDER AND SPHERE...*/
    Dictionary<int, Mesh> _meshMap = new Dictionary<int, Mesh>();
    Mesh _sphereMesh;
    Mesh _cylinderMesh;
    Material _cylinderMat;
    Material _sphereMat;
    Vector3 _sphereSize;

    

    public virtual void Start()
    {
        _cylinderMat = new Material(Shader.Find("Specular"));
        _cylinderMat.color = new Color(1, 0, 0);

        _sphereMat = new Material(Shader.Find("Specular"));
        _sphereMat.color = new Color(0, 0, 0);
    }

    public virtual void Update()
    {

        if ((Input.GetKeyDown(saveKey) && isSaved )|| (save && isSaved))
        {
            Save(pathOfSavingFiles + "\\" + nameSaveFiles);
            isSaved = false;
            Clear();

            save = !save;
           // Debug.Log(calibrationData.rigidBodyHandler.Count);
        }

        if (Input.GetKeyDown(loadKey)|| load)
        {
            Clear();
            Load(pathOfLoadingFiles + "\\" + nameLoadFiles);
            isLoaded = true;

            //load = !load;
        }
    }

    //the tag "RigidBody_Transform" needs to be attached to each rigidbody
    public void HideAndShowRigidBody(bool showRigiBody)
    {

        if (GameObject.FindGameObjectWithTag("RigidBody_Transform") != null)
        {
            if (!showRigiBody)
            {
                foreach (GameObject tracker in GameObject.FindGameObjectsWithTag("RigidBody_Transform"))
                {
                    if ( tracker.GetComponent<MeshRenderer>()!=null && tracker.GetComponent<MeshRenderer>().enabled)
                        tracker.GetComponent<MeshRenderer>().enabled = false;

                }

            }
            else
                foreach (GameObject tracker in GameObject.FindGameObjectsWithTag("RigidBody_Transform"))
                {
                    if ( tracker.GetComponent<MeshRenderer>() != null && !tracker.GetComponent<MeshRenderer>().enabled)
                        tracker.GetComponent<MeshRenderer>().enabled = true;

                }

        }
        else
            Debug.LogWarning("No Tracker/Rigibody in your scene or the tag RigidBody_Transform has been set on the trackers");
    }

    /*......RIGIDBODY MATRIX COMPUTATION AND ASSOCIATION.....*/


    public Matrix4x4 TransformationMatrix(Transform rigidBody, Transform tracker)
    {
        // new coordinate of the rigidbody in the local cordinate of the tracker
        return  Matrix4x4.Inverse( Matrix4x4.TRS(tracker.position, tracker.rotation, Vector3.one))* Matrix4x4.TRS(rigidBody.position, rigidBody.rotation, Vector3.one);
       // return tracker.worldToLocalMatrix * rigidBody.localToWorldMatrix;// this is equivalent 


    }


    public void AssociateRigidBody(Matrix4x4 transformationMatrix , Transform rigidbody , Transform tracker, bool updateRotation, bool updatePosition)
    {
        //Matrice world to local(tracker) * Matrix offset ( rotation and initial position of the rigidbody according to the tracker)
        Matrix4x4 result = Matrix4x4.TRS(tracker.position, tracker.rotation, Vector3.one)*transformationMatrix ;


        if (updatePosition)
            rigidbody.position = result.GetColumn(3);
        // rigidbody.position = transformationMatrix.MultiplyPoint3x4(tracker.position);

        if (updateRotation)       
        {
            rigidbody.rotation =result.rotation;

        }


    }


    /*......SET LIMB LENGTH AND RIGIDBODY MATRIX.....*/

    //need to be called first before setting anything if it s used from another script
    public void NewRigidBodyData( ref int IdNumberofRigidbody)
    {
        calibrationData.rigidBodyHandler.Add(new Rigid_Body_Handler());
        IdNumberofRigidbody = calibrationData.rigidBodyHandler.Count-1;
        calibrationData.rigidBodyHandler[IdNumberofRigidbody].attachedMarkerCSes = new List<Marker_CS>();
        calibrationData.rigidBodyHandler[IdNumberofRigidbody].attachedMarkerCSes.Add(new Marker_CS());
        calibrationData.rigidBodyHandler[IdNumberofRigidbody].attachedBodies = new List<Attached_Body>();
        calibrationData.rigidBodyHandler[IdNumberofRigidbody].attachedBodies.Add(new Attached_Body());
    }

    public void SetAvatarHeight(float avatarHeight)
    {
        calibrationData.height = avatarHeight;
    }


    public void SetLimbSegmentLength(string segmentName, float segmentLength, Vector3 direction, int IdNumberofRigidbody)
    {
        if (calibrationData.rigidBodyHandler.ElementAtOrDefault(IdNumberofRigidbody) == null)
        {
            Debug.LogError("This rigidbody does not exist and needs to be created with NewRigidBody Function");

            return;
        }

        calibrationData.rigidBodyHandler[IdNumberofRigidbody].limb= segmentName;
        calibrationData.rigidBodyHandler[IdNumberofRigidbody].length = segmentLength;
        calibrationData.rigidBodyHandler[IdNumberofRigidbody].direction = direction;

    }


    public void SetJointMatrix(string jointName, string rigidBodyName, Matrix4x4 jointMatrix, int IdNumberofRigidbody)
    {

        if (calibrationData.rigidBodyHandler.ElementAtOrDefault(IdNumberofRigidbody) == null)
        {
            Debug.LogError("This rigidbody does not exist and needs to be created with NewRigidBody Function");

            return;
        }

        calibrationData.rigidBodyHandler[IdNumberofRigidbody].attachedBodies[0].loc_dir = new float[16];
        for (int x = 0; x < 16; ++x)
        {
            calibrationData.rigidBodyHandler[IdNumberofRigidbody].attachedBodies[0].loc_dir[x] = jointMatrix[x];
        }


        calibrationData.rigidBodyHandler[IdNumberofRigidbody].attachedBodies[0].name = jointName;
        calibrationData.rigidBodyHandler[IdNumberofRigidbody].name = rigidBodyName;


    }


    public void SetRigidBodyMatrix( string trackerName, Matrix4x4 rigidBodyMatrix, int IdNumberofRigidbody )
    {
        
        if (calibrationData.rigidBodyHandler.ElementAtOrDefault(IdNumberofRigidbody) == null)
        {
            Debug.LogError("This rigidbody does not exist and needs to be created with NewRigidBody Function");

            return;
        }

        calibrationData.rigidBodyHandler[IdNumberofRigidbody].attachedMarkerCSes[0].loc_dir = new float[16];
        for (int x = 0; x < 16; ++x)
        {
            calibrationData.rigidBodyHandler[IdNumberofRigidbody].attachedMarkerCSes[0].loc_dir[x] = rigidBodyMatrix[x];
        }


        calibrationData.rigidBodyHandler[IdNumberofRigidbody].attachedMarkerCSes[0].name = trackerName;


    }

    /*......GET LIMB LENGTH AND RIGIDBODY MATRIX.....*/

    public float GetAvatarHeight()
    {
        return calibrationData.height;
    }


    //name of the limb you to set so the parents of the child you want to translate
    public float GetLimbSegmentLength( string  segmentName , string endName)
    {
        float segmentLength=0;

        for (int i=0; i< calibrationData.rigidBodyHandler.Count;i++ )
        {
            if(calibrationData.rigidBodyHandler[i].limb == segmentName && calibrationData.rigidBodyHandler[i].attachedBodies[0].name == endName)
               segmentLength=calibrationData.rigidBodyHandler[i].length ;
        }

        return segmentLength;
        
    }

    public Vector3 GetLimbDirection(string segmentName, string endName)
    {
        Vector3 segmentDirection =Vector3.zero;

        for (int i = 0; i < calibrationData.rigidBodyHandler.Count; i++)
        {
            if (calibrationData.rigidBodyHandler[i].limb == segmentName && calibrationData.rigidBodyHandler[i].attachedBodies[0].name == endName)
                segmentDirection = Vector3.Normalize(calibrationData.rigidBodyHandler[i].direction);
        }

        return segmentDirection;

    }

    public Matrix4x4 GetJointMatrix(string jointName, ref string rigidBodyName)
    {


        Matrix4x4 jointMatrix = Matrix4x4.zero;

        for (int i = 0; i < calibrationData.rigidBodyHandler.Count; i++)
        {
          if (calibrationData.rigidBodyHandler[i].attachedBodies[0].name == jointName)
                {
                    rigidBodyName = calibrationData.rigidBodyHandler[i].name;

                if (calibrationData.rigidBodyHandler[i].attachedBodies[0].loc_dir != null)
                    for (int n = 0; n < calibrationData.rigidBodyHandler[i].attachedBodies[0].loc_dir.Length; n++)
                    {
                        jointMatrix[n] = calibrationData.rigidBodyHandler[i].attachedBodies[0].loc_dir[n];
                    }
                }


        }

        return jointMatrix;


    }

    public Matrix4x4 GetRigidBodyMatrix(string targetName , ref string trackerName)
    {
       
        Matrix4x4 rigidBodyMatrix = Matrix4x4.zero;


        for (int i = 0; i < calibrationData.rigidBodyHandler.Count; i++)
        {
            if (calibrationData.rigidBodyHandler[i].name == targetName)
            {
                    trackerName= calibrationData.rigidBodyHandler[i].attachedMarkerCSes[0].name;

                if (calibrationData.rigidBodyHandler[i].attachedMarkerCSes[0].loc_dir != null)
                {
                    for (int n = 0; n < calibrationData.rigidBodyHandler[i].attachedMarkerCSes[0].loc_dir.Length; n++)
                    {
                        rigidBodyMatrix[n] = calibrationData.rigidBodyHandler[i].attachedMarkerCSes[0].loc_dir[n];
                    }
                }

            }
                   
               
        }

        return rigidBodyMatrix;
    }

  

    /*......DRAW CYLINDER AND SPHERE.....*/

    void getSphereProperties(ref Mesh sphereMesh, Vector3 sphereSize)
    {
        //avoid to recalculate the mesh every frame
        if (_sphereMesh != null && sphereSize == _sphereSize)
        {
            sphereMesh = _sphereMesh;
            return;
        }


        //Initialize a Unity sphere but you initialise with any type of object ( color and mesh)
        GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);

        //sphere.transform.localScale = sphereSize;

        _sphereSize = sphereSize;

        sphereMesh = sphere.GetComponent<MeshFilter>().mesh;

        Destroy(sphere);

    }

    //allow to generate a cylinder mesh and flagMeshUpdated tell us when the mesh ia regenerated because the limb change of lenght
    private void getCylinderProperties(ref Mesh _cylinderMesh, float length, float cylinderRadius, ref bool flagMeshUpdated)
    {
        //Material cylinderMat = new Material(Shader.Find("Specular"));
        //cylinderMat.color = new Color(0, 0, 1);
        //_cylinderMat = cylinderMat; 


        //Dictionary<int, Mesh> _meshMap = new Dictionary<int, Mesh>();
        const float CYLINDER_MESH_RESOLUTION = 0.1f;
        float CYLINDER_RADIUS = cylinderRadius;
        int _cylinderResolution = 12;

        int lengthKey = Mathf.RoundToInt(length * 100 / CYLINDER_MESH_RESOLUTION);

        Mesh mesh;

        //avoid to recalculate the mesh every frame but only when the mesh size change
        if (_meshMap.TryGetValue(lengthKey, out mesh))
        {
            _cylinderMesh = mesh;
            flagMeshUpdated = false;
            return;
        }

        mesh = new Mesh();
        mesh.name = "GeneratedCylinder";
        mesh.hideFlags = HideFlags.DontSave;

        List<Vector3> verts = new List<Vector3>();
        List<Color> colors = new List<Color>();
        List<int> tris = new List<int>();

        Vector3 p0 = Vector3.zero;
        Vector3 p1 = Vector3.forward * length;
        for (int i = 0; i < _cylinderResolution; i++)
        {
            float angle = (Mathf.PI * 2.0f * i) / _cylinderResolution;
            float dx = CYLINDER_RADIUS * Mathf.Cos(angle);
            float dy = CYLINDER_RADIUS * Mathf.Sin(angle);

            Vector3 spoke = new Vector3(dx, dy, 0);

            verts.Add(p0 + spoke);
            verts.Add(p1 + spoke);

            colors.Add(Color.white);
            colors.Add(Color.white);

            int triStart = verts.Count;
            int triCap = _cylinderResolution * 2;

            tris.Add((triStart + 0) % triCap);
            tris.Add((triStart + 2) % triCap);
            tris.Add((triStart + 1) % triCap);

            tris.Add((triStart + 2) % triCap);
            tris.Add((triStart + 3) % triCap);
            tris.Add((triStart + 1) % triCap);
        }

        mesh.SetVertices(verts);
        mesh.SetIndices(tris.ToArray(), MeshTopology.Triangles, 0);
        mesh.RecalculateBounds();
        mesh.RecalculateNormals();
        //assing true in a markNoLongerReadable argument makes Mesh data
        //not be readable from the script anymore, and frees up system memory copy of the data.
        mesh.UploadMeshData(false);

        _meshMap[lengthKey] = mesh;

        _cylinderMesh = mesh;

        flagMeshUpdated = true;


    }

    //draw the mesh only once per frame so it needs to be in the update 
    //with no gameobject attached  and a collider .
    public void DrawSphere(GameObject target, Vector3 sphereSize, bool isCollider)
    {

        getSphereProperties(ref _sphereMesh, sphereSize);

        Graphics.DrawMesh(_sphereMesh, Matrix4x4.TRS(target.transform.position, Quaternion.identity, sphereSize), _sphereMat, 0);

        if (isCollider)
            AddSphereCollider(target, sphereSize.y / 2);
    }

    //draw the mesh only once per frame so it needs to be in the update 
    //with no gameobject attached and a  collider .
    public void DrawCylinder(GameObject start, GameObject end, float thickness, bool isCollider, bool flagMeshColliderAdded)
    {
        //properties to draw a sphere.
        // Material _cylinderMat = new Material(Shader.Find("Specular"));

        bool flagMeshUpdated = true;

        Vector3 a = start.transform.position;
        Vector3 b = end.transform.position;

        float currentScale = end.transform.root.localScale.y;


        float _cylinderLength = (a - b).magnitude;


        getCylinderProperties(ref _cylinderMesh, _cylinderLength, thickness, ref flagMeshUpdated);

     /*   if(scale!=currentScale)
        {
            scale = currentScale;
           // flagMeshColliderAdded = true;
            Debug.Log(scale);
        }*/

        if ((b - a) != Vector3.zero)
            Graphics.DrawMesh(_cylinderMesh,
                Matrix4x4.TRS(a, Quaternion.LookRotation(b - a), Vector3.one),
                _cylinderMat,
                gameObject.layer);

        //AddCapsuleCollider(start , end , thickness, _cylinderLength);

        //TO DO: find a way not to call this fonction all the frame but only when the mesh changed
        if (/*flagMeshUpdated &&*/isCollider && (b - a) != Vector3.zero)
            AddMeshCollider(start, _cylinderMesh, start.transform.InverseTransformDirection(b - a),currentScale, flagMeshColliderAdded,flagMeshUpdated);

        //_cylinderMesh.UploadMeshData(true);

    }

    //Add a linear renderer component to the target gameobject with its sphere collider.
    void DrawLineOnGameObject(GameObject start, GameObject end, Color color/*, float duration = 0.2f*/)
    {
        // GameObject myLine = new GameObject();
        //myLine.transform.position = start;
        LineRenderer lr = start.AddComponent<LineRenderer>();
        lr.material = new Material(Shader.Find("Particles/Alpha Blended Premultiply"));
        lr.startColor = color;
        lr.endColor = color;
        lr.startWidth = 0.015f;
        lr.endWidth = 0.015f;
        lr.SetPosition(0, start.transform.position);
        lr.SetPosition(1, end.transform.position);

        //GameObject.Destroy(myLine, duration);
    }

    /*......DRAW COLLIDERS....*/

    void AddSphereCollider(GameObject target, float radius)
    {
        SphereCollider targetCollider;

        if (target.GetComponent<SphereCollider>() != null)
        {
            if (target.GetComponent<SphereCollider>().radius*target.transform.root.localScale.y != radius)
            {
                targetCollider = target.GetComponent<SphereCollider>();
                targetCollider.radius = radius / target.transform.root.localScale.y;
            }
            else
                return;
        }
        else
        {
            targetCollider = target.AddComponent<SphereCollider>();
            targetCollider.radius = radius;
        }

    }

    void AddCapsuleCollider(GameObject start, GameObject end, float radius, float length)
    {

        Vector3 newCenter = end.transform.localPosition / 2;
        float newRadius = radius;
        float newHeight = length;
        int newDirection = LimbDirectionFunction(end);

        if (start.GetComponent<CapsuleCollider>() != null)
        {

            CapsuleCollider cylinderCollider = start.GetComponent<CapsuleCollider>();

            if (cylinderCollider.center != newCenter)
                cylinderCollider.center = newCenter;

            if (cylinderCollider.radius != newRadius)
                cylinderCollider.radius = newRadius;

            if (cylinderCollider.height != newHeight)
                cylinderCollider.height = newHeight;

            if (cylinderCollider.direction != newDirection)
                cylinderCollider.direction = newDirection;


        }

        else
        {
            CapsuleCollider cylinderCollider = start.AddComponent<CapsuleCollider>();
            cylinderCollider.center = newCenter;
            cylinderCollider.radius = newRadius;
            cylinderCollider.height = newHeight;
            cylinderCollider.direction = newDirection;
        }
    }

    //Work for any mesh but computational costly
    void AddMeshCollider(GameObject target, Mesh mesh, Vector3 axis, float scale, bool flagMeshColliderAdded, bool flagMeshUpdated)
    {

       

        if (flagMeshColliderAdded)
        {
            float invertScale = 1 / scale;
            Mesh meshInverted = new Mesh();
            MeshCollider meshcollider;
            RotateMesh(mesh, ref meshInverted, axis);
            MeshScaling(ref meshInverted, invertScale);
            meshcollider = target.AddComponent<MeshCollider>();
            //meshcollider.convex = true;
            // meshcollider.isTrigger = true;
            meshcollider.sharedMesh = meshInverted;
            //meshInverted.UploadMeshData(true);
        }
        else if (flagMeshUpdated)
        {
            MeshCollider[] MeshCollidersTable = target.GetComponents<MeshCollider>();
            for (int i = 0; i < MeshCollidersTable.Length; i++)
            {
                float invertScale = 1 / scale;
                Mesh meshInverted = new Mesh();
                RotateMesh(mesh, ref meshInverted, axis);
                MeshScaling(ref meshInverted, invertScale);

                if (MeshCollidersTable[i].sharedMesh != meshInverted)
                {
                    MeshCollidersTable[i].sharedMesh = meshInverted;
                }

                //meshInverted.UploadMeshData(true);

            }


        }





    }


    /*...................OTHER................*/

    int LimbDirectionFunction(GameObject end)
    {
        Vector3 limbDirection = end.transform.localPosition;

        float X = Mathf.Abs(limbDirection.x);
        float Y = Mathf.Abs(limbDirection.y);
        float Z = Mathf.Abs(limbDirection.z);

        // Debug.Log("X:=" + X + "Y:=" + Y + "Z:=" + Z);

        if (X > Y && X > Z)
            return 0;
        else if (Y > X && Y > Z)
            return 1;
        else
            return 2;


    }

    void RotateMesh(Mesh meshOriginal, ref Mesh meshInverted, Vector3 axis)
    {
        meshInverted.name = "MeshInverted";

        Quaternion matrixRotation = Quaternion.LookRotation(axis);

        //Debug.Log("Mesh collider generated");

        List<Vector3> verticesList = new List<Vector3>();
        List<int> trisList = new List<int>();

        for (int i = 0; i < meshOriginal.vertices.Length; i++)
        {
            // verticesList.Add(new Vector3(meshOriginal.vertices[i].x * xFactor, meshOriginal.vertices[i].y * yFactor, meshOriginal.vertices[i].z * zFactor));
            verticesList.Add(Matrix4x4.TRS(Vector3.one, matrixRotation, Vector3.one) * meshOriginal.vertices[i]);

            //trisList.Add(meshOriginal.GetIndices(0)[i]);
        }

        meshInverted.SetVertices(verticesList);
        // meshInverted.SetIndices(trisList.ToArray(), MeshTopology.Triangles, 0);
        meshInverted.triangles = meshOriginal.triangles;


    }


    void MeshScaling(ref Mesh targetMesh, float scalingFactor)
    {
       // Mesh targetMesh = new Mesh();


        Vector3[] vertices = targetMesh.vertices;

        int i = 0;
        while (i < vertices.Length)
        {
            vertices[i] = vertices[i] * scalingFactor;
            i++;
        }
        targetMesh.vertices = vertices;

    }


    /*......SAVE ....*/

    public void Save(string pathFile)
    {
           Debug.Log("Saving Calibration...");
        calibrationData.Save( pathFile + ".pc");
    }

    public void Load(string pathFile)
    {
        Debug.Log("Loading Calibration...");
        calibrationData= calibrationData.Load(pathFile + ".pc");
    }

    public void Clear()
    {
        calibrationData.Clear();
    }

   /* public static Quaternion QuaternionFromMatrix(Matrix4x4 m)
    {
        // Adapted from: http://www.euclideanspace.com/maths/geometry/rotations/conversions/matrixToQuaternion/index.htm
        Quaternion q = new Quaternion();
        q.w = Mathf.Sqrt(Mathf.Max(0, 1 + m[0, 0] + m[1, 1] + m[2, 2])) / 2;
        q.x = Mathf.Sqrt(Mathf.Max(0, 1 + m[0, 0] - m[1, 1] - m[2, 2])) / 2;
        q.y = Mathf.Sqrt(Mathf.Max(0, 1 - m[0, 0] + m[1, 1] - m[2, 2])) / 2;
        q.z = Mathf.Sqrt(Mathf.Max(0, 1 - m[0, 0] - m[1, 1] + m[2, 2])) / 2;
        q.x *= Mathf.Sign(q.x * (m[2, 1] - m[1, 2]));
        q.y *= Mathf.Sign(q.y * (m[0, 2] - m[2, 0]));
        q.z *= Mathf.Sign(q.z * (m[1, 0] - m[0, 1]));
        return q;
    }*/
}
