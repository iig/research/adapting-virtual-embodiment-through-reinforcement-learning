﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// /You can set the avatar hand in a good initial orientation and ask the user to put his hand in the same posture and then save the calibration.
/// Or you can use this sript after the calibration ask the user to have the good posture in order to correct the avatar posture. But if you use this script it will take care of the rotation of your hand and you will not be able to modify it.
/// </summary>



public class CorrectHandOrientation : MonoBehaviour
{
	public GameObject objectRef;
	public GameObject objectToLinkWith;
	public GameObject cylinder;
	public Character_Generator_Settings avatarOriginal;
	public KeyCode calibrationButton;
	public bool isAutomaticAfterCalibration;
	public bool notBallTarget;


	bool updatePosition = false;
	bool updateRotation = true;
	



	Calibration calibrationMethods;

	[HideInInspector]
	public bool orientationDone;
	bool flag;
	Matrix4x4 matrixTransformation;
    Quaternion offsetRotationLateralAvatarHand = Quaternion.identity;
	bool isCalibrated;


	// Start is called before the first frame update
	void Start()
    {
		calibrationMethods = new Calibration();
	}

    // Update is called once per frame
    void Update()
    {
		if ( orientationDone ) {

			//Debug.Log( "EULER" + Quaternion.FromToRotation( Vector3.up, cylinder.transform.up ).eulerAngles );
			//Debug.Log( "OLD" + offsetRotationLateralAvatarHand.eulerAngles );
			calibrationMethods.AssociateRigidBody( matrixTransformation, objectRef.transform, objectToLinkWith.transform, updateRotation, updatePosition );
		}
	

		if ( (Input.GetKeyDown( calibrationButton )|| isAutomaticAfterCalibration)  && !isCalibrated && objectRef != null && objectToLinkWith != null ) {

			offsetRotationLateralAvatarHand = Quaternion.FromToRotation( Vector3.up, cylinder.transform.up );
			//offsetRotationLateralAvatarHand = Quaternion.FromToRotation( cylinder.transform.up, Vector3.up);
			objectRef.transform.rotation = Quaternion.Inverse( offsetRotationLateralAvatarHand ) * objectToLinkWith.transform.rotation;
			//objectRef.transform.rotation =  offsetRotationLateralAvatarHand  * objectToLinkWith.transform.rotation;
			isCalibrated = true;

		}else if( !isCalibrated && objectRef != null && objectToLinkWith != null ) {
			if( !notBallTarget )
			objectRef.transform.rotation = objectToLinkWith.transform.rotation;
		}


		if ( objectRef != null && objectToLinkWith != null && isCalibrated && !flag ) {

	
			
				matrixTransformation = calibrationMethods.TransformationMatrix( objectRef.transform, objectToLinkWith.transform );

			StartCoroutine(Wait());
			flag = true;
			


		} else if ( objectToLinkWith == null ) {

			objectToLinkWith = GameObject.Find( avatarOriginal.settingsReferences.rightHand.name + "Target");
		
		}

	
			


	}

	IEnumerator Wait() {

		yield return new WaitForSeconds( 0.5f );
		orientationDone = true;
	}
	
}
