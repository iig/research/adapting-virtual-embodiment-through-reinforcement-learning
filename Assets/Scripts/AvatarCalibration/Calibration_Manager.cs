﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//////////////////////////////////////////////////////////////////
// CALIBRATION : FUNCTIONS NEVER CHANGE 

public class Calibration_Manager : MonoBehaviour
{
	//Interface
	public string limbSelected;
	public enum Data { Live, File };
	public Data mocapSource;
	public enum CalibrationMode {Manual, Automatique};
	public CalibrationMode calibrationMode;
	public KeyCode saveKey = KeyCode.Return;
	public KeyCode loadKey = KeyCode.Space;
	public KeyCode automaticCalibrationKey = KeyCode.F;
	public KeyCode firstCalibrationCode = KeyCode.U;
	public KeyCode secondCalibrationCode = KeyCode.T;
	public KeyCode showAvatarKey = KeyCode.Alpha4;
	public bool showAvatar=true;
	public bool showUser;
	public bool showTrackers;
	public float subjectHeight;
	public string fileTosave;
	public string fileToload;

	

	//Visble component
	public Modularity.PlayerLog_Tracker playerLogTracker;
	public Calibration_Manual_Generic calibrationOriginal;
	public Calibration_Manual_Generic calibrationDistorted;
	public GameObject avatarOriginal;
	public GameObject avatarDistorted;
	public GameObject iKDistorted;
	public GameObject iKOriginal;



	public GameObject waitingMessage1;
	public GameObject waitingMessage;

	public GameObject leftObjectHeld;
	public GameObject rightObjectHeld;
	public GameObject leftRectangleAvatarDistortedShoulder;
	public GameObject rightRectangleAvatarDistortedShoulder;
	public GameObject leftRectangleAvatarOriginalShoulder;
	public GameObject rightRectangleAvatarOriginalShoulder;
	public GameObject leftRectangleAvatarDistortedHand;
	public GameObject rightRectangleAvatarDistortedHand;
	//public GameObject leftRectangleAvatarDistortedElbow;
//	public GameObject rightRectangleAvatarDistortedElbow;
	public string leftHand;
	public string rightHand; 
	public string leftShoulder; 
	public string rightShoulder;
	public string leftElbow; 
	public string rightElbow;
	public List<GameObject> avatarRectangleAfterCalibration;

	public GameObject carpet;
	public GameObject chair;
	public GameObject table;
	public bool withTolerance;

	//Non visible component
	[HideInInspector]
	public bool calibration1;
	[HideInInspector]
	public bool calibration2;
	[HideInInspector]
	public bool isPostSaving;
	[HideInInspector]
	public Camera oculusCamera;

    GameObject leftRectangleUserHand;
    GameObject rightRectangleUserHand;
    GameObject leftRectangleUserShoulder;
    GameObject rightRectangleUserShoulder;
    GameObject leftRectangleUserElbow;
    GameObject rightRectangleUserElbow;
	GameObject[] ObjectsToHide;
	float timeStarted = 0;
	float timeStarted2 = 0;
	float counter = 0;
	bool flag1;
	bool heightCalibrated;
	static bool isSaved;

	//bool flag3;
	float toleranceHandX = 0.02f;
	float toleranceHandY = 0.01f;
	float toleranceHandZ = 0.02f;
	float toleranceShoulder = 0.001f;


	Character_Generator_Settings avatarOriginalComponent;
	Character_Generator_Settings avatarDistortedComponent;
	IIG.RotationCorrectionModified correction;

	Renderer leftRectangleUserHandRenderer;
	Renderer rightRectangleUserHandRenderer;
	Renderer leftRectangleUserElbowRenderer;
	Renderer rightRectangleUserElbowRenderer;


	private void Awake () {

	

		avatarOriginalComponent = avatarOriginal.GetComponent<Character_Generator_Settings>();
		avatarOriginalComponent.userHeight = subjectHeight;



		avatarDistortedComponent = avatarDistorted.GetComponent<Character_Generator_Settings>();
		avatarDistortedComponent.userHeight = subjectHeight;

		
		//Initialisation 
		
		leftRectangleAvatarDistortedHand.GetComponent<Renderer>().enabled = false;
		rightRectangleAvatarDistortedHand.GetComponent<Renderer>().enabled = false;
	//	leftRectangleAvatarDistortedElbow.GetComponent<Renderer>().enabled = false;
	//	rightRectangleAvatarDistortedElbow.GetComponent<Renderer>().enabled = false;
		//leftRectangleAvatarDistortedShoulder.GetComponent<Renderer>().enabled = false;
		//rightRectangleAvatarDistortedShoulder.GetComponent<Renderer>().enabled = false;


		for ( int i = 0; i < avatarRectangleAfterCalibration.Count; i++ ) {
			avatarRectangleAfterCalibration[i].GetComponent<Renderer>().enabled = false;

		}

		//calibrationDistorted.showAvatar = true;
		showAvatar = true;


		rightObjectHeld.SetActive( false );
		leftObjectHeld.SetActive( false );

		//HIDE ALL OBJECTS in the scene TAG CALIBRATE HIDE
		ObjectsToHide = GameObject.FindGameObjectsWithTag("CalibrationHidden");
		HideOrShowObjects( ObjectsToHide );

		//Set the shoulder references
		calibrationOriginal.leftAboveShoulderReference = leftRectangleAvatarOriginalShoulder;
		calibrationOriginal.rightAboveShoulderReference = rightRectangleAvatarOriginalShoulder;
		calibrationDistorted.leftAboveShoulderReference = leftRectangleAvatarDistortedShoulder;
		calibrationDistorted.rightAboveShoulderReference = rightRectangleAvatarDistortedShoulder;

	}

	void Start () {

		//Initialisation 
		leftRectangleUserHand = GameObject.Find(leftHand);
		rightRectangleUserHand = GameObject.Find(rightHand);
		leftRectangleUserElbow = GameObject.Find(leftElbow);
		rightRectangleUserElbow = GameObject.Find(rightElbow);
		leftRectangleUserShoulder=GameObject.Find(leftShoulder);
		rightRectangleUserShoulder = GameObject.Find(rightShoulder);


		if ( leftRectangleUserHand!=null && leftRectangleUserHand.GetComponent<Renderer>() !=null) {

			leftRectangleUserHandRenderer = leftRectangleUserHand.GetComponent<Renderer>();
			leftRectangleUserHandRenderer.enabled = false;
		}


		if ( rightRectangleUserHand != null && rightRectangleUserHand.GetComponent<Renderer>() != null ) {

			rightRectangleUserHandRenderer = rightRectangleUserHand.GetComponent<Renderer>();
			rightRectangleUserHandRenderer.enabled = false;
		}

		if ( leftRectangleUserElbow != null && leftRectangleUserElbow.GetComponent<Renderer>() != null ) {

			leftRectangleUserElbowRenderer = leftRectangleUserElbow.GetComponent<Renderer>();
			leftRectangleUserElbowRenderer.enabled = false;
		}
		if ( rightRectangleUserElbow != null && rightRectangleUserElbow.GetComponent<Renderer>() != null ) {

			rightRectangleUserElbowRenderer = rightRectangleUserElbow.GetComponent<Renderer>();
			rightRectangleUserElbowRenderer.enabled = false;
		}


		//leftRectangleUserShoulder.GetComponent<Renderer>().enabled = false;
		//rightRectangleUserShoulder.GetComponent<Renderer>().enabled = false; 

		oculusCamera = Camera.main;

		correction = oculusCamera.transform.parent.GetComponent<IIG.RotationCorrectionModified>();

		calibrationDistorted.nameSaveFiles = fileTosave;
		calibrationOriginal.nameSaveFiles = fileTosave;

		 calibrationDistorted.nameLoadFiles = fileToload;
		calibrationOriginal.nameLoadFiles = fileToload;

	}

	void Update () {

		calibrationDistorted.avatarKey=showAvatarKey;
			calibrationDistorted.showAvatar = showAvatar;
		calibrationOriginal.showAvatar = showUser;
		calibrationDistorted.showRigidBody = showTrackers;
		calibrationDistorted.freezeKey = secondCalibrationCode;

		calibrationDistorted.automaticLimbCalibrationKey = automaticCalibrationKey;
		calibrationOriginal.automaticLimbCalibrationKey = automaticCalibrationKey;

		if(calibrationDistorted.limbSelected !=null)
		limbSelected = calibrationDistorted.limbSelected.name;

		if ( Input.GetKeyDown( showAvatarKey)) {

			showAvatar = !showAvatar;

		}

	}

	public void HideOrShowObjects( GameObject[] ObjectsToHide)
	{
		foreach ( GameObject objectToHide in ObjectsToHide)
		{
			if ( objectToHide.activeSelf )
				objectToHide.SetActive( false );
			else
				objectToHide.SetActive( true );
		}
	}

	public bool Calibration () {
		/*if ( mocapSource == Data.Live ) {
			//generate trackers in the scene
			// trackerManager.isTrackerReady = true;

			playerLogTracker.instantiateDebugTrackers = false;
		} else {
			//Do not generate trackers in the scene
			//trackerManager.isTrackerReady = false;

			playerLogTracker.instantiateDebugTrackers = true;
		}*/




		//calibrate avatar size 
		if (Input.GetKeyDown( loadKey ) || (mocapSource == Data.File /*&& !flag3*/))
		{


			if ( LoadingCalibration() )
				return WaitingMessageControl( false, 1f );
			else
				return false;
			
		} else if ( (mocapSource == Data.Live) ) {
			if ( globalCalibration() || calibration1 ) {
				//calibrate limb size
				if ( limbCalibration() || calibration2 ) {
					//save and load the calibartion
					if ( PostSavingCalibration() ) {

						isPostSaving = true;
						return false;
					} else if ( isPostSaving ) {
						if ( timeStarted2 > 0.001f ) {
							isPostSaving = false;
							if ( SavingCalibration() ) {
								if ( LoadingCalibration() )
									return WaitingMessageControl( false, 1f );
								else
									return false;
							} else
								return false;


						} else {
							timeStarted2 += Time.deltaTime;


							return false;

						}

					} else {

						return false;
					}
				} else {

					return false;
				}
			} else
				return false;
		} else {

			return false;
		}



	}

	//calibrate global scaling of the avatar.
	bool globalCalibration () {
		Vector3 leftDistanceTotheCenter = Vector3.one;
		Vector3 rightDistanceTotheCenter = Vector3.one;

		if ( !calibration1 ) {
			


			if( calibrationMode == CalibrationMode.Automatique ) {
				
				if ( correction != null )
					correction.smoothingFactor = 0.0005f;		

					



			}else if( WaitingMessageControl( true, 1f ) ) {
				
				if (correction!=null)
				correction.smoothingFactor = 0.0005f;

			}
			


			if( leftRectangleUserShoulder !=null)
			leftDistanceTotheCenter = leftRectangleUserShoulder.transform.position - leftRectangleAvatarDistortedShoulder.transform.position;

			if(rightRectangleUserShoulder)
			rightDistanceTotheCenter = rightRectangleUserShoulder.transform.position - rightRectangleAvatarDistortedShoulder.transform.position;


	
		} else if ( !heightCalibrated ) {

			avatarDistortedComponent.userHeight = subjectHeight;
			// calibrationDistorted.translatePosition.x = translateAvatar;
			if(counter==100){
				if ( calibrationMode == CalibrationMode.Automatique ) {
					calibrationDistorted.isAutomaticCalib = true;
					calibrationOriginal.isAutomaticCalib = true;
				}



				heightCalibrated = true;
			}

			counter++;
			
			

		}




		if ( (Mathf.Abs( leftDistanceTotheCenter.x ) < toleranceShoulder && Mathf.Abs( leftDistanceTotheCenter.y ) < toleranceShoulder && Mathf.Abs( rightDistanceTotheCenter.x ) < toleranceShoulder && Mathf.Abs( rightDistanceTotheCenter.y ) < toleranceShoulder && !calibration1)
			|| ((Input.GetKeyDown( firstCalibrationCode ) || calibrationMode == CalibrationMode.Automatique) && !calibration1) ) {

			//calibrationDistorted.showAvatar = false;
			showAvatar = false;


			//Modularity
			//mocapInputController.drawMarkersTransform = false;
			if ( leftRectangleUserHandRenderer != null )
				leftRectangleUserHandRenderer.enabled = true;

			if ( rightRectangleUserHandRenderer != null )
				rightRectangleUserHandRenderer.enabled = true;

			leftRectangleAvatarDistortedHand.GetComponent<Renderer>().enabled = true;
			rightRectangleAvatarDistortedHand.GetComponent<Renderer>().enabled = true;

			if ( leftRectangleUserElbowRenderer != null )
				leftRectangleUserElbowRenderer.enabled = true;

			if ( rightRectangleUserElbowRenderer != null )
				rightRectangleUserElbowRenderer.enabled = true;

			//	leftRectangleAvatarDistortedElbow.GetComponent<Renderer>().enabled = true;
			//	rightRectangleAvatarDistortedElbow.GetComponent<Renderer>().enabled = true;

			leftRectangleAvatarDistortedShoulder.GetComponent<Renderer>().enabled = false;
			rightRectangleAvatarDistortedShoulder.GetComponent<Renderer>().enabled = false;
			leftRectangleAvatarOriginalShoulder.GetComponent<Renderer>().enabled = false;
			rightRectangleAvatarOriginalShoulder.GetComponent<Renderer>().enabled = false;


			//Destroy( leftRectangleAvatarDistortedShoulder );
			//Destroy( rightRectangleAvatarDistortedShoulder );
			//Destroy( leftRectangleAvatarOriginalShoulder );
			//Destroy( rightRectangleAvatarOriginalShoulder );

			calibration1 = true;

			return true;
		}  else
			return false;
		


	}

	bool limbCalibration () {
		Vector3 leftDistanceHandTotheCenter = Vector3.one;
		Vector3 rightDistanceHandTotheCenter = Vector3.one;
		Vector3 leftDistanceElbowTotheCenter = Vector3.one;
		Vector3 rightDistanceElbowTotheCenter = Vector3.one;


		if ( !calibration2 ) {
			//activate message
			if ( !flag1 ) {
				waitingMessage1.SetActive( true );
				//deactivate message
				WaitingMessageControl( false, 1f );
			} else {
				// waitingMessage.SetActive(true);

			}






			//detection of the user alignement 
			leftDistanceHandTotheCenter = leftRectangleUserHand.transform.position - leftRectangleAvatarDistortedHand.transform.position;
			rightDistanceHandTotheCenter = rightRectangleUserHand.transform.position - rightRectangleAvatarDistortedHand.transform.position;
		//	leftDistanceElbowTotheCenter = leftRectangleUserElbow.transform.position - leftRectangleAvatarDistortedElbow.transform.position;
		//	rightDistanceElbowTotheCenter = rightRectangleUserElbow.transform.position - rightRectangleAvatarDistortedElbow.transform.position;

			//Debug.Log( "qq " + leftRectangleAvatarDistortedHand.transform.position );
			//Debug.Log( "tt "+leftRectangleUserHand.transform.position );

		}


		/*if ( (Mathf.Abs( leftDistanceHandTotheCenter.x ) < tolerance && Mathf.Abs( leftDistanceHandTotheCenter.y ) < tolerance
			 && Mathf.Abs( leftDistanceHandTotheCenter.z ) < tolerance
			&& Mathf.Abs( rightDistanceHandTotheCenter.x ) < tolerance && Mathf.Abs( rightDistanceHandTotheCenter.y ) < tolerance
			 && Mathf.Abs( leftDistanceHandTotheCenter.z) < tolerance
			&& Mathf.Abs( leftDistanceElbowTotheCenter.y ) < tolerance2
			&& Mathf.Abs( rightDistanceElbowTotheCenter.y ) < tolerance2
			&& !calibration2)
			|| (Input.GetKey( calibrationDistorted.freezeKey ) && !calibration2) ) */
		if ( (Mathf.Abs( leftDistanceHandTotheCenter.x ) < toleranceHandX && Mathf.Abs( leftDistanceHandTotheCenter.y ) < toleranceHandY
		 && Mathf.Abs( leftDistanceHandTotheCenter.z ) < toleranceHandZ
		&& Mathf.Abs( rightDistanceHandTotheCenter.x ) < toleranceHandX && Mathf.Abs( rightDistanceHandTotheCenter.y ) < toleranceHandY
		 && Mathf.Abs( leftDistanceHandTotheCenter.z ) < toleranceHandZ
		&& !calibration2 && withTolerance)
		|| (Input.GetKey( calibrationDistorted.freezeKey ) && !calibration2) ) {


			waitingMessage1.SetActive( false );
			// Generate the Waiting Message
			if ( leftRectangleUserHandRenderer != null )
				leftRectangleUserHandRenderer.enabled = false;
			if ( rightRectangleUserHandRenderer != null )
				rightRectangleUserHandRenderer.enabled = false;


			leftRectangleAvatarDistortedHand.GetComponent<Renderer>().enabled = false;
			rightRectangleAvatarDistortedHand.GetComponent<Renderer>().enabled = false;

			if ( leftRectangleUserElbowRenderer!= null )
				leftRectangleUserElbowRenderer.enabled = false;
			if ( rightRectangleUserElbowRenderer != null )
				rightRectangleUserElbowRenderer.enabled = false;

			//leftRectangleAvatarDistortedElbow.GetComponent<Renderer>().enabled = false;
			//rightRectangleAvatarDistortedElbow.GetComponent<Renderer>().enabled = false;





			carpet.GetComponent<Renderer>().enabled = false;
			foreach ( Renderer renderer in chair.GetComponentsInChildren<Renderer>() )
				renderer.enabled = false;




			if ( !WaitingMessageControl( true, 0.01f ) ) {

				flag1 = true;

				return false;
			}


			Destroy( leftRectangleAvatarDistortedHand.transform.parent.gameObject );
			Destroy( rightRectangleAvatarDistortedHand.transform.parent.gameObject );

			if ( leftRectangleUserHandRenderer != null )
				leftRectangleUserHandRenderer.enabled = true;

			if ( rightRectangleUserHandRenderer != null )
				rightRectangleUserHandRenderer.enabled = true;

			leftRectangleAvatarDistortedHand.GetComponent<Renderer>().enabled = true;
			rightRectangleAvatarDistortedHand.GetComponent<Renderer>().enabled = true;

			if ( leftRectangleUserElbowRenderer != null )
				leftRectangleUserElbowRenderer.enabled = true;
			if ( rightRectangleUserElbowRenderer != null )
				rightRectangleUserElbowRenderer.enabled = true;

		//	leftRectangleAvatarDistortedElbow.GetComponent<Renderer>().enabled = true;
		//	rightRectangleAvatarDistortedElbow.GetComponent<Renderer>().enabled = true;

			leftRectangleAvatarDistortedShoulder.GetComponent<Renderer>().enabled = true;
			rightRectangleAvatarDistortedShoulder.GetComponent<Renderer>().enabled = true;
			//leftRectangleAvatarOriginalShoulder.GetComponent<Renderer>().enabled = true;
			//rightRectangleAvatarOriginalShoulder.GetComponent<Renderer>().enabled = true;


			carpet.GetComponent<Renderer>().enabled = true;
			foreach ( Renderer renderer in chair.GetComponentsInChildren<Renderer>() )
				renderer.enabled = true;
			//chair.GetComponent<Renderer>().enabled = true;

			//Modularity
			//mocapInputController.drawMarkersTransform = true;


			for ( int i = 0; i < avatarRectangleAfterCalibration.Count; i++ ) {
				avatarRectangleAfterCalibration[i].GetComponent<Renderer>().enabled = true;

			}


			


			calibrationDistorted.showSkeleton = true;
			calibrationDistorted.takeACapture = true;

			//Modularity
			//leftRectangleUserHand.GetComponent<RigidBodyTracker>().enabled = false;
			//rightRectangleUserHand.GetComponent<RigidBodyTracker>().enabled = false;
			//leftRectangleUserHand.GetComponent<RigidBodyTracker>().enabled = false;
			//rightRectangleUserHand.GetComponent<RigidBodyTracker>().enabled = false;

			/*	foreach( GameObject tracker in GameObject.FindGameObjectsWithTag("RigidBody_Transform" )) 
				{
					foreach(MonoBehaviour scripts in tracker.GetComponents<MonoBehaviour>()) 
					{
						scripts.enabled = false;

					}
				}*/


			//calibrationDistorted.showAvatar = false;
			showAvatar = false;
			calibration2 = true;

			return true;
		} else {
			return false;
		}

	}

	bool WaitingMessageControl ( bool enableMessage, float delay ) {
		if ( enableMessage ) {



			//activate message
			waitingMessage.SetActive( true );



			//change target display 
			if ( timeStarted > delay && oculusCamera.targetDisplay != 1 ) {
				// increase near
				oculusCamera.nearClipPlane = 10f; //1.5f;

				oculusCamera.targetDisplay = 1;
				timeStarted = 0;


				return true;
			} else if ( oculusCamera.targetDisplay == 1 ) {
				return true;
			} else {
				timeStarted += Time.deltaTime;
				return false;
			}

		} else {
			//deactivate message
			waitingMessage.SetActive( false );

			// restore the  near 
			oculusCamera.nearClipPlane = 0.1f;

			//change target display 
			oculusCamera.targetDisplay = 0;



			return true;

		}


	}

	bool PostSavingCalibration () {

		if ( (Input.GetKeyDown( saveKey ) || (calibrationMode==CalibrationMode.Automatique) && !isSaved )) {


			Destroy( leftRectangleAvatarDistortedHand );
			Destroy( rightRectangleAvatarDistortedHand );
			Destroy( leftRectangleAvatarDistortedShoulder );
			Destroy( rightRectangleAvatarDistortedShoulder );


			Destroy( leftRectangleAvatarDistortedShoulder );
			Destroy( rightRectangleAvatarDistortedShoulder );
			Destroy( leftRectangleAvatarOriginalShoulder );
			Destroy( rightRectangleAvatarOriginalShoulder );

			for ( int i = 0; i < avatarRectangleAfterCalibration.Count; i++ ) {
				Destroy( avatarRectangleAfterCalibration[i] );

			}

			calibrationDistorted.generateTargetConfig = true;


			calibrationDistorted.enableAvatarMoveControl = false;


			calibrationDistorted.save = true;
			isSaved = true;


			return true;
		} else {
			return false;
		}



	}

	bool SavingCalibration () {
		return calibrationDistorted.save = true;
	}

	bool LoadingCalibration () {
		//prepared avatar

		//if ( leftRectangleUserShoulder != null ) {
			
			for ( int i = 0; i < avatarRectangleAfterCalibration.Count; i++ ) {
				Destroy( avatarRectangleAfterCalibration[i] );

			}

			Destroy( leftRectangleAvatarOriginalShoulder );
			Destroy( rightRectangleAvatarOriginalShoulder );
			Destroy( leftRectangleAvatarDistortedShoulder );
			Destroy( rightRectangleAvatarDistortedShoulder );
			Destroy( leftRectangleAvatarDistortedHand );
			Destroy( rightRectangleAvatarDistortedHand );
		//	Destroy( leftRectangleAvatarDistortedElbow );
		//	Destroy( rightRectangleAvatarDistortedElbow );

			// Destroy(leftRectangleUserShoulder);
			// Destroy(rightRectangleUserShoulder);
			// Destroy(leftRectangleUserHand);
			// Destroy(rightRectangleUserHand);
			// Destroy(leftRectangleUserElbow);
			//  Destroy(rightRectangleUserElbow);
	//	}


		//	calibrationDistorted.showAvatar = false;
		showAvatar = true;

		calibrationDistorted.showSkeleton = false;


		rightObjectHeld.SetActive( true );
		leftObjectHeld.SetActive( true );




		//loading data
		calibrationDistorted.takeACapture = false;

		if ( mocapSource == Data.File || Input.GetKeyDown( loadKey ) ) {
			calibrationDistorted.nameLoadFiles = fileToload;//.Remove(fileToload.Length-1);
			calibrationOriginal.nameLoadFiles = fileToload;//.Remove(fileToload.Length - 1);
														   //GameObject.Find( "Markers debug" ).SetActive(false);
			//Debug.Log("heeerrrre"+ calibrationDistorted.nameLoadFiles);

		}



		calibrationDistorted.load = true;

		calibrationOriginal.isMatrixLoaded = false;
		calibrationOriginal.load = true;

		//Order of the four steps bellow is very important


		calibrationDistorted.enableAvatarMoveControl = false;
		iKDistorted.SetActive( true );
		calibrationDistorted.isTargetsTrackerAssociated = true;
		iKOriginal.SetActive( true );

		//if ( mocapSource == Data.Live )
		//	mocapInputController.drawMarkersTransform = false;

		calibrationDistorted.hideOrShowTargets = true;
		showTrackers = false;
		//calibrationDistorted.showRigidBody = false;


		return true;
	}

	public static bool GetSaveStatus () {
		return isSaved;
	}
}
