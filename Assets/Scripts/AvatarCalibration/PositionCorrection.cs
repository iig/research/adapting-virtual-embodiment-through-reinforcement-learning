﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionCorrection : MonoBehaviour {

    public GameObject avatarDistorted;
    public GameObject avatarOriginal;
    public bool isFeetReady;
	public KeyCode calibrateFeet= KeyCode.Alpha6;
	public KeyCode resetPositionAvatar= KeyCode.Alpha5;

    Character_Generator_Settings settingsDistorted;
    Character_Generator_Settings settingsOriginal;

    Vector3 oldHipsPosition;
    Vector3 oldLeftFootPosition;
    Vector3 oldRightFootPosition;
    GameObject targetLeftFoot;
    GameObject targetRightFoot;
   // bool flag = true;

    // Use this for initialization
    void Start () {

        settingsDistorted = avatarDistorted.GetComponent<Character_Generator_Settings>();
        settingsOriginal = avatarOriginal.GetComponent<Character_Generator_Settings>();

        oldHipsPosition = settingsDistorted.settingsReferences.pelvis.position;
        oldLeftFootPosition= settingsDistorted.settingsReferences.leftFoot.position;
        oldRightFootPosition = settingsDistorted.settingsReferences.rightFoot.position;

    }
	
	// Update is called once per frame
	void Update () {

        targetLeftFoot = GameObject.Find(settingsDistorted.settingsReferences.leftFoot.name + "Target");
        targetRightFoot = GameObject.Find(settingsDistorted.settingsReferences.rightFoot.name + "Target");

        if (Input.GetKeyDown(resetPositionAvatar))
        {

            settingsDistorted.settingsReferences.pelvis.position = new Vector3(settingsDistorted.settingsReferences.pelvis.position.x
                , oldHipsPosition.y, oldHipsPosition.z);

            settingsOriginal.settingsReferences.pelvis.position = new Vector3(settingsDistorted.settingsReferences.pelvis.position.x
                , oldHipsPosition.y, oldHipsPosition.z); ;
        }

      

        if(Input.GetKeyDown(calibrateFeet) || isFeetReady)
        {
            if (GameObject.Find(settingsDistorted.settingsReferences.leftFoot.name + "Target") != null && GameObject.Find(settingsDistorted.settingsReferences.rightFoot.name + "Target") != null)
            {   //enable les target first
                targetLeftFoot.transform.position = new Vector3(targetLeftFoot.transform.position.x,
                        targetLeftFoot.transform.position.y - targetLeftFoot.transform.position.y + oldLeftFootPosition.y,
                        targetLeftFoot.transform.position.z);

                targetRightFoot.transform.position = new Vector3(targetRightFoot.transform.position.x,
                            targetRightFoot.transform.position.y - targetRightFoot.transform.position.y + oldRightFootPosition.y,
                            targetRightFoot.transform.position.z);

                // finally enable the IK

                isFeetReady = !isFeetReady;
            }
        }
        


    }
}
