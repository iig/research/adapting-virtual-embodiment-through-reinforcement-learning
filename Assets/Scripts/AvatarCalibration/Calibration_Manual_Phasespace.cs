﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Calibration_Manual_Phasespace : Calibration_Manual
{
    public GameObject phasespaceManager;

    [Header("Option")]
    public bool showMarker=true;


    RigidBody_Phasespace_Tracker rigidBodyPhasespaceTrackerComponent;
    IIG.MocapInputController mocapInputControllerComponent;

    public override void  Start()
    {
        base.Start();

        if (phasespaceManager.GetComponent<RigidBody_Phasespace_Tracker>() != null)
        {
            rigidBodyPhasespaceTrackerComponent = phasespaceManager.GetComponent<RigidBody_Phasespace_Tracker>();
        }
        else
            Debug.LogError("No  RigidBody_Phasespace_Tracker attached to the phasespaceManager ");

        if (phasespaceManager.GetComponent<IIG.MocapInputController>() != null)
        {
            mocapInputControllerComponent = phasespaceManager.GetComponent<IIG.MocapInputController>();
        }
        else
            Debug.LogError("No  MocapInputController attached to the phasespaceManager ");
    }

    public override void Update()
    {
        base.Update();

        if (mocapInputControllerComponent != null)
        { 
            
            HideOrShowMarker(showMarker);
        }
        else
            Debug.LogError("No  VrpnClientWrapper attached to the phasespaceManager ");
    }

    //this function will freeze the marker and consequently the rigidbody of the phasespace 
    // in their current rotation and position
    public override void FreezeTracker( bool isFreeze)
    {
        if (rigidBodyPhasespaceTrackerComponent != null)
        {
            if(isFreeze)
            {
                rigidBodyPhasespaceTrackerComponent.updatePosition = false;
                rigidBodyPhasespaceTrackerComponent.updateRotation = false;
                mocapInputControllerComponent.updateMarkers = false;
            }
            else
            {
                rigidBodyPhasespaceTrackerComponent.updatePosition = true;
                rigidBodyPhasespaceTrackerComponent.updateRotation = true;
                mocapInputControllerComponent.updateMarkers = true;
            }
        
        }
        else
            Debug.LogError("No  RigidBody_Phasespace_Tracker attached to the phasespaceManager ");
    }


    public void HideOrShowMarker( bool showMarker)
    {


        mocapInputControllerComponent.drawMarkersTransform = showMarker;
        


    }

 


}
