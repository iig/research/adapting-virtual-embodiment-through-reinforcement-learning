﻿using UnityEngine;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.IO;


public abstract class QuestionManager_Generic : MonoBehaviour
{
    public bool shuffleQuestions = false;
   // public string folderToSave = "./SavedData/";
    public bool isAnswered { get; protected set; }

   // protected ExperimentController_Generic mExperimentController;

    /// <summary>
    /// Use this to start displaying the questions
    /// </summary>
    public abstract void ShowNextQuestion();
}

[XmlRoot("questionRecordContainer")]
public class QuestionRecordContainer
{
    [XmlArray("questionRecords")]
    [XmlArrayItem("questionRecord")]
    public List<QuestionRecord> records;

    public QuestionRecordContainer()
    {
        records = new List<QuestionRecord>();
    }


    public void Save(string path)
    {
        var serializer = new XmlSerializer(typeof(QuestionRecordContainer));
        using (var stream = new FileStream(path, FileMode.Create))
        {
            serializer.Serialize(stream, this);
        }
    }

    public override string ToString()
    {
        var asString = "";
        foreach (QuestionRecord r in records)
        {
            asString += r.ToString() + "\n";
        }
        return asString;
    }
}

public class QuestionRecord
{
    [XmlAttribute("question")]
    public string question;
    [XmlArray("answers")]
    [XmlArrayItem("answer")]
    public string[] answers;
    [XmlAttribute("selected_answer")]
    public string selectedAnswer;

    public QuestionRecord() { }

    public QuestionRecord(string question, string[] answers, string selectedAnswer)
    {
        this.question = question;
        this.answers = answers;
        this.selectedAnswer = selectedAnswer;
    }

    public override string ToString()
    {
        return question + " -> " + selectedAnswer;
    }
}