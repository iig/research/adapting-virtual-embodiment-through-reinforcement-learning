﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; // Required when Using UI elements.


/// <summary>
/// This class handles position, materials and state of this question and its answers.
/// </summary>
/// [Req
[RequireComponent(typeof(TextMesh))]
public class SliderQuestion : Question
{

    public Transform rayCaster;
    public GameObject[] answers;
    //[Range(0, 2)]
    //public float totalAnswerSpacing;
    [Range(0.1f, 3f)]
    public float animationSpeed = 0.1f;
    public bool reverseCircles = false;
	public bool doubleCheck;




	//public Material frameMaterial;
	public GameObject progressBar;
	public GameObject checkedBar;
	public Color checkedBarColor;
	// public Material highlightMaterial;
	//public Material selectedMaterial;


	public TextMesh questionText { get; private set; }

	bool doubleCheckDone;
	bool firstCheck;
	bool secondCheck;
    bool isPushed;
     float frameScale = 1f;
    //public float frameDownFactor = -0.31f;

    public GameObject Cursor;
    //private const float frameScale = 0.24f;
    //private const float frameDownFactor = -0.31f;
    //private string frameName = "Frame";
   // private string progressBarName = "ProgressBar";
    RectTransform currentProgressBar;
	Image checkedBarImageComponent;
    public KeyCode validateQuestion;

    void Start()
    {
        OnEnable();
        //ArrangeAnswers();
    }

    void OnEnable()
    {
	
		doubleCheckDone = false;
		questionText = GetComponent<TextMesh>();
        isPushed = false;
		checkedBarImageComponent = checkedBar.GetComponent<Image>();
		checkedBarImageComponent.color = Color.black;

		if (rayCaster == null)
            rayCaster = Camera.main.transform;

        if (Camera.main.transform.eulerAngles.y <= 200 && Camera.main.transform.eulerAngles.y >= 160)
        {
            Cursor.GetComponent<Slider>().value = (0.5f / 15) * (Camera.main.transform.eulerAngles.y - 180) + 0.5f;
        }
        else if (Camera.main.transform.eulerAngles.y < 160)
        {
            // Cursor.GetComponent<Slider>().value = 0;
        }
        else
        {

        }
    }

    void Update()
    {
        Debug.DrawLine(rayCaster.position, rayCaster.position + 100f * rayCaster.forward, Color.yellow);

        if (!isAnswered && currentProgressBar != null && doubleCheckDone)
        {



            if (ControllerExperiment.isButtonDown)
            {
                isPushed = true;
            }
                if (ControllerExperiment.isButton && isPushed)
                {


                    // currentAnswer.transform.Find(frameName).GetComponent<Renderer>().sharedMaterial = highlightMaterial;
                    currentProgressBar.localScale = new Vector3(currentProgressBar.localScale.x, currentProgressBar.localScale.y + Time.deltaTime * animationSpeed
                        , currentProgressBar.localScale.z);


                    // if (Cursor != null)
                    //    Cursor.SetActive(false);

                    if (currentProgressBar.localScale.y >= frameScale)
                    {
                        if (ignoreAnswers)
                        {
                            isAnswered = true;
                            currentProgressBar.localScale = new Vector3(currentProgressBar.localScale.x, 0, currentProgressBar.localScale.z);
						
						//  currentAnswer.transform.Find(frameName).GetComponent<Renderer>().sharedMaterial = selectedMaterial;

					}    //currentProgressBar.localScale -= Time.deltaTime * animationSpeed * Vector3.one;
                        else
                        {
                            isAnswered = true;
                            currentProgressBar.localScale = new Vector3(currentProgressBar.localScale.x, 0, currentProgressBar.localScale.z);

						// currentAnswer.transform.Find(frameName).GetComponent<Renderer>().sharedMaterial = selectedMaterial;
					}
                    }

			} else {

				if ( Camera.main.transform.eulerAngles.y <= 195 && Camera.main.transform.eulerAngles.y >= 165 ) {
					Cursor.GetComponent<Slider>().value = (0.5f / 15) * (Camera.main.transform.eulerAngles.y - 180) + 0.5f;
				}
			}
            
        }
        else
        {
			//  if (Cursor != null)
			//     Cursor.SetActive(true);

			if ( Camera.main.transform.eulerAngles.y <= 195 && Camera.main.transform.eulerAngles.y >= 165 ) {
				Cursor.GetComponent<Slider>().value = (0.5f / 15) * (Camera.main.transform.eulerAngles.y - 180) + 0.5f;
			}


		}

        if (Input.GetKeyDown(validateQuestion))
        {
            //ignoreAnswers = true;
            isAnswered = true;
      
        }

        //TO DO: modify with my own event ( button)
        /*  if (ExperimentEvent_Generic.GetEventDown(ExperimentEvent_Generic.ExperimentEvent.AnswerOne))
          {
              currentAnswer = answers[0];
              isAnswered = true;
          }
          if (ExperimentEvent_Generic.GetEventDown(ExperimentEvent_Generic.ExperimentEvent.AnswerTwo))
          {
              currentAnswer = answers[1];
              isAnswered = true;
          }*/
		
		if(!doubleCheckDone) {

			if( Camera.main.transform.eulerAngles.y >= 195 && !firstCheck) {
				firstCheck = true;
			}

			if ( Camera.main.transform.eulerAngles.y <= 165 && !secondCheck) {
				secondCheck = true;
			}

			if(firstCheck && secondCheck ) {
				checkedBarImageComponent.color = checkedBarColor;
				doubleCheckDone = true;
				firstCheck = false;
				secondCheck = false;
			}
				

		}


	}

    public override string GetAnswer()
    {
        if (!isAnswered)
            throw new System.Exception("Cannot provide the answer to an unanswered question.");

        //return GetTextWithoutReturns(currentAnswer);
        return GetSliderValue(currentAnswer);
    }

  



 

    /// <summary>
    /// Creates a serializable snaphshot of this question.
    /// </summary>
    /// <returns>A record of this question to be exported using xml.</returns>
    /// TO DO : own savew system
  /*  public List<string> ToRecord()
    {
        string question = GetTextWithoutReturns(gameObject);
        //List<string> answersText = new List<string>(answers.Length);

        if (!isAnswered)
        {
            Debug.LogError("Cannot export unanswered question '" + question + "'");
            return null;
        }
        else
        {
           // foreach (GameObject answerGO in answers)
            //    answersText.Add(GetTextWithoutReturns(answerGO));

            string answer = GetTextWithoutReturns(currentAnswer);
            List<string> record = new List<string>();
            record.Add(question);
            record.Add(answer);
            Debug.Log("Question exported as: " + record.ToString());
            return record;
        }
    }
    */
    /// <summary>
    /// Called by the answer when the cursor arrives on it.
    /// </summary>
    public void OnAnswerIn(GameObject answer)
    {
        if (!isAnswered)
        {
           // answer.transform.Find(frameName).GetComponent<Renderer>().sharedMaterial = highlightMaterial;
            currentAnswer = answer;
            currentProgressBar = progressBar.GetComponent<RectTransform>(); //answer.transform.Find(progressBarName);
        }
    }

    /// <summary>
    /// Called by the answer when the cursor goes out of it.
    /// </summary>
    public void OnAnswerOut(GameObject answer)
    {
        if (!isAnswered)
        {
           // answer.transform.Find(frameName).GetComponent<Renderer>().sharedMaterial = frameMaterial;
            currentAnswer = null;

            if (currentProgressBar != null)
                currentProgressBar.localScale = new Vector3(currentProgressBar.localScale.x, 0, currentProgressBar.localScale.z);

           // currentProgressBar = null;
        }
    }

    string GetSliderValue(GameObject go)
    {
		if(go!=null)
        return go.GetComponent<Slider>().value.ToString();
		else
		return "NaN";
    }

   

 /*   private void ArrangeAnswers()
    {

        float spacing;
        if (answers.Length == 1)
        {
            spacing = 0;
            totalAnswerSpacing = 0;
        }
        else
            spacing = totalAnswerSpacing / (answers.Length - 1); // a question has at least two answers, i.e. no divide by zero

        for (int i = 0; i < answers.Length; i++)
        {
            if (answers[i].transform.childCount != 0)
                return;
            // create and assign frame quad
            var frame = GameObject.CreatePrimitive(PrimitiveType.Quad);
            SetupQuad(frame, answers[i], frameName, frameMaterial);
            frame.AddComponent<BoxCollider>();

            //create and assign progress bar quad
            var progressBar = GameObject.CreatePrimitive(PrimitiveType.Quad);
            SetupQuad(progressBar, answers[i], progressBarName, selectedMaterial);
            progressBar.transform.localScale = Vector3.zero;
            progressBar.transform.localPosition += new Vector3(0f, 0f, 0.02f); // so that it gets on top of the text

            // setup the answer itself
            var answerScript = answers[i].GetComponent<AnswerGUI_Generic>();
            answerScript.question = this;
            answerScript.rayCaster = rayCaster;
            answers[i].transform.position = transform.position;
            float startingPosition = -totalAnswerSpacing * 0.5f;
            // answers[i].transform.localPosition = new Vector3(startingPosition + i * spacing - Random.value /5, -0.05f- Random.value/5, 0f);
            answers[i].transform.localPosition = new Vector3(startingPosition + i * spacing, -0.05f, 0f);
            // answers[i].AddComponent<BoxCollider>();
            //var collider = answers[i].GetComponent<BoxCollider>();
            // float size = Mathf.Max(collider.size.x, collider.size.y);
            //collider.size = new Vector3(size, size, 0f);
        }
    }*/

  /*  private void SetupQuad(GameObject quad, GameObject parent, string name, Material mat)
    {
        quad.name = name;
        quad.transform.localScale *= frameScale;
        quad.transform.position = transform.position;
        quad.transform.localPosition += new Vector3(0f, frameDownFactor * frameScale, 0.01f);
        if (reverseCircles)
            quad.transform.localRotation *= Quaternion.Euler(0f, 180f, 0f);
        quad.transform.parent = parent.transform;
        quad.GetComponent<Renderer>().sharedMaterial = mat;
        Destroy(quad.GetComponent<MeshCollider>());
    }*/
}


    
    // Update is called once per frame
	/*void Update () {

        if(Input.GetButtonDown("Left: 16"))
        {
            //get value

        // save value

        // Validate question 

        }
		
	}*/

