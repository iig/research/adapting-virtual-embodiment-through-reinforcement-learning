﻿using UnityEngine;
using System.Collections;

public class AnswerSlider : MonoBehaviour
{
  
    public SliderQuestion question; // set at startup by the question


    public Transform rayCaster; // set at startup by the question

  // private bool isSelected = false;

    private void OnEnable()
    {
        if (rayCaster == null)
            rayCaster = Camera.main.transform;

        //isSelected = true;
    }

    void Update()
    {
        RaycastHit hitinfo;
        Physics.Raycast(rayCaster.position, rayCaster.forward, out hitinfo, 100f, 11);


       // if (!isSelected && isOnMe(hitinfo))
       if ( ControllerExperiment.isButtonDown )
        {
            //isSelected = true;
            question.OnAnswerIn(gameObject);
        }
       // else if (isSelected && !isOnMe(hitinfo))
       else if ( ControllerExperiment.isButtonUp )
        {
           // isSelected = false;
            question.OnAnswerOut(gameObject);
        }
    }

    bool isOnMe(RaycastHit hitinfo)
    {
        return hitinfo.collider != null && hitinfo.collider.transform.parent.Equals(gameObject.transform);
    }
}

