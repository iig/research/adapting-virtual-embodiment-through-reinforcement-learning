﻿using UnityEngine;
using System.Collections;

public class AnswerGUIButton : MonoBehaviour {
    [HideInInspector]
    public QuestionGUIButton question; // set at startup by the question
    [HideInInspector]
    public Transform rayCaster; // set at startup by the question

   // private bool isSelected = false;

    private void OnEnable()
    {
       // isSelected = true;
    }

    void Update () {
       // RaycastHit hitinfo;
       // Physics.Raycast(rayCaster.position, rayCaster.forward, out hitinfo, 100f,11);


		if (ControllerExperiment.isButtonDown)
		{
          //  isSelected = true;
            question.OnAnswerIn(gameObject);
        }
		else if ( ControllerExperiment.isButtonUp )
		{
          //  isSelected = false;
            question.OnAnswerOut(gameObject);
        }
	}

 
}
