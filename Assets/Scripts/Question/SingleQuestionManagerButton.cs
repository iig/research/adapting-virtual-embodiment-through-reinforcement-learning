﻿using UnityEngine;
using System.Collections.Generic;



public class SingleQuestionManagerButton : QuestionManager_Generic
{

    private QuestionGUIButton[] questions;
    private List<string> questionsText=new List<string>();
    private int currentQuestion = -1;
    public RecordQuestionsSettings recordDataQuestionsSettings;
    public string fileNameToRecord;
    public float condition;
    public List<string> answersText;
    public bool notRecordingAnswer;
    // private QuestionRecordContainer savedAnswers = new QuestionRecordContainer();


    void OnEnable()
    {
        answersText.Clear();
        questionsText.Clear();

        currentQuestion = -1;
        isAnswered = false;
        questions = GetComponentsInChildren<QuestionGUIButton>(true);


        if (shuffleQuestions)
            questions = Utils.Arrays.Shuffle(questions);

        foreach (QuestionGUIButton question in questions)
        {
            questionsText.Add(question.GetQuestion());
        }


        if (!recordDataQuestionsSettings.qnLog.logIsOpen)
        {
            recordDataQuestionsSettings.fileNameToRecord = fileNameToRecord;

            if(!notRecordingAnswer)
            recordDataQuestionsSettings.StartRecording(questionsText);

            
        }

        ShowNextQuestion();
    }

    /// <summary>
    /// Use this to start displaying the questions
    /// </summary>
    public override void ShowNextQuestion()
    {
        if (currentQuestion > -1)
            questions[currentQuestion].HideQuestion();

        currentQuestion++;

        if (currentQuestion < questions.Length)
        {
            questions[currentQuestion].DisplayQuestion();
        }
        else
        {
            //Debug.Log("Saving records:\n" + savedAnswers.ToString());
            //var timestamp = string.Format("{0:s}", System.DateTime.Now).Replace(':', '-');
            //savedAnswers.Save(folderToSave + "answers_"  + "_" + timestamp + ".xml");

            if (recordDataQuestionsSettings.qnLog.logIsOpen && answersText.Count!=0)
                recordDataQuestionsSettings.qnLog.Logging(answersText, condition);

            isAnswered =true;


            answersText.Clear();

            if (!notRecordingAnswer)
                recordDataQuestionsSettings.StopRecording();


            gameObject.SetActive(false);
            //mExperimentController.EndOfQuestionnaire(questions);
        }
    }

    // add the answer in the tab
    private void SaveCurrentAnswer()
    {
        answersText.Add(questions[currentQuestion].GetAnswer());
    }

    void Update()
    {
        if (currentQuestion >= 0 &&
            currentQuestion < questions.Length &&
            questions[currentQuestion].isAnswered && !questions[currentQuestion].ignoreAnswers)
        {
            SaveCurrentAnswer();
            ShowNextQuestion();
        }
        else if(currentQuestion >= 0 &&
            currentQuestion < questions.Length &&
            questions[currentQuestion].isAnswered && questions[currentQuestion].ignoreAnswers)
        {
         
            //SaveCurrentAnswer();
            ShowNextQuestion();
        }
    }
}