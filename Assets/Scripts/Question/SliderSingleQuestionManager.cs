﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ErrPExperiment
{


        [System.Serializable]
        public class RecordQuestionsSettings
        {
            [HideInInspector]
            public bool isRecording;

            [HideInInspector]
            public bool recordFlag;

            public LogDynamicDistortionQuestion qnLog = new LogDynamicDistortionQuestion();
            public string folderPathToRecord = ".\\adjusted-id-schubert-day-1-exp-1-trial-1OK.mkr16Hz.csv";

            [HideInInspector]
            public string fileNameToRecord = "";

            bool writeDirectToFile = true;


            public string StartRecording(List<string> questionsList)
            {
                return qnLog.OpenLog(folderPathToRecord + fileNameToRecord, questionsList, writeDirectToFile);
            }
            public void StopRecording()
            {
                if (qnLog.logIsOpen)
                    qnLog.CloseLog();
            }

        };



        public class SliderSingleQuestionManager : QuestionManager_Generic
        {

            public Question[] questions;
            private List<string> questionsText = new List<string>();
            private int currentQuestion = -1;
            public RecordQuestionsSettings recordDataQuestionsSettings;
            public string fileNameToRecord;
            public float condition;
			public int pairNumber;
            public List<string> answersText;
            public bool notRecordingAnswer;
            // private QuestionRecordContainer savedAnswers = new QuestionRecordContainer();


            void OnEnable()
            {
                answersText.Clear();
                questionsText.Clear();

                currentQuestion = -1;
                isAnswered = false;
                questions = GetComponentsInChildren<Question>(true);


                if (shuffleQuestions)
                    questions = Utils.Arrays.Shuffle(questions);

                foreach (Question question in questions)
                {
                    questionsText.Add(question.GetQuestion());
                }


                if (!recordDataQuestionsSettings.qnLog.logIsOpen)
                {
                    recordDataQuestionsSettings.fileNameToRecord = fileNameToRecord;

                    if (!notRecordingAnswer)
                        recordDataQuestionsSettings.StartRecording(questionsText);


                }

                ShowNextQuestion();
            }

            /// <summary>
            /// Use this to start displaying the questions
            /// </summary>
            public override void ShowNextQuestion()
            {
                if (currentQuestion > -1)
                    questions[currentQuestion].HideQuestion();

                currentQuestion++;

                if (currentQuestion < questions.Length)
                {
                    questions[currentQuestion].DisplayQuestion();
                }
                else
                {
				//Debug.Log("Saving records:\n" + savedAnswers.ToString());
				//var timestamp = string.Format("{0:s}", System.DateTime.Now).Replace(':', '-');
				//savedAnswers.Save(folderToSave + "answers_"  + "_" + timestamp + ".xml");

					if (recordDataQuestionsSettings.qnLog.logIsOpen && answersText.Count != 0)
                        recordDataQuestionsSettings.qnLog.Logging(answersText, condition, pairNumber);

                    isAnswered = true;


                   // answersText.Clear();

                    if (!notRecordingAnswer)
                        recordDataQuestionsSettings.StopRecording();


                    gameObject.SetActive(false);
                    //mExperimentController.EndOfQuestionnaire(questions);
                }
            }

            // add the answer in the tab
            private void SaveCurrentAnswer()
            {
                answersText.Add(questions[currentQuestion].GetAnswer());
            }

		
            void Update()
            {
                if (currentQuestion >= 0 &&
                    currentQuestion < questions.Length &&
                    questions[currentQuestion].isAnswered && !questions[currentQuestion].ignoreAnswers)
                {
       
                    SaveCurrentAnswer();
                    ShowNextQuestion();
                }
                else if (currentQuestion >= 0 &&
                    currentQuestion < questions.Length &&
                    questions[currentQuestion].isAnswered && questions[currentQuestion].ignoreAnswers)
                {

                    //SaveCurrentAnswer();
                    ShowNextQuestion();
                }
            }
        }

}
