﻿using UnityEngine;
using System.Collections.Generic;
using System.Xml.Serialization;


/// <summary>
/// This class handles position, materials and state of this question and its answers.
/// </summary>
/// [Req
[RequireComponent(typeof(TextMesh))]
public class QuestionGUIButton : Question {

	//public Transform rayCaster;
	public GameObject[] answers;
	[Range(0, 2)]
	public float totalAnswerSpacing;
	[Range(0.1f, 0.35f)]
	public float animationSpeed = 0.1f;
	public bool reverseCircles = false;
	



	public Material frameMaterial;
	public Material highlightMaterial;
	public Material selectedMaterial;

	public TextMesh questionText { get; private set; }


	public float frameScale = 0.24f;
	public float frameDownFactor = -0.31f;

	bool isPushed;
	//private const float frameScale = 0.24f;
	//private const float frameDownFactor = -0.31f;
	private string frameName = "Frame";
	private string progressBarName = "ProgressBar";
	private Transform currentProgressBar;
	public KeyCode validateQuestion;

	void Start() {
		OnEnable();
		//ArrangeAnswers();
	}

	void OnEnable() {

		isPushed = false;
		isAnswered = false;

		questionText = GetComponent<TextMesh>();
	}

	void Update() {

		if ( ControllerExperiment.isButtonDown ) {
			isPushed = true;
	
		}
		if ( ControllerExperiment.isButton && isPushed) {


			if (!isAnswered && currentProgressBar != null) {
				// currentAnswer.transform.Find(frameName).GetComponent<Renderer>().sharedMaterial = highlightMaterial;
		
				currentProgressBar.localScale += Time.deltaTime * animationSpeed * Vector3.one;

			

				if (currentProgressBar.localScale.x >= frameScale) {
					if (ignoreAnswers) {
				
						isAnswered = true;
						currentProgressBar.localScale = Vector3.zero;
						//  currentAnswer.transform.Find(frameName).GetComponent<Renderer>().sharedMaterial = selectedMaterial;

					}    //currentProgressBar.localScale -= Time.deltaTime * animationSpeed * Vector3.one;
					else {
						isAnswered = true;
						currentProgressBar.localScale = Vector3.zero;
						// currentAnswer.transform.Find(frameName).GetComponent<Renderer>().sharedMaterial = selectedMaterial;
					}
				}
			}
		}

		if (Input.GetKeyDown(validateQuestion)) {
			ignoreAnswers = true;
			isAnswered = true;
		}

		//TO DO: modify with my own event ( button)
		/*  if (ExperimentEvent_Generic.GetEventDown(ExperimentEvent_Generic.ExperimentEvent.AnswerOne))
          {
              currentAnswer = answers[0];
              isAnswered = true;
          }
          if (ExperimentEvent_Generic.GetEventDown(ExperimentEvent_Generic.ExperimentEvent.AnswerTwo))
          {
              currentAnswer = answers[1];
              isAnswered = true;
          }*/
	}



	public override void DisplayQuestion() {
		isAnswered = false;
		gameObject.SetActive(true);
		ArrangeAnswers();
	}


	/// <summary>
	/// Creates a serializable snaphshot of this question.
	/// </summary>
	/// <returns>A record of this question to be exported using xml.</returns>
	/// TO DO : own savew system
	/*  public List<string> ToRecord()
	  {
		  string question = GetTextWithoutReturns(gameObject);
		  //List<string> answersText = new List<string>(answers.Length);

		  if (!isAnswered)
		  {
			  Debug.LogError("Cannot export unanswered question '" + question + "'");
			  return null;
		  }
		  else
		  {
			 // foreach (GameObject answerGO in answers)
			  //    answersText.Add(GetTextWithoutReturns(answerGO));

			  string answer = GetTextWithoutReturns(currentAnswer);
			  List<string> record = new List<string>();
			  record.Add(question);
			  record.Add(answer);
			  Debug.Log("Question exported as: " + record.ToString());
			  return record;
		  }
	  }
	  */
	/// <summary>
	/// Called by the answer when the cursor arrives on it.
	/// </summary>
	public void OnAnswerIn(GameObject answer) {
		if (!isAnswered) {
			answer.transform.Find(frameName).GetComponent<Renderer>().sharedMaterial = highlightMaterial;
			currentAnswer = answer;
			currentProgressBar = answer.transform.Find(progressBarName);
		}
	}

	/// <summary>
	/// Called by the answer when the cursor goes out of it.
	/// </summary>
	public void OnAnswerOut(GameObject answer) {
		if (!isAnswered) {
			answer.transform.Find(frameName).GetComponent<Renderer>().sharedMaterial = frameMaterial;
			currentAnswer = null;

			if (currentProgressBar != null)
				currentProgressBar.localScale = Vector3.zero;

			currentProgressBar = null;
		}
	}

	private string GetTextWithoutReturns(GameObject go) {
		return go.GetComponent<TextMesh>().text.Replace('\n', ' ');
	}

	private void ArrangeAnswers() {

		float spacing;
		if (answers.Length == 1) {
			spacing = 0;
			totalAnswerSpacing = 0;
		}
		else
			spacing = totalAnswerSpacing / (answers.Length - 1); // a question has at least two answers, i.e. no divide by zero

		for (int i = 0; i < answers.Length; i++) {
			if (answers[i].transform.childCount != 0)
				return;
			// create and assign frame quad
			var frame = GameObject.CreatePrimitive(PrimitiveType.Quad);
			SetupQuad(frame, answers[i], frameName, frameMaterial);
			frame.AddComponent<BoxCollider>();

			//create and assign progress bar quad
			var progressBar = GameObject.CreatePrimitive(PrimitiveType.Quad);
			SetupQuad(progressBar, answers[i], progressBarName, selectedMaterial);
			progressBar.transform.localScale = Vector3.zero;
			progressBar.transform.localPosition += new Vector3(0f, 0f, 0.02f); // so that it gets on top of the text

			// setup the answer itself
			var answerScript = answers[i].GetComponent<AnswerGUIButton>();
			answerScript.question = this;
			//answerScript.rayCaster = rayCaster;
			answers[i].transform.position = transform.position;
			float startingPosition = -totalAnswerSpacing * 0.5f;
			// answers[i].transform.localPosition = new Vector3(startingPosition + i * spacing - Random.value /5, -0.05f- Random.value/5, 0f);
			answers[i].transform.localPosition = new Vector3(startingPosition + i * spacing, -0.05f, 0f);
			// answers[i].AddComponent<BoxCollider>();
			//var collider = answers[i].GetComponent<BoxCollider>();
			// float size = Mathf.Max(collider.size.x, collider.size.y);
			//collider.size = new Vector3(size, size, 0f);
		}
	}

	private void SetupQuad(GameObject quad, GameObject parent, string name, Material mat) {
		quad.name = name;
		quad.transform.localScale *= frameScale;
		quad.transform.position = transform.position;
		quad.transform.localPosition += new Vector3(0f, frameDownFactor * frameScale, 0.01f);
		if (reverseCircles)
			quad.transform.localRotation *= Quaternion.Euler(0f, 180f, 0f);
		quad.transform.parent = parent.transform;
		quad.GetComponent<Renderer>().sharedMaterial = mat;
		Destroy(quad.GetComponent<MeshCollider>());
	}
}


