﻿using UnityEngine;
using System.Collections;

public class AnswerGUI_Generic : MonoBehaviour {
    [HideInInspector]
    public QuestionGUI_Generic question; // set at startup by the question
    [HideInInspector]
    public Transform rayCaster; // set at startup by the question

    private bool isSelected = false;

    private void OnEnable()
    {
        isSelected = true;
    }

    void Update () {
        RaycastHit hitinfo;
        Physics.Raycast(rayCaster.position, rayCaster.forward, out hitinfo, 100f,11);


        if (!isSelected && isOnMe(hitinfo))
        {
            isSelected = true;
            question.OnAnswerIn(gameObject);
        }
        else if (isSelected && !isOnMe(hitinfo))
        {
            isSelected = false;
            question.OnAnswerOut(gameObject);
        }
	}

    bool isOnMe(RaycastHit hitinfo)
    {
        return hitinfo.collider != null && hitinfo.collider.transform.parent.Equals(gameObject.transform);
    }
}
