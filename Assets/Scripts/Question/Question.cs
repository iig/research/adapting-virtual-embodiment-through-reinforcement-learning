﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Question : MonoBehaviour
{
	public GameObject currentAnswer;
	public bool isAnswered { get;  set; }
	public bool ignoreAnswers = false;

	public string GetQuestion () {

		return GetTextWithoutReturns( gameObject );
	}

	private string GetTextWithoutReturns ( GameObject go ) {

		if(go!=null)
		return go.GetComponent<TextMesh>().text.Replace( '\n', ' ' );
		else
		return "NaN";
	}

	public virtual void HideQuestion () {
		gameObject.SetActive( false );
	}

	public virtual void DisplayQuestion () {
		isAnswered = false;
		gameObject.SetActive( true );
		// ArrangeAnswers();
	}

	public virtual string GetAnswer () {
		if ( !isAnswered )
			throw new System.Exception( "Cannot provide the answer to an unanswered question." );

		return GetTextWithoutReturns( currentAnswer );
	}
}
