﻿/**************************************************************************
 * This script allows to manage the experiment protocol only ( Task, Block and recording) 
 * Written by Thibault Porssut
 * Last update: 15/11/19
 * *************************************************************************/

using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using UnityEngine;
using ReadWriteCsv;


namespace DynamicDistortionWithErrPExperiment {

	public class ExperimentController_ReinforcementLearning : MonoBehaviour
	{

		public DynamicExperiment.IkGoalDynamicDistorter distortedHand;
	
		/// <summary>
		/// Object held in the right hand of the original avatar
		/// </summary>
		public GameObject originalRightObjectHeld;
		/// <summary>
		/// Object held in the right hand of the distorted avatar
		/// </summary>
		public GameObject distortedRightObjectHeld;

		
        //public GameObject[] twoPhaseReachingTargets; // contains 2 balls
		//public GameObject targetPosition;
		public ExperimentSetting experimentSetting;

		//public ErrPExperiment.SliderSingleQuestionManager questionOnwership;
		public ErrPExperiment.SliderSingleQuestionManager questionThreshold;
		public SingleQuestionManagerButton breakMessage;

		public GameObject chair;
		public LogRLDistortion rlLogger;
		public SendTriggerParallel parallelPort;

        // Which experiment to run
        public bool runTwoPhaseExperiment;
        public GameObject reachingTarget;
		public GameObject intermediateTarget;
        //int currentActiveTargetIndex;

        public GameObject eyeTrackingTarget;
		public bool greenAppearAtStart;
		
		EyeTrackingTarget eyeTarget;
        bool trialIsInvalid;


		

		SetCanvasActive messageTargetLimit;
		GameObject headTracker;
		Target_Calibration calibrateEllipseTrajectory;
		Target_Trajectory_NonBiological ellipseTrajectory;
        TargetReaching targetReaching;
		TargetReaching intermedTargetReaching;


		//Parameters that can be adjusted
		float distanceTarget=0.9f;
		float taskDuration = 6;
		float drange=0.4f;
		const float MAX_GAIN = 4f; // maximal distortion gain

		//Internal variable
		bool startOrStop;
		float armDistance = 99999;


		// Interval var for RL
		string type; // either UCB / UCB2 or Staircase
		bool startOrStopTargetMonitoring;
		bool startOrStopFirstTargetMonitoring;

		//flag to control state in update
		bool flag1;
		bool flag2;
		bool isExplanationStarted;
		bool isTrainingStarted;
		bool isEndStarted;
		bool isEllipseCalibrated;

		//Variable for the progress of the experiment
		//block number for explanation=0, for training =1 and for online =2
		int blockNumber;
		int trial;
		float currentCondition;
		int trigger;
		bool answerSubject;
		float currentDistortionGain;
		float currentRadius;
		List<List<float>> explanationBlock=new List<List<float>>();
		List<float> explanationDistortionFor2Parameters = new List<float>() {0,10,0,10};
		List<float> explanationRadiusFor2Parameters = new List<float>() {0.1f,0.05f};
		List<List<float>> trainingBlock=new List<List<float>>();
		IDictionary<float, int> distortionToTrigger = new Dictionary<float, int>()
											{
												{0,1},
												{0.25f, 2},
												{0.5f,3},
												{0.75f,4},
												{1f,5},
												{1.25f,6},
												{1.5f,7},
												{1.75f,8},
												{2f,9},
												{2.25f,10},
												{2.5f,11},
												{2.75f,12},
												{3f,13},
												{4.0f,14},
												{5.0f,15},
                                                {7.0f, 16},
												{10.0f,17},
											};
		IDictionary<bool, int> answerToTrigger = new Dictionary<bool, int>() {

			{true,20 },
			{false,21 }

		};
		//Mode of the answer
		public enum AnswersMode { Question, Button, EEG , None};
		AnswersMode mode;

		//Coroutine Status
		bool taskIsRunning;
		bool timerIsRunning;
		bool questionIsRunning;
		bool getAnswerIsRunning;
		bool buttonIsRunning;
		bool trialIsRunning; 
		bool breakIsRunning;
		bool trainingblockIsRunning;
		bool distortionGainReceived; // True when received the next distortion value from client
		bool rlBlockIsRunning; // whether we are still trying to converge distortion
		bool rlBlockDone;
		bool staircaseIsRunning;
		bool staircaseBlockDone;
		bool RLconverged; // true when RL converges
		bool isBreakTime; // Timer that times 10 min to give a break
		bool theEndIsRunning;
		bool staircaseRlRunning;
		bool answerBCIReceived;
		bool waitingIsRunning;

		//Recording data
		List<Vector3> worldPositions = new List<Vector3>();

        private void Awake() {
            if (ExperimentInterface.Instance.protocoleType!=ExperimentInterface.ProtocoleType.RL2Param ) {
				//Load the config file with all the condition
				LoadConfig( ExperimentInterface.Instance.configPath + ExperimentInterface.Instance.configConditionDistortionFilename, ref explanationBlock, ref trainingBlock );

				//Initialize parallel port
				parallelPort.SetAdress(ExperimentInterface.Instance.parallelPort);
			}
		}


		// Start is called before the first frame update
		void Start()
		{
			//Find the head tracker
			headTracker = GameObject.Find( "HEAD" );

            // Determine which experiment to run
            //Get the ellipse trajectory component to make the target move
            if (!runTwoPhaseExperiment)
            {
				intermediateTarget.SetActive( false);

			}
       

           // reachingTarget.SetActive(false);

            //Find all the warning messages
            messageTargetLimit = Camera.main.GetComponentInChildren<SetCanvasActive>();

			//Calibrate the ellipse trajectory position
			calibrateEllipseTrajectory = reachingTarget.transform.parent.GetComponent<Target_Calibration>();

            ellipseTrajectory = reachingTarget.transform.GetComponent<Target_Trajectory_NonBiological>();
            targetReaching = reachingTarget.GetComponent<TargetReaching>();
			intermedTargetReaching=intermediateTarget.GetComponent<TargetReaching>();

			eyeTarget = eyeTrackingTarget.GetComponent<EyeTrackingTarget>();
            trialIsInvalid = false;

            distortionGainReceived = false;
			answerBCIReceived = false;
			rlBlockIsRunning = false;
			rlBlockDone = false;
			staircaseIsRunning = false;
			staircaseBlockDone = false;
			RLconverged = false;
			isBreakTime = true;
			currentDistortionGain = -1;
			currentRadius = -1;
			rlLogger = new LogRLDistortion();
			staircaseRlRunning = false;           

		}

		// Update is called once per frame
		void Update()
		{
			//Wait for the calibration process to be completed
			if ( experimentSetting.start ) {

                // Get messages
                messageDispatcher();

                //Calibrate the target position according to the user limb length 
                if ( !isEllipseCalibrated ) {
					
					//Set the target at a good distance according to the user chest
					calibrateEllipseTrajectory.D0 = distanceTarget;

					calibrateEllipseTrajectory.isCalibrated = true;
					isEllipseCalibrated = true;
				}

				//Allow to test different parameters for the task
				if ( ExperimentInterface.Instance.protocoleErrP == ExperimentInterface.Protocoles.Prototype ) {

					//Update setting parameters from experimenter interface
					SetParameters();

					// Start a trial if not alrealdy one has started
					if(!trialIsRunning)
					StartCoroutine( Trial(ExperimentInterface.Instance.gain) );


				}
				// Execute the experiment
				else {

					// EEG block without Reinforcement Learning
					if (ExperimentInterface.Instance.protocoleType == ExperimentInterface.ProtocoleType.Offline) {

						//Explanation Block
						if (!isExplanationStarted && !trainingblockIsRunning) {
							StartCoroutine(TrainingBlock(explanationBlock[0], 0, 1));
							isExplanationStarted = true;
						}


						//Training Block
						if (!isTrainingStarted && !trainingblockIsRunning) {
							trial = 0;
							StartCoroutine(TrainingBlock(trainingBlock[ExperimentInterface.Instance.subjectID], 1, 6));
							isTrainingStarted = true;
						}

						//The End
						if (!isEndStarted && !trainingblockIsRunning) {
							StartCoroutine(TheEnd(true));
							isEndStarted = true;
						}

					}
					// EEG block with Reinforcement Learning
					else if (ExperimentInterface.Instance.protocoleType == ExperimentInterface.ProtocoleType.Online) {

						// Run RL
						if (!rlBlockDone && !rlBlockIsRunning && !trainingblockIsRunning) {
							StartCoroutine( RLBlock( RLType.UCB.ToString() ) );
						}


						//The End
						if (!isEndStarted && rlBlockDone) {
							StartCoroutine(TheEnd(true));
							isEndStarted = true;
						}


					} else if (ExperimentInterface.Instance.protocoleType == ExperimentInterface.ProtocoleType.RLStaircase) {

						//Explanation Block
						if ( !isExplanationStarted && !trainingblockIsRunning ) {
							StartCoroutine( TrainingBlock( explanationBlock[0], 0, 1 ) );
							isExplanationStarted = true;
						}

						// Run either staircase or RL
						if (!rlBlockDone && !staircaseBlockDone && !staircaseRlRunning && !trainingblockIsRunning) {
							trial = 0;
							StartCoroutine(RLStaircaseBlock(80));
						}


						//The End
						if (!isEndStarted && rlBlockDone && staircaseBlockDone) {
							StartCoroutine(TheEnd(true));
							isEndStarted = true;
						}

					} else if (ExperimentInterface.Instance.protocoleType == ExperimentInterface.ProtocoleType.RL2Param) {
						// RL2
						// Explanation Block
						if ( !isExplanationStarted && !trainingblockIsRunning ) {
							StartCoroutine( TrainingBlock( explanationDistortionFor2Parameters, 0, 1 , explanationRadiusFor2Parameters ) );
							isExplanationStarted = true;
						}

						// Run either staircase or RL
						if (!rlBlockDone && !rlBlockIsRunning && !trainingblockIsRunning) {
							trial = 0;
							StartCoroutine(RLBlock("UCB2"));
						}


						//The End
						if (!isEndStarted && rlBlockDone) {
							StartCoroutine(TheEnd(true));
							isEndStarted = true;
						}

					}


				}

				// record trigger and Hand Position and Gain Value
				recordingCustomPosition();

				//Send trigger when the subject entre inside the target
				TargetState();
				FirstTargetState();


			} else
				return;

			//Compute the hand distance of the user from the chair
			armDistance = Mathf.Abs( chair.transform.position.z - originalRightObjectHeld.transform.position.z );


		}

		/// <summary>
		///  Read a configuration table with condition or distortion value and generate the trial and the break
		/// </summary>
		/// <param List of distortion value you want to display to the user="distortionValueList"></param>
		/// <param the block type block explanation=0, block training=1, block online=2 ="block"></param>
		/// <param the number of part you want to split this block="numberOfPart"></param>
		/// <returns></returns>
		IEnumerator TrainingBlock ( List<float> distortionValueList, int block, int numberOfPart,  List<float> radiusValueList = null ) {

			trainingblockIsRunning = true;
			
			blockNumber = block;

			int nbOfTrialBeforeABreak = distortionValueList.Count / numberOfPart;

			int currentNbofBreak = 0;

			if ( radiusValueList == null )
				radiusValueList = new List<float>(){-1};

			trial = 0;

			//Send a trigger to the parallel port ->Start session
			parallelPort.Trigger( 22 );
			trigger = 22;
			// let the time to send the trigger
			yield return new WaitForSeconds( SendTriggerParallel.PulseWidthS );

			foreach ( float distortionValue in distortionValueList ) {

				foreach( float radiusValue in radiusValueList ) {

					if ( trial == nbOfTrialBeforeABreak * (1 + currentNbofBreak)  && radiusValueList[0]==-1 ) {

						if ( !breakIsRunning )
							StartCoroutine( Break() );

						//Send a trigger to the parallel port ->Stop session
						parallelPort.Trigger( 23 );
						trigger = 23;
						// let the time to send the trigger
						yield return new WaitForSeconds( SendTriggerParallel.PulseWidthS );

						yield return new WaitUntil( () => !breakIsRunning );

						//Send a trigger to the parallel port ->Start session
						parallelPort.Trigger( 22 );
						trigger = 22;
						// let the time to send the trigger
						yield return new WaitForSeconds( SendTriggerParallel.PulseWidthS );

						currentNbofBreak += 1;


					}

					// Only increase the number of iteration if the target is reached
					do {
						StartCoroutine( Trial( distortionValue, radiusValue ) );
						yield return new WaitUntil( () => !trialIsRunning );
						if ( !isTrialValid() ) {
							Debug.Log( "Trial" + trial + " error: hand did not attain objective" );
							trial--;
						}
					} while ( !isTrialValid() );
				}
			
			}

			// Handle the case where you want only one part for your block
			if ( numberOfPart==1 ) {

				if ( !breakIsRunning )
					StartCoroutine( Break() );

				//Send a trigger to the parallel port ->Stop session
				parallelPort.Trigger( 23 );
				trigger = 23;
				// let the time to send the trigger
				yield return new WaitForSeconds( SendTriggerParallel.PulseWidthS );

				yield return new WaitUntil( () => !breakIsRunning );


			}

			trainingblockIsRunning = false;
		}

		IEnumerator RLStaircaseBlock(/*int rlMaxturns,*/ int staircaseMaxTurns)
		{
			staircaseRlRunning = true;
			// Decide randomly to start by staircase or RL
			//float number = UnityEngine.Random.Range(0f, 1f);
			if (ExperimentInterface.Instance.startWithRL) {
				// Rl => Staircase
				while (!rlBlockDone) {
					if (!rlBlockIsRunning) {
						StartCoroutine(RLBlock(RLType.UCB.ToString()));
						yield return new WaitUntil(() => rlBlockDone);
					}
				}

				StartCoroutine(StaircaseBlock(staircaseMaxTurns));
				yield return new WaitUntil(() => staircaseBlockDone);

			} else {
				// Staircase => RL
				StartCoroutine(StaircaseBlock(staircaseMaxTurns));
				yield return new WaitUntil(() => staircaseBlockDone);

				while (!rlBlockDone) {
					if (!rlBlockIsRunning) {
						StartCoroutine(RLBlock(RLType.UCB.ToString()));
						yield return new WaitUntil(() => rlBlockDone);
					}
				}

			}

			staircaseRlRunning = false;
		}

		IEnumerator StaircaseBlock(int maxTurns)
		{
			staircaseIsRunning = true;

			StaircaseWrapper staircase = new StaircaseWrapper();

			this.type = "Staircase";
			sendMessageToClient(-1);

			// Start timer
			StartCoroutine(BreakTimer());

			int iteration = 1;

			do {

				float gainValue = staircase.Step();

				// Only increase the number of iteration if the target is reached
				do {
					StartCoroutine(Trial(gainValue));
					yield return new WaitUntil(() => !trialIsRunning);
					if (!isTrialValid()) {
						Debug.Log("Staircase Turn " + iteration + " error: hand did not attain objective");
					}
				} while (!isTrialValid()) ;

				staircase.UpdateDetection(answerSubject);
				//Debug.Log("Staircase total iteration " + iteration + " has detected: " + answerSubject);
				++iteration;

				if (isBreakTime) {
					Debug.Log("Turn " + iteration + " Break time!");
					StartCoroutine(Break());
					yield return new WaitUntil(() => !breakIsRunning);
					// Start new break timer of 10min
					StartCoroutine(BreakTimer());
				}

			} while (!staircase.hasConverged());

			staircase.StopStaircases();

			staircaseIsRunning = false;
			staircaseBlockDone = true;
		}

		IEnumerator RLBlock(string type)
		{
			rlBlockIsRunning = true;
			this.type = type;
			sendMessageToClient(-1);
			yield return new WaitUntil(() => distortionGainReceived && currentDistortionGain >= 0);

			// Start timer
			StartCoroutine(BreakTimer());

			int rlTurns = 0;

			/*for (int rlTurns = 1; rlTurns <= maxTurns;)*/
			while (!RLconverged) {

				if (isBreakTime) {
					Debug.Log("Turn " + rlTurns + " Break time!");
					StartCoroutine(Break());
					yield return new WaitUntil(() => !breakIsRunning);
					// Start new break timer of 10min
					StartCoroutine(BreakTimer());
				}

				//bool useMaximalGain = false;
				//float gainValue = currentDistortionGain;
				//if (this.type == RLType.UCB.ToString()) {
				//	// For UCB: For 1/5 of the time, send a random var
				//	useMaximalGain = UnityEngine.Random.Range(0f, 10f) < 2f;
				//	gainValue = useMaximalGain ? MAX_GAIN : currentDistortionGain;
				//}

				do {
					if (this.type == RLType.UCB.ToString()) {
						StartCoroutine(Trial(currentDistortionGain));
					} else {
						StartCoroutine(Trial(currentDistortionGain, currentRadius));
					}
					yield return new WaitUntil(() => !trialIsRunning);

					if (!isTrialValid()) {
						Debug.Log("Turn " + rlTurns + " error: hand did not attain objective");
					}
				} while (!isTrialValid());


				if (this.type == RLType.UCB.ToString()) {
					recordRLDistortion(rlTurns, currentDistortionGain, answerSubject);
					Debug.Log("Turn " + rlTurns + " For gain :" + currentDistortionGain + " subject has detected: " + answerSubject);
				} else {
					recordRLDistortion(rlTurns, currentDistortionGain, answerSubject, currentRadius);
					Debug.Log("Turn " + rlTurns + " For gain :" + currentDistortionGain + " radius: " + currentRadius + " subject has detected: " + answerSubject);
				}
				++rlTurns;

				// Send message to client
				sendMessageToClient(currentDistortionGain);

				// wait until the user answer the question
				yield return new WaitUntil(() => distortionGainReceived);
				
			}

			Debug.Log(" rlBlock done!");
			// Wait until get final convergence value
			Debug.Log(this.type + " converged at " + rlTurns + " iterations at value: " + this.currentDistortionGain);
			if (this.type == RLType.UCB.ToString()) {
				rlLogger.LogConvergence(this.currentDistortionGain);
			} else {
				rlLogger.LogConvergence(this.currentDistortionGain, this.currentRadius);
			}
			rlLogger.CloseLog();
			rlBlockIsRunning = false;
			rlBlockDone = true;
		}

		bool isTrialValid()
		{
            //Debug.Log("Trial is Invalid: " + trialIsInvalid);
            return targetReaching.flagEnter && !trialIsInvalid;
            //return targetReaching.flagEnter;

        }

		//This coroutine handle a whole trial ( Question + Task) and reduce the number of parameters
		IEnumerator Trial(float gainDistortion, float radius = -1) 
		{
			trialIsRunning = true;

			trial++;

			//Start the task with taskDuration and drange as global variable 
			if (radius < 0) {
				// UCB1
				StartCoroutine(Task(taskDuration, true, gainDistortion, drange));
			} else {
				// UCB2
				StartCoroutine(Task(taskDuration, true, gainDistortion, drange, false, radius));
			}

			yield return new WaitUntil(() => !taskIsRunning);

			if (isTrialValid()) {
				//Start the recording of the answer
				StartCoroutine(GetAnswer(mode));

				// wait until the user answer the question
				yield return new WaitUntil(() => !getAnswerIsRunning);

			

			}
            else if (!targetReaching.flagEnter && !trialIsInvalid)
            {
				
				messageTargetLimit.setmessageTrialInvalid = true;
                // Wait until we finish
                yield return new WaitUntil(() => !messageTargetLimit.setmessageTrialInvalid);


			

			}

			trialIsRunning = false;
		}

		// Make break after several trials
		IEnumerator Break () {

			breakIsRunning = true;

			//Make all the stuff disappear
			MakeObjectsVisible( false );

			breakMessage.gameObject.SetActive( true );

			//Send a trigger to the parallel port ->Start break
		//	parallelPort.Trigger( 5 );

			yield return new WaitUntil( () => breakMessage.isAnswered );
			blockNumber += 1;

			//Send a trigger to the parallel port ->Start break
			//parallelPort.Trigger( 6 );

			breakIsRunning = false;
		}

		//The end of the experiment
		IEnumerator TheEnd (bool theEnd) {


			theEndIsRunning = true;


			//activate the Waiting Message
			experimentSetting.calibrationManager.waitingMessage.SetActive( true );

			//disable
			// Make every thing visible
			MakeObjectsVisible( false );



			experimentSetting.experimentStop = true;

			//always true so the end never finish
			yield return new WaitUntil( () => !theEnd );

			//yield return 0;
			theEndIsRunning = false;
		}

		/// <summary>
		/// Task performed by the user during the experiment
		/// </summary>
		/// <param Limit maximum of the time duration of the task="taskDurationLimit"></param>
		/// <param State of the coroutine="isRunning"></param>
		/// <param Apply a distortion="applyDistortion"></param>
		/// <param Maximun of the distortion="gain"></param>
		/// <param Range of the distortion attraction="drange"></param>
		/// <param Radius of the hole of the distortion="radius"></param>
		/// <param the radius of the distortion is equal to the radius of the target="isRadiusDependent"></param>
		/// <returns></returns>
		IEnumerator Task ( float taskDurationLimit, bool applyDistortion,float gain, float drange, bool isRadiusDependent=true, float radius=0)
		{
			//The coroutine starts
			taskIsRunning = true;

			trialIsInvalid = false;

          //  if (!runTwoPhaseExperiment)
          //  {

           // }

            // Set trajectory parameters
            //Look at the script Target_Trajectory_NonBiological attached to TargetTask directly in the scene

            // Do not apply distortion
           // changeDistortionSetting(false, gain, drange, isRadiusDependent, radius);

            // wait until the users bring back their hand at the rest position
            yield return new WaitUntil(() => handComeback(armDistance));

		

			//Calibrate the distortion range of the distortion
			if ( runTwoPhaseExperiment )
				drange = Vector3.Distance( intermediateTarget.transform.position, reachingTarget.transform.position );

			// Make every thing visible
			MakeObjectsVisible(true);

			//if(greenAppearAtStart)
			//reachingTarget.SetActive( true );

			//Start Recording
			startOrStop = true;

			//Send a trigger to the parallel port ->Start the task
			parallelPort.Trigger( distortionToTrigger[gain] );
			answerBCIReceived = false;
			trigger = distortionToTrigger[gain];
			yield return new WaitForSeconds( SendTriggerParallel.PulseWidthS );

			currentCondition = gain;

			// Start monitoring the user hand position for trigger
			startOrStopTargetMonitoring = true;
			startOrStopFirstTargetMonitoring = true;

			if (runTwoPhaseExperiment)
            {
                // Wait until first ball is reached
                yield return new WaitUntil(() => intermediateTarget.GetComponentInChildren<TargetReaching>().flagEnter);
            }
			// Make 2nd ball start moving
			if ( !ellipseTrajectory.isMoving )
				// Start target moving
				ellipseTrajectory.isStartedMoving = true;

			//if ( !greenAppearAtStart )
			//	reachingTarget.SetActive( true );

			//Send a trigger to the parallel port ->validate the first target
			parallelPort.Trigger( 26 );
			trigger = 26;
			yield return new WaitForSeconds( SendTriggerParallel.PulseWidthS );

			// Set distortion Parameters
			changeDistortionSetting(applyDistortion, gain, drange, isRadiusDependent, radius);

            // Activate eye tracking
            eyeTarget.setActive(true);

            //Instantiate a coroutine
            Coroutine TimeLimit;
			//start timer to be sur the task does not last too much
			TimeLimit = StartCoroutine( Timer( taskDurationLimit ) );

		

			//Wait until the user performs 1 turn with its hand or the trial lasts more than 5s.
			yield return new WaitUntil( () => targetReaching.flagEnter || timerIsRunning == false  || eyeTarget.invalidateTrial() );
            //yield return new WaitUntil( () => !ellipseTrajectory.isMoving || Timer );

            trialIsInvalid = eyeTarget.invalidateTrial();
            if (trialIsInvalid)
                Debug.Log("current trial is invalid");
            yield return new WaitUntil(() => !messageTargetLimit.setMessageLookAtHand);

            // Stop eyeTracking
            eyeTarget.setActive(false);

            // Switch back to 1st ball
            //SwitchTarget();
            

            // Make every thing visible
            MakeObjectsVisible( false );

			

			// Stop monitoring the user hand position for trigger
			startOrStopTargetMonitoring = false;
			startOrStopFirstTargetMonitoring = false;

			//If the timer is still running need to be stopped
			if ( timerIsRunning )
				//stopped the instantiated coroutine ( if not we will not stop the good coroutine)
				StopCoroutine( TimeLimit );

			//if ( !ellipseTrajectory.isMoving )
				// Stop target moving
			//	ellipseTrajectory.isStartedMoving = true;


			// Disable distortion 
			changeDistortionSetting( false, gain, drange, isRadiusDependent, radius);

			// Make 2nd ball start moving
			if ( ellipseTrajectory.isMoving )
				// Start target moving
				ellipseTrajectory.isStartedMoving = true;


			//The coroutine stops
			taskIsRunning = false;

			//Stop Recording
			startOrStop = false;

		
			if ( trialIsInvalid || (!targetReaching.flagEnter && !trialIsInvalid )) {

				yield return new WaitUntil( () => !waitingIsRunning );
				//If Trial Rejected
				parallelPort.Trigger( 19 );
				trigger = 19;
				// let the time to send the trigger
				yield return new WaitForSeconds( SendTriggerParallel.PulseWidthS );
			} else {
				yield return new WaitUntil( () => !waitingIsRunning );
				//If Trial Accepted
				//Send a trigger to the parallel port ->Stop the task
				parallelPort.Trigger( 18 );
				trigger = 18;
				yield return new WaitForSeconds( SendTriggerParallel.PulseWidthS );
			}
		}

		//get the answer from the user (from EEG, from a button clicked by the user or from the answer to a question)
		IEnumerator GetAnswer( AnswersMode mode)
		{
			getAnswerIsRunning = true;

			if ( mode==AnswersMode.Question) {
				StartCoroutine( Question(true, currentCondition, blockNumber,trial) );
				yield return new WaitUntil( () => (questionIsRunning == false) );
			}
			else if( mode == AnswersMode.EEG ) {
				
				StartCoroutine( Question(false, currentCondition, blockNumber, trial ) );
				yield return new WaitUntil( () => (answerBCIReceived == true) );
			} 
			else if ( mode == AnswersMode.Button ) {
				StartCoroutine( Button() );
				yield return new WaitUntil( () => (buttonIsRunning == false));
			} 
			else {
				yield return 0;
			}

			

			getAnswerIsRunning = false;
		}

		// the question you want to ask at the end of the trial
		IEnumerator Question ( bool updateGetAnswer=true, float condition=0, float blockNumber=0, float trial=0) {

			questionIsRunning = true;

			// Wait until the user performs 1 turn with its hand or the trial lasts more than 5s.
			yield return new WaitUntil( () => taskIsRunning == false );

			questionThreshold.condition = condition;
			questionThreshold.pairNumber = 0;


			questionThreshold.fileNameToRecord = "subjectID_" + ExperimentInterface.Instance.subjectID + "_" + "Block" + blockNumber + "_" + "Trial" + trial + "_" + DateTime.Now.ToString( "yyyyMMdd_HHmmss" );
			questionThreshold.gameObject.SetActive( true );

			//Send a trigger to the parallel port ->Start question
			//parallelPort.Trigger( 3 );

			// wait 0.1s apres etre desactive
			yield return new WaitUntil( () => questionThreshold.isAnswered );

		

			if(updateGetAnswer)
			//Get answer of the subject
			answerSubject = getAnswerQuestion();

			//Send a trigger to the parallel port ->Stop question
			parallelPort.Trigger( answerToTrigger[answerSubject] );
			trigger = answerToTrigger[answerSubject];
			// let the time to send the trigger
			yield return new WaitForSeconds(SendTriggerParallel.PulseWidthS );

			//Debug.Log(answerSubject);

			questionIsRunning = false;



		}

		//Gather the button click from the subject when he notices a Break in Embodiment
		IEnumerator Button() 
		{
			buttonIsRunning = true;

			GetButtonAnswer();

			yield return new WaitUntil( () => taskIsRunning == false );

			buttonIsRunning = false;
		}


		// Count the number of second since started
		IEnumerator Timer (float time=0 ) {

			timerIsRunning = true;
			//float timeTest = Time.time;
			yield return new WaitForSecondsRealtime( time );
			//print(- timeTest + Time.time );

			timerIsRunning = false;


		}

		// Count the number of second since started
		IEnumerator BreakTimer(float time = 600)
		{
		    
		    if (isBreakTime) {
    			isBreakTime = false;
    			yield return new WaitForSecondsRealtime(time);
    			isBreakTime = true;
    			blockNumber += 1;
		    } // Else => a break has already been started
		}

		IEnumerator Waiting() {
			waitingIsRunning = true;
			yield return new WaitForSeconds( SendTriggerParallel.PulseWidthS );
			waitingIsRunning=false;
		}
		/// <summary>
		/// Control the distortion parameters from the Experimenter Interface
		/// </summary>
		/// <param Apply a distorstion="applyDistortion"></param>
		/// <param Maximun of the distortion="gain"></param>
		/// <param Range of the distortion attraction="drange"></param>
		/// <param the radius of the distortion is equal to the radius of the target="isRadiusDependent"></param>
		/// <param Radius of the hole of the distortion="radius"></param>
		/// <param How many times the radius of the target is equal the radius of the distortion="ratioOfTheTarget"></param>
		void changeDistortionSetting ( bool applyDistortion, float gain, float drange, bool isRadiusDependent, float radius=0 , float ratioOfTheTarget=1) {

			distortedHand.applyDistortion = applyDistortion;
			distortedHand.drange = drange;
			distortedHand.gain = gain;
			distortedHand.isRadiusDependent = isRadiusDependent;
			distortedHand.radius =radius;
			distortedHand.ratioOfTheTarget = ratioOfTheTarget;
		
		}

		void GetButtonAnswer() {

			if ( ControllerExperiment.isButtonDown )
				//update the file position
				trigger = 1;

		}

		// Make objects in the scene visible or not
		void MakeObjectsVisible(bool active) {

			experimentSetting.allbObjectsVisible( active );
			reachingTarget.SetActive( active );

			if (runTwoPhaseExperiment)
			intermediateTarget.SetActive( active );
            
			eyeTrackingTarget.SetActive(active);
            //messageTargetLimit.setMessageLookAtHand = false;

            //taskInstruction.SetActive( active );
        }

      /*  void SwitchTarget()
        {
            if (!runTwoPhaseExperiment)
            {
                return;
            }

            reachingTarget.SetActive(false); 
            reachingTarget = twoPhaseReachingTargets[1 - currentActiveTargetIndex];
            reachingTarget.SetActive(true);
        
            currentActiveTargetIndex = (currentActiveTargetIndex + 1) % 2;

            ellipseTrajectory = reachingTarget.GetComponentInChildren<Target_Trajectory_NonBiological>();
            targetReaching = reachingTarget.GetComponentInChildren<TargetReaching>();

        }*/

		// Active message when the users do not comeback with their hand
		bool handComeback ( float distance ) {

			if ( reachingTarget.activeSelf == false ) {

				messageTargetLimit.messageComebackActive = true;
				messageTargetLimit.messageReachingTargetActive = false;

				//if (( (tableDessus.transform.lossyScale.y / 2)* ExperimentInterface.Instance.comebackDistance) < distance ) 
				if ( distance < ExperimentInterface.Instance.comebackDistance ) {

					messageTargetLimit.setMessageComeback = false;

					return true;

				} else {

					messageTargetLimit.setMessageComeback = true;

					return false;

				}
			} else {

				messageTargetLimit.setMessageComeback = false;

				return true;
			}
		}

		//Active message for over reaching the target
		void limitTargetRaching ( float distance ) {

			if ( reachingTarget.activeSelf == true ) {

				messageTargetLimit.messageComebackActive = false;
				messageTargetLimit.messageReachingTargetActive = true;

				if ( distance > ExperimentInterface.Instance.limitDistance ) {

					messageTargetLimit.setMessageReachingTarget = true;

				} else if ( messageTargetLimit.enabled ) {
					messageTargetLimit.setMessageReachingTarget = false;

				}
			}

		}

		void recordRLDistortion(int rlTurns, float gain, bool detected, float radius = -1f)
		{
			if (!rlLogger.logIsOpen) {
				string filename = "subjectID_" + ExperimentInterface.Instance.subjectID + "_" + "Type" + this.type + "_" + "Trial" + trial + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss");
				rlLogger.OpenLog(ExperimentInterface.Instance.subjectID, filename, true, this.type);
			}

			if (this.type == RLType.UCB.ToString()) {
				rlLogger.Logging(rlTurns, gain, detected);
			} else {
				rlLogger.Logging(rlTurns, gain, detected, radius);
			}

		}

		//Record the position of certain bordy if needed
		void recordingCustomPosition () {


			if ( startOrStop ) {

				if ( !ExperimentInterface.Instance.recordDataPositionsSettings.posLog.logIsOpen ) {
					ExperimentInterface.Instance.recordDataPositionsSettings.fileNameToRecord = "subjectID_" + ExperimentInterface.Instance.subjectID + "_" + "Block" + blockNumber + "_" + "Trial" + trial + "_" + DateTime.Now.ToString( "yyyyMMdd_HHmmss" );
					ExperimentInterface.Instance.recordDataPositionsSettings.StartRecording();
					ExperimentInterface.Instance.trialInfo = "subjectID_" + ExperimentInterface.Instance.subjectID + "_Condition " + currentCondition + "_Trial_" + trial + "_Block " + blockNumber;

				} else {

					//order to add data is very important and must correspond to the header set in the Log
					worldPositions.Clear();
					worldPositions.Add( experimentSetting.avatarDistortedComponent.settingsReferences.rightHand.position );
					worldPositions.Add( experimentSetting.avatarDistortedComponent.settingsReferences.rightForearm.position );
					worldPositions.Add( experimentSetting.avatarDistortedComponent.settingsReferences.rightUpperArm.position );
					worldPositions.Add( experimentSetting.avatarOriginalComponent.settingsReferences.rightHand.position );
					worldPositions.Add( experimentSetting.avatarOriginalComponent.settingsReferences.rightForearm.position );
					worldPositions.Add( experimentSetting.avatarOriginalComponent.settingsReferences.rightUpperArm.position );
					worldPositions.Add( reachingTarget.transform.position );
					worldPositions.Add( originalRightObjectHeld.transform.position );
					worldPositions.Add( distortedRightObjectHeld.transform.position );
					worldPositions.Add( experimentSetting.avatarDistortedComponent.settingsReferences.pelvis.position );
					worldPositions.Add( headTracker.transform.position );
					worldPositions.Add( headTracker.transform.rotation.eulerAngles );

					//int trigger1 = 0;
					ExperimentInterface.Instance.recordDataPositionsSettings.posLog.Logging( worldPositions, currentCondition, distortedHand.gain,trigger);//,trigger2,trigger3);
					trigger = 0;


				}

			}

			if ( !startOrStop ) {
				if ( ExperimentInterface.Instance.recordDataPositionsSettings.posLog.logIsOpen )
					ExperimentInterface.Instance.recordDataPositionsSettings.StopRecording();

			}




		}

		// Adjust parameters from the experimenter interface
		void SetParameters() {

			distanceTarget = ExperimentInterface.Instance.distanceTarget;
			distortedHand.applyDistortion = ExperimentInterface.Instance.applyDistortion;
			distortedHand.drange = ExperimentInterface.Instance.drange;
			distortedHand.gain = ExperimentInterface.Instance.gainDynamic;
			distortedHand.isRadiusDependent = ExperimentInterface.Instance.isRadiusDependent;
			distortedHand.radius = ExperimentInterface.Instance.radius;
			distortedHand.ratioOfTheTarget = ExperimentInterface.Instance.ratioOfTheTarget;
			taskDuration = ExperimentInterface.Instance.taskDuration;
			mode = ExperimentInterface.Instance.Mode;

		}

		//Get the answer from a question
		bool getAnswerQuestion() {

			if ( questionThreshold.answersText[0] == "Yes" )
				return true; // detect
			else
				return false; //  did not detected

		}

		//Load my lists of distortion value into list I will feed to muy block
		// parse CSV file with headder and block
		public bool LoadConfig ( string fileName, ref List<List<float>> conditionsList , ref List<List<float>> conditionsList1 ) {

			// open CSV file
			Debug.Log( "conditionCSV: opening file " + fileName );

			CsvFileReader reader = new CsvFileReader( fileName );

			string fileContent = reader.ReadToEnd();
			reader.Close();


			using ( StringReader strReader = new StringReader( fileContent ) ) {

				string line;
				int sizeOfFirstBlock=7;

				// skip header
				line = strReader.ReadLine();
				line = strReader.ReadLine();

				int offset = 0;
				while ( (line = strReader.ReadLine()) != null ) {


					string[] mocapLineArray = line.Split( new string[] { "," }, StringSplitOptions.None );

					List<float> conditionsLine = new List<float>();
					List<float> conditionsLine1 = new List<float>();


					for ( int i = 1; i < sizeOfFirstBlock; i++ ) {

							float distorEntry = float.Parse( mocapLineArray[i + offset] );

							conditionsLine.Add( distorEntry );

						}


					for ( int i = sizeOfFirstBlock; i < mocapLineArray.Length; i++ ) {

						float distorEntry = float.Parse( mocapLineArray[i + offset] );

						conditionsLine1.Add( distorEntry );

					}

					conditionsList.Add( conditionsLine );
					conditionsList1.Add( conditionsLine1 );
				}
			}


			Debug.Log( "conditionCSV: " + fileName + " loaded!" );
			return true;
		}

        public void messageDispatcher()
        {
            string message = "";
            bool hasMessage = false;
            lock (ReadSocket.instance.messageLock)
            {
                if (ReadSocket.instance.messageReceived)
                {
                    hasMessage = true;
                    message = ReadSocket.instance.clientMessage;
                    ReadSocket.instance.messageReceived = false;
                }
            }

            if (!hasMessage)
                return;

            if (message.Length > 10)
            {
                messageReceived(message);
            } else
            {
                messageReceivedFromBCI(message);
            }
    
        }

        /*
		* Receive message from client
		*/
        public void messageReceived(string message)
		{
			DistortionValue msg = JsonUtility.FromJson<DistortionValue>(message);

			Debug.Log("Received gain: " + msg.gain + " radius: " + msg.radius);
			RLconverged = msg.convergence;
			this.currentDistortionGain = msg.gain;
			this.currentRadius = msg.radius / 100; // put in meter
			distortionGainReceived = true;
		}

		public void messageReceivedFromBCI( string message )
		{
			if ( message == "t" )
				this.answerSubject = true;
			else if ( message == "f" )
				this.answerSubject = false;

			answerBCIReceived = true;
		}

		/*
		 * Send answer to client
		 */
		public void sendMessageToClient(float gain)
		{
			distortionGainReceived = false;
			SubjectAnswer answer = new SubjectAnswer(ExperimentInterface.Instance.subjectID, gain, answerSubject, this.type);
			string json = JsonUtility.ToJson(answer);
			Debug.Log("Sending message : " + json + " to client from RL");
			ReadSocket.instance.SendMessageToClient(json);
		}




		//Send trigger when the subject enter inside the target
		public void TargetState() {

			if( startOrStopTargetMonitoring ) {

				if(targetReaching.targetInside && !flag1 && !waitingIsRunning ) {

					//Send a trigger to the parallel port ->Just enter inside the target
					parallelPort.Trigger( 24 );
					trigger = 24;

					StartCoroutine(Waiting());

					flag1 = true;
				}
				else if( !targetReaching.targetInside && flag1 && !waitingIsRunning ) {

					//Send a trigger to the parallel port ->Just leave the target
					parallelPort.Trigger( 25);
					trigger = 25;

					StartCoroutine( Waiting() );

					flag1 = false;
				}
			}

		}

		public void FirstTargetState () {

			if ( startOrStopFirstTargetMonitoring ) {

				if ( intermedTargetReaching.targetInside && !flag2 && !waitingIsRunning ) {

					if( intermediateTarget.GetComponentInChildren<TargetReaching>().flagEnter ) {
						//Send a trigger to the parallel port ->Just enter inside the target
						parallelPort.Trigger( 29 );
						trigger = 29;

						StartCoroutine( Waiting() );

					} else {
						//Send a trigger to the parallel port ->Just enter inside the target
						parallelPort.Trigger( 27 );
						trigger = 27;

						StartCoroutine( Waiting() );
					}
				

					flag2 = true;
				} else if ( !intermedTargetReaching.targetInside && flag2 && !waitingIsRunning ) {

					if ( intermediateTarget.GetComponentInChildren<TargetReaching>().flagEnter ) {
						//Send a trigger to the parallel port ->Just exit the target
						parallelPort.Trigger( 30 );
						trigger = 30;

						StartCoroutine( Waiting() );

					} else {
						//Send a trigger to the parallel port ->Just exit the target
						parallelPort.Trigger( 28 );
						trigger = 28;

						StartCoroutine( Waiting() );
					}


					flag2 = false;
				}

			
			}

		}
	}



	[Serializable]
	public class SubjectAnswer
	{
		public int subjectId;
		public float distortion;
		public bool detected;
		public string type;
		public SubjectAnswer(int subjectId, float distortion, bool detected, string type)
		{
			this.subjectId = subjectId;
			this.distortion = distortion;
			this.detected = detected;
			this.type = type;
		}
	}


	[Serializable]
	public class DistortionValue
	{
		public bool convergence;
		public float gain;
		public float radius;
		public DistortionValue(float distortion, float radius=0f, bool convergence=false)
		{
			this.convergence = convergence;
			this.gain = distortion;
			this.radius = radius;
		}
	}




}
