using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ReadWriteCsv;

public class StaircaseWrapper {
    public bool complete {get; private set;}
	int totalTurns;
	public int totalTrials {get; private set;}

	List<Staircase> staircases;
	System.Random rnd;
	Staircase currentStaircase;
	int iteration;


    public StaircaseWrapper() {
		staircases = new List<Staircase>();

		// Staircase params: int id, float initialGain, bool initialClimbing, float initialStepSize=0.5f, float minimalStepSize=0.25f, 
		// int maxIterations=20, float maximalGain=4f, float minimalGain=0f,  float reductionFactor = 0.5f
		Staircase staircase1 = new Staircase(1, 0, 0, true);
		Staircase staircase2 = new Staircase(2, 4, 1, true);
		Staircase staircase3 = new Staircase(3, 15, 7, false); 
		Staircase staircase4 = new Staircase(4, 16, 10, false);

		staircases.Add(staircase1);
		staircases.Add(staircase2);
		staircases.Add(staircase3);
		staircases.Add(staircase4);

        Reset();
	}


	public float Step()
	{
		//int index = rnd.Next(0, 4); // between 0-3
		if (iteration % 4 == 0) {
			ShuffleStaircases();
		}

		int index = iteration % 4;
		++iteration;
		currentStaircase = staircases[index];
		float gain = currentStaircase.Step();
		return gain;
	}

	public void UpdateDetection(bool detected)
	{
		this.currentStaircase.UpdateDetection(detected);
	}


	public void Reset()
	{
		foreach(Staircase s in staircases) {
			s.ResetStaircase();
		}

		iteration = 0;
		rnd = new System.Random();
	}

	public bool hasConverged()
	{
		bool converged = true;
		foreach (Staircase s in staircases) {
			converged &= s.hasConverged();
		}

		return converged;
	}

	private void ShuffleStaircases()
	{
		List<Staircase> newStaircases = new List<Staircase>();
		while (this.staircases.Count > 0) {
			int index = rnd.Next(0, staircases.Count);
			newStaircases.Add(staircases[index]);
			staircases.RemoveAt(index);
		}

		this.staircases = newStaircases;

		string output = "Shuffled: ";
		foreach (Staircase s in staircases) {
			output += "Staircase " + s.id + ", ";
		}

		Debug.Log(output);
	}

	public void StopStaircases()
	{
		foreach (Staircase s in staircases) {
			s.StopStaircase();
		}

		this.Reset();
	}
}