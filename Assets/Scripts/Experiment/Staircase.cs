using UnityEngine;
using System.Collections.Generic;
using ReadWriteCsv;
using System;


class Staircase {

    // initial signal value
	protected float initialGain;
	public float currentGain;
	private readonly float maximalGain;
	private readonly float minimalGain;
    private readonly int initialIdx;

    // direction, if climbing is True, increase value
    protected bool climbing; 
    protected bool initialClimbing; 

    protected float reductionFactor; // diminish a step by x%
	private readonly float initialReductionFactor;
    protected float currentStepSize;
    protected float initialStepSize;
    protected float minimalStepSize;

	private int turnsChanged;
	private readonly int maxTurnsChanged;
	private int iteration;
	private readonly int maxIterations;

	private bool previousClimbing;
	private List<float> last4Distortions;
    private List<float> possibleValues;

    private int currentIdx;

    public int id;

	// CSVWriter
	readonly string filename;
	LogStaircase logger;

    public Staircase(int id, int initialIdx, float initialGain, bool initialClimbing, float initialStepSize=0.5f, float minimalStepSize=0.25f, int maxIterations=20, float maximalGain=10f, float minimalGain=0f,  float reductionFactor = 0.5f) {
		this.id = id;
		this.initialGain = initialGain;
        this.initialIdx = initialIdx;
        this.currentIdx = initialIdx;
        this.initialClimbing = initialClimbing;
        this.previousClimbing = initialClimbing;
        this.initialReductionFactor = reductionFactor;
        this.initialStepSize = initialStepSize;
        this.minimalStepSize = minimalStepSize;
		this.maximalGain = maximalGain;
		this.minimalGain = minimalGain;
		this.maxIterations = maxIterations;
		this.maxTurnsChanged = 7;
        this.filename = "subjectID_" + ExperimentInterface.Instance.subjectID + "_" + "Type_Staircase_" + id + "_"; // + DateTime.Now.ToString("yyyyMMdd_HHmmss");

		this.logger = new LogStaircase();

		last4Distortions = new List<float>();

        possibleValues = new List<float>() { 0f, 0.25f, 0.5f, 0.75f, 1f, 1.25f, 1.5f, 1.75f, 2f, 2.25f, 2.5f, 2.75f, 3f, 4f, 5f, 7f, 10f };

		//for (float val = 0; val < 4; val += 0.25f)
		//{
		//    possibleValues.Add(val);
		//}

		this.logger.OpenLog(ExperimentInterface.Instance.subjectID);

        // Take initial values
        ResetStaircase();
    }

    public float Step() {

        if (iteration == 0) {
			Debug.Log("Staircase " + id + " iteration " + iteration + " distortion: " + this.currentGain);
			return initialGain;
		}

		if (hasConverged()) {
			// Return random value
			int idx = UnityEngine.Random.Range(0, possibleValues.Count);
            this.currentGain = possibleValues[idx];
            Debug.Log("Staircase " + id + "has converged; Returning random distortion: " + this.currentGain);
			return this.currentGain;
		}

        //this.currentGain += this.climbing ? this.currentStepSize : -this.currentStepSize;
        this.currentIdx += this.climbing ? 1 : -1;
        this.currentIdx = this.currentIdx < 0 ? 0 : this.currentIdx;
        this.currentIdx = this.currentIdx > this.possibleValues.Count - 1 ? this.possibleValues.Count - 1 : this.currentIdx;

        this.currentGain = this.possibleValues[this.currentIdx];

		//if (currentGain > maximalGain)
		//	this.currentGain = maximalGain;
		//else if (currentGain < minimalGain)
		//	this.currentGain = minimalGain;

		//Debug.Log("Staircase " + id + " iteration " + iteration + " distortion: " + this.currentGain);


		return this.currentGain;
    }

	public void UpdateDetection(bool detected)
	{

        Debug.Log("Staircase " + id + " iteration " + iteration + " distortion: " + this.currentGain + " detected: " + detected + " climbing: " + climbing);

        string prevFilename = filename + "Iteration_" + (iteration - 1) + "_" + DateTime.Now.ToString("yyyyMMdd");
        string currentFilename = filename + "Iteration_" + iteration + "_" + DateTime.Now.ToString("yyyyMMdd");

        // Reverse direction
        bool isTurn = false;
        this.climbing = !detected;
        // First time set initial detection
        //if (iteration == 0) {
            //this.logger.Logging(prevFilename, currentFilename, iteration, this.currentGain, detected, isTurn);
			//this.previousClimbing = this.climbing;
			//++iteration;
        //}

        // set direction
        Debug.Log("Staircase " + id + " iteration " + iteration + " climbing: " + climbing);

        if ((this.climbing != this.previousClimbing) && iteration > 0) {
            isTurn = true;

			++this.turnsChanged;

			// Record last 4 turn changed
			if (this.turnsChanged >= this.maxTurnsChanged - 3 && this.turnsChanged <= this.maxTurnsChanged) {
				this.last4Distortions.Add(this.currentGain);
			}

			//ReduceStepSize();
			Debug.Log("Staircase " + id + " changes direction " + turnsChanged + " times; Climbing: " + this.climbing + " Stepsize: " + this.currentStepSize);
		}

        this.previousClimbing = this.climbing;

        this.logger.Logging(prevFilename, currentFilename, iteration, this.currentGain, detected, isTurn);

        //this.logger.CloseLog();

       ++iteration;
	}

	public bool hasConverged()
	{
		bool converged = (iteration >= maxIterations) || (turnsChanged >= maxTurnsChanged);
        if (converged)
        {
            getFinalConvergedValue();
            logger.CloseLog();
        }

        return converged;
	}

	public float getFinalConvergedValue()
	{
        string prevFilename = filename + "Iteration_" + (iteration - 1) + "_" + DateTime.Now.ToString("yyyyMMdd");
        string currentFilename = filename + "Iteration_" + iteration + "_" + DateTime.Now.ToString("yyyyMMdd");

        if (turnsChanged >= maxTurnsChanged) {
			float mean = 0;
			foreach (float f in this.last4Distortions) {
				mean += f;
			}

			mean /= this.last4Distortions.Count;
			Debug.Log("Staircase " + id + " converged at value: " + mean);
			this.logger.LogConvergence(prevFilename, currentFilename, mean, true);
            return mean;
		} else {
			Debug.Log("Staircase " + id + " has not converged!");
			this.logger.LogConvergence(prevFilename, currentFilename, -1, false);
            return -1;
		}

	}

    private void ReduceStepSize() {
        this.currentStepSize *= this.reductionFactor;
        if (currentStepSize < this.minimalStepSize) {
            this.currentStepSize = this.minimalStepSize;
        }
    }

    public void ResetStaircase() {
        this.currentGain = this.initialGain;
        this.currentIdx = this.initialIdx;
        this.currentStepSize = this.initialStepSize;
        this.climbing = initialClimbing;
		this.turnsChanged = 0;
		this.iteration = 0;
		this.reductionFactor = initialReductionFactor;
		this.last4Distortions.Clear();

		if ( this.currentGain != this.possibleValues[this.initialIdx] ) {
			this.currentGain = this.possibleValues[this.initialIdx];
		}
	}


	public void StopStaircase()
	{
        if (!hasConverged())
        {
            this.getFinalConvergedValue();
            this.logger.CloseLog();
        }
        this.ResetStaircase();
	}

}