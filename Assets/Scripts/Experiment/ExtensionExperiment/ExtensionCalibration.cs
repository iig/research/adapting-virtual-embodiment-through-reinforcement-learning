﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

public class ExtensionCalibration : MonoBehaviour
{
	public Character_Generator_Settings AvatarDistorted;
	public Character_Generator_Settings AvatarOriginal;
	public GameObject rightCylinderHeldDistorted;
	public EgocentricCoordinatesFormalism.BodyPart[] sourceIgnoredBodyParts, targetIgnoredBodyParts;
	public IkGoalDistorter rightHandDistortion;
	public KeyCode calibrationButton;
	public TargetReaching targetReachingComponent;
	public bool isButton;
//	[Range( 0, 0.1f )]
//	public float offsetCylinder=1;

	[HideInInspector]
	public bool isCalibrated;
	[HideInInspector]
	public bool isCalibrated2;
	[HideInInspector]
	public bool isInstanciated;
	[HideInInspector]
	public Vector3 cylinderPosition;

	//DATA SAVE
	public EgocentricCoordinatesFormalism.EgocentricCoordinates egocentricCoordinatesSaved;
	Vector3[] Vextensions;
	

	// Start is called before the first frame update
	void Start()
    {
		IkGoalDistorter.InitializeEgocentricCorrdinates( AvatarDistorted, AvatarOriginal, sourceIgnoredBodyParts, targetIgnoredBodyParts, out egocentricCoordinatesSaved );
		Vextensions = new Vector3[AvatarOriginal.settingsReferences.root.GetComponentsInChildren<EgocentricCoordinatesFormalism.BodyPart>().Length];
	}

    // Update is called once per frame
    void Update()
    {
		if( CalibrationTable.isLoad && !isInstanciated) {

			targetReachingComponent.transform.position = cylinderPosition;

			isInstanciated = true;
		}

		if (  ((Input.GetKeyDown( calibrationButton) && isButton ) || (targetReachingComponent.flagEnter && !isButton)) && CalibrationTable.isLoad && !isCalibrated) {

			cylinderPosition = new Vector3( rightCylinderHeldDistorted.transform.position.x, rightCylinderHeldDistorted.transform.position.y, rightCylinderHeldDistorted.transform.position.z );
			Array.Copy( rightHandDistortion.egocentricCoordinates.coordinates, egocentricCoordinatesSaved.coordinates, rightHandDistortion.egocentricCoordinates.coordinates.Length) ;
			rightHandDistortion.egocentricCoordinates.CopyVvectors( Vextensions );

			isCalibrated = true;
		}

		if ( !isCalibrated2 && Calibration_Manager.GetSaveStatus() ) {

			cylinderPosition =new Vector3( rightCylinderHeldDistorted.transform.position.x, rightCylinderHeldDistorted.transform.position.y, rightCylinderHeldDistorted.transform.position.z);
			//cylinderPosition = new Vector3( rightCylinderHeldDistorted.transform.position.x, rightCylinderHeldDistorted.transform.position.y, rightCylinderHeldDistorted.transform.position.z * offsetCylinder );
				isCalibrated2 = true;
		}

	}

	/***Compute the target position for the user arm extended***/
	public Vector3 ComputeTargetPositionForExtendedArm (IIG.Distortions.DistortionFunction distortionType, float offset, float gain, float distanceRate,float offsetCylinder) 
	{
		
		//Reset the V vectors with the one when the arm is extended
		egocentricCoordinatesSaved.ReplaceVvectors( Vextensions );

		// Compute the distortion by modifying the distance between the body part on the joint in the case the subject is extended
		egocentricCoordinatesSaved.Update_To_Vectors(distortionType , offset, gain );

		// Compute the target position from the new egocentric coordinate
		return new Vector3( egocentricCoordinatesSaved.RetrievePosition().x, egocentricCoordinatesSaved.RetrievePosition().y, egocentricCoordinatesSaved.RetrievePosition().z*distanceRate - offsetCylinder);

	}
	
}
 

/*

//Compute the target position for the user arm extended (case 4)//
//Reset the V vectors with the one when the arm is extended
rightHandDistortion.egocentricCoordinates.ReplaceVvectors(Vextensions );

		// Compute the distortion by modifying the distance between the body part on the joint in the case the subject is extended
		egocentricCoordinatesSaved.Update_To_Vectors(IIG.Distortions.GetDistortionFunction(rightHandDistortion.distortionType ), rightHandDistortion.offset, rightHandDistortion.gain );

		// Compute the target position from the new egocentric coordinate
		targetPosition = egocentricCoordinatesSaved.RetrievePosition();


		//Compute the target position for the avatar arm extended (case 2 and 6)//
		//Reset the V vectors with the one when the arm is extended
		rightHandDistortion.egocentricCoordinates.ReplaceVvectors(Vextensions );

		// Compute the distortion by modifying the distance between the body part on the joint in the case the subject is extended
		egocentricCoordinatesSaved.Update_To_Vectors(IIG.Distortions.GetDistortionFunction(rightHandDistortion.distortionType ),0, 0 );

		// Compute the target position from the new egocentric coordinate
		targetPosition = egocentricCoordinatesSaved.RetrievePosition();


		//Compute the target position for the user arm extended ( case 1, 3 and 5)//
		//Reset the V vectors with the one when the arm is extended
		rightHandDistortion.egocentricCoordinates.ReplaceVvectors(Vextensions );

		// Compute the distortion by modifying the distance between the body part on the joint in the case the subject is extended
		egocentricCoordinatesSaved.Update_To_Vectors(IIG.Distortions.GetDistortionFunction(rightHandDistortion.distortionType ), 0, 0 );

		// Compute the target position from the new egocentric coordinate 
		targetPosition = egocentricCoordinatesSaved.RetrievePosition() *0.75f;
	*/