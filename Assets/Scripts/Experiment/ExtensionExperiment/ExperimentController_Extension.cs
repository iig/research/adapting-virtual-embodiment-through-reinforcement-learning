﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using UnityEngine;
using ReadWriteCsv;


public class ExperimentController_Extension : MonoBehaviour
{

	public ExperimentSetting experimentSetting;
	public ExtensionCalibration extensionCalibration;
	public GameObject originalCylinder;
	//public float comebackDistance=0.4f;
	public GameObject reachingCylinder;
	public ErrPExperiment.SliderSingleQuestionManager questionOnwership;
	public ErrPExperiment.SliderSingleQuestionManager questionThreshold;
	public ErrPExperiment.SliderSingleQuestionManager questionThresholdWithTwoChoices;
	public QuestionGUI_Generic theQuestionThresholdOnly;
	//public ErrPExperiment.SliderSingleQuestionManager questionManagerComponentOne;
	public IK_Manager IKManager;
	public enum blockTypes {ownership,threshold};
	

	public GameObject rightObjectHeld;
	public GameObject leftObjectHeld;
	public GameObject table;
	public GameObject carpet;
	public GameObject chair;
	public GameObject taskInstruction;
	public SingleQuestionManagerButton breakMessage;
	public GameObject tableDessus;

	


	List<List<float>> conditionsList = new List<List<float>>();
	List<float> conditionsTrainingList= new List<float>() {1, 3, 1, 5,0 };
	List<float> conditionsCalibList = new List<float>();
	float[] conditionsCalibArray = { 2, 7 };
	List<List<float>> blockOrder= new List<List<float>>();
	List<Vector3> worldPositions = new List<Vector3>();
	ErrPExperiment.SliderSingleQuestionManager questionManagerComponentSeveral;
	SetCanvasActive messageTargetLimit;
	GameObject headTracker;
	

	float handToTargetDistance = 0;
	float armDistance = 99999;
	int blockNumber = 0;
	int trial = 0;
	float currentCondition=9999;
	float previousCondition = 9999;
	float D0 = 0;
	int nbrOfCase = 0;
	//float limitOffset;


	bool blockIsRunning;
	bool taskIsRunning;
	bool questionIsRunning;
	bool breakIsRunning;
	bool theEndIsRunning;

	bool startOrStop;
	bool targetValidated;
	bool theEnd = true;
	bool blockDone;
	bool trainingDone;
	bool calibDone;
	bool isNotExtended;
	




	// Start is called before the first frame update
	void Awake()
    {
		if ( ExperimentInterface.Instance.protocole == ExperimentInterface.Protocoles.Prototype )
			this.enabled = false;

		if( ExperimentInterface.Instance.blockType == blockTypes.ownership ) {
			//Load the config file with all the condition
			LoadConfig( ExperimentInterface.Instance.configPath + ExperimentInterface.Instance.configConditionOwnershipFileName, ref conditionsList );
			questionManagerComponentSeveral= questionOnwership;
		} else {
			
			
			if(ExperimentInterface.Instance.isThresholdWithComparaison) {
				//Load the config file with all the condition
				LoadConfig( ExperimentInterface.Instance.configPath + ExperimentInterface.Instance.configConditionThresholdTwoChoicesFileName, ref conditionsList );
				questionManagerComponentSeveral = questionThresholdWithTwoChoices;
			}
			else {
				//Load the config file with all the condition
				LoadConfig( ExperimentInterface.Instance.configPath + ExperimentInterface.Instance.configConditionThresholdFileName, ref conditionsList );
				questionManagerComponentSeveral = questionThreshold;
			}
		
		}

	}

	private void Start () {

		messageTargetLimit = Camera.main.GetComponentInChildren<SetCanvasActive>();
		headTracker = GameObject.Find( "HEAD" );
	}

	// Update is called once per frame
	void Update()
    {

		if ( experimentSetting.start && extensionCalibration.isCalibrated ) {

			if ( ExperimentInterface.Instance.Training && !trainingDone ) {
				trial = 0;
				StartCoroutine( Block( conditionsTrainingList,true, false) );
				trainingDone = true;
			}

			if ( !ExperimentInterface.Instance.infinitTraining && !calibDone && !blockIsRunning ) {
				shuffleList();
				trial =776;
				StartCoroutine( Block( conditionsCalibList, false, true ) );
				calibDone = true;
			}


			if ( !ExperimentInterface.Instance.infinitTraining && !blockDone && !blockIsRunning ) {
				trial =0;
				StartCoroutine( Block( conditionsList[ExperimentInterface.Instance.subjectID],false, false ) );
				blockDone = true;
			}
				

			if ( !ExperimentInterface.Instance.infinitTraining && !blockIsRunning && !theEndIsRunning)
				StartCoroutine( TheEnd() );
		} else {
			if ( table.activeSelf )
				table.SetActive( false );

			return;
		}
			




		//handToTargetDistance = Mathf.Abs( reachingCylinder.transform.position.z - extensionCalibration.rightCylinderHeldDistorted.transform.position.z) ;
		handToTargetDistance = -extensionCalibration.rightCylinderHeldDistorted.transform.position.z + reachingCylinder.transform.position.z;
		limitTargetRaching( handToTargetDistance );

		//Compute the distance between the cylinder held and the cylinder to reach
		//armDistance = Mathf.Abs( tableDessus.transform.position.z - originalCylinder.transform.position.z); 
		armDistance = Mathf.Abs(chair.transform.position.z-originalCylinder.transform.position.z);


		//record the positionfor data analysis
		recordingCustomPosition();




			

	}

	//All the corp of the experiment after Calibration
	IEnumerator Block ( List<float> conditionsList, bool isTrainingBlock, bool isCalib ) {

		blockIsRunning = true;


		foreach (float condition in  conditionsList) {


			if( condition == 0) {
				
				blockNumber++;
				trial = 0;

				if ( !breakIsRunning )
				StartCoroutine( Break());

				yield return new WaitUntil( () => !breakIsRunning );

		
			}
			else {
					
					StartCoroutine( Task( condition, isTrainingBlock,isCalib ) );

					yield return new WaitUntil( () => !taskIsRunning );

				if(nbrOfCase==2) {

					StartCoroutine( Question( 1, condition, blockNumber, trial, isCalib ) );

					yield return new WaitUntil( () => !questionIsRunning );

					nbrOfCase = 0;
				}
			
			
			}

		}

		blockIsRunning = false;
	}


	// the task you want to perform during a trial
	IEnumerator Task ( float experimentCase, bool isTrainingBlock,bool isCalib ) {

		taskIsRunning = true;



		currentCondition = experimentCase;


		switch ( experimentCase ) {

			case 1:


				IKManager.isShoulderTracked = true;

				extensionCalibration.rightHandDistortion.gain = 0;
				extensionCalibration.rightHandDistortion.offset = 0;

				if ( !isTrainingBlock )
					D0 = ExperimentInterface.Instance.D0;
				else
					D0 = ExperimentInterface.Instance.D0Training;

				reachingCylinder.transform.position = extensionCalibration.ComputeTargetPositionForExtendedArm( IIG.Distortions.GetDistortionFunction( extensionCalibration.rightHandDistortion.distortionType ), extensionCalibration.rightHandDistortion.offset, extensionCalibration.rightHandDistortion.gain, D0, 0 );

				handComeback( armDistance );
				yield return new WaitUntil( () => handComeback( armDistance ) );

				//Start Recording
				startOrStop = true;

				//Make all the stuff appear
				allObjectsVisible( true );
				reachingCylinder.SetActive( true );


				//targetValidated = false;


				break;
			case 2:
				//trial++;


				IKManager.isShoulderTracked = true;

				extensionCalibration.rightHandDistortion.gain = 0;

				if ( !isNotExtended )
					extensionCalibration.rightHandDistortion.offset = ExperimentInterface.Instance.offsetExtra;
				else
					extensionCalibration.rightHandDistortion.offset = -ExperimentInterface.Instance.offsetExtra;



				//D0 = 1;

				//Distance d0
				reachingCylinder.transform.position = extensionCalibration.ComputeTargetPositionForExtendedArm( IIG.Distortions.GetDistortionFunction( extensionCalibration.rightHandDistortion.distortionType ), extensionCalibration.rightHandDistortion.offset, 0, 1, ExperimentInterface.Instance.offsetCylinderCase2 );
				//reachingCylinder.transform.position = extensionCalibration.cylinderPosition+new Vector3(0,0,ExperimentInterface.Instance.offsetCylinderCase2);//extensionCalibration.ComputeTargetPositionForExtendedArm( IIG.Distortions.GetDistortionFunction( extensionCalibration.rightHandDistortion.distortionType ),0, 0, 1,0 );

				handComeback( armDistance );
				yield return new WaitUntil( () => handComeback( armDistance ) );

				//Start Recording
				startOrStop = true;

				//Make all the stuff appear
				allObjectsVisible( true );
				reachingCylinder.SetActive( true );

				//targetValidated = false;

				break;
			case 3:

			//	trial++;


				IKManager.isShoulderTracked = false;

				if ( !isTrainingBlock ) {

					extensionCalibration.rightHandDistortion.gain = -ExperimentInterface.Instance.gain;
					extensionCalibration.rightHandDistortion.offset = -ExperimentInterface.Instance.offset;
					D0 = ExperimentInterface.Instance.D0;

				} else {

					extensionCalibration.rightHandDistortion.gain = -ExperimentInterface.Instance.gainTraining;
					extensionCalibration.rightHandDistortion.offset = -ExperimentInterface.Instance.offsetTraining;
					D0 = ExperimentInterface.Instance.D0Training;

				}


				reachingCylinder.transform.position = extensionCalibration.ComputeTargetPositionForExtendedArm( IIG.Distortions.GetDistortionFunction( extensionCalibration.rightHandDistortion.distortionType ), 0, extensionCalibration.rightHandDistortion.gain, D0, 0 );
				handComeback( armDistance );
				yield return new WaitUntil( () => handComeback( armDistance ) );

				//Start Recording
				startOrStop = true;

				//Make all the stuff appear
				allObjectsVisible( true );
				reachingCylinder.SetActive( true );

				//targetValidated = false;
				break;
			case 4:

			//	trial++;


				IKManager.isShoulderTracked = false;

				if ( !isTrainingBlock ) {

					extensionCalibration.rightHandDistortion.gain = -ExperimentInterface.Instance.gain;
					extensionCalibration.rightHandDistortion.offset = -ExperimentInterface.Instance.offset;


				} else {

					extensionCalibration.rightHandDistortion.gain = -ExperimentInterface.Instance.gainTraining;
					extensionCalibration.rightHandDistortion.offset = -ExperimentInterface.Instance.offsetTraining;


				}


				reachingCylinder.transform.position = extensionCalibration.ComputeTargetPositionForExtendedArm( IIG.Distortions.GetDistortionFunction( extensionCalibration.rightHandDistortion.distortionType ), extensionCalibration.rightHandDistortion.offset, extensionCalibration.rightHandDistortion.gain, 1, ExperimentInterface.Instance.offsetCylinderCase4 );
				handComeback( armDistance );
				yield return new WaitUntil( () => handComeback( armDistance ) );

				//Start Recording
				startOrStop = true;

				//Make all the stuff appear
				allObjectsVisible( true );
				reachingCylinder.SetActive( true );

				//targetValidated = false;

				break;
			case 5:

			//	trial++;


				IKManager.isShoulderTracked = false;

				if ( !isTrainingBlock ) {

					extensionCalibration.rightHandDistortion.gain = ExperimentInterface.Instance.gain;
					extensionCalibration.rightHandDistortion.offset = ExperimentInterface.Instance.offset;
					D0 = ExperimentInterface.Instance.D0;

				} else {

					extensionCalibration.rightHandDistortion.gain = ExperimentInterface.Instance.gainTraining;
					extensionCalibration.rightHandDistortion.offset = ExperimentInterface.Instance.offsetTraining;
					D0 = ExperimentInterface.Instance.D0Training;

				}
				//D0 = 0.75f;

				//Distance d0
				reachingCylinder.transform.position = extensionCalibration.ComputeTargetPositionForExtendedArm( IIG.Distortions.GetDistortionFunction( extensionCalibration.rightHandDistortion.distortionType ), 0, extensionCalibration.rightHandDistortion.gain, D0, 0 );
				handComeback( armDistance );
				yield return new WaitUntil( () => handComeback( armDistance ) );

				//Start Recording
				startOrStop = true;

				//Make all the stuff appear
				allObjectsVisible( true );
				reachingCylinder.SetActive( true );

				//targetValidated = false;

				break;
			case 6:
			//	trial++;


				IKManager.isShoulderTracked = false;

				if ( !isTrainingBlock ) {

					extensionCalibration.rightHandDistortion.gain = ExperimentInterface.Instance.gain;
					extensionCalibration.rightHandDistortion.offset = ExperimentInterface.Instance.offset;

				} else {

					extensionCalibration.rightHandDistortion.gain = ExperimentInterface.Instance.gainTraining;
					extensionCalibration.rightHandDistortion.offset = ExperimentInterface.Instance.offsetTraining;

				}

				//D0 = 1;

				//Distance d0
				if( isNotExtended)
				reachingCylinder.transform.position = //extensionCalibration.cylinderPosition+new Vector3(0,0,-ExperimentInterface.Instance.offsetCylinderCase6);
				extensionCalibration.ComputeTargetPositionForExtendedArm( IIG.Distortions.GetDistortionFunction( extensionCalibration.rightHandDistortion.distortionType ), -ExperimentInterface.Instance.offsetExtra, 0, 1, ExperimentInterface.Instance.offsetCylinderCase2 );
				//extensionCalibration.ComputeTargetPositionForExtendedArm( IIG.Distortions.GetDistortionFunction( extensionCalibration.rightHandDistortion.distortionType ), ExperimentInterface.Instance.offset, extensionCalibration.rightHandDistortion.gain, 1, 0 );
				else
					reachingCylinder.transform.position = //extensionCalibration.cylinderPosition+new Vector3(0,0,-ExperimentInterface.Instance.offsetCylinderCase6);
					extensionCalibration.ComputeTargetPositionForExtendedArm( IIG.Distortions.GetDistortionFunction( extensionCalibration.rightHandDistortion.distortionType ), ExperimentInterface.Instance.offsetExtra, 0, 1, ExperimentInterface.Instance.offsetCylinderCase2 );
				
				handComeback( armDistance );
				yield return new WaitUntil( () => handComeback( armDistance ) );

				//Start Recording
				startOrStop = true;

				//Make all the stuff appear
				allObjectsVisible( true );
				reachingCylinder.SetActive( true );

				//targetValidated = false;

				break;
			case 7:
				//trial++;


				IKManager.isShoulderTracked = true;

				extensionCalibration.rightHandDistortion.gain = 0;

				extensionCalibration.rightHandDistortion.offset = -ExperimentInterface.Instance.offsetExtra;



				//D0 = 1;

				//Distance d0
				reachingCylinder.transform.position = extensionCalibration.ComputeTargetPositionForExtendedArm( IIG.Distortions.GetDistortionFunction( extensionCalibration.rightHandDistortion.distortionType ), 0, 0, 1, ExperimentInterface.Instance.offsetCylinderCase2 );
				handComeback( armDistance );
				yield return new WaitUntil( () => handComeback( armDistance ) );

				//Start Recording
				startOrStop = true;

				//Make all the stuff appear
				allObjectsVisible( true );
				reachingCylinder.SetActive( true );

				//targetValidated = false;

				break;

		}

		//}
		//yield return new WaitForSeconds( 10 );
		if ( (ExperimentInterface.Instance.blockType == blockTypes.ownership || !ExperimentInterface.Instance.isThresholdWithComparaison) && !isCalib )
			nbrOfCase = 1;


		if ( nbrOfCase ==1 ) 			
			trial++;
		
		

		if( nbrOfCase == 0 )
		previousCondition = currentCondition;

		//yield return 0;
		nbrOfCase++;

		yield return new WaitUntil( () => reachingCylinder.GetComponent<TargetReaching>().flagEnter );

		taskIsRunning = false;


		//Stop Recording
		startOrStop = false;

	}


	// the question you want to ask at the end of the trial
	IEnumerator Question ( float question, float condition, float blockNumber, float trial,bool isCalib ) {

		questionIsRunning = true;
		
		if ( question == 1 ) {

			//Hide everything during question
			allObjectsVisible( false );


				if ( isCalib ) {

				if( !ExperimentInterface.Instance.isThresholdWithComparaison )
					questionThresholdWithTwoChoices.condition = condition;

					questionThresholdWithTwoChoices.pairNumber = IdentifyPair( previousCondition, currentCondition );
					questionThresholdWithTwoChoices.fileNameToRecord = "subjectID_" + ExperimentInterface.Instance.subjectID + "_" + "Block" + blockNumber + "_" + "Trial" + trial + "_" + DateTime.Now.ToString( "yyyyMMdd_HHmmss" );
					questionThresholdWithTwoChoices.gameObject.SetActive( true );
			     }
				else {
				

				if ( ExperimentInterface.Instance.isThresholdWithComparaison && ExperimentInterface.Instance.blockType == blockTypes.threshold)
					questionManagerComponentSeveral.pairNumber = IdentifyPair(previousCondition,currentCondition);
				else {
					questionManagerComponentSeveral.condition = condition;
					questionManagerComponentSeveral.pairNumber = 0;
				}
					

				questionManagerComponentSeveral.fileNameToRecord = "subjectID_" + ExperimentInterface.Instance.subjectID + "_" + "Block" + blockNumber + "_" + "Trial" + trial + "_" + DateTime.Now.ToString( "yyyyMMdd_HHmmss" );
					questionManagerComponentSeveral.gameObject.SetActive( true );
				} 

				if ( isCalib ) {
					// wait 0.1s apres etre desactive
					yield return new WaitUntil( () => theQuestionThresholdOnly.isAnswered );
					
					if( !theQuestionThresholdOnly.ignoreAnswers &&  theQuestionThresholdOnly.GetAnswer()=="First") 
					{
				
						if (conditionsCalibList[0]==7)
						{
							questionThresholdWithTwoChoices.condition = 7;
							isNotExtended = true;
						}
						else
						questionThresholdWithTwoChoices.condition = 2;
				}
					else if( !theQuestionThresholdOnly.ignoreAnswers && theQuestionThresholdOnly.GetAnswer() == "Second") {

					
						if ( conditionsCalibList[1] == 7 ) {
							isNotExtended = true;
						}
						else
						questionThresholdWithTwoChoices.condition = 2;

				}

					// wait 0.1s apres etre desactive
					yield return new WaitUntil( () => questionThresholdWithTwoChoices.isAnswered );
				}
				else {


		

				if ( ExperimentInterface.Instance.isThresholdWithComparaison && ExperimentInterface.Instance.blockType == blockTypes.threshold ) {
					// wait 0.1s apres etre desactive
					yield return new WaitUntil( () => theQuestionThresholdOnly.isAnswered );

					if ( theQuestionThresholdOnly.GetAnswer() == "First" ) {

						questionThresholdWithTwoChoices.condition = previousCondition;


					} else if ( theQuestionThresholdOnly.GetAnswer() == "Second" ) {

						questionThresholdWithTwoChoices.condition = currentCondition;
					} else
						questionThresholdWithTwoChoices.condition = 0;
				}

				// wait 0.1s apres etre desactive
				yield return new WaitUntil( () => questionManagerComponentSeveral.isAnswered );

				
				
				
				} 

			questionIsRunning = false;


		}

	}

	// Make break after several trials
	IEnumerator Break () {

		breakIsRunning = true;

		//Make all the stuff disappear
		allObjectsVisible( false );

		breakMessage.gameObject.SetActive( true );

		yield return new WaitUntil( () => breakMessage.isAnswered );

		breakIsRunning = false;
	}

	//The end of the experiment
	IEnumerator TheEnd () {
		
		
		 theEndIsRunning = true;


			//activate the Waiting Message
			experimentSetting.calibrationManager.waitingMessage.SetActive( true );

			//disable
			rightObjectHeld.SetActive( false );
			leftObjectHeld.SetActive( false );



			experimentSetting.calibrationManager.showAvatar = false;

			carpet.GetComponent<Renderer>().enabled = false;
			foreach ( Renderer renderer in chair.GetComponentsInChildren<Renderer>() )
				renderer.enabled = false;



			 experimentSetting.experimentStop = true;

		yield return new WaitUntil( () => !theEnd );

		//yield return 0;
		theEndIsRunning = false;
	}

	//Active message for over reaching the target
	void limitTargetRaching(float distance ) {

		if ( reachingCylinder.activeSelf == true ) {

			messageTargetLimit.messageComebackActive = false;
			messageTargetLimit.messageReachingTargetActive = true;

			if ( distance > ExperimentInterface.Instance.limitDistance ) {

				messageTargetLimit.setMessageReachingTarget = true;

			} else if ( messageTargetLimit.enabled ) {
				messageTargetLimit.setMessageReachingTarget = false;

			}
		}

	}

	// Active message for did not comeback with your hand
	bool handComeback ( float distance ) {

		if ( reachingCylinder.activeSelf == false ) {

			messageTargetLimit.messageComebackActive = true;
			messageTargetLimit.messageReachingTargetActive =false;
	
			//if (( (tableDessus.transform.lossyScale.y / 2)* ExperimentInterface.Instance.comebackDistance) < distance ) 
			if(distance< ExperimentInterface.Instance.comebackDistance )
			{
		
				messageTargetLimit.setMessageComeback = false;

				return true;

			} else {

				messageTargetLimit.setMessageComeback = true;

				return false;

			}
		} else {

			messageTargetLimit.setMessageComeback = false;

			return true;
		}
	}

	//Record the position of certain bordy if needed
	void recordingCustomPosition ( ) {
		
		
	      if( startOrStop ) {

			if ( !ExperimentInterface.Instance.recordDataPositionsSettings.posLog.logIsOpen ) {
				ExperimentInterface.Instance.recordDataPositionsSettings.fileNameToRecord = "subjectID_" + ExperimentInterface.Instance.subjectID + "_" + "Block" + blockNumber + "_" + "Trial" + trial + "_" +"Case"+nbrOfCase+"_"+DateTime.Now.ToString( "yyyyMMdd_HHmmss" );
				ExperimentInterface.Instance.recordDataPositionsSettings.StartRecording();
				ExperimentInterface.Instance.trialInfo = "subjectID_" + ExperimentInterface.Instance.subjectID +"_Condition "+ currentCondition + "_Trial_" + trial+ "_Block "+ blockNumber;

				//Debug.Log("recordStart");
			} else {

				//order to add data is very important and must correspond to the header set in the Log
				worldPositions.Clear();
				worldPositions.Add( experimentSetting.avatarDistortedComponent.settingsReferences.rightHand.position );
				worldPositions.Add( experimentSetting.avatarDistortedComponent.settingsReferences.rightForearm.position );
				worldPositions.Add( experimentSetting.avatarDistortedComponent.settingsReferences.rightUpperArm.position );
				worldPositions.Add( experimentSetting.avatarOriginalComponent.settingsReferences.rightHand.position );
				worldPositions.Add( experimentSetting.avatarOriginalComponent.settingsReferences.rightForearm.position );
				worldPositions.Add( experimentSetting.avatarOriginalComponent.settingsReferences.rightUpperArm.position );
				worldPositions.Add( reachingCylinder.transform.position );
				worldPositions.Add( originalCylinder.transform.position );
				worldPositions.Add( extensionCalibration.rightCylinderHeldDistorted.transform.position );
				worldPositions.Add( experimentSetting.avatarDistortedComponent.settingsReferences.pelvis.position );
				worldPositions.Add( headTracker.transform.position );
				worldPositions.Add( headTracker.transform.rotation.eulerAngles);

				//int trigger1 = 0;
				ExperimentInterface.Instance.recordDataPositionsSettings.posLog.Logging( worldPositions, currentCondition, extensionCalibration.rightHandDistortion.offset );//,trigger2,trigger3);


			}

		} 

		if(!startOrStop) {
			if ( ExperimentInterface.Instance.recordDataPositionsSettings.posLog.logIsOpen )
				ExperimentInterface.Instance.recordDataPositionsSettings.StopRecording();
			
		}
			



	}

	//To set the objects in the scene visible or not
	void allObjectsVisible ( bool active ) {

		if ( !active ) {
			if ( experimentSetting.calibrationManager.showAvatar )
				experimentSetting.calibrationManager.showAvatar = false;
		}

		if ( active ) {

			if ( !experimentSetting.calibrationManager.showAvatar )
				experimentSetting.calibrationManager.showAvatar = true;


		}

		rightObjectHeld.SetActive( active );
		leftObjectHeld.SetActive( active );
		if(table.activeSelf)
		table.SetActive( false );
		carpet.GetComponent<Renderer>().enabled = active;
		taskInstruction.SetActive( active );

		foreach ( Renderer renderer in chair.GetComponentsInChildren<Renderer>() )
			renderer.enabled = active;
	}

	void shuffleList( ) {
	
			conditionsCalibArray = Utils.Arrays.Shuffle( conditionsCalibArray );
			foreach ( float condition in conditionsCalibArray ) {
				conditionsCalibList.Add( condition );
			}


	
	} 

	int IdentifyPair(float firstCondition, float secondCondition) {

		// Identify all the condition pair
		if ( (firstCondition == 1 || firstCondition == 3) && (secondCondition == 1 || secondCondition == 3) )
			return 1;
		else if ( (firstCondition == 1 || firstCondition == 5) && (secondCondition == 1 || secondCondition == 5) )
			return 2;
		else if ( (firstCondition == 2 || firstCondition == 6) && (secondCondition == 2 || secondCondition == 6) )
			return 3;
		else if ( (firstCondition == 2 || firstCondition == 4) && (secondCondition == 2 || secondCondition == 4) )
			return 4;
		else
			return 0;


	}

	//Load Condition
	// parse CSV file with headder and block
	public bool LoadConfig ( string fileName, ref List<List<float>> conditionsList ) {

		// open CSV file
		Debug.Log( "conditionCSV: opening file " + fileName );

		CsvFileReader reader = new CsvFileReader( fileName );

		string fileContent = reader.ReadToEnd();
		reader.Close();


		using ( StringReader strReader = new StringReader( fileContent ) ) {

			string line;

			// skip header
			line = strReader.ReadLine();
			line = strReader.ReadLine();

			int offset = 0;
			while ( (line = strReader.ReadLine()) != null ) {


				string[] mocapLineArray = line.Split( new string[] { "," }, StringSplitOptions.None );

			//	List<float> conditionsLine = new List<float>();
				List<float> conditionsLine1 = new List<float>();


			/*	for ( int i = 0; i < 2; i++ ) {

					float distorEntry = float.Parse( mocapLineArray[i + offset] );

					conditionsLine.Add( distorEntry );

				}*/


				for ( int i = 0; i < mocapLineArray.Length-1; i++ ) {

					float distorEntry = float.Parse( mocapLineArray[i + offset] );

					conditionsLine1.Add( distorEntry );

				}

				//blockOrder.Add( conditionsLine );
				conditionsList.Add( conditionsLine1 );

			}
		}


		Debug.Log( "conditionCSV: " + fileName + " loaded!" );
		return true;
	}




}
