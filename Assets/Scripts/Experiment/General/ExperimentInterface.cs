﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net;
using System;

public class ExperimentInterface : Singleton<ExperimentInterface> {

	[Header( "Display Info" )]
	public string trialInfo;
	public string limbSelected;

	[Header( "User Info" )]
	public int subjectID;
	public int subjectHeight=178;
	public bool isRightHanded;
	

	[Header( "Source Manager" )]
	public Calibration_Manager.Data MotionSource;
	public enum Manager { Phasespace, Steam , Debug};
	public  Manager manager;
	public  enum Tracker { marker, tracker };
	public Tracker trackingObject;
	public bool play = false;
	[Range( 0, 4 )]
	public float playerSpeed = 1.0f;
	public string fileToload;

	[Header( "Calibration" )]
	public Calibration_Manager.CalibrationMode calibrationMode;
	public bool ManualWithTolerance;

	public bool showAvatar;
	public bool showUser;
	public bool showTrackers = true;
	public bool showMarkers=true;
	public KeyCode showAvatarKey = KeyCode.Alpha4;
	public SeatCalibration.CalibrationMode ObjectToTranslateAndRotate;
	public SeatCalibration.TranslationAxis AxisTranslation;
	public KeyCode automaticCalibrationKey = KeyCode.F;
	public KeyCode firstCalibrationCode = KeyCode.U;
	public KeyCode secondCalibrationCode = KeyCode.T;
	public KeyCode saveKey = KeyCode.Return;
	public KeyCode loadKey = KeyCode.Space;//TOUT
	public bool correctHandOrientation=true;
	public bool eyeCalibration;
	public EyeTrackingExperiment.eyeTrackingMethods eyeTracking;
	public bool isTable;

	[Header( "Experiment Setting ErrP" )]
	public Protocoles protocoleErrP;
	// RLStaircase= Reinforcement Learning + Staricase, Offline= EEG protocole without RL, Online= EEG protocole with RL
	public enum ProtocoleType {RLStaircase, RL2Param, Offline, Online}
	[SerializeField]
	public bool startWithRL;
	public  ProtocoleType protocoleType;
	public DynamicDistortionWithErrPExperiment.ExperimentController_ReinforcementLearning.AnswersMode Mode;
	[Range( 0, 1 )]
	public float distanceTarget=0.7f;
	public float taskDuration;
	public bool applyDistortion ;
	public float drange ;
	 [Range( 0, 100 )]
	public float gainDynamic ;
	public bool isRadiusDependent ;
	public float radius ;
	 [Range( 0, 1 )]
	public float ratioOfTheTarget ;
	public string configConditionDistortionFilename;

	[Header( "Experiment Setting Extension" )]
	public  Protocoles protocole;
	public  enum Protocoles { Prototype, Experiment };
	public bool infinitTraining;
	public bool Training;
	[Range( 0, 1 )]
	public float D0Training = 1;
	[Range( 0, 10 )]
	public float gainTraining = 0;
	[Range( 0, 10 )]
	public float offsetTraining = 0;
	[Range(0, 1)]
    public float D0 = 1;
	[Range( 0, 10 )]
	public float offsetCylinderCase2 = 0;
	[Range( 0, 10 )]
	public float offsetCylinderCase4 = 0;
	//[Range( 0, 10 )]
	//public float offsetCylinderCase6 = 0;
	[Range(0, 10)]
	public float gain = 0;
	[Range( 0, 10 )]
	public float offset = 0;
	[Range( -10, 10 )]
	public float offsetExtra = 0;


	public float comebackDistance;
	public float limitDistance;
	public TargetCreation_Egocentric.ExperimentCases experimentCase;
	public ExperimentController_Extension.blockTypes blockType;
	public bool isThresholdWithComparaison;
	public string configConditionThresholdFileName;
	public string configConditionThresholdTwoChoicesFileName;
	public string configConditionOwnershipFileName;


	[Header( "Path" )]
	public string configPath;
	public RecordPositionSettings recordDataPositionsSettings;
	

	[Header( "Serial Protocol" )]
	public int baudRate;
	public string portName;

	[Header( "TC IP" )]
	public Int32 port = 13000;
	public IPAddress iP = IPAddress.Any;

	[Header( "Parallel Protocol" )]
	public int parallelPort = 0xCFF8;

	// (Optional) Prevent non-singleton constructor use.
	protected ExperimentInterface () { }




}
