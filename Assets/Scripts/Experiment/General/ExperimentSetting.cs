﻿using System.Collections;
using System.IO;
using System;
using System.Collections.Generic;
using UnityEngine;
using ReadWriteCsv;
using IIG;



public class ExperimentSetting : MonoBehaviour {

	[Header( "Experiment Components" )]

	public Calibration_Manager calibrationManager;
    public IIG.MocapInputController mocapInputController;
	public GameObject ScriptManager;
	public GameObject[] trackingManagers;
	public Modularity.PlayerLog_Tracker playerLogTracker;


	public GameObject rightDistortedTarget;
    public GameObject leftDistortedTarget;

	public GameObject carpet;
	public GameObject chair;



	[HideInInspector]
	public bool start;
	[HideInInspector]
	public bool experimentStop;
	[HideInInspector]
	public ReadSocket readerSocket;
	[HideInInspector]
	public Character_Generator_Settings avatarOriginalComponent;
	[HideInInspector]
	public Character_Generator_Settings avatarDistortedComponent;


	bool isExperimentSet;


	Transform formerParentDistortedTarget;

	GameObject[] objectHelds;




	SeatCalibration seatControl;




    // Use this for initialization
    void Awake() {

		if ( ExperimentInterface.Instance.MotionSource == Calibration_Manager.Data.File ) {
			if ( ExperimentInterface.Instance.trackingObject == ExperimentInterface.Tracker.tracker ) {
				playerLogTracker.fileOptions.fileToLoad = ExperimentInterface.Instance.fileToload + ".tkr.csv";
				playerLogTracker.fileOptions.initAtStartup = true;
			} else if ( ExperimentInterface.Instance.manager == ExperimentInterface.Manager.Phasespace )
				mocapInputController.fileOptions.fileNameToLoad = ExperimentInterface.Instance.fileToload + ".mkr.csv";


			foreach ( GameObject manager in trackingManagers ) {
				if ( manager.name == "File_Manager" )
					manager.SetActive( true );
				else
					manager.SetActive( false );
			}

		} else {

			if ( ExperimentInterface.Instance.manager == ExperimentInterface.Manager.Phasespace ) {

				foreach ( GameObject manager in trackingManagers ) {

					if ( manager.name == "PhaseSpace_Manager" )
						manager.SetActive( true );
					else
						manager.SetActive( false );
				}

			} else if ( ExperimentInterface.Instance.manager == ExperimentInterface.Manager.Steam ) {
				foreach ( GameObject manager in trackingManagers ) {

					if ( manager.name == "Steam_Manager" )
						manager.SetActive( true );
					else
						manager.SetActive( false );
				}
			} else {
				foreach ( GameObject manager in trackingManagers ) {

					if ( manager.name == "Debug_Manager" )
						manager.SetActive( true );
					else
						manager.SetActive( false );
				}
			}


		}





		seatControl = ScriptManager.GetComponent<SeatCalibration>();


		if ( ExperimentInterface.Instance.manager == ExperimentInterface.Manager.Phasespace )
			mocapInputController.recordOptions.fileNameToRecord = "subjectID_" + ExperimentInterface.Instance.subjectID + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss");


        calibrationManager.fileTosave = "subjectID_" + ExperimentInterface.Instance.subjectID + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss");

		if(ExperimentInterface.Instance.MotionSource==Calibration_Manager.Data.Live)
		calibrationManager.fileToload = "subjectID_" + ExperimentInterface.Instance.subjectID + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss");
		else
			calibrationManager.fileToload = ExperimentInterface.Instance.fileToload;


		avatarOriginalComponent = calibrationManager.avatarOriginal.GetComponent<Character_Generator_Settings>();
     

        avatarDistortedComponent = calibrationManager.avatarDistorted.GetComponent<Character_Generator_Settings>();

	
		calibrationManager.subjectHeight = ExperimentInterface.Instance.subjectHeight;

		ExperimentInterface.Instance.showMarkers = false;
		ExperimentInterface.Instance.showTrackers = false;


        playerLogTracker = ScriptManager.GetComponent<Modularity.PlayerLog_Tracker>();
        playerLogTracker.fileName = "subjectID_" + ExperimentInterface.Instance.subjectID + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss");
		
		readerSocket = ScriptManager.GetComponent<ReadSocket>();
		readerSocket.port = ExperimentInterface.Instance.port;
		readerSocket.iP = ExperimentInterface.Instance.iP;



	
      
      
        playerLogTracker.InitializeSerialPort( ExperimentInterface.Instance.portName, ExperimentInterface.Instance.baudRate );

		objectHelds = GameObject.FindGameObjectsWithTag( "ObjectHeld" );

	}


    // Update is called once per frame
    void Update()
    {
		////////////////////////////////////////////////////////////////////////
		//////////////////Interface Set//////////////////////////////////////////
		/////////////////////////////////////////////////////////////////////////
		ExperimentInterface.Instance.limbSelected = calibrationManager.limbSelected;
		playerLogTracker.playerRate = ExperimentInterface.Instance.playerSpeed;
		playerLogTracker.play = ExperimentInterface.Instance.play;
		seatControl.calibrationMode = ExperimentInterface.Instance.ObjectToTranslateAndRotate;
		seatControl.translationAxis = ExperimentInterface.Instance.AxisTranslation;
		calibrationManager.saveKey = ExperimentInterface.Instance.saveKey;
        calibrationManager.loadKey = ExperimentInterface.Instance.loadKey;//TOUT
		calibrationManager.automaticCalibrationKey = ExperimentInterface.Instance.automaticCalibrationKey;
        calibrationManager.firstCalibrationCode = ExperimentInterface.Instance.firstCalibrationCode;
		calibrationManager.secondCalibrationCode = ExperimentInterface.Instance.secondCalibrationCode;//TOUT
		calibrationManager.showAvatarKey = ExperimentInterface.Instance.showAvatarKey;
		calibrationManager.mocapSource = ExperimentInterface.Instance.MotionSource;
		calibrationManager.calibrationMode = ExperimentInterface.Instance.calibrationMode;
		calibrationManager.withTolerance = ExperimentInterface.Instance.calibrationMode == Calibration_Manager.CalibrationMode.Automatique ? true : ExperimentInterface.Instance.ManualWithTolerance;

		EyeTrackingExperiment.Instance.eyeTrackingMethod = ExperimentInterface.Instance.eyeTracking;

		if ( ExperimentInterface.Instance.eyeCalibration ) {
			EyeTrackingExperiment.Instance.eyeCalibrationLaunched = !EyeTrackingExperiment.Instance.eyeCalibrationLaunched;
			ExperimentInterface.Instance.eyeCalibration = false;
		}
		


		if ( ExperimentInterface.Instance.showTrackers ) {
			calibrationManager.showTrackers = !calibrationManager.showTrackers;
			ExperimentInterface.Instance.showTrackers = false;
		}

		if ( ExperimentInterface.Instance.showUser ) {
			calibrationManager.showUser = !calibrationManager.showUser;
			ExperimentInterface.Instance.showUser = false;
		}

		if ( ExperimentInterface.Instance.showAvatar ) {
			calibrationManager.showAvatar = !calibrationManager.showAvatar;
			ExperimentInterface.Instance.showAvatar = false;
		}

		if ( ExperimentInterface.Instance.manager == ExperimentInterface.Manager.Phasespace ) {
			if ( ExperimentInterface.Instance.showMarkers ) {
				mocapInputController.drawMarkersTransform = !mocapInputController.drawMarkersTransform;
				ExperimentInterface.Instance.showMarkers = false;
			}

		} else if ( ExperimentInterface.Instance.showMarkers )
			Debug.LogError( "You need to set the manager to Phasespace" );


		if ( Input.GetKeyDown( ExperimentInterface.Instance.loadKey ) )
			calibrationManager.fileToload = ExperimentInterface.Instance.fileToload;
		///////////////////////////////////////////////////////////////////////////////////////////
		/////////////////////////////////////////////////////////////////////////////////////////////


		//Start Calibration
        if (!start)
        {

            start = calibrationManager.Calibration();


            if (start)
            {
                //Change parents the hand 
                formerParentDistortedTarget = rightDistortedTarget.transform.parent;
                rightDistortedTarget.transform.parent = avatarDistortedComponent.settingsReferences.rightHand;
                leftDistortedTarget.transform.parent = avatarDistortedComponent.settingsReferences.leftHand;

               
            }


        }//Set Envirronement After Calibration
        else if (!isExperimentSet)
        {



                if ( ExperimentInterface.Instance.MotionSource == Calibration_Manager.Data.File )
                {
                    if ( ExperimentInterface.Instance.trackingObject == ExperimentInterface.Tracker.tracker)
                    {
                        playerLogTracker.m_mocapSource = Modularity.PlayerLog_Tracker.MocapSource.FILE;
                        playerLogTracker.play = true;
                    }
                    else
                    {
						if ( ExperimentInterface.Instance.manager == ExperimentInterface.Manager.Phasespace )
							mocapInputController.m_mocapSource = IIG.MocapInputController.MocapSource.FILE;

                    }

                }
                else
                {
                    //record marker motion
                    // mocapInputController.recordOptions.isRecording = true;

                    // record tracker 
                    playerLogTracker.isRecording = true;

                }

			
			    //Come back to the previous parents
			    //Change parents the hand 
			    rightDistortedTarget.transform.localPosition = Vector3.zero;
			    rightDistortedTarget.transform.localEulerAngles = Vector3.zero;
			    leftDistortedTarget.transform.localPosition = Vector3.zero;
			    leftDistortedTarget.transform.localEulerAngles = Vector3.zero;
			
			    rightDistortedTarget.transform.parent = formerParentDistortedTarget;
			    leftDistortedTarget.transform.parent = formerParentDistortedTarget;
			
			    rightDistortedTarget.SetActive(true);
			    leftDistortedTarget.SetActive(true);

				//RHI Experiment
				if ( ExperimentInterface.Instance.isTable )
					gameObject.GetComponentInChildren<CalibrationTable>().enabled = true;
			else {
				gameObject.GetComponentInChildren<CalibrationTable>().enabled = false;
				CalibrationTable.isLoad = true;
			}
					

				if(ExperimentInterface.Instance.correctHandOrientation)
					gameObject.GetComponentInChildren<CorrectHandOrientation>().enabled = true;




			isExperimentSet = true;


		 }



		//Stop Experiment Recording
		if (experimentStop)
        {
			
			if ( ExperimentInterface.Instance.MotionSource == Calibration_Manager.Data.Live)
            {
				if ( ExperimentInterface.Instance.manager == ExperimentInterface.Manager.Phasespace ) {
					//stop recording marker motion
					//mocapInputController.recordOptions.isRecording = false;
				}
				

                //stop recording tracker 
                playerLogTracker.isRecording = false;
            }

        }



    }


	//To set the original objects of the scene visible or not
	public void allbObjectsVisible ( bool active ) {

		if ( !active ) {
			if ( calibrationManager.showAvatar )
				 calibrationManager.showAvatar = false;
		}

		if ( active ) {

			if ( !calibrationManager.showAvatar )
				 calibrationManager.showAvatar = true;


		}
	
		if ( gameObject.GetComponentInChildren<CalibrationTable>().table )
			gameObject.GetComponentInChildren<CalibrationTable>().table.SetActive( false );
		
		carpet.GetComponent<Renderer>().enabled = active;


		foreach ( GameObject objectHeld in objectHelds)
			objectHeld.SetActive(active);


		foreach ( Renderer renderer in chair.GetComponentsInChildren<Renderer>() )
			renderer.enabled = active;
	}

}

