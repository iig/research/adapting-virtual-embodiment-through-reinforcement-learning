﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetCreation_Egocentric : MonoBehaviour {

    public Autodesk_Character_Generator_Settings avatarOriginal;
    public Autodesk_Character_Generator_Settings avatarDistorted;
	public ExperimentSetting ExperimentSetting;
	public ExtensionCalibration extensionCalibration;
	public IK_Manager IKManager;
	public GameObject tableDessus;


	//public float comebackDistance=1;

   /* [Range(0, 1)]
    public float D0 = 1;
    [Range(0, 10)]
	public float gain = 0;
	[Range( 0, 10 )]
	public float offset = 0;*/

	public GameObject reachingBall;
	public  IkGoalDistorter distortionRightHanded;


	public enum ExperimentCases { Case1, Case2 , Case3, Case4, Case5 , Case6 };

    //public ExperimentCases ExperimentInterface.Instance.experimentCase;

	GameObject ballInstantiated;
	SetCanvasActive messageTargetLimit;
	float handToTargetDistance;
	bool isBallIntantiated = false;
	bool targetValidated=true;


	// Use this for initialization
	void Awake () {

		if ( ExperimentInterface.Instance.protocole == ExperimentInterface.Protocoles.Experiment )
			this.enabled = false;
	}

	private void Start () {

		messageTargetLimit = Camera.main.GetComponentInChildren<SetCanvasActive>();

	}

	// Update is called once per frame
	void Update ()
    {
		

		if ( targetValidated == true && ballInstantiated!=null ) {
			//handToTargetDistance = Mathf.Abs( ballInstantiated.transform.position.z - extensionCalibration.rightCylinderHeldDistorted.transform.position.z );
			handToTargetDistance =- extensionCalibration.rightCylinderHeldDistorted.transform.position.z + ballInstantiated.transform.position.z ;
			if ( handToTargetDistance > ExperimentInterface.Instance.limitDistance ) {

				//messageTargetLimit.setMessage = true;

			} else if ( messageTargetLimit.enabled ) {
				//messageTargetLimit.setMessage = false;

			}
		}

			if (ExperimentSetting.start && extensionCalibration.isCalibrated  )
        {
			//Debug.Log( Mathf.Abs( tableDessus.transform.position.y - extensionCalibration.rightCylinderHeldDistorted.transform.position.y ) );
			//if ( Vector3.Distance( distortionRightHanded.transform.position, extensionCalibration.cylinderPosition ) > ExperimentInterface.Instance.comebackDistance )
			if ( Mathf.Abs( tableDessus.transform.position.y - extensionCalibration.rightCylinderHeldDistorted.transform.position.y  ) < ExperimentInterface.Instance.comebackDistance )
				targetValidated = false;

			if (!isBallIntantiated)
            {
                ballInstantiated = Instantiate(reachingBall);

               isBallIntantiated = true;
            }

            if (ExperimentInterface.Instance.experimentCase==ExperimentCases.Case1 && !targetValidated )
            {
				IKManager.isShoulderTracked = true;

				ballInstantiated.SetActive(true);

				distortionRightHanded.gain = 0;

				distortionRightHanded.offset = 0;

			//	D0 = 0.75f;

				//Distance d0
				ballInstantiated.transform.position = extensionCalibration.ComputeTargetPositionForExtendedArm( IIG.Distortions.GetDistortionFunction( distortionRightHanded.distortionType ), distortionRightHanded.offset, distortionRightHanded.gain, ExperimentInterface.Instance.D0,0 );

				targetValidated=true;

            }
            else if (ExperimentInterface.Instance.experimentCase == ExperimentCases.Case2 && !targetValidated )
            {
				IKManager.isShoulderTracked = true;

				ballInstantiated.SetActive( true );

				distortionRightHanded.gain = 0;

				distortionRightHanded.offset = ExperimentInterface.Instance.offsetExtra;

				//D0 = 1;

				//Distance d0
				ballInstantiated.transform.position = extensionCalibration.ComputeTargetPositionForExtendedArm( IIG.Distortions.GetDistortionFunction( distortionRightHanded.distortionType ), distortionRightHanded.offset, distortionRightHanded.gain, 1 , ExperimentInterface.Instance.offsetCylinderCase2);
				targetValidated = true;
			}
            else if (ExperimentInterface.Instance.experimentCase == ExperimentCases.Case3 && !targetValidated )
            {
				IKManager.isShoulderTracked = false;

				ballInstantiated.SetActive( true );

				distortionRightHanded.gain = - ExperimentInterface.Instance.gain;

				distortionRightHanded.offset = -ExperimentInterface.Instance.offset- ExperimentInterface.Instance.offsetExtra;

			//	D0 = 0.75f;

				//Distance d0
				ballInstantiated.transform.position = extensionCalibration.ComputeTargetPositionForExtendedArm( IIG.Distortions.GetDistortionFunction( distortionRightHanded.distortionType ), 0 , distortionRightHanded.gain, ExperimentInterface.Instance.D0, 0 );
				targetValidated = true;

			}
            else if (ExperimentInterface.Instance.experimentCase == ExperimentCases.Case4 && !targetValidated )
            {
				IKManager.isShoulderTracked = false;

				ballInstantiated.SetActive( true );

				distortionRightHanded.gain = -ExperimentInterface.Instance.gain;

				distortionRightHanded.offset = -ExperimentInterface.Instance.offset- ExperimentInterface.Instance.offsetExtra;

				//D0 = 1;

				//Distance d0
				ballInstantiated.transform.position = extensionCalibration.ComputeTargetPositionForExtendedArm( IIG.Distortions.GetDistortionFunction( distortionRightHanded.distortionType ), distortionRightHanded.offset, distortionRightHanded.gain, 1 , ExperimentInterface.Instance.offsetCylinderCase4);
				targetValidated = true;
			}
            else if (ExperimentInterface.Instance.experimentCase == ExperimentCases.Case5 && !targetValidated )
            {
				IKManager.isShoulderTracked = false;

				ballInstantiated.SetActive( true );

				distortionRightHanded.gain = ExperimentInterface.Instance.gain;

				distortionRightHanded.offset = ExperimentInterface.Instance.offset+ ExperimentInterface.Instance.offsetExtra;

				//D0 = 0.75f;

				//Distance d0
				ballInstantiated.transform.position = extensionCalibration.ComputeTargetPositionForExtendedArm( IIG.Distortions.GetDistortionFunction( distortionRightHanded.distortionType ), 0, distortionRightHanded.gain, ExperimentInterface.Instance.D0,0 );
				targetValidated = true;
			}
            else if (ExperimentInterface.Instance.experimentCase == ExperimentCases.Case6 && !targetValidated )
            {
				IKManager.isShoulderTracked = false;

				ballInstantiated.SetActive( true );

				distortionRightHanded.gain = ExperimentInterface.Instance.gain;

				distortionRightHanded.offset = ExperimentInterface.Instance.offset+ ExperimentInterface.Instance.offsetExtra;

				//D0 = 1;

				//Distance d0
				ballInstantiated.transform.position = extensionCalibration.ComputeTargetPositionForExtendedArm( IIG.Distortions.GetDistortionFunction( distortionRightHanded.distortionType ), 0, 0, 1 ,0);
				targetValidated = true;
			}


        }
    }
}
