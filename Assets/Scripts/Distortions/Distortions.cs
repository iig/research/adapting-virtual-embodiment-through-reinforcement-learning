﻿using UnityEngine;

namespace IIG {

    public static class Distortions {
        
        public delegate float DistortionFunction(float d, float offset, float gain);

        public static DistortionFunction GetDistortionFunction(DistortionType type)
        {
            switch (type)
            {
                case DistortionType.Sqrt:
                    return SqrtDistortion;
                case DistortionType.Linear:
                    return LinearDistortion;
                case DistortionType.NoContact:
                    return NoContactDistortion;
                case DistortionType.Quadratic:
                    return QuadraticDistortion;
                case DistortionType.PositiveSine:
                    return PositiveSineDistortion;
                case DistortionType.NegativeSine:
                    return NegativeSineDistortion;
                case DistortionType.PositiveExp:
                    return PositiveExpDistortion;
                case DistortionType.NegativeExp:
                    return NegativeExpDistortion;
                case DistortionType.StrengthBased:
                    return StrengthBasedDistortion;
                case DistortionType.Cosine:
                    return CosineDistortion;
                case DistortionType.LinearAndOffset:
                    return LinearAndOffsetDistortion;
                case DistortionType.None:
                default:
                    return NoDistortion;
            }
        }

        public enum DistortionType {
            None,
            Sqrt,
            Linear,
            NoContact,
            Quadratic,
            PositiveSine,
            NegativeSine,
            PositiveExp,
            NegativeExp,
            StrengthBased,
            Cosine,
            LinearAndOffset
        }

        public static float LinearAndOffsetDistortion(float d, float offset, float gain)
        {
			//distance between the point projected on the body and the joint
            return d * Mathf.Pow(10f, gain / 10f) + 0.05f * offset;
        }

        public static float SqrtDistortion(float d, float actionRange, float strength)
        {
            return Mathf.Sqrt(d);
        }

        public static float NoDistortion(float d, float actionRange, float strength)
        {
            return d;
        }

        public static float LinearDistortion(float d, float actionRange, float strength)
        {
            return d * Mathf.Pow(10f, strength / 10f);
        }

        public static float NoContactDistortion(float d, float actionRange, float strength)
        {
            return d + 0.05f * strength;
        }

        public static float QuadraticDistortion(float d, float actionRange, float strength) {
            return strength * d * d;
        }

        public static float PositiveSineDistortion(float d, float aR, float strength) {
            const float str = 90f;
            if (d < aR) {
                return d - str / 100f * aR / Mathf.PI * Mathf.Sin(Mathf.PI * d / aR);
            } else {
                return d;
            }
        }

        public static float NegativeSineDistortion(float d, float aR, float strength) {
            const float str = 90f;
            if (d < aR) {
                return d + str / 100f * aR / Mathf.PI * Mathf.Sin(Mathf.PI * d / aR);
            } else {
                return d;
            }
        }

        public static float PositiveExpDistortion(float d, float actionRange, float strength) {
            float virtualTargetRadius = 0.01f;
            float targetRadius = 0.04f;
            if (d <= 0 || d > actionRange)
                return d;
            else
                return actionRange * Mathf.Pow(d / actionRange,
                                               Mathf.Log(virtualTargetRadius / actionRange) /
                                               Mathf.Log(targetRadius / actionRange));
        }

        public static float NegativeExpDistortion(float d, float actionRange, float strength) {
            float virtualTargetRadius = 0.045f;
            float targetRadius = 0.005f;
            if (d <= 0 || d > actionRange)
                return d;
            else
                return actionRange * Mathf.Pow(d / actionRange,
                                               Mathf.Log(virtualTargetRadius / actionRange) /
                                               Mathf.Log(targetRadius / actionRange));
        }

        public static float StrengthBasedDistortion(float d, float actionRange, float strength) {
            if (d <= 0 || d > actionRange)
                return d;
            else
                return actionRange * Mathf.Pow(d / actionRange, Mathf.Pow(2f, -strength));
        }

        public static float CosineDistortion(float d, float actionRange, float strength) {
            //strength / 2 * (1 - np.cos(1/aR * 2 * np.pi * d)) + d
            if (d <= 0 || d > actionRange)
                return d;
            else
                return strength / 2f * (1f - Mathf.Cos(1f / actionRange * 2 * Mathf.PI * d)) + d;
        }
    }
}
