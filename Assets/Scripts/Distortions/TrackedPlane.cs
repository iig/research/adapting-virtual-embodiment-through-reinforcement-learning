﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EgocentricCoordinatesFormalism
{
    public class TrackedPlane : BodyPart
    {
        public IIG.VrpnClientWrapper vrpnClientWrapper;
        public List<string> markers;
        public Color boxColor = Color.blue;
        public bool drawBox = true;
        public bool drawCoordinates = true;

        public Vector3 dx { get { return GetA() * 0.5f; } }
        public Vector3 dy { get { return (GetB() - GetA() * Vector3.Dot(GetA(), GetB()) / GetA().sqrMagnitude) * 0.5f; } }
        public Vector3 n {  get { return Vector3.Cross(dx, dy).normalized; } }

        private Vector3[] positionCache;

        private void Start()
        {
            positionCache = new Vector3[markers.Count];
        }

        private void Update()
        {
            if(drawBox)
                DrawLines();
        }

        private void DrawLines()
        {
            // store to avoid computation
            var n = this.n;
            var dx = this.dx;
            var dy = this.dy;

            Vector3 centerWorld = GetCenterWorld();
            Debug.DrawLine(centerWorld + dx + dy, centerWorld - dx + dy, boxColor, 0f, false);
            Debug.DrawLine(centerWorld + dx + dy, centerWorld + dx - dy, boxColor, 0f, false);
            Debug.DrawLine(centerWorld - dx - dy, centerWorld - dx + dy, boxColor, 0f, false);
            Debug.DrawLine(centerWorld - dx - dy, centerWorld + dx - dy, boxColor, 0f, false);
            //Debug.DrawLine(centerWorld, centerWorld + n, Color.magenta, 0f, false);
        }

        private Vector3 GetA()
        {
            if (markers.Count != 4)
                throw new UnityException("Please provide exactly four markers. Least square fitting of plane is still TODO.");
            Vector3 a0 = GetMarkerPosition(markers[0]);
            Vector3 a1 = GetMarkerPosition(markers[1]);

            if (drawCoordinates)
                Debug.DrawLine(a0, a1, Color.yellow, 0f, false);

            return a0 - a1;
        }

        private Vector3 GetB()
        {
            if (markers.Count != 4)
                throw new UnityException("Please provide exactly four markers. Least square fitting of plane is still TODO.");
            Vector3 b0_0 = GetMarkerPosition(markers[2]);
            Vector3 b0_1 = GetMarkerPosition(markers[3]);
            Vector3 b0 = (b0_0 + b0_1) / 2; // compute midpoint of the two last markers as an approximate for torso plane
            Vector3 b1 = GetMarkerPosition(markers[1]); ;

            if (drawCoordinates)
                Debug.DrawLine(b0, b1, Color.cyan, 0f, false);

            return b0 - b1;
        }

        private Vector3 GetCenterWorld()
        {
            Vector3 center = Vector3.zero;
            foreach(string marker in markers)
            {
                center += GetMarkerPosition(marker);
            }
            center /= markers.Count;
            return center;
        }

        /// <summary>
        /// This gets the marker position through the vrpn client or returns the last known position if marker is hidden
        /// </summary>
        /// <returns>Either the marker position or its last known position if hidden</returns>
        private Vector3 GetMarkerPosition(string marker)
        {
            int markerIndex = markers.FindIndex(x => x == marker);
            Vector3 newMarkerPosition;
            vrpnClientWrapper.vrpn_client.GetMarkerPosition(marker, out newMarkerPosition);
            if (!float.IsNaN(newMarkerPosition.x) && !float.IsNaN(newMarkerPosition.y) && !float.IsNaN(newMarkerPosition.z))
                positionCache[markerIndex] = newMarkerPosition;
            return positionCache[markerIndex];
        }

        private Vector3 ProjectOnPlane(Vector3 worldPoint)
        {
            var n = this.n; // optimization
            return worldPoint - n * Vector3.Dot(worldPoint - GetCenterWorld(), n);
        }

        public override ProjectionResult GetReferencePoint(Vector3 position)
        {
            Vector3 pointOnPlane = ProjectOnPlane(position);
            Vector3 pointLocal = pointOnPlane - GetCenterWorld();
            var dx = this.dx; // optimization
            var dy = this.dy; // optimization

            // compute coordinates in local space (dx,dy)
            float a = Vector3.Dot(pointLocal, dx) / dx.sqrMagnitude;
            float b = Vector3.Dot(pointLocal, dy) / dy.sqrMagnitude;
            // fit to rectangle
            a = Mathf.Clamp(a, -1, 1);
            b = Mathf.Clamp(b, -1, 1);

            //*

            if (drawCoordinates)
            {
                Vector3 centerWorld = GetCenterWorld();
                Debug.DrawLine(centerWorld, centerWorld + dx, Color.red, 0f, false);
                Debug.DrawLine(centerWorld, centerWorld + dy, Color.red, 0f, false);
                Debug.DrawLine(centerWorld, centerWorld + pointLocal, Color.yellow, 0f, false);
                Debug.DrawLine(centerWorld, centerWorld + a * dx, Color.green, 0f, false);
                Debug.DrawLine(centerWorld, centerWorld + b * dy, Color.green, 0f, false);
            }
            //*/

            return new SquareProjection(new Vector2(a, b));
        }

        public override Vector3 ProjectionResultToWorld(ProjectionResult projectionResult)
        {
            SquareProjection asSquareProjection = (SquareProjection)projectionResult; // will throw an excpetion if cast fails, unlike 'as'

            Vector3 returned = asSquareProjection.projectionCoordinates.x * dx
                                + asSquareProjection.projectionCoordinates.y * dy
                                + GetCenterWorld();
            return returned;
        }

        public override Vector3 MirrorIfNeeded(Vector3 position)
        {
            // Not quite needed now, TODO later
            return position;
        }
    }
}
