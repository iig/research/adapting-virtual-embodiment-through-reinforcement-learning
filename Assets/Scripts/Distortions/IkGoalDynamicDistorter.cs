﻿/**************************************************************************
 * Egocentric distortion 
 * Written by Thibault Porssut
 * Last update: 28/03/19
 * *************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RootMotion.FinalIK;


namespace DynamicExperiment
{


    public class IkGoalDynamicDistorter : MonoBehaviour//, EgocentricCoordinatesDistorter
    {
        [Header("Body-related")]
        public Character_Generator_Settings AvatarDistorted;
        public Character_Generator_Settings AvatarOriginal;
		public CorrectHandOrientation correctHandOrientation;
        public Transform initialLocation;
		public bool isCorrectedExternaly;

		[Header("Distortion")]
        Transform originalIkGoal;
        public GameObject reachingTarget;
        public GameObject handBallOriginal;
        public GameObject handBallDistorted;
        public enum BoneType { LeftHand, RightHand, LeftTennisBall , RightTennisBall };
        public BoneType boneType;

        public bool applyDistortion = true;
		public bool isBallTarget;

        public enum DistortionType { Gaussian, SplineHermite };
        public DistortionType distortionType;


        //public bool drawLines = true;

        public bool isRadiusDependent=true;
        [Range(0, 1)]
        public float ratioOfTheTarget = 1f;

        public bool isRadiusTargetDependent = true;

        [Range(0, 100)]
        public float drange = 0f;
        [Range(0, 10)]
        public float gain = 0f;
        public float radius = 0.125f;

        // public IIG.Distortions.DistortionType distortionType = IIG.Distortions.DistortionType.None;

        //public Vector3 originalPosition { get { return originalIkGoal.position; } }

        [HideInInspector]
       // public bool isStart = true;

        private Vector3 myRestPosition;
        private Quaternion myRestRotation;
        private float distance;
        private Color initialColor;
        Vector3 originalBallLocalPositionFormer;
        float ratioTargetBall = 0.15f;//0.15f

      

        void Awake()
        {
            if (initialLocation != null)
            {
                transform.position = initialLocation.position;
                transform.rotation = initialLocation.rotation;

            }

            myRestPosition = transform.position;
            myRestRotation = transform.rotation;

            initialColor = transform.GetComponent<Renderer>().material.color;

       
       

        }

   


        void Update()
        {
		

			// when you calibrate the avatar the ball held change size and not the target. So the level of difficulty will change according the user hand size so to avaoid that this function will adjust the ball size according the target size
			if (isRadiusTargetDependent)
            {

                float ratio = reachingTarget.transform.lossyScale.x / reachingTarget.transform.localScale.x;

				reachingTarget.transform.localScale = ((1 + ratioTargetBall) *2* handBallDistorted.transform.lossyScale)* (1 / ratio);// 2 is beacuse the modele of ball held does not have the unit  scale as the unity sphere
              

               isRadiusTargetDependent = !isRadiusTargetDependent;
            }

          

            //Target
            if (originalIkGoal == null )//&& isStart)
            {
                if (boneType == BoneType.LeftHand && GameObject.Find(AvatarOriginal.settingsReferences.leftHand.name + "Target") != null)
                    originalIkGoal = GameObject.Find(AvatarOriginal.settingsReferences.leftHand.name + "Target").transform;

                    

                if (boneType == BoneType.RightHand && GameObject.Find(AvatarOriginal.settingsReferences.rightHand.name + "Target") != null)
                    originalIkGoal = GameObject.Find(AvatarOriginal.settingsReferences.rightHand.name + "Target").transform;


            }

        }



        void LateUpdate()
        {
            if (originalIkGoal != null)
            {
                if (applyDistortion)
                {
                    RangeDistortion();
                 
                }
                else
                {
                 

					if(isBallTarget) {
						handBallDistorted.transform.position = handBallOriginal.transform.position;
						if ( correctHandOrientation==null || !correctHandOrientation.orientationDone)
							handBallDistorted.transform.rotation = handBallOriginal.transform.rotation;
					} else {
							transform.position = originalIkGoal.position;
						if ( correctHandOrientation == null || !correctHandOrientation.orientationDone )
							transform.rotation = originalIkGoal.rotation;
					}
        
                }
            }

        }

        void RangeDistortion()
        {
            // distance = Vector3.Distance(originalIkGoal.transform.position, reachingBall.transform.position);
            distance = Vector3.Distance(handBallOriginal.transform.position, reachingTarget.transform.position);


            if (distance <= drange)
            {
                //Debug.Log(distance);
                transform.GetComponent<Renderer>().material.color = Color.yellow;

                // transform.position = GetDistortedPosition();
                handBallDistorted.transform.position = GetDistortedPosition();
                // Debug.Log("original " + originalIkGoal.transform.position + " distorted " + GetDistortedPosition());
            }
              else
              {

                  transform.GetComponent<Renderer>().material.color = initialColor;

                // transform.position = originalIkGoal.transform.position;
                handBallDistorted.transform.position = handBallOriginal.transform.position;
        


            }

			// transform.rotation = originalIkGoal.rotation;
			if ( !isCorrectedExternaly )
			handBallDistorted.transform.rotation = handBallOriginal.transform.rotation;


        }

        Vector3 GetDistortedPosition()
        {
            float X = distance / drange;

           
           if (isRadiusDependent)
            {
				//  radius = reachingTarget.transform.lossyScale.x * ratioOfTheTarget;

				//   drange = radius / 0.25f;

				// normalisation du radius pour comparer a X
				radius = (reachingTarget.transform.lossyScale.x * ratioOfTheTarget) / drange;

				
			}

          

          
           



            float distortion = DynamicDistortion(X, gain, radius);


            // Debug.Log(distortion);

            //Vector3 originalIkGoalLocalPosition = originalIkGoal.position - reachingTarget.transform.position;
            Vector3 originalBalllLocalPosition = handBallOriginal.transform.position - reachingTarget.transform.position;

            Vector3 distortedLocaPosition;
           // Debug.Log(originalIkGoalLocalPosition);

            if (originalBalllLocalPosition != originalBallLocalPositionFormer)
            {

                //if (originalIkGoalLocalPosition.x < 0)
                //{
                //    Debug.Log("x negatif");
                //    distortedLocaPosition.x = (originalIkGoalLocalPosition.x + drange) * distortion - drange;
                //}
                //else
                //{
                //    Debug.Log("x positif");
                //    distortedLocaPosition.x = (originalIkGoalLocalPosition.x - drange) * distortion + drange;
                //}

                //if (originalIkGoalLocalPosition.y < 0)
                //{
                //    Debug.Log("y negatif");
                //    distortedLocaPosition.y = (originalIkGoalLocalPosition.y + drange) * distortion - drange;
                //}
                //else
                //{
                //    Debug.Log("y positif");
                //    distortedLocaPosition.y = (originalIkGoalLocalPosition.y - drange) * distortion + drange;
                //}

                //if (originalIkGoalLocalPosition.z < 0)
                //{
                //    Debug.Log("z negatif");
                //    distortedLocaPosition.z = (originalIkGoalLocalPosition.z+ drange) * distortion - drange;
                //}
                //else
                //{
                //    Debug.Log("z positif");
                //    distortedLocaPosition.z = (originalIkGoalLocalPosition.z - drange) * distortion + drange;
                //}

                //distortedLocaPosition = originalIkGoalLocalPosition * distortion;

                distortedLocaPosition = originalBalllLocalPosition * distortion;

                originalBallLocalPositionFormer = originalBalllLocalPosition;

                return distortedLocaPosition + reachingTarget.transform.position;
            }
            else
            {
                return  handBallDistorted.transform.position; //transform.position;
            }
        }

        float DynamicDistortion(float X, float amplitude, float radius)
        {
            float d = 1 / radius;

            if (distortionType == DistortionType.Gaussian)
                return 1 / (1 + Rectangular(X, amplitude) + Gaussian(X, -amplitude, radius));
            else
                if (Mathf.Abs(X) * d < 1)
                    return 1 /(1 + CubicHermiteSplineUnit(Mathf.Abs(X) * d, 0, 0, amplitude, 0));
                else
                    return 1 /(1 + CubicHermiteSplineUnit((Mathf.Abs(X) - 1 / d) * d / (d - 1), amplitude, 0, 0, 0));
      
            /*else if (distortionType == DistortionType.SplineHermite)
                if (Mathf.Abs(X) * d < 1)
                    return 1 / (1 + CubicHermiteSplineUnit(Mathf.Abs(X) * d, 0, 0, amplitude, 0));
                else if (((Mathf.Abs(X) - 1 / d) * d / (d - 1) >= 0) && ((Mathf.Abs(X) - 1 / d) * (d / (d - 1)) <= 1))
                    return 1/ (CubicHermiteSplineUnit((Mathf.Abs(X) - 1/d) * d / (d - 1), amplitude, 0, 0, 0)) ;
                else
                    return 1;
            else
                return 1;*/   
            
        }

        float CubicHermiteSplineUnit(float X, float a, float b, float c,float d)
        {
                 // a = value in 0
                 // b = derivative in 0
                 // c = value in 1
                 // d = derivative in 1
                 

            float h0 = 2*Mathf.Pow(X, 3) - 3 * Mathf.Pow(X, 2) + 1;
            float h1 = Mathf.Pow(X, 3) - 2 * Mathf.Pow(X, 2) + X;
            float h2 = -2 * Mathf.Pow(X, 3) + 3 * Mathf.Pow(X, 2);
            float h3 = Mathf.Pow(X, 3) - Mathf.Pow(X, 2);

            return a* h0 + b * h1 + c * h2 + d * h3;

        }

        float Gaussian(float X, float amplitude, float radius)
        {
            float center = 0f;
            return amplitude * Mathf.Exp(-Mathf.Pow((X - center) / radius, 2));
        }

        float Rectangular(float X, float amplitude)
        {
            if (Mathf.Abs(X) <= 1)
            {
                return amplitude * (1 - Mathf.Abs(X));

            }
            else
            {
                return 0;
            }


        }

        // A REMPLACER PAR MA PROPRE FONCTION
        /*  public void SetDistortionStrength(TrialSetup distortionSetup)
          {
              distortionType = IIG.Distortions.DistortionType.LinearAndOffset;

              switch (distortionSetup.amplitudeValue)
              {
                  case AmplitudeValue.sixtyFive:
                      gain = Experiment.gainSixtyFive;
                      break;
                  case AmplitudeValue.thirtyFive:
                      gain = Experiment.gainThirtyFive;
                      break;
                  case AmplitudeValue.zero:
                      gain = 0f;
                      break;
                  default:
                      Debug.LogError("Unknown amplitude value: " + distortionSetup.amplitudeValue);
                      break;
              }

              switch (distortionSetup.offsetValue)
              {
                  case OffsetValue.high:
                      offset = Experiment.highOffset;
                      break;
                  case OffsetValue.zero:
                      offset = 0f;
                      break;
                  default:
                      Debug.LogError("Unknown offset value: " + distortionSetup.offsetValue);
                      break;
              }
          }*/
        }



}

