﻿/**************************************************************************
 * Egocentric distortion 
 * Written by Sidney Bovey
 * Last update: 28/03/18
 * *************************************************************************/
/* This script has been adapted for my experiement
* Modified by Thibault Porssut*/ 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RootMotion.FinalIK;


namespace RHIExperiment
{


    public class IkGoalStaticDistorter : MonoBehaviour//, EgocentricCoordinatesDistorter
    {
        [Header("Body-related")]
        public Character_Generator_Settings AvatarDistorted;
        public Character_Generator_Settings AvatarOriginal;
        public Transform initialLocation;

        [Header("Distortion")]
        Transform originalIkGoal;
        public GameObject handBallOriginal;
        public GameObject handBallDistorted;
        public enum BoneType { LeftHand, RightHand, LeftTennisBall, RightTennisBall };
        public BoneType boneType;

        public bool applyDistortion = true;

        public enum DistortionType { Gaussian, SplineHermite };
        public DistortionType distortionType;
        public GameObject targets;

        public bool animateArm;
        public float distanceTarget;
		public bool isAnimation;



        private Vector3 myRestPosition;
        private Quaternion myRestRotation;
        private float distance;
        private Color initialColor;
        Vector3 originalBallLocalPositionFormer;
        //float ratioTargetBall = 0.15f;//0.15f
        float counter = 0f;
        Vector3 initialFakeCylinderPosition;
        Quaternion initialFakeCylinderRotation;
        bool isInitialSet;
        bool isInitialSet2;
        //bool isParentReinitilised=true;
        bool isParentNull;
        bool isReady;
        public bool isEmbodied;
        Transform initialParent;
        public bool test;
		public bool isFixedRotation;

        void Awake()
        {
            if (initialLocation != null)
            {
                transform.position = initialLocation.position;
                transform.rotation = initialLocation.rotation;

            }

            myRestPosition = transform.position;
            myRestRotation = transform.rotation;

            initialColor = transform.GetComponent<Renderer>().material.color;


            //handBallDistorted.transform.parent = null;


        }




        void Update()
        {

          //  Debug.Log("distance2 " + Vector3.Distance(handBallOriginal.transform.position, targets.transform.GetChild(3).position));



            //Target
            if (originalIkGoal == null)//&& isStart)
            {
                if (boneType == BoneType.LeftHand && GameObject.Find(AvatarOriginal.settingsReferences.leftHand.name + "Target") != null)
                    originalIkGoal = GameObject.Find(AvatarOriginal.settingsReferences.leftHand.name + "Target").transform;



                if (boneType == BoneType.RightHand && GameObject.Find(AvatarOriginal.settingsReferences.rightHand.name + "Target") != null)
                    originalIkGoal = GameObject.Find(AvatarOriginal.settingsReferences.rightHand.name + "Target").transform;


            }

            if (applyDistortion  )
            {

				RangeDistortion();




			}
            else if(!applyDistortion)
            {
                if(test)
                {
					handBallDistorted.transform.position = handBallOriginal.transform.position;
					handBallDistorted.transform.rotation = handBallOriginal.transform.rotation;
				}
                else
                {
					transform.position = originalIkGoal.position;
					transform.rotation = originalIkGoal.rotation;
					
                }
                

  
            
            }

             
        }
 



        void LateUpdate()
        {
            if (originalIkGoal != null)
            {
                if (applyDistortion)
                {
                  //  RangeDistortion();
                    //   Debug.Log("Distortion");
                    
                   
                }
                else
                {
                    //  Debug.Log("noDistortion");

                    if (!isInitialSet)
                    {
                      
                          handBallDistorted.transform.rotation = handBallOriginal.transform.rotation;
                     
                          isInitialSet =true;
                    }

                  //  handBallDistorted.transform.position = handBallOriginal.transform.position;
                  //  handBallDistorted.transform.rotation = handBallOriginal.transform.rotation;



                }
            }

        }

        void RangeDistortion()
        {
            
            if (animateArm)
            {
               
                if (Vector3.Distance(handBallOriginal.transform.position, targets.transform.GetChild(3).position) >= distanceTarget && isAnimation)
                {

                    handBallDistorted.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 180));
                    counter += 0.002f;
                    //Debug.Log("COUNTER " + counter);
                    handBallDistorted.transform.position = new Vector3(targets.transform.GetChild(3).position.x, targets.transform.GetChild(3).position.y
                           , targets.transform.GetChild(3).position.z + Mathf.PingPong(counter, 0.1f));
                }
                else
                {

					// handBallDistorted.transform.position = handBallOriginal.transform.position;
					// handBallDistorted.transform.rotation = handBallOriginal.transform.rotation;
					if (isAnimation)
					{
						handBallDistorted.transform.position = targets.transform.GetChild(3).position;
						handBallDistorted.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 180));
					}
					else if(!isInitialSet2)
					{
						  handBallDistorted.transform.position = handBallOriginal.transform.position;
						  handBallDistorted.transform.rotation = handBallOriginal.transform.rotation;
						isInitialSet2=true;
					}
					isInitialSet = true;
                        counter = 0f;

                }
            }
            else
            {
                isInitialSet = false;
				isInitialSet2 = false;
				if (isEmbodied)
                {
                 if(test)
					{
						//handBallDistorted.transform.position = handBallOriginal.transform.position;
					}
				// else
						//transform.position = originalIkGoal.position;
				
				}
                else
				{
					if(isFixedRotation)
					//handBallDistorted.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 180));
					if(isAnimation)
					handBallDistorted.transform.position = targets.transform.GetChild(3).position;
					//Debug.Log("target 3 position"+ targets.transform.GetChild(3).position);
				}
                   
              
                
            }



          


        }

        GameObject GetTarget(string targetNumber)
        {

            GameObject targetSelected=null;

            foreach (Transform child in targets.transform)
            {


                if (child.GetChild(0).GetComponent<TextMesh>().text != targetNumber)
                {
                    targetSelected= child.gameObject;

                }
                else
                    targetSelected= null;


            }

            return targetSelected;
        }
    }
}

