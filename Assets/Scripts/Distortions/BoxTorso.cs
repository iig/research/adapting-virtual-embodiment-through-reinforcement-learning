﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EgocentricCoordinatesFormalism
{
    [ExecuteInEditMode]
    public class BoxTorso : BodyPart
    {
        public Vector3 localPositionOffset, localRotation;
        public Vector2 scale;
        public bool drawBox = true;
        public bool drawCoordinates = true;

        public Color boxColor = Color.magenta;

        public Vector3 centerWorld { get { return transform.TransformPoint(localPositionOffset); } }
        public Vector3 dx { get { return transform.rotation * Quaternion.Euler(localRotation) * new Vector3(1f, 0f, 0f) * scale.x * 0.5f; } }
        public Vector3 dy { get { return transform.rotation * Quaternion.Euler(localRotation) * new Vector3(0f, 1f, 0f) * scale.y * 0.5f; } }
        public Vector3 n {  get { return Vector3.Cross(dx, dy).normalized; } }

        void Update()
        {
            if(drawBox)
                DrawLines();
        }

        private void DrawLines()
        {
            Debug.DrawLine(centerWorld + dx + dy, centerWorld - dx + dy, boxColor, 0f, false);
            Debug.DrawLine(centerWorld + dx + dy, centerWorld + dx - dy, boxColor, 0f, false);
            Debug.DrawLine(centerWorld - dx - dy, centerWorld - dx + dy, boxColor, 0f, false);
            Debug.DrawLine(centerWorld - dx - dy, centerWorld + dx - dy, boxColor, 0f, false);
            //Debug.DrawLine(centerWorld, centerWorld + n, Color.magenta, 0f, false);
        }

        private Vector3 ProjectOnPlane(Vector3 worldPoint)
        {
            return worldPoint - n * Vector3.Dot(worldPoint - centerWorld, n);
        }

        public override ProjectionResult GetReferencePoint(Vector3 position)
        {
            Vector3 pointOnPlane = ProjectOnPlane(position);
            Vector3 pointLocal = pointOnPlane - centerWorld;

            // compute coordinates in local space (dx,dy)
            float a = Vector3.Dot(pointLocal, dx) / dx.sqrMagnitude;
            float b = Vector3.Dot(pointLocal, dy) / dy.sqrMagnitude;
            // fit to rectangle
            a = Mathf.Clamp(a, -1, 1);
            b = Mathf.Clamp(b, -1, 1);
            
            if (drawCoordinates)
            {
                Debug.DrawLine(centerWorld, centerWorld + dx, Color.red, 0f, false);
                Debug.DrawLine(centerWorld, centerWorld + dy, Color.red, 0f, false);
                Debug.DrawLine(centerWorld, centerWorld + pointLocal, Color.yellow, 0f, false);
                Debug.DrawLine(centerWorld, centerWorld + a * dx, Color.green, 0f, false);
                Debug.DrawLine(centerWorld, centerWorld + b * dy, Color.green, 0f, false);
            }
            
            return new SquareProjection(new Vector2(a, b));
        }

        public override Vector3 ProjectionResultToWorld(ProjectionResult projectionResult)
        {
            SquareProjection asSquareProjection = (SquareProjection)projectionResult; // will throw an excpetion if cast fails, unlike 'as'

            Vector3 returned = asSquareProjection.projectionCoordinates.x * dx
                                + asSquareProjection.projectionCoordinates.y * dy
                                + centerWorld;
            return returned;
        }

        public override Vector3 MirrorIfNeeded(Vector3 position)
        {
            // Not quite needed now, TODO later
            return position;
        }
    }
}
