﻿/**************************************************************************
 * Egocentric distortion 
 * Written by Sidney Bovey
 * Last update: 28/03/18
 * *************************************************************************/
/* This script has been adapted for my experiement
* Modified by Thibault Porssut*/ 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RootMotion.FinalIK;
using EgocentricCoordinatesFormalism;
//using IIG.Experiment;

public class IkGoalDistorter : MonoBehaviour//, EgocentricCoordinatesDistorter
{
    [Header("Body-related")]
    public Character_Generator_Settings AvatarDistorted;
    public Character_Generator_Settings AvatarOriginal;
    public BodyPart[] sourceIgnoredBodyParts, targetIgnoredBodyParts;
    public Vector3 localPositionOffset;
	public Transform offsetLocation; 
	public Transform initialLocation;
    //public TargetController targetController;

    [Header("Distortion")]
    Transform originalIkGoal;
    public enum BoneType { LeftHand, RightHand };
    public BoneType boneType;
	public bool applyDistortion = true;
	public bool isCorrectedExternaly;
    public bool drawLines = true;
    [Range(0, 200)]
    public float offset = 20f;
    [Range(-10, 10)]
    public float gain = 1f;
    public IIG.Distortions.DistortionType distortionType = IIG.Distortions.DistortionType.None;

	[HideInInspector]
	public Vector3 positionOffset;

	//public Vector3 originalPosition { get { return originalIkGoal.position; } }


	private Vector3 myRestPosition;
    private Quaternion myRestRotation;
    /// <summary>
    /// Don't mess up with these.
    /// </summary>
    public EgocentricCoordinates egocentricCoordinates;
	
    void Start()
    {
        if( initialLocation!=null )
        {
            transform.position = initialLocation.position;
            transform.rotation = initialLocation.rotation;

        }
        
        myRestPosition = transform.position;
        myRestRotation = transform.rotation;
        InitializeEgocentricCorrdinates(AvatarDistorted, AvatarOriginal, sourceIgnoredBodyParts, targetIgnoredBodyParts, out egocentricCoordinates);
    }

    public static void InitializeEgocentricCorrdinates(Character_Generator_Settings sourceBody, Character_Generator_Settings targetBody, BodyPart[] sourceIgnoredBodyParts, BodyPart[] targetIgnoredBodyParts, out EgocentricCoordinates ec)
    {
        var sourceBodyParts = sourceBody.settingsReferences.root.GetComponentsInChildren<BodyPart>();
        sourceBodyParts = RemoveIgnoredBodyParts(sourceBodyParts, sourceIgnoredBodyParts);

        var targetBodyParts = targetBody.settingsReferences.root.GetComponentsInChildren<BodyPart>();
        targetBodyParts = RemoveIgnoredBodyParts(targetBodyParts, targetIgnoredBodyParts);

        ec = new EgocentricCoordinates(sourceBodyParts, targetBodyParts);
    }

    private static BodyPart[] RemoveIgnoredBodyParts(BodyPart[] original, BodyPart[] ignored)
    {
        List<BodyPart> bodyParts = new List<BodyPart>(original);

        foreach (BodyPart toIgnore in ignored)
            if (bodyParts.Contains(toIgnore))
                bodyParts.Remove(toIgnore);

        /*
        // DE-BUGFIX
        bodyParts.Add(original[original.Length - 2]);
        bodyParts.Add(original[original.Length - 1]);
        */

        return bodyParts.ToArray();
    }

    void Update()
    {

        //Target
        if (originalIkGoal == null)
        {
            if (boneType == BoneType.LeftHand && GameObject.Find(AvatarOriginal.settingsReferences.leftHand.name + "Target") != null)
                originalIkGoal = GameObject.Find(AvatarOriginal.settingsReferences.leftHand.name + "Target").transform;

            if (boneType == BoneType.RightHand && GameObject.Find(AvatarOriginal.settingsReferences.rightHand.name + "Target") != null)
                originalIkGoal = GameObject.Find(AvatarOriginal.settingsReferences.rightHand.name + "Target").transform;
        }

    }



    void LateUpdate()
    {
        if( originalIkGoal != null)
        {
            if (applyDistortion)
            {

				//if (targetController.isMovingBetweenTargets)
				//{
		
				//localToWorldMatrix just cancels the scale if this one is different from 1

				//This position Offset can be used externally
				if( offsetLocation !=null)
					positionOffset =  offsetLocation.position-transform.position;
				else
					positionOffset = transform.localToWorldMatrix * localPositionOffset;


				// Compute egocentric corrdinate of one joint for all the body parts
				egocentricCoordinates.UpdateCoordinates(originalIkGoal.position + positionOffset);

				// Compute the distortion by modifying the distance between the body part on the joint
                egocentricCoordinates.Update_To_Vectors(IIG.Distortions.GetDistortionFunction(distortionType), offset, gain);

                if (drawLines)
                {
                    egocentricCoordinates.DrawLines(true);
                    //Debug.DrawLine(originalPosition, originalPosition + positionOffset, Color.cyan, 0f, false);
                }

				//Compute the position of the joint for the body target from the egocentric coordinates after distortion
				// The position offset is used only to update the egocentric coordinate for external used that is why it is cancelled here
				//If you want a distortion with offset you can use the distortion called offset
				var position = egocentricCoordinates.RetrievePosition()- positionOffset;
                if (!float.IsNaN(position.x))
                    transform.position = position;
				
				if(!isCorrectedExternaly)
					transform.rotation = originalIkGoal.rotation ;
		
				//else
				/*{
                    transform.position = myRestPosition;
					 transform.rotation = CalibrationTable.offsetRotationFowardAvatarHand * CalibrationTable.offsetRotationLateralAvatarHand * transform.rotation;
                    transform.rotation = myRestRotation;
                }*/
			} else
            {
                transform.position = originalIkGoal.position;
				if ( !isCorrectedExternaly )
					transform.rotation = originalIkGoal.rotation;
			}
        }
      
    }

    public void DistortCoordinates(DistortableEgocentricCoordinates dec)
    {
        dec.Update_To_Vectors(IIG.Distortions.GetDistortionFunction(distortionType), offset, gain);
    }

    // A REMPLACER PAR MA PROPRE FONCTION
  /*  public void SetDistortionStrength(TrialSetup distortionSetup)
    {
        distortionType = IIG.Distortions.DistortionType.LinearAndOffset;

        switch (distortionSetup.amplitudeValue)
        {
            case AmplitudeValue.sixtyFive:
                gain = Experiment.gainSixtyFive;
                break;
            case AmplitudeValue.thirtyFive:
                gain = Experiment.gainThirtyFive;
                break;
            case AmplitudeValue.zero:
                gain = 0f;
                break;
            default:
                Debug.LogError("Unknown amplitude value: " + distortionSetup.amplitudeValue);
                break;
        }

        switch (distortionSetup.offsetValue)
        {
            case OffsetValue.high:
                offset = Experiment.highOffset;
                break;
            case OffsetValue.zero:
                offset = 0f;
                break;
            default:
                Debug.LogError("Unknown offset value: " + distortionSetup.offsetValue);
                break;
        }
    }*/
}
