﻿using System;
using UnityEngine;

namespace EgocentricCoordinatesFormalism
{
    public class PositionCapsule : BodyPart
    {

        [Header("Prefabs")]
        public GameObject spherePrefab;
        public GameObject cylinderPrefab;
        [Header("Capsule properties")]
        public float radius = 1f;
        public Transform p1, p2;
        [Header("Debug")]
        public bool drawCapsule;

        private Transform sphere1, sphere2, cylinder;
        private Vector3 axis { get { return p2.position - p1.position; } }

        void Start()
        {
            SetupObject(ref sphere1, spherePrefab, "sphere1");
            SetupObject(ref sphere2, spherePrefab, "sphere2");
            SetupObject(ref cylinder, cylinderPrefab, "cylinder");
        }

        private void SetupObject(ref Transform targetTransform, GameObject prefab, string name)
        {
            var gameObject = Instantiate(prefab, transform);
            gameObject.name = name;
            targetTransform = gameObject.transform;
            gameObject.GetComponent<MeshRenderer>().enabled = drawCapsule;
        }

        void Update()
        {
            UpdateCapsuleElements();
            /*
            if (name.Equals("l_shoulder"))
                Debug.DrawRay(transform.position, transform.forward, Color.green);
            //*/
        }

        private void UpdateCapsuleElements()
        {
            // size
            Vector3 scale = radius * 2f * Vector3.one;
            sphere1.localScale = scale;
            sphere2.localScale = scale;
            scale.y = axis.magnitude;
            cylinder.localScale = scale;
            // position
            sphere1.position = p1.position;
            sphere2.position = p2.position;
            cylinder.position = (p1.position + p2.position) / 2f;
            //orientation
            sphere1.rotation = Quaternion.FromToRotation(sphere1.worldToLocalMatrix * -sphere1.up, axis);
            sphere2.rotation = Quaternion.FromToRotation(sphere2.worldToLocalMatrix * sphere2.up, axis);
            cylinder.rotation = Quaternion.FromToRotation(cylinder.worldToLocalMatrix * cylinder.up, axis);
        }

        public override ProjectionResult GetReferencePoint(Vector3 position)
        {
            Vector3 projectedPoint = ProjectOnCapsule(position);
            CapsuleProjection cp = new CapsuleProjection();
            // TODO: change this so that capsules actually affect ECs
            cp.worldPoint = projectedPoint;
            return cp;
        }


        public override Vector3 ProjectionResultToWorld(ProjectionResult projectionResult)
        {
            CapsuleProjection cp = (CapsuleProjection)projectionResult;
            return cp.worldPoint;
        }
        
        public override Vector3 MirrorIfNeeded(Vector3 position)
        {
            if (IsInside(position))
            {
                var projection = ProjectOnCapsule(position);
                float factor = 0.5f;
                position = projection + factor * (projection - position);
            }
            return position;
        }

        #region Geometry Utilities

        public Vector3 ProjectOnCapsule(Vector3 point)
        {
            if (p1.position.Equals(p2.position))
            {
                return ProjectOnSPhere(point, p1.position);
            }
            else
            {
                Vector3 center = GetSphereCenter(point);
                return ProjectOnSPhere(point, center);
            }
        }

        private Vector3 GetSphereCenter(Vector3 point)
        {
            Vector3 pointOnAxis = ProjectOnAxis(point);
            if (Vector3.Dot(p2.position - p1.position, pointOnAxis - p1.position) <= 0)
                return p1.position;
            else if (Vector3.Dot(p2.position - p1.position, pointOnAxis - p2.position) >= 0)
                return p2.position;
            else
                return pointOnAxis;
        }

        private Vector3 ProjectOnAxis(Vector3 point)
        {
            return Vector3.Dot(point - p1.position, axis) / Vector3.Dot(axis, axis) * axis + p1.position;
        }

        private Vector3 ProjectOnSPhere(Vector3 point, Vector3 center)
        {
            return (point - center).normalized * radius + center;
        }

        public bool IsInside(Vector3 point)
        {
            Vector3 centerPoint;
            if (p1.position.Equals(p2.position))
                centerPoint = p1.position;
            else
                centerPoint = GetSphereCenter(point);

            return (centerPoint - point).magnitude < radius;
        }

        #endregion
    }
}