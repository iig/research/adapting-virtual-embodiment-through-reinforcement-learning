﻿using UnityEngine;
using IIG;
using System.Collections;

namespace EgocentricCoordinatesFormalism
{
  /*  public interface EgocentricCoordinatesDistorter
    {
        void SetDistortionStrength(IIG.Experiment.TrialSetup distortionSetup);
        void DistortCoordinates(DistortableEgocentricCoordinates ec);
    }*/
    public interface DistortableEgocentricCoordinates
    {
        void Update_To_Vectors(Distortions.DistortionFunction Distortion, float actionRange, float strengh);
    }

    /// <summary>
    /// As defined by Molla et al.
    /// </summary>
    public struct EgocentricCoordinate
    {
        public float lambda;
        public Vector3 x;
        public Vector3 v;
        public float C;
        public float beta;
        public LineRenderer lineRenderer;
    }
    /// <summary>
    /// Contains a set of ECs for one end position (i.e. one joint)
    /// </summary>
    public class EgocentricCoordinates: DistortableEgocentricCoordinates
    {
        public EgocentricCoordinate[] coordinates;
        public BodyPart[] sourceBodyParts, targetBodyParts;

        public EgocentricCoordinates(BodyPart[] sourceBodyParts, BodyPart[] targetBodyParts)
        {
            if (sourceBodyParts.Length != targetBodyParts.Length)
                throw new System.Exception("Cannot create Egocentric Coordinates out of two body part arrays of different size: " + sourceBodyParts.Length + " vs " + targetBodyParts.Length);

            this.sourceBodyParts = sourceBodyParts;
            this.targetBodyParts = targetBodyParts;
            coordinates = new EgocentricCoordinate[sourceBodyParts.Length];
            //UpdateCoordinates(Vector3.zero);
            // Line renderers for each component
            for (int i = 0; i < sourceBodyParts.Length; i++)
            {
                var go = GameObject.CreatePrimitive(PrimitiveType.Quad);
                go.name = "LineRenderer";
                go.transform.localScale = Vector3.zero; // dirty hack
                var lr = go.AddComponent<LineRenderer>();
                Material whiteDiffuseMat = new Material(Shader.Find("Unlit/Color"));
                lr.material = whiteDiffuseMat;

                lr.startWidth = 0.005f;
                lr.endWidth = 0.005f;

                coordinates[i].lineRenderer = lr;
            }
        }
        private void OnDestroy()
        {
            foreach(EgocentricCoordinate ec in coordinates)
            {
                Object.Destroy(ec.lineRenderer.material);
            }
        }
        /// <summary>
        /// Updates this set of coordinates according to a new described position.
        /// 
        /// Note: we completely skip the kinematic path normalization step as we are not dealing with
        ///       different morphologies.
        /// </summary>
        /// <param name="newPoint">The new position used to update the ECs</param>
        public void UpdateCoordinates(Vector3 refPoint)
        {


            /*
            Debug.Log("============ NEW FRAME ============");
            foreach (BodyPart bp in sourceBodyParts)
            {
                Debug.Log(bp.name);
            }
            Debug.Log("[EC] " + sourceBodyParts[6].transform.rotation.eulerAngles + sourceBodyParts[1].name);
            Debug.DrawRay(sourceBodyParts[6].transform.position, sourceBodyParts[6].transform.forward, Color.yellow);
            //*/

            var lambdaSum = 0f;
            for (int i = 0; i < coordinates.Length; i++)
            {
                var newPoint = sourceBodyParts[i].MirrorIfNeeded(refPoint);
                ProjectionResult projectionResult = sourceBodyParts[i].GetReferencePoint(newPoint);
               
				// World coordinate of the projection point in target space
                Vector3 x = targetBodyParts[i].ProjectionResultToWorld(projectionResult);

				// vector from the source body so the target is at the place as the source according his own reference
                Vector3 v = newPoint - sourceBodyParts[i].ProjectionResultToWorld(projectionResult);
               
				//float lambda_p = v.magnitude < Mathf.Epsilon ? 1f / Mathf.Epsilon : 1f / v.magnitude;
                //float lambda_p = v.magnitude < Mathf.Epsilon ? 1f / Mathf.Epsilon : 1f / (v.magnitude * v.magnitude);
                float lambda_p = v.magnitude < Mathf.Epsilon ? 1f / Mathf.Epsilon : 1f / (v.magnitude * v.magnitude);
                var lambda_t = 1f; // ignored for simplicity of result reading

                //Debug.DrawLine(sourceBodyParts[i].ProjectionResultToWorld(projectionResult), sourceBodyParts[i].ProjectionResultToWorld(projectionResult) + v, Color.red, 0f, false);

                coordinates[i].lambda = lambda_p * lambda_t; // not yet normalized
                coordinates[i].x = x;
                coordinates[i].v = v;// - v.normalized * BodyCalibration.HAND_DEPTH;
               
				coordinates[i].C = 0f;

				//it is only to keep the orientation of the extremities
				coordinates[i].beta = 0f;
                lambdaSum += coordinates[i].lambda;
            }

            for (int i = 0; i < coordinates.Length; i++)
            {
                coordinates[i].lambda /= lambdaSum;
            }
        }

        public Vector3 RetrievePosition()
        {
            var position = Vector3.zero;
            foreach (EgocentricCoordinate coordinate in coordinates)
            {
                position += coordinate.lambda * (coordinate.x + coordinate.v);// + coordinate.v.normalized * BodyCalibration.HAND_DEPTH);
            }
            return position;
        }

        public void Update_To_Vectors(Distortions.DistortionFunction Distortion, float actionRange, float strength)
        {
            for (int i = 0; i < coordinates.Length; i++)
            {
                float newLength = Distortion(coordinates[i].v.magnitude, actionRange, strength);
                coordinates[i].v = coordinates[i].v.normalized * (newLength);
            }

            /*
            // check for interpenetrations
            Vector3 point = RetrievePosition();
            for (int i = 0; i < bodyParts.Length; i++) {
                if (bodyParts[i].IsInside(point)) {
                    UpdateCoordinates(coordinates[i].x);
                }
            }
            //*/
        }

		public void CopyVvectors(Vector3[] ArrayDestination) {

			for ( int i = 0; i < coordinates.Length; i++ ) {

				ArrayDestination[i]=coordinates[i].v;
			}
		}

		public void ReplaceVvectors ( Vector3[] ArrayBackup ) {

			for ( int i = 0; i < coordinates.Length; i++ ) {

				coordinates[i].v=ArrayBackup[i];
			}
		}

		public void DrawLines(bool lambdaToAlpha)
        {
            for (int i = 0; i < sourceBodyParts.Length; i++)
            {
                var pointA = coordinates[i].x;
                var pointB = coordinates[i].x + coordinates[i].v;

                Color c = Color.white;
                if (lambdaToAlpha)
                {
                    const float oneThird = (float)(1.0 / 3.0);
                    float newColor = 1f - Mathf.Pow(coordinates[i].lambda, oneThird);
                    c.b = newColor;
                    c.g = newColor;
                }

                coordinates[i].lineRenderer.material.color = c;
                coordinates[i].lineRenderer.positionCount = 2;
                coordinates[i].lineRenderer.SetPosition(0, pointA);
                coordinates[i].lineRenderer.SetPosition(1, pointB);
            }
        }

        public void DestroyLines()
        {
            for (int i = 0; i < sourceBodyParts.Length; i++)
            {
                //coordinates[i].lineRenderer.numPositions = 0;
                Object.Destroy(coordinates[i].lineRenderer.gameObject);
            }
        }
    }
}
