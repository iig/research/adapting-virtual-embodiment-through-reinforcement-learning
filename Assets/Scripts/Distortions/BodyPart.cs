using UnityEngine;

namespace EgocentricCoordinatesFormalism
{
    public abstract class BodyPart : MonoBehaviour
    {
        abstract public ProjectionResult GetReferencePoint(Vector3 position);
        abstract public Vector3 ProjectionResultToWorld(ProjectionResult projectionResult);
        abstract public Vector3 MirrorIfNeeded(Vector3 position);
    }

    public abstract class ProjectionResult
    {
    }


    public class SquareProjection : ProjectionResult
    {
        public Vector2 projectionCoordinates;

        public SquareProjection(Vector2 projectionCoordinates)
        {
            this.projectionCoordinates = projectionCoordinates;
        }
    }

    /// <summary>
    /// Currently just a container for the projected point in world cvoordinates.
    /// </summary>
    public class CapsuleProjection : ProjectionResult
    {
        /*
        public enum CapsulePart
        {
            Sphere1,
            Cylinder,
            Sphere2
        }
        public CapsulePart projectionPart;
        public Vector2 projectionCoordinates; // cylindrical or polar coordinates depending on projectionPart
        //*/
        public Vector3 worldPoint;
    }
}