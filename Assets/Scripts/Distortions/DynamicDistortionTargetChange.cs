﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DynamicDistortionTargetChange : MonoBehaviour
{
	public ExperimentSetting experimentSetting;
	public GameObject rightTarget;
	public GameObject leftTarget;
	

	bool settingDone;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
		if ( !settingDone && experimentSetting.start ) {

			rightTarget.GetComponent<DynamicExperiment.IkGoalDynamicDistorter>().handBallDistorted.transform.parent = null;
			rightTarget.GetComponent <ObjectLinked>().enabled = true;


			leftTarget.GetComponent<DynamicExperiment.IkGoalDynamicDistorter>().handBallDistorted.transform.parent = null;
			leftTarget.GetComponent<ObjectLinked>().enabled = true;

			StartCoroutine( SetBall() );

			settingDone = true;
		}
	}

	IEnumerator SetBall () {
		
		yield return new WaitForSeconds( 0.5f );
		rightTarget.GetComponent<DynamicExperiment.IkGoalDynamicDistorter>().isBallTarget = true;
		rightTarget.GetComponent<ObjectLinked>().resetInitiate = true;

		leftTarget.GetComponent<DynamicExperiment.IkGoalDynamicDistorter>().isBallTarget = true;
		leftTarget.GetComponent<ObjectLinked>().resetInitiate = true;
	}
}
