26
; MarkerID MarkerName 
; You have to leave a space after the marker name
; if you want to put a comment 
; LHAR ; -> correct
; LHAR; -> not correct

0 LHAN
1 LHAN1
2 LHAN2
3 LELB
4 LELB1
5 LELB2

6 RSHLD
7 RSHLD1
8 RSHLD2

9 LSHLD
10 LSHLD1
11 LSHLD2


12 CHE
13 CHE1
14 CHE2

15 HEAD
16 HEAD1
17 HEAD2

18 RHAN
19 RHAN1
20 RHAN2
21 RELB
22 RELB1
23 RELB2

24 CHAIR
25 CHAIR1




